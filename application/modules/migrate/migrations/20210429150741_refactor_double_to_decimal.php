<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Refactor_double_to_decimal extends CI_Migration
    {

        public function up()
        {
            $this->db->query("ALTER TABLE `material_requests` CHANGE COLUMN `request_amount` `request_amount` DECIMAL(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_request_items` CHANGE COLUMN `unit_cost` `unit_cost` DECIMAL(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_request_items` CHANGE COLUMN `total_cost` `total_cost` DECIMAL(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `purchase_order_requests` CHANGE COLUMN `total_cost` `total_cost` DECIMAL(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `purchase_order_request_items` CHANGE COLUMN `unit_cost` `unit_cost` DECIMAL(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `purchase_order_request_items` CHANGE COLUMN `total_cost` `total_cost` DECIMAL(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `purchase_orders` CHANGE COLUMN `total` `total` DECIMAL(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `purchase_order_items` CHANGE COLUMN `unit_cost` `unit_cost` DECIMAL(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `purchase_order_items` CHANGE COLUMN `total_cost` `total_cost` DECIMAL(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_receivings` CHANGE COLUMN `total_cost` `total_cost` DECIMAL(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_receivings` CHANGE COLUMN `landed_cost` `landed_cost` DECIMAL(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_receivings` CHANGE COLUMN `freight_amount` `freight_amount` DECIMAL(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_receivings` CHANGE COLUMN `input_tax` `input_tax` DECIMAL(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_receiving_items` CHANGE COLUMN `unit_cost` `unit_cost` DECIMAL(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_receiving_items` CHANGE COLUMN `total_cost` `total_cost` DECIMAL(20,2) NOT NULL DEFAULT '0'");
        }

        public function down()
        {
            $this->db->query("ALTER TABLE `material_requests` CHANGE COLUMN `request_amount` `request_amount` DOUBLE(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_request_items` CHANGE COLUMN `unit_cost` `unit_cost` DOUBLE(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_request_items` CHANGE COLUMN `total_cost` `total_cost` DOUBLE(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `purchase_order_requests` CHANGE COLUMN `total_cost` `total_cost` DOUBLE(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `purchase_order_request_items` CHANGE COLUMN `unit_cost` `unit_cost` DOUBLE(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `purchase_order_request_items` CHANGE COLUMN `total_cost` `total_cost` DOUBLE(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `purchase_orders` CHANGE COLUMN `total` `total` DOUBLE(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `purchase_order_items` CHANGE COLUMN `unit_cost` `unit_cost` DOUBLE(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `purchase_order_items` CHANGE COLUMN `total_cost` `total_cost` DOUBLE(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_receivings` CHANGE COLUMN `total_cost` `total_cost` DOUBLE(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_receivings` CHANGE COLUMN `landed_cost` `landed_cost` DOUBLE(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_receivings` CHANGE COLUMN `freight_amount` `freight_amount` DOUBLE(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_receivings` CHANGE COLUMN `input_tax` `input_tax` DOUBLE(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_receiving_items` CHANGE COLUMN `unit_cost` `unit_cost` DOUBLE(20,2) NOT NULL DEFAULT '0'");
            $this->db->query("ALTER TABLE `material_receiving_items` CHANGE COLUMN `total_cost` `total_cost` DOUBLE(20,2) NOT NULL DEFAULT '0'");
        }
    }
