<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_journal_voucher_table extends CI_Migration
{

    private $tbl = "journal_vouchers";

    private $fields = array(
        'id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'auto_increment' => TRUE,
            'NOT NULL' => TRUE,
        ),
        'reference' => array(
            'type' => 'VARCHAR',
            'constraint' => '50',
            'NULL' => TRUE,
        ),
        'payable_type_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'due_date' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'is_complete' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'is_paid' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'total_amount' => array(
            'type' => 'decimal',
            'constraint' => '15,2',
            'NULL' => false,
            'DEFAULT' => "0.00",
        ),
        'requesting_staff_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'requesting_department_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'prepared_by' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'approving_staff_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'approving_department_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'property_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'project_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'origin_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'date_requested' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'accounting_entry_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'particulars' => array(
            'type' => 'VARCHAR',
            'constraint' => '250',
            'NULL' => TRUE
        ),
        'created_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'created_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'updated_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'updated_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'deleted_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'deleted_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
    );

    public function up()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->tbl, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
