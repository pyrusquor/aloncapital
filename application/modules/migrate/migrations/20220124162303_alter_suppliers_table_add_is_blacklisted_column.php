<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_suppliers_table_add_is_blacklisted_column extends CI_Migration
{

    public function up()
    {

        if (!$this->db->field_exists('is_blacklisted', 'suppliers')) {
            $this->db->query("ALTER TABLE `suppliers` ADD `is_blacklisted` TinyInt(1) DEFAULT 0 AFTER `is_purchasing_related`");
        }
    }

    public function down()
    {

        if ($this->db->field_exists('is_blacklisted', 'suppliers')) {
            $this->db->query("ALTER TABLE `suppliers` DROP COLUMN `is_blacklisted`");
        }
    }
}
