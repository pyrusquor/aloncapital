<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_sub_warehouse extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => true,
                'auto_increment' => true,
                'NOT NULL' => false,
            ),
            'sub_warehouse_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'sub_warehouse_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '5',
                'NULL' => false,
            ),
            'warehouse_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => false,
            ),
            'address' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'telephone_number' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => false,
            ),
            'fax_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '7',
                'NULL' => false,
            ),
            'mobile_number' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => false,
            ),
            'email_address' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'contact_person' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'is_active' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'created_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('sub_warehouse', true);

        // $this->load->model('sub_warehouse/Sub_warehouse_model', 'sub_warehouse');
        // $this->sub_warehouse->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('sub_warehouse')) {

            $this->dbforge->drop_table('sub_warehouse');
        }
    }
}
