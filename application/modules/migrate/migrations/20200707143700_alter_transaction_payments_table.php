<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_payments_table extends CI_Migration
{

    public function up()
    {

        if ($this->db->table_exists('transaction_payments')) {

            if (!$this->db->field_exists('accounting_entry_id', 'transaction_payments')) {

                $this->db->query("ALTER TABLE `transaction_payments` ADD COLUMN `accounting_entry_id` INT(11) DEFAULT NULL  AFTER `transaction_id`");
            }

        }

    }

    public function down()
    {

        if ($this->db->field_exists('accounting_entry_id', 'transaction_payments')) {

            $this->dbforge->drop_column('transaction_payments', 'accounting_entry_id');
        }

    }
}
