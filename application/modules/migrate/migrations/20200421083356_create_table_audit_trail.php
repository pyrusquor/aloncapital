<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_audit_trail extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => true,
                'auto_increment' => true,
                'NOT NULL' => false,
            ),
            'affected_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => false,
            ),
            'affected_table' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'action' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'previous_record' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => true,
            ),
            'new_record' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => true,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('audit_trail', true);

        $this->load->model('audit_trail/Audit_trail_model', 'audit_trail');
        // $this->login_history->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('audit_trail')) {

            $this->dbforge->drop_table('audit_trail');
        }
    }
}
