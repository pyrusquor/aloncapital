<?php defined('BASEPATH') or exit('No direct script access allowed.');

/**
 * 
 */
class Migration_Create_table_checklists extends CI_Migration {

	function up () {

		if ( $this->db->table_exists('checklist') ) {

			$this->dbforge->drop_table('checklist');
		}

		if ( $this->db->table_exists('checklists') ) {

			$this->dbforge->drop_table('checklists');
		}

		$_fields	=	array(
									'id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => TRUE,
                    'comment'	=>	'checklists Table ID'
									),
									'name'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'checklists name'
									),
									'category_id'	=>	array(
										'type'	=>	'INT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'checklists category_id'
									),
									'description'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'checklists description'
									),
									'is_active'	=>	array(
										'type'	=>	'TINYINT',
										'constraint' => '1',
                    'NULL' => TRUE,
                    'default'	=>	1,
                    'comment'	=>	'checklist is_active 1:TRUE|FALSE:0'
									),
	                'created_by'	=>	array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'checklist created_by'
	                ),
	                'created_at' => array(
	                	'type'	=>	'DATETIME',
	                	'NULL'	=>	TRUE,
                    'comment'	=>	'checklist created_at'
	                ),
	                'updated_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'checklist updated_by'
	                ),
	                'updated_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'checklist updated_at'
	                ),
	                'deleted_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'checklist deleted_by'
	                ),
	                'deleted_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'checklist deleted_at'
	                )
								);

		$this->dbforge->add_field($_fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('checklists', TRUE);

		// $this->load->model('checklist/Checklist_model', 'M_checklist');
		// $this->M_checklist->insert_dummy();
	}

	function down () {

		if ( $this->db->table_exists('checklists') ) {

			$this->dbforge->drop_table('checklists');
		}
	}
}