<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_table extends CI_Migration
{

    public function up()
    {
       if ( ! $this->db->field_exists('package_type_id', 'transaction_properties') ) {
	      	$this->db->query("ALTER TABLE `transaction_properties` ADD COLUMN `package_type_id` int(11) AFTER `house_model_id`");
	    }

        if ( ! $this->db->field_exists('period_id', 'transaction_official_payments') ) {
            $this->db->query("ALTER TABLE `transaction_official_payments` ADD COLUMN `period_id` int(11) AFTER `id`");
        }

        if ( ! $this->db->field_exists('is_complete', 'transaction_payments') ) {
            $this->db->query("ALTER TABLE `transaction_payments` ADD COLUMN `is_complete` int(11) AFTER `is_paid`");
        }

        if ( ! $this->db->field_exists('pdc_status', 'transaction_post_dated_checks') ) {
            $this->db->query("ALTER TABLE `transaction_post_dated_checks` ADD COLUMN `pdc_status` int(11) AFTER `particulars`");
        }
    }

    public function down()
    {
    	if ( $this->db->field_exists('package_type_id', 'transaction_properties') ) {
			$this->dbforge->drop_column('transaction_properties', 'package_type_id');
		}

        if ( $this->db->field_exists('period_id', 'transaction_official_payments') ) {
            $this->dbforge->drop_column('transaction_official_payments', 'period_id');
        }

        if ( $this->db->field_exists('is_complete', 'transaction_payments') ) {
            $this->dbforge->drop_column('transaction_payments', 'is_complete');
        }

        if ( $this->db->field_exists('pdc_status', 'transaction_post_dated_checks') ) {
            $this->dbforge->drop_column('transaction_post_dated_checks', 'pdc_status');
        }

    }
}
