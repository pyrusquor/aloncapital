<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_buyer_identifications_add_column extends CI_Migration
{

    function up () {

		if( $this->db->table_exists('buyer_identifications') ) {

	      	if ( !$this->db->field_exists('buyer_id', 'buyer_identifications') ) {

	      		$this->db->query("ALTER TABLE `buyer_identifications` ADD COLUMN `buyer_id` INT(11) DEFAULT NULL  AFTER `id`");
	      	}
	      	
	    }
	}

	function down () {

		if ( $this->db->field_exists('buyer_id', 'buyer_identifications') ) {

			$this->dbforge->drop_column('buyer_identifications', 'buyer_id');
		}

	}
}
