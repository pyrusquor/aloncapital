<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Update_material_request_item_extra_fields extends CI_Migration
{
    private $tbl = "material_request_items";
    private $new_fields = array(
        'item_group_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'NULL' => TRUE,
        ),
        'item_type_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'NULL' => TRUE,
        )
    );

    public function up()
    {
        $this->dbforge->add_column($this->tbl, $this->new_fields);
    }

    public function down()
    {
        foreach($this->new_fields as $field => $data){
            $this->dbforge->drop_column($this->tbl, $field);
        }
    }
}
