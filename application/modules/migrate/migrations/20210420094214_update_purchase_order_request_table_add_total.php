<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Update_purchase_order_request_table_add_total extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('purchase_order_requests')) {
            if(!$this->db->field_exists('total_cost', 'purchase_order_requests')){
                $this->db->query("ALTER TABLE `purchase_order_requests` ADD COLUMN `total_cost` DOUBLE(20, 2) DEFAULT 0 NULL");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('purchase_order_requests')){
            if($this->db->field_exists('total_cost', 'purchase_order_requests')){
                $this->dbforge->drop_column('purchase_order_requests', 'total_cost');
            }
        }
    }
}
