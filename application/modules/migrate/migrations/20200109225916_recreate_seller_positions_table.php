<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Recreate_seller_positions_table extends CI_Migration
{

    public function up()
    {
        if ( $this->db->table_exists('seller_positions') ) {

			$this->dbforge->drop_table('seller_positions');
        }

        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'NULL' => FALSE
            ),
            'position_type_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => FALSE
            ),
            'sales_group_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => FALSE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'NULL' => FALSE
            ),
            'rate' => array(
                'type' => 'DECIMAL',
                'constraint' => '11, 2',
                'NULL' => FALSE
            ),
            'level' => array(
                'type' => 'VARCHAR',
                'constraint' => '10',
                'NULL' => FALSE
            ),
            'is_bypassable' => array(
                'type' => 'TINYINT',
                'constraint' => '1',
                'NULL' => FALSE
            ),
            'created_at' => array(
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ),
            'created_by'=> array(
                'type'=>'INT',
                'unsigned'=>TRUE,
                'NULL'=>TRUE,
            ),
            'updated_at' => array(
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ),
            'updated_by'=> array(
                'type'=>'INT',
                'unsigned'=>TRUE,
                'NULL'=>TRUE,
            ),
            'deleted_at' => array(
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ),
            'deleted_by'=> array(
                'type'=>'INT',
                'unsigned'=>TRUE,
                'NULL'=>TRUE,
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('seller_positions', TRUE);
        
    }

    public function down()
    {
        if ( $this->db->table_exists('seller_positions') ) {

			$this->dbforge->drop_table("seller_positions");
		}
    }
}
