<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Add_construction_order_overhead_cost_table extends CI_Migration
{

    private $table = "construction_order_overhead_cost";
    private $fields = array(

        "id" => array(
            "type" => "INT",
            "constraint" => "11",
            "unsigned" => TRUE,
            "auto_increment" => TRUE,
            "NOT NULL" => TRUE,
        ),

        "construction_order_id" => [
            "type" => "int",
            "constraint" => "11",
            "null" => false,
            'unsigned' => true
        ],

        "amount" => [
            "type" => "DOUBLE",
            "constraint" => "20,2",
            "NOT NULL" => True,
            "default" => "0",
        ],

        "description" => [
            "type" => 'varchar',
            "constraint" => '255',
            "null" => TRUE
        ],

        // Audit Info
        "created_by" => array(
            "type" => "INT",
            "NOT NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "created_at" => array(
            "type" => "DATETIME",
            "NOT NULL" => TRUE,
        ),

        "updated_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "updated_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        "deleted_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "deleted_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

    );

    public function up()
    {
        if (!$this->db->table_exists($this->table)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_key('construction_order_id');
            $this->dbforge->create_table($this->table, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->table)) {
            $this->dbforge->drop_table($this->table);
        }
    }
}
