<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_fees_table extends CI_Migration
{

   public function up()
    {
        if(!$this->db->table_exists('fees'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'name' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'description' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => FALSE
                ),
                'value' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'value_type' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE,
                    'COMMENT' => '0=Percentage,1=Currency'
                ),
                'add_to' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE,
                    'COMMENT' => '1=tsp,2=tcp,3=collectible'
                ),
                'fees_type' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE,
                    'COMMENT' => '1=individual,2=compounded'
                ),
                'is_active' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'NULL' => FALSE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('fees', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('fees') ) {

			$this->dbforge->drop_table('fees');
		}
    }
}
