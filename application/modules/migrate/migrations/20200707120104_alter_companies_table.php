<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_companies_table extends CI_Migration
{

    public function up()
    {

        if ($this->db->table_exists('companies')) {

            if (!$this->db->field_exists('image', 'companies')) {

                $this->db->query("ALTER TABLE `companies` ADD COLUMN `image` varchar(250) DEFAULT NULL  AFTER `id`");
            }

        }

    }

    public function down()
    {

        if ($this->db->field_exists('image', 'companies')) {

            $this->dbforge->drop_column('companies', 'image');
        }

    }
}
