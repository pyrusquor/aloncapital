<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_communication_template_table extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'name' => array(
                'name' => 'name',
                'type' => 'VARCHAR',
                'constraint' => '254',
                'NULL' => true
            )
        );

        if($this->db->table_exists('communication_templates')) {
            $this->dbforge->modify_column('communication_templates', $fields);
        }

    }

    public function down()
    {
        $fields = array(
            'name' => array(
                'name' => 'name',
                'type' => 'VARCHAR',
                'constraint' => '11',
                'NULL' => true
            )
        );

        if($this->db->table_exists('communication_templates')) {
            $this->dbforge->modify_column('communication_templates', $fields);
        }
    }
}
