<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_financing_scheme_values_table_add_order_id_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('financing_scheme_values')) {

            if (!$this->db->field_exists('order_id', 'financing_scheme_values')) {
                $this->db->query("ALTER TABLE `financing_scheme_values` ADD COLUMN `order_id` INT(11) NULL AFTER `is_active`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('financing_scheme_values')) {

            if ($this->db->field_exists('order_id', 'financing_scheme_values')) {
                $this->dbforge->drop_column('financing_scheme_values', 'order_id');
            }
        }
    }
}
