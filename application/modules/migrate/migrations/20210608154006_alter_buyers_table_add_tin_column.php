<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_buyers_table_add_tin_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('buyers')) {

            if (!$this->db->field_exists('tin', 'buyers')) {
                $this->db->query("ALTER TABLE `buyers` ADD COLUMN `tin` VARCHAR(50) AFTER source_referral");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('buyers')) {

            if ($this->db->field_exists('tin', 'buyers')) {
                $this->dbforge->drop_column('buyers', 'tin');
            }
        }
    }
}
