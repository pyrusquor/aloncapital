<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_complaint_tickets_table_add_turn_over_date_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('properties')) {

            if (!$this->db->field_exists('turn_over_date', 'properties')) {
                $this->db->query("ALTER TABLE `properties` ADD COLUMN `turn_over_date` DATETIME NULL AFTER `raw_land_cost`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('properties')) {

            if ($this->db->field_exists('turn_over_date', 'properties')) {
                $this->dbforge->drop_column('properties', 'turn_over_date');
            }
        }
    }
}
