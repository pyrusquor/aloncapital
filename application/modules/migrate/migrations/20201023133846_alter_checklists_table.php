<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_checklists_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('checklists')) {
            if(!$this->db->field_exists('is_reservation_document', 'checklists')) {
                 $this->db->query("ALTER TABLE `checklists` ADD COLUMN `is_reservation_document` INT(1) DEFAULT NULL AFTER `description`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('checklists')) {
            $this->dbforge->drop_column('checklists', 'is_reservation_document');
        }
    }
}
