<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_entries_add_land_inventory_id extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('accounting_entries')) {

            if (!$this->db->field_exists('land_inventory_id', 'accounting_entries')) {
                $this->db->query("ALTER TABLE `accounting_entries` ADD COLUMN `land_inventory_id` INT(11) UNSIGNED DEFAULT 0 AFTER cancelled_at");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('accounting_entries')) {

            if ($this->db->field_exists('land_inventory_id', 'accounting_entries')) {
                $this->dbforge->drop_column('accounting_entries', 'land_inventory_id');
            }
        }
    }
}
