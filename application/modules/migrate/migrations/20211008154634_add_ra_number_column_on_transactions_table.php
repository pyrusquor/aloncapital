<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Add_ra_number_column_on_transactions_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transactions')) {

            if (!$this->db->field_exists('ra_number', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` ADD COLUMN `ra_number` VARCHAR(55) NULL after `documentation_category`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transactions')) {

            if ($this->db->field_exists('ra_number', 'transactions')) {
                $this->dbforge->drop_column('transactions', 'ra_number');
            }
        }
    }
}
