<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_price_logs_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('property_price_logs')) {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'date_of_price_increase' => array(
                    'type' => 'DATETIME',
                    'NULL' => TRUE,
                ),
                'total_selling_price' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'property_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'percentage_increase' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
            
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('property_price_logs', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('property_price_logs') ) {

            $this->dbforge->drop_table('property_price_logs');
        }
    }
}
