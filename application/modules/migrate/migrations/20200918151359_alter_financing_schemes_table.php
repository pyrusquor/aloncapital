<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_financing_schemes_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('financing_schemes')) {
            if (!$this->db->field_exists('scheme_type', 'financing_schemes')) {
                $this->db->query("ALTER TABLE `financing_schemes` ADD COLUMN `scheme_type` int(1) DEFAULT NULL  AFTER `description`");
            }
            if (!$this->db->field_exists('category_id', 'financing_schemes')) {
                $this->db->query("ALTER TABLE `financing_schemes` ADD COLUMN `category_id` int(1) DEFAULT NULL  AFTER `scheme_type`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('financing_schemes')) {
            if ($this->db->field_exists('scheme_type', 'financing_schemes')) {
                $this->dbforge->drop_column('financing_schemes', 'scheme_type');
            }
        }
        if ($this->db->table_exists('financing_schemes')) {
            if ($this->db->field_exists('category_id', 'financing_schemes')) {
                $this->dbforge->drop_column('financing_schemes', 'category_id');
            }
        }
    }
}
