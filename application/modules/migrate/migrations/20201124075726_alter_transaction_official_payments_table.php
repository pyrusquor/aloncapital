<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_official_payments_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transaction_official_payments')) {
            if(!$this->db->field_exists('payment_application', 'transaction_official_payments')) {
                 $this->db->query("ALTER TABLE `transaction_official_payments` ADD COLUMN `payment_application` INT(1) DEFAULT 0 AFTER `transaction_payment_id`");
            }

        }
    }

    public function down()
    {
        if($this->db->table_exists('transaction_official_payments')) {
            $this->dbforge->drop_column('transaction_official_payments', 'payment_application');
        }
    }
}
