<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_sellers_reservation_payments extends CI_Migration
{

    private $tbl = "sellers_reservation_payments";

    private $fields = array(
        'id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'auto_increment' => TRUE,
            'NOT NULL' => TRUE,
        ),
        'sellers_reservation_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'receipt_type' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'or_number' => array(
            'type' => 'varchar',
            'constraint' => '128',
            'NULL' => TRUE
        ),
        'accounting_entry_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'amount' => array(
            'type' => 'decimal',
            'constraint' => '20,2',
            'NULL' => true,
            'DEFAULT' => "0.00",
        ),
        'date_paid' => array(
            'type' => 'DATE',
            'NULL' => TRUE
        ),
        'created_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'created_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'updated_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'updated_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'deleted_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'deleted_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
    );

    public function up()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_key('sellers_reservation_id');
            $this->dbforge->add_key('accounting_entry_id');
            $this->dbforge->create_table($this->tbl, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
