<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_adhoc_fee extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transaction_adhoc_fees')) {

            if (!$this->db->field_exists('turn_over_date', 'transaction_adhoc_fees')) {
                $this->db->query("ALTER TABLE `transaction_adhoc_fees` ADD COLUMN `is_complete` INT(1) DEFAULT 0 NULL AFTER `amount`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transaction_adhoc_fees')) {

            if ($this->db->field_exists('is_complete', 'transaction_adhoc_fees')) {
                $this->dbforge->drop_column('transaction_adhoc_fees', 'is_complete');
            }
        }
    }
}
