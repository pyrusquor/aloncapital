<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_multiple_tables extends CI_Migration
{

    public function up()
    {

       	if(!$this->db->table_exists('communications'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'communication_group_id' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'date_sent' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'communication_title' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'recipient_id' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'recipient_address' => array(
                    'type'=>'text',
                    'NULL'=> TRUE,
                ),
                'recipient_mobile' => array(
                    'type'=>'text',
                    'NULL'=> TRUE,
                ),
                'recipient_table' => array(
                    'type'=>'text',
                    'NULL'=> TRUE,
                ),
                'medium_type' => array(
                    'type'=>'text',
                    'NULL'=> TRUE,
                ),
                'is_sent' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'content' => array(
                    'type'=>'text',
                    'NULL'=> TRUE,
                ),
                'template_id' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'status' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'sent_type' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('communications', TRUE);
        }

        if(!$this->db->table_exists('communication_groups'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'content' => array(
                    'type'=>'TEXT',
                    'NULL'=> TRUE,
                ),
                'communication_template_id' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'communication_setting_id' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'send_schedule_date' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'medium_type' => array(
                    'type'=>'TEXT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'sent_type' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'status' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('communication_groups', TRUE);
        }

        if(!$this->db->table_exists('communication_settings'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'name' => array(
                    'type'=>'TEXT',
                    'NULL'=> TRUE,
                ),
                'days_to_send' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'days_to_send_type' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '50',
                    'NULL'=> TRUE,
                ),
                'period_id' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'setting_slug' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '50',
                    'NULL'=> TRUE,
                ),
                'sms' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'sms_template_id' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'email' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '50',
                    'NULL'=> TRUE,
                ),
                'email_template_id' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('communication_settings', TRUE);
        }

        if(!$this->db->table_exists('communication_templates'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'name' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'description' => array(
                    'type'=>'TEXT',
                    'NULL'=> TRUE,
                ),
                'content' => array(
                    'type'=>'TEXT',
                    'NULL'=> TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('communication_templates', TRUE);
        }

        if(!$this->db->table_exists('payable_types'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'name' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'description' => array(
                    'type'=>'TEXT',
                    'NULL'=> TRUE,
                ),
                'content' => array(
                    'type'=>'TEXT',
                    'NULL'=> TRUE,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('payable_types', TRUE);
        }


        if(!$this->db->table_exists('payment_requests'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'reference' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '11',
                ),
                'payable_type_id' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                ),
                'origin_id' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                    'NULL'=> TRUE,
                ),
                'payee_type' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '11',
                ),
                'payee_type_id' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                ),
                'gross_amount' => array(
                    'type'=>'DOUBLE',
                    'constraint' => '20,2',
                ),
                'net_amount' => array(
                    'type'=>'DOUBLE',
                    'constraint' => '20,2',
                ),
                'wht_amount' => array(
                    'type'=>'DOUBLE',
                    'constraint' => '20,2',
                ),
                'wht_percentage' => array(
                    'type'=>'DOUBLE',
                    'constraint' => '20,2',
                ),
                'payment_voucher_id' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                ),
                'is_paid' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                ),
                'is_complete' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                ),
                'due_date' => array(
                    'type'=>'DATETIME',
                ),
                'remarks' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '250',
                    'NULL'=> TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('payment_requests', TRUE);
        }


       	if(!$this->db->table_exists('payment_vouchers'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'reference' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '50',
                ),
                'payment_type_id' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                ),
                'payee_type' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '11',
                ),
                'payee_type_id' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                ),
                'net_amount' => array(
                    'type'=>'DOUBLE',
                    'constraint' => '20,2',
                ),
                'wht_amount' => array(
                    'type'=>'DOUBLE',
                    'constraint' => '20,2',
                ),
                'check_id' => array(
                    'type'=>'INT',
                    'constraint' => '11',
                ),
                'paid_date' => array(
                    'type'=>'DATETIME',
                ),
                'remarks' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '250',
                    'NULL'=> TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('payment_vouchers', TRUE);
        }


        if ( ! $this->db->field_exists('remarks', 'transaction_ticketing') ) {
            $this->db->query("ALTER TABLE `transaction_ticketing` ADD COLUMN `remarks` varchar(255) NULL AFTER `transaction_id`");
        }

        if ( ! $this->db->field_exists('category', 'transaction_ticketing') ) {
            $this->db->query("ALTER TABLE `transaction_ticketing` ADD COLUMN `category` varchar(50) NULL AFTER `remarks`");
        }

        if ( ! $this->db->field_exists('past_due', 'transaction_ticketing') ) {
            $this->db->query("ALTER TABLE `transaction_ticketing` ADD COLUMN `past_due` int(11) DEFAULT 0 AFTER `category`");
        }

        if ( ! $this->db->field_exists('past_due_status', 'transaction_ticketing') ) {
            $this->db->query("ALTER TABLE `transaction_ticketing` ADD COLUMN `past_due_status` INT(11) DEFAULT 0 AFTER `past_due`");
        }

        if ( ! $this->db->field_exists('received_date', 'transaction_ticketing') ) {
            $this->db->query("ALTER TABLE `transaction_ticketing` ADD COLUMN `received_date` DATETIME NULL AFTER `past_due_status`");
        }

        if ( ! $this->db->field_exists('sent_date', 'transaction_ticketing') ) {
            $this->db->query("ALTER TABLE `transaction_ticketing` ADD COLUMN `sent_date` DATETIME NULL AFTER `received_date`");
        }

        if ( ! $this->db->field_exists('letter_request', 'transaction_ticketing') ) {
            $this->db->query("ALTER TABLE `transaction_ticketing` ADD COLUMN `letter_request` INT(11) NULL AFTER `sent_date`");
        }

        if ( ! $this->db->field_exists('letter_request_name', 'transaction_ticketing') ) {
            $this->db->query("ALTER TABLE `transaction_ticketing` ADD COLUMN `letter_request_name` varchar(50) NULL AFTER `letter_request`");
        }

        if ( ! $this->db->field_exists('letter_request_status', 'transaction_ticketing') ) {
            $this->db->query("ALTER TABLE `transaction_ticketing` ADD COLUMN `letter_request_status` int(11) default 0 AFTER `letter_request_name`");
        }

        if ( ! $this->db->field_exists('date_requested', 'transaction_ticketing') ) {
            $this->db->query("ALTER TABLE `transaction_ticketing` ADD COLUMN `date_requested` datetime NULL AFTER `letter_request_status`");
        }

         if ( ! $this->db->field_exists('date_approved', 'transaction_ticketing') ) {
            $this->db->query("ALTER TABLE `transaction_ticketing` ADD COLUMN `date_approved` datetime NULL AFTER `date_requested`");
        }

        if ( ! $this->db->field_exists('date_disapproved', 'transaction_ticketing') ) {
            $this->db->query("ALTER TABLE `transaction_ticketing` ADD COLUMN `date_disapproved` datetime NULL AFTER `date_approved`");
        }

        if ( ! $this->db->field_exists('date_cancelled', 'transaction_ticketing') ) {
            $this->db->query("ALTER TABLE `transaction_ticketing` ADD COLUMN `date_cancelled` datetime NULL AFTER `date_disapproved`");
        }

    }

    public function down()
    {
    	if ( $this->db->table_exists('communications') ) {

			$this->dbforge->drop_table('communications');
		}

		if ( $this->db->table_exists('communication_groups') ) {

			$this->dbforge->drop_table('communication_groups');
		}

		if ( $this->db->table_exists('communication_settings') ) {

			$this->dbforge->drop_table('communication_settings');
		}

		if ( $this->db->table_exists('communication_templates') ) {

			$this->dbforge->drop_table('communication_templates');
		}

		if ( $this->db->table_exists('payable_types') ) {

			$this->dbforge->drop_table('payable_types');
		}

		if ( $this->db->table_exists('payment_requests') ) {

			$this->dbforge->drop_table('payment_requests');
		}

		if ( $this->db->table_exists('payment_vouchers') ) {

			$this->dbforge->drop_table('payment_vouchers');
		}


        if ( $this->db->field_exists('remarks', 'transaction_ticketing') ) {
            $this->dbforge->drop_column('transaction_ticketing', 'remarks');
        }

         if ( $this->db->field_exists('category', 'transaction_ticketing') ) {
            $this->dbforge->drop_column('transaction_ticketing', 'category');
        }

         if ( $this->db->field_exists('past_due', 'transaction_ticketing') ) {
            $this->dbforge->drop_column('transaction_ticketing', 'past_due');
        }

         if ( $this->db->field_exists('past_due_status', 'transaction_ticketing') ) {
            $this->dbforge->drop_column('transaction_ticketing', 'past_due_status');
        }

         if ( $this->db->field_exists('received_date', 'transaction_ticketing') ) {
            $this->dbforge->drop_column('transaction_ticketing', 'received_date');
        }

         if ( $this->db->field_exists('sent_date', 'transaction_ticketing') ) {
            $this->dbforge->drop_column('transaction_ticketing', 'sent_date');
        }

         if ( $this->db->field_exists('letter_request', 'transaction_ticketing') ) {
            $this->dbforge->drop_column('transaction_ticketing', 'letter_request');
        }

         if ( $this->db->field_exists('letter_request_name', 'transaction_ticketing') ) {
            $this->dbforge->drop_column('transaction_ticketing', 'letter_request_name');
        }

         if ( $this->db->field_exists('letter_request_status', 'transaction_ticketing') ) {
            $this->dbforge->drop_column('transaction_ticketing', 'letter_request_status');
        }

         if ( $this->db->field_exists('date_requested', 'transaction_ticketing') ) {
            $this->dbforge->drop_column('transaction_ticketing', 'date_requested');
        }

         if ( $this->db->field_exists('date_approved', 'transaction_ticketing') ) {
            $this->dbforge->drop_column('transaction_ticketing', 'date_approved');
        }

         if ( $this->db->field_exists('date_disapproved', 'transaction_ticketing') ) {
            $this->dbforge->drop_column('transaction_ticketing', 'date_disapproved');
        }

        if ( $this->db->field_exists('date_cancelled', 'transaction_ticketing') ) {
            $this->dbforge->drop_column('transaction_ticketing', 'date_cancelled');
        }


    }
}














