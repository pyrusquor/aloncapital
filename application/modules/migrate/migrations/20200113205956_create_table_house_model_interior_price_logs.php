<?php defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_house_model_interior_price_logs extends CI_Migration {

	function up () {

		if ( $this->db->table_exists('house_model_interior_price_logs') ) {

			$this->dbforge->drop_table('house_model_interior_price_logs');
		}

		$_fields	=	array(
									'id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => TRUE,
                    'comment'	=>	'house_model_interior_price_logs Table ID'
									),
									'house_model_interior_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'house_model_interior_price_logs house_model_interior_id | house_model_interiors ID'
									),
									'price'	=>	array(
										'type'	=>	'DECIMAL(20,2)',
                    'NULL' => TRUE,
                    'comment'	=>	'house_model_interior_price_logs price'
									),
									'is_active'	=>	array(
										'type'	=>	'TINYINT',
										'constraint' => '1',
                    'NOT NULL' => TRUE,
                    'default'	=>	1,
                    'comment'	=>	'house_model_interior_price_logs is_active 1:TRUE|FALSE:0'
									),
	                'created_by'	=>	array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'house_model_interior_price_logs created_by'
	                ),
	                'created_at' => array(
	                	'type'	=>	'DATETIME',
	                	'NULL'	=>	TRUE,
                    'comment'	=>	'house_model_interior_price_logs created_at'
	                ),
	                'updated_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'house_model_interior_price_logs updated_by'
	                ),
	                'updated_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'house_model_interior_price_logs updated_at'
	                ),
	                'deleted_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'house_model_interior_price_logs deleted_by'
	                ),
	                'deleted_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'house_model_interior_price_logs deleted_at'
	                )
								);

		$this->dbforge->add_field($_fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('house_model_interior_price_logs', TRUE);
	}

	function down () {

		if ( $this->db->table_exists('house_model_interior_price_logs') ) {

			$this->dbforge->drop_table("house_model_interior_price_logs");
		}
	}
}