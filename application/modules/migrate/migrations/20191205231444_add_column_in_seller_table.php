<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Add_column_in_seller_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('sellers'))
		{
			if ($this->db->field_exists('height_uom', 'sellers'))
			{
				$this->dbforge->drop_column('sellers', 'height_uom');
            }
            if ($this->db->field_exists('weight_uom', 'sellers'))
			{
				$this->dbforge->drop_column('sellers', 'weight_uom');
			}
        }
        
        if($this->db->table_exists('sellers'))
		{	
			if (!$this->db->field_exists('philhealth', 'sellers'))
			{
				$this->db->query("ALTER TABLE `sellers` ADD COLUMN `philhealth` VARCHAR(50) NULL AFTER `tin`");
            }

            if (!$this->db->field_exists('hdmf', 'sellers'))
			{
				$this->db->query("ALTER TABLE `sellers` ADD COLUMN `hdmf` VARCHAR(50) NULL AFTER `philhealth`");
			}
		}
    }

    public function down()
    {
        if($this->db->table_exists('sellers'))
		{
			if ($this->db->field_exists('hdmf', 'sellers'))
			{
				$this->dbforge->drop_column('sellers', 'hdmf');
            }
            
            if ($this->db->field_exists('philhealth', 'sellers'))
			{
				$this->dbforge->drop_column('sellers', 'philhealth');
			}
		}
    }
}
