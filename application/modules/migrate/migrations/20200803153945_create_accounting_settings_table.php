<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_accounting_settings_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('accounting_settings')) {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NULL' => false,
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '80',
                    'NULL' => false,
                ),
                'description' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '250',
                    'NULL' => false,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('accounting_settings', true);
        }
        if(!$this->db->table_exists('accounting_settings_items')) {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NULL' => false,
                ),
                'accounting_settings_id' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '80',
                    'NULL' => false,
                ),
                'accounting_entry_type' => array(
                    'type' => 'char',
                    'constraint' => '1',
                ),
                'accounting_entry' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('accounting_setting_items', true);
        }
    }

    public function down()
    {
        if($this->db->table_exists('accounting_settings')) {
            $this->dbforge->drop_table('accounting_settings');
        }
        if($this->db->table_exists('accounting_setting_items')) {
            $this->dbforge->drop_table('accounting_setting_items');
        }
    }
}
