<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Add_column_date_effectivity extends CI_Migration
{

    public function up()
    {

        if($this->db->table_exists('house_model_interiors'))
		{	
			if (!$this->db->field_exists('date_effectivity', 'house_model_interiors'))
			{
				$this->db->query("ALTER TABLE `house_model_interiors` ADD COLUMN `date_effectivity` TEXT NULL AFTER `price`");
            }

		}
    }

    public function down()
    {
        if($this->db->table_exists('house_model_interiors'))
		{
            if ($this->db->field_exists('date_effectivity', 'house_model_interiors'))
			{
				$this->dbforge->drop_column('house_model_interiors', 'date_effectivity');
			}
		}
    }
}
