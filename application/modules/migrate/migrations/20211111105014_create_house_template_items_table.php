<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_house_template_items_table extends CI_Migration
{
    private $tbl = "house_template_items";
    private $fields = array(

        "id" => array(
            "type" => "INT",
            "constraint" => "11",
            "unsigned" => True,
            "auto_increment" => True,
            "NOT NULL" => True,
        ),

        'house_template_id' => array(
            "type" => "INT",
            "NOT NULL" => True,
            "unsigned" => True,
        ),

        'item_id' => array(
            "type" => "INT",
            "NOT NULL" => True,
            "unsigned" => True,
        ),

        'count' => array(
            "type" => "INT",
            "default" => 0
        ),

        "created_by" => array(
            "type" => "INT",
            "NOT NULL" => True,
            "unsigned" => True,
        ),

        "created_at" => array(
            "type" => "DATETIME",
            "NOT NULL" => True,
        ),

        "updated_by" => array(
            "type" => "INT",
            "NULL" => True,
            "unsigned" => True,
        ),

        "updated_at" => array(
            "type" => "DATETIME",
            "NULL" => True,
        ),

        "deleted_by" => array(
            "type" => "INT",
            "NULL" => True,
            "unsigned" => True,
        ),

        "deleted_at" => array(
            "type" => "DATETIME",
            "NULL" => True,
        ),

    );
    public function up()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->tbl, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
