<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_buyer_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('buyer_employment_table'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'buyer_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'occupation_type_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'industry_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'occupation_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'designation' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => FALSE
                ),
                'employer' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => FALSE
                ),
                'gross_salary' => array(
                    'type'=>'DOUBLE',
                    'constraint' => '20,2',
                    'NULL'=> TRUE,
                ),
                'location_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => FALSE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('buyer_employment_table', TRUE);
        }

        if(!$this->db->table_exists('buyer_identifications'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'buyer_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'type_of_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'id_number' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => FALSE
                ),
                'date_issued' => array(
                    'type' => 'DATETIME',
                    'NULL' => TRUE
                ),
                'date_expiration' => array(
                    'type' => 'DATETIME',
                    'NULL' => TRUE
                ),
                'place_issued' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '12551',
                    'NULL' => TRUE
                ),
                'file_id' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('buyer_identifications', TRUE);
        }

        if ( ! $this->db->field_exists('created_at', 'buyer_employment_table') ) {
            $this->db->query("ALTER TABLE `buyer_employment_table` ADD COLUMN `created_at` datetime NULL AFTER `address`");
        }

        if ( ! $this->db->field_exists('created_by', 'buyer_employment_table') ) {
            $this->db->query("ALTER TABLE `buyer_employment_table` ADD COLUMN `created_by` int(10) NULL AFTER `created_at`");
        }

        if ( ! $this->db->field_exists('updated_at', 'buyer_employment_table') ) {
            $this->db->query("ALTER TABLE `buyer_employment_table` ADD COLUMN `updated_at` datetime NULL AFTER `created_by`");
        }

        if ( ! $this->db->field_exists('updated_by', 'buyer_employment_table') ) {
            $this->db->query("ALTER TABLE `buyer_employment_table` ADD COLUMN `updated_by` int(10) NULL AFTER `updated_at`");
        }

        if ( ! $this->db->field_exists('deleted_at', 'buyer_employment_table') ) {
            $this->db->query("ALTER TABLE `buyer_employment_table` ADD COLUMN `deleted_at` datetime NULL AFTER `updated_by`");
        }

        if ( ! $this->db->field_exists('deleted_by', 'buyer_employment_table') ) {
            $this->db->query("ALTER TABLE `buyer_employment_table` ADD COLUMN `deleted_by` int(10) NULL AFTER `deleted_at`");
        }

        if ( ! $this->db->field_exists('created_at', 'buyer_identifications') ) {
            $this->db->query("ALTER TABLE `buyer_identifications` ADD COLUMN `created_at` datetime NULL AFTER `file_id`");
        }

        if ( ! $this->db->field_exists('created_by', 'buyer_identifications') ) {
            $this->db->query("ALTER TABLE `buyer_identifications` ADD COLUMN `created_by` int(10) NULL AFTER `created_at`");
        }

        if ( ! $this->db->field_exists('updated_at', 'buyer_identifications') ) {
            $this->db->query("ALTER TABLE `buyer_identifications` ADD COLUMN `updated_at` datetime NULL AFTER `created_by`");
        }

        if ( ! $this->db->field_exists('updated_by', 'buyer_identifications') ) {
            $this->db->query("ALTER TABLE `buyer_identifications` ADD COLUMN `updated_by` int(10) NULL AFTER `updated_at`");
        }

        if ( ! $this->db->field_exists('deleted_at', 'buyer_identifications') ) {
            $this->db->query("ALTER TABLE `buyer_identifications` ADD COLUMN `deleted_at` datetime NULL AFTER `updated_by`");
        }

        if ( ! $this->db->field_exists('deleted_by', 'buyer_identifications') ) {
            $this->db->query("ALTER TABLE `buyer_identifications` ADD COLUMN `deleted_by` int(10) NULL AFTER `deleted_at`");
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('buyer_employment_table') ) {
            $this->dbforge->drop_table('buyer_employment_table');
        }

        if ( $this->db->table_exists('buyer_identifications') ) {
            $this->dbforge->drop_table('buyer_identifications');
        }
    }
}
