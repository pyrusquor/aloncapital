<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_departments extends CI_Migration
{

    public function up()
    {
        if (!$this->db->table_exists('departments')) {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NOT NULL' => false,
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '250',
                    'NULL' => false,
                ),
                'description' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => true,
                ),
                'signatory_level_1' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => true,
                ),
                'signatory_level_2' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => true,
                ),
                'signatory_level_3' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => true,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('departments', true);

            // $this->load->model('seller_group/Seller_teams_model', 'seller_group');

            // $this->seller_group->insert_dummy();
        }
    }

    public function down()
    {
        if ($this->db->table_exists('departments')) {

            $this->dbforge->drop_table('departments');
        }
    }
}
