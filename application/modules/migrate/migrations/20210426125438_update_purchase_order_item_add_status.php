<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Update_purchase_order_item_add_status extends CI_Migration
    {
        private $tbl = "purchase_order_items";
        private $new_fields = array(
            'receiving_status' => array(
                'type' => 'INT',
                'constraint' => '2',
                'NULL' => TRUE,
                'default' => '1'
            )
        );

        public function up()
        {
            $this->dbforge->add_column($this->tbl, $this->new_fields);
        }

        public function down()
        {
            foreach ($this->new_fields as $field => $data) {
                $this->dbforge->drop_column($this->tbl, $field);
            }
        }
    }
