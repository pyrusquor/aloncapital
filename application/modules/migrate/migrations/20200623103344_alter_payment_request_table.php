<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_payment_request_table extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('date_requested', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `date_requested` DATE NULL AFTER `payee_type_id`");
        }

        if (!$this->db->field_exists('department_id', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `department_id` INT(11) NULL AFTER `date_requested`");
        }

        if (!$this->db->field_exists('staff_id', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `staff_id` INT(11) NULL AFTER `department_id`");
        }

        if (!$this->db->field_exists('project_id', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `project_id` INT(11) NULL AFTER `staff_id`");
        }

        if (!$this->db->field_exists('property_id', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `property_id` INT(11) NULL AFTER `project_id`");
        }

        if (!$this->db->field_exists('approving_department_id', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `approving_department_id` INT(11) NULL AFTER `property_id`");
        }

        if (!$this->db->field_exists('approving_staff_id', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `approving_staff_id` INT(11) NULL AFTER `approving_department_id`");
        }

        if (!$this->db->field_exists('requesting_department_id', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `requesting_department_id` INT(11) NULL AFTER `approving_staff_id`");
        }

        if (!$this->db->field_exists('requesting_staff_id', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `requesting_staff_id` INT(11) NULL AFTER `requesting_department_id`");
        }

        if (!$this->db->field_exists('prepared_by', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `prepared_by` INT(11) NULL AFTER `approving_staff_id`");
        }

        if (!$this->db->field_exists('total_due_amount', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `total_due_amount` DECIMAL(20, 2) NULL AFTER `net_amount`");
        }

        $fields = array(
            'wht_percentage' => array(
                'name' => 'tax_percentage',
                'type' => 'DOUBLE',
                'constraint' => '20, 2',
                'NULL' => false
            ),
            'wht_amount' => array (
                'name' => 'tax_amount',
                'type' => 'DOUBLE',
                'constraint' => '20, 2',
                'NULL' => false
            ),
            'remarks' => array(
                'name' => 'particulars',
                'type' => 'VARCHAR',
                'constraint' => '250',
                'NULL' => true
            ),
        );

        if ($this->db->table_exists('payment_requests')) {
            $this->dbforge->modify_column('payment_requests', $fields);
        }
    }

    public function down()
    {
        if ($this->db->field_exists('date_requested', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'date_requested');
        }
        if ($this->db->field_exists('department_id', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'department_id');
        }
        if ($this->db->field_exists('staff_id', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'staff_id');
        }
        if ($this->db->field_exists('project_id', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'project_id');
        }
        if ($this->db->field_exists('property_id', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'property_id');
        }
        if ($this->db->field_exists('approving_department_id', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'approving_department_id');
        }
        if ($this->db->field_exists('approving_staff_id', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'approving_staff_id');
        }
        if ($this->db->field_exists('requesting_department_id', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'requesting_department_id');
        }
        if ($this->db->field_exists('requesting_staff_id', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'requesting_staff_id');
        }
        if ($this->db->field_exists('prepared_by', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'prepared_by');
        }
        if ($this->db->field_exists('total_due_amount', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'total_due_amount');
        }

        $fields = array(
            'tax_percentage' => array(
                'name' => 'wht_percentage',
                'type' => 'DOUBLE',
                'constraint' => '20, 2',
                'NULL' => false
            ),
            'tax_amount' => array (
                'name' => 'wht_amount',
                'type' => 'DOUBLE',
                'constraint' => '20, 2',
                'NULL' => false
            ),
            'particulars' => array(
                'name' => 'remarks',
                'type' => 'VARCHAR',
                'constraint' => '250',
                'NULL' => true
            ),
        );

        if ($this->db->table_exists('payment_requests')) {
            $this->dbforge->modify_column('payment_requests', $fields);
        }
    }
}
