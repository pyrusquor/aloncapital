<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_official_payments_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transaction_official_payments')) {
            if(!$this->db->field_exists('receipt_type', 'transaction_official_payments')) {
                $this->db->query("ALTER TABLE `transaction_official_payments` ADD COLUMN `receipt_type` INT(11) DEFAULT 0 NULL AFTER `or_date`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transaction_official_payments')) {
            $this->dbforge->drop_column('transaction_official_payments', 'receipt_type');
        }
    }
}
