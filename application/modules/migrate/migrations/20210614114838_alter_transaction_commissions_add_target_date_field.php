<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_commissions_add_target_date_field extends CI_Migration
{
    private $fields = array(
        'target_date' => array (
            'type' => 'DATETIME',
            'NOT_NULL' => 'FALSE',
            'after' => 'wht_rate'
        )
    );

    private $table = 'transaction_commissions';

    public function up()
    {
        if ($this->db->table_exists($this->table)) {
            if (!$this->db->field_exists('target_date', $this->table)){
                $this->dbforge->add_column($this->table, $this->fields);
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->table)) {
            if ($this->db->field_exists('target_date', $this->table)){
                $this->dbforge->drop_column($this->table, 'target_date');
            }
        }
    }
}
