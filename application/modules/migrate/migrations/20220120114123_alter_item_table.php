<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_item_table extends CI_Migration
{

    public function up()
    {

        if (!$this->db->field_exists('beginning_unit_price', 'item')) {
            $this->db->query("ALTER TABLE `item` ADD `beginning_unit_price` DEC(15,2) DEFAULT 0 NOT NULL AFTER `code`");
        }
    }

    public function down()
    {

        if ($this->db->field_exists('beginning_unit_price', 'item')) {
            $this->db->query("ALTER TABLE `item` DROP COLUMN `beginning_unit_price`");
        }
    }
}
