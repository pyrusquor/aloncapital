<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_warehouse extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => true,
                'auto_increment' => true,
                'NOT NULL' => false,
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'warehouse_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '5',
                'NULL' => false,
            ),
            'sbu_id' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'address' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => true,
            ),
            'telephone_number' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true,
            ),
            'fax_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '7',
                'NULL' => true,
            ),
            'mobile_number' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true,
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'contact_person' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'is_active' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'created_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('warehouse', true);

        $this->load->model('warehouse/Warehouse_model', 'warehouse');
        $this->warehouse->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('warehouse')) {

            $this->dbforge->drop_table('warehouse');
        }
    }
}
