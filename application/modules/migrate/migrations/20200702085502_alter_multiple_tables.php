<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_tables extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('accounting_entries')) {
            if ($this->db->field_exists('payment_request_id', 'accounting_entries')) {
                $this->dbforge->drop_column('accounting_entries', 'payment_request_id');
            }
        }

        if ($this->db->table_exists('payment_requests')) {

            if (!$this->db->field_exists('accounting_entry_id', 'payment_requests')) {

                $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `accounting_entry_id` int(11) DEFAULT NULL  AFTER `reference`");
            }
        }

        if ($this->db->table_exists('payable_items')) {

            if (!$this->db->field_exists('payment_voucher_id', 'payable_items')) {

                $this->db->query("ALTER TABLE `payable_items` ADD COLUMN `payment_voucher_id` int(11) DEFAULT NULL  AFTER `payment_request_id`");
            }
        }

    }

    public function down()
    {
        if ($this->db->table_exists('accounting_entries')) {

            if (!$this->db->field_exists('payment_request_id', 'accounting_entries')) {

                $this->db->query("ALTER TABLE `accounting_entries` ADD COLUMN `payment_request_id` int(11) DEFAULT NULL  AFTER `payee_type_id`");
            }
        }

        if ($this->db->table_exists('payment_requests')) {
            if ($this->db->field_exists('accounting_entry_id', 'payment_requests')) {
                $this->dbforge->drop_column('payment_requests', 'accounting_entry_id');
            }
        }

        if ($this->db->table_exists('payable_items')) {
            if ($this->db->field_exists('payment_voucher_id', 'payable_items')) {
                $this->dbforge->drop_column('payable_items', 'payment_voucher_id');
            }
        }
    }
}
