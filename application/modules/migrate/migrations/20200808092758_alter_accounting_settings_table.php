<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_settings_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('accounting_settings')) {
            if (!$this->db->field_exists('code', 'accounting_settings')) {
                $this->db->query("ALTER TABLE `accounting_settings` ADD COLUMN `code` VARCHAR(50) DEFAULT NULL  AFTER `description`");
            }
        }
      
    }
    

    public function down()
    {
        if($this->db->table_exists('accounting_settings')) {
            if($this->db->field_exists('code', 'accounting_settings')) {
                $this->dbforge->drop_column('accounting_settings', 'code');
            }
        }
    }
}
