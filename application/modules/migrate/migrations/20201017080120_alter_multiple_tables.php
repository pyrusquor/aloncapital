<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_tables extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'recipient_id' => array(
                'name' => 'recipient_id',
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => false
            ),
            'is_sent' => array(
                'name' => 'is_sent',
                'type' => 'INT',
                'constraint' => '1',
                'default' => '0',
                'NULL' => true
            ),
            'template_id' => array(
                'name' => 'template_id',
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true
            )
        );

       if($this->db->table_exists('communications')) {
            $this->dbforge->modify_column('communications', $fields);
       }

       if($this->db->table_exists('communication_templates')) {
           if(!$this->db->field_exists('type', 'communication_templates')) {
                $this->db->query("ALTER TABLE `communication_templates` ADD COLUMN `type` VARCHAR(50) DEFAULT NULL AFTER `name`");
           }
       }
    }

    public function down()
    {
        $fields = array(
            'recipient_id' => array(
                'name' => 'recipient_id',
                'type' => 'DATETIME',
                'NULL' => true
            ),
            'is_sent' => array(
                'name' => 'is_sent',
                'type' => 'DATETIME',
                'NULL' => true
            ),
            'template_id' => array(
                'name' => 'template_id',
                'type' => 'DATETIME',
                'NULL' => true
            )
        );

        if($this->db->table_exists('communications')) {
            if($this->db->field_exists('template_id', 'communications')) {
                 $this->dbforge->modify_column('communications', $fields);
            }
        }

        if($this->db->table_exists('communication_templates')) {
            $this->dbforge->drop_column('communication_templates', 'type');
        }
    }
}
