<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_items_table_add_references extends CI_Migration
{
    protected $tbl = "item";
    protected $fields = array(
        array('item_class_id', 'item_class'),
        array('item_brand_id', 'item_brand'),
        array('item_abc_id', 'item_abc'),
        array('item_group_id', 'item_group'),
    );

    private function check($field)
    {
        if ($this->db->table_exists($this->tbl)) {
            if (!$this->db->field_exists($field, $this->tbl)) {
                return true;
            }
        }
        return false;
    }


    public function up()
    {
        foreach($this->fields as $field){
            $key = $field[0];
            $fk = $field[1];
            if($this->check($key)){
                $this->db->query("ALTER TABLE `$this->tbl` ADD COLUMN `$key` INT(11) UNSIGNED NULL");
                $this->db->query("ALTER TABLE `$this->tbl` ADD INDEX ($key)");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists($this->tbl)){
            foreach($this->fields as $field){
                $key = $field[0];
                $fk = $field[1];
                $this->dbforge->drop_column($this->tbl, $key);
            }
        }
    }
}
