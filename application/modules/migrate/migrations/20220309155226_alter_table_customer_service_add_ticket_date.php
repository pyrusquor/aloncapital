<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_customer_service_add_ticket_date extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('ticket_date', 'customer_services')) {
            $this->db->query("ALTER TABLE `customer_services` ADD `ticket_date` datetime NULL AFTER `department_id`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('ticket_date', 'customer_services')) {
            $this->db->query("ALTER TABLE `customer_services` DROP COLUMN `ticket_date`");
        }
    }
}
