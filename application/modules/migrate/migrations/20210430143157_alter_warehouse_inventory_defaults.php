<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Alter_warehouse_inventory_defaults extends CI_Migration
    {

        public function up()
        {
            $this->db->query("ALTER TABLE `warehouse_inventories`
	CHANGE COLUMN `created_by` `created_by` INT UNSIGNED NULL AFTER `id`,
	CHANGE COLUMN `created_at` `created_at` DATETIME NULL AFTER `created_by`,
	CHANGE COLUMN `updated_by` `updated_by` INT UNSIGNED NULL AFTER `created_at`,
	CHANGE COLUMN `updated_at` `updated_at` DATETIME NULL AFTER `updated_by`,
	CHANGE COLUMN `deleted_by` `deleted_by` INT UNSIGNED NULL AFTER `updated_at`,
	CHANGE COLUMN `deleted_at` `deleted_at` DATETIME NULL AFTER `deleted_by`;
");
        }

        public function down()
        {

        }
    }
