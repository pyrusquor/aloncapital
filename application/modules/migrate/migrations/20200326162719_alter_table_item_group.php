<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_item_group extends CI_Migration
{

    public function up()
    {
        $this->db->query("ALTER TABLE `item_group` CHANGE `status` `is_active` int(1) NOT NULL AFTER `account_number`;");

    }

    public function down()
    {
        if ( $this->db->field_exists('is_active', 'item_group') ) {
            $this->db->query("ALTER TABLE `item_group` DROP `is_active`");
        }
    }
}
