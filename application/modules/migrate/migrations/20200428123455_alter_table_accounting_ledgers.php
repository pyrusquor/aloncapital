<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_accounting_ledgers extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('created_by', 'accounting_ledgers')) {
            $this->db->query("ALTER TABLE `accounting_ledgers` ADD COLUMN `created_by` int(1) NULL AFTER `company_id`");

        }
    }

    public function down()
    {
        if ($this->db->field_exists('created_by', 'accounting_ledgers')) {
            $this->dbforge->drop_column('accounting_ledgers', 'created_by');
        }
    }
}
