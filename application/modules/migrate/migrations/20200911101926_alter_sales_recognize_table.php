<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_sales_recognize_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('sales_recognize')) {
            if (!$this->db->field_exists('is_recognized', 'sales_recognize')) {
                $this->db->query("ALTER TABLE `sales_recognize` ADD COLUMN `is_recognized` int(1) DEFAULT NULL  AFTER `id`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('sales_recognize')) {
            if ($this->db->field_exists('is_recognized', 'sales_recognize')) {
                $this->dbforge->drop_column('sales_recognize', 'is_recognized');
            }
        }
    }
}
