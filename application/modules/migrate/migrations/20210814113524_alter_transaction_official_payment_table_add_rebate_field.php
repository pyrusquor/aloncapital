<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_official_payment_table_add_rebate_field extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transaction_official_payments')) {
            if (!$this->db->field_exists('rebate_amount', 'transaction_official_payments')) {
                $this->db->query("ALTER TABLE `transaction_official_payments` ADD COLUMN `rebate_amount` double(20,2) DEFAULT 0.00 AFTER `penalty_amount`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transaction_official_payments')) {
            if ($this->db->field_exists('rebate_amount', 'transaction_official_payments')) {
                $this->dbforge->drop_column('transaction_official_payments', 'excess');
            }
        }
    }
}
