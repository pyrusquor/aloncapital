<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_financing_scheme_commission_values extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('financing_scheme_commission_values'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'financing_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'period_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'percentage_to_finish' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'commission_rate_amount' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'commission_rate_percentage' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'commission_term' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'commission_vat' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'is_active' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'NULL' => FALSE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
			
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('financing_scheme_commission_values', TRUE);
        }



        $this->db->query("ALTER TABLE `financing_scheme_commission_values` CHANGE `period_id` `period_id` int(11) NOT NULL DEFAULT '0' AFTER `financing_id`, CHANGE `percentage_to_finish` `percentage_to_finish` double(20,2) NOT NULL DEFAULT '0.00' AFTER `period_id`, CHANGE `commission_rate_amount` `commission_rate_amount` double(20,2) NOT NULL DEFAULT '0.00' AFTER `percentage_to_finish`, CHANGE `commission_rate_percentage` `commission_rate_percentage` double(20,2) NOT NULL DEFAULT '0.00' AFTER `commission_rate_amount`, CHANGE `commission_term` `commission_term` double(20,2) NOT NULL DEFAULT '0.00' AFTER `commission_rate_percentage`, CHANGE `commission_vat` `commission_vat` double(20,2) NOT NULL DEFAULT '0.00' AFTER `commission_term`");

        $this->db->query("ALTER TABLE `financing_scheme_commission_values` ADD `is_cash_advance` int(11) NOT NULL DEFAULT '0' AFTER `commission_vat`");
        
        $this->db->query("ALTER TABLE `sellers` ADD `upline_id` int NULL DEFAULT '0' AFTER `landline`");

    }

    public function down()
    {
        if ( $this->db->table_exists('financing_scheme_commission_values') ) {

			$this->dbforge->drop_table('financing_scheme_commission_values');
		}
    }
}
