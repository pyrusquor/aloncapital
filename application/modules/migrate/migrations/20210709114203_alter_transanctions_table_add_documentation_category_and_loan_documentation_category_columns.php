<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transanctions_table_add_documentation_category_and_loan_documentation_category_columns extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transactions')) {

            if (!$this->db->field_exists('documentation_category', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` ADD COLUMN `documentation_category` INT(11) NULL AFTER `aris_id`");
            }

            if (!$this->db->field_exists('loan_documentation_category', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` ADD COLUMN `loan_documentation_category` INT(11) NULL AFTER `aris_id`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transactions')) {

            if ($this->db->field_exists('documentation_category', 'transactions')) {
                $this->dbforge->drop_column('transactions', 'documentation_category');
            }

            if ($this->db->field_exists('loan_documentation_category', 'transactions')) {
                $this->dbforge->drop_column('transactions', 'loan_documentation_category');
            }
        }
    }
}
