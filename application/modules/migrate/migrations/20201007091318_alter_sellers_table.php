<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_sellers_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('sellers')) {
            if (!$this->db->field_exists('designation', 'sellers')) {
                $this->db->query("ALTER TABLE `sellers` ADD COLUMN `designation` VARCHAR(255) DEFAULT NULL  AFTER `middle_name`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('sellers')) {
            if ($this->db->field_exists('designation', 'sellers')) {
                $this->dbforge->drop_column('sellers', 'designation');
            }
        }
    }
}
