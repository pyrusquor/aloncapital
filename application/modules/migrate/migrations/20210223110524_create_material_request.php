<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_material_request extends CI_Migration
{
    protected $tbl = 'material_requests';

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'NOT NULL' => FALSE
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'project_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => FALSE,
            ),
            'company_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => FALSE,
            ),
            'approving_staff_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => FALSE,
            ),
            'requesting_staff_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => FALSE,
            ),
            'reference' => array(
                'type' => 'VARCHAR',
                'NULL' => FALSE,
                'constraint' =>'12'
            ),
            'days_lapsed' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'particulars' => array(
                'type' => 'VARCHAR',
                'NULL' => TRUE,
                'constraint' =>'255'
            ),
            'request_date' => array(
                'type' => 'DATE',
                'NULL' => FALSE
            ),
            'request_amount' => array(
                'type' => 'DOUBLE',
                'constraint' => '20,2',
                'NULL' => FALSE
            ),
            'request_reason'=> array(
                'type' => 'VARCHAR',
                'NULL' => TRUE,
                'constraint' =>'255'
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl, TRUE);
    }

    public function down()
    {
        if($this->db->table_exists($this->tbl)){
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
