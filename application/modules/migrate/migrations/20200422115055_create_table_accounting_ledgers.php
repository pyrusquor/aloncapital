<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_accounting_ledgers extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => true,
                'auto_increment' => true,
                'NOT NULL' => false,
            ),
            'group_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => false,
            ),
            'account_id' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'NULL' => false,
            ),
            'color' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'NULL' => false,
            ),
            'op_balance' => array(
                'type' => 'DECIMAL',
                'constraint' => '15,2',
                'NULL' => false,
            ),
            'op_balance_dc' => array(
                'type' => 'CHAR',
                'constraint' => '1',
                'NULL' => false,
            ),
            'type' => array(
                'type' => 'INT',
                'constraint' => '2',
                'NULL' => false,
            ),
            'reconciliation' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
            ),
            'accounts_receivable' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
            ),
            'accounts_payable' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
            ),
            'non_operating' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
            ),
            'asset_type' => array(
                'type' => 'text',
                'constraint' => '10',
                'NULL' => false,
            ),
            'slug' => array(
                'type' => 'tinytext',
                'NULL' => true,
            ),
            'expense_type' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true,
            ),
            'company_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('accounting_ledgers', true);

        $this->load->model('chart_of_accounts/Chart_of_accounts_model', 'chart_of_accounts');
        // $this->accounting_ledgers->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('accounting_ledgers')) {

            $this->dbforge->drop_table('accounting_ledgers');
        }
    }
}
