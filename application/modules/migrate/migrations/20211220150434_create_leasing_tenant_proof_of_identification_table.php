<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_leasing_tenant_proof_of_identification_table extends CI_Migration
{

    private $table = "leasing_tenant_proof_of_identification";
    private $fields = array(

        "id" => array(
            "type" => "INT",
            "constraint" => "11",
            "unsigned" => TRUE,
            "auto_increment" => TRUE,
            "NOT NULL" => TRUE,
        ),

        'leasing_tenant_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'type_of_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'id_number' => array(
            "type" => "VARCHAR",
            'constraint' => '255',
            "NULL" => TRUE,
        ),

        "date_issued" => array(
            "type" => "DATE",
            "NULL" => TRUE,
        ),

        "date_expiration" => array(
            "type" => "DATE",
            "NULL" => TRUE,
        ),

        'place_issued' => array(
            "type" => "TEXT",
            "NULL" => TRUE,
        ),

        // File Info
        'file_name' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),

        'full_path' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),

        // Audit Info
        "created_by" => array(
            "type" => "INT",
            "NOT NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "created_at" => array(
            "type" => "DATETIME",
            "NOT NULL" => TRUE,
        ),

        "updated_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "updated_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        "deleted_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "deleted_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

    );

    public function up()
    {
        if (!$this->db->table_exists($this->table)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->table, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->table)) {
            $this->dbforge->drop_table($this->table);
        }
    }
}
