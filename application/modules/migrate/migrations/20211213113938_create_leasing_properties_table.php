<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_leasing_properties_table extends CI_Migration
{

    private $table = "leasing_properties";
    private $fields = array(

        "id" => array(
            "type" => "INT",
            "constraint" => "11",
            "unsigned" => TRUE,
            "auto_increment" => TRUE,
            "NOT NULL" => TRUE,
        ),

        'leasing_project_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'unit_type_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'unit_number' => array(
            "type" => "VARCHAR",
            'constraint' => '255',
            "NULL" => TRUE,
        ),

        'property_code' => array(
            "type" => "VARCHAR",
            'constraint' => '255',
            "NULL" => TRUE,
        ),

        'general_status_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'company_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'real_property_tax' => array(
            "type" => "FLOAT",
            "default" => 0
        ),

        'area' => array(
            "type" => "FLOAT",
            "default" => 0
        ),

        'cusa_price' => array(
            "type" => "FLOAT",
            "default" => 0
        ),

        'price_per_sqm' => array(
            "type" => "FLOAT",
            "default" => 0
        ),

        'base_rental_rate' => array(
            "type" => "FLOAT",
            "default" => 0
        ),

        // Audit Info
        "created_by" => array(
            "type" => "INT",
            "NOT NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "created_at" => array(
            "type" => "DATETIME",
            "NOT NULL" => TRUE,
        ),

        "updated_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "updated_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        "deleted_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "deleted_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

    );

    public function up()
    {
        if (!$this->db->table_exists($this->table)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->table, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->table)) {
            $this->dbforge->drop_table($this->table);
        }
    }
}
