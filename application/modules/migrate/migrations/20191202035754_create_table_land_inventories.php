<?php defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_land_inventories extends CI_Migration {

	function up () {

		if ( $this->db->table_exists('land_inventories') ) {

			$this->dbforge->drop_table('land_inventories');
		}

		$_fields	=	array(
									'id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => TRUE,
                    'comment'	=>	'land_inventories Table ID'
									),
									'land_owner_name'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => FALSE,
                    'comment'	=>	'land_inventories land_owner_name'
									),
									'location'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories location'
									),
									'owner_contact_info'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories owner_contact_info'
									),
									'land_area'	=>	array(
										'type'	=>	'DECIMAL(20,2)',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories land_area'
									),
									'estimated_price'	=>	array(
										'type'	=>	'DECIMAL(20,2)',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories estimated_price'
									),
									'negotiated_price'	=>	array(
										'type'	=>	'DECIMAL(20,2)',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories negotiated_price'
									),
									'final_price'	=>	array(
										'type'	=>	'DECIMAL(20,2)',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories final_price'
									),
	                'agent_id'	=>	array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventories agent_id'
	                ),
									'land_classification_id'	=>	array(
										'type'	=>	'INT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories land_classification_id'
									),
									'location_of_registration'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories location_of_registration'
									),
									'title'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories title'
									),
									'title_details'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories title_details'
									),
									'lot_number'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories lot_number'
									),
									'encumbrance'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories encumbrance'
									),
									'ownership_classification_id'	=>	array(
										'type'	=>	'INT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories ownership_classification_id'
									),
									'status'	=>	array(
										'type'	=>	'INT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories status'
									),
	                'date_offered' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventories date_offered'
	                ),
	                'decision_date' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventories decision_date'
	                ),
	                'date_purchased' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventories date_purchased'
	                ),
	                'date_transfer_of_title' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventories date_transfer_of_title'
	                ),
									'ep_code'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories ep_code'
									),
									'market_value'	=>	array(
										'type'	=>	'DECIMAL(20,2)',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories market_value'
									),
	                'date_of_market_value_assessment' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventories date_of_market_value_assessment'
	                ),
									'appraised_value'	=>	array(
										'type'	=>	'DECIMAL(20,2)',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories appraised_value'
									),
	                'date_of_appraisal' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventories date_of_appraisal'
	                ),
									'assessed_value'	=>	array(
										'type'	=>	'DECIMAL(20,2)',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories assessed_value'
									),
									'mother_lot_tax_declaration'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories mother_lot_tax_declaration'
									),
									'tax_declaration'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories tax_declaration'
									),
									'company'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories company'
									),
									'remarks'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories remarks'
									),
	                'created_by'	=>	array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventories created_by'
	                ),
	                'created_at' => array(
	                	'type'	=>	'DATETIME',
	                	'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventories created_at'
	                ),
	                'updated_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventories updated_by'
	                ),
	                'updated_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventories updated_at'
	                ),
	                'deleted_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventories deleted_by'
	                ),
	                'deleted_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventories deleted_at'
	                )
								);

		$this->dbforge->add_field($_fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('land_inventories', TRUE);

		// $this->load->model('land/Land_inventory_model', 'M_land_inventory');
		// $this->M_land_inventory->insert_dummy();
	}

	function down () {

		if ( $this->db->table_exists('land_inventories') ) {

			$this->dbforge->drop_table("land_inventories");
		}
	}
}