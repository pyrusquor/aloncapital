<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_aris_project_information extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('filename', 'aris_project_information')) {
            $this->db->query("ALTER TABLE `aris_project_information` ADD `filename` VARCHAR(255) NULL AFTER `FaxNumber`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('filename', 'aris_project_information')) {
            $this->db->query("ALTER TABLE `aris_project_information` DROP COLUMN `filename`");
        }
    }
}
