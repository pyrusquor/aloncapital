<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_buyers_table_add_source_referral_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('buyers')) {

            if (!$this->db->field_exists('source_referral', 'buyers')) {
                $this->db->query("ALTER TABLE `buyers` ADD COLUMN `source_referral` INT(2) AFTER aris_id");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('buyers')) {

            if ($this->db->field_exists('source_referral', 'buyers')) {
                $this->dbforge->drop_column('buyers', 'source_referral');
            }
        }
    }
}
