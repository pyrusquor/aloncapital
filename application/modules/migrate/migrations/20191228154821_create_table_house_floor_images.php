<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_house_floor_images extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'NULL' => FALSE
            ),
            'house_model_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => FALSE
            ),
            'filename' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'NULL' => FALSE
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('house_floor_images', TRUE);
    }

    public function down()
    {
        if ( $this->db->table_exists('house_floor_images') ) {

			$this->dbforge->drop_table("house_floor_images");
		}
    }
}
