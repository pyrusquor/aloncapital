<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_table_modify_aris_id extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transactions')) {

            if ($this->db->field_exists('aris_id', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` MODIFY COLUMN `aris_id` varchar(32) NULL");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transactions')) {

            if ($this->db->field_exists('aris_id', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` MODIFY COLUMN `aris_id` INT(11) NULL");
            }
        }
    }
}
