<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_supplier_table_add_witholding_tax extends CI_Migration
{

    public function up()
    {

        if (!$this->db->field_exists('witholding_tax', 'suppliers')) {
            $this->db->query("ALTER TABLE `suppliers` ADD `witholding_tax` double(20,2) null after `payment_type`");
        }
    }

    public function down()
    {

        if ($this->db->field_exists('witholding_tax', 'suppliers')) {
            $this->db->query("ALTER TABLE `suppliers` DROP COLUMN `witholding_tax`");
        }
    }
}
