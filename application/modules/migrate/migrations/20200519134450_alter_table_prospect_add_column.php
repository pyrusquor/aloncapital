<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_prospect_add_column extends CI_Migration
{

    public function up()
    {

        if ($this->db->table_exists('prospect')) {

            if (!$this->db->field_exists('image', 'prospect')) {

                $this->db->query("ALTER TABLE `prospect` ADD COLUMN `image` varchar(255) DEFAULT NULL  AFTER `id`");
            }

            if (!$this->db->field_exists('social_media', 'prospect')) {

                $this->db->query("ALTER TABLE `prospect` ADD COLUMN `social_media` VARCHAR(255) DEFAULT NULL  AFTER `landline`");
            }
        }

    }

    public function down()
    {

        if ($this->db->field_exists('image', 'prospect')) {

            $this->dbforge->drop_column('prospect', 'image');
        }

        if ($this->db->field_exists('social_media', 'prospect')) {

            $this->dbforge->drop_column('prospect', 'social_media');
        }

    }
}
