<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Alter_material_issuances_add_transaction_type extends CI_Migration
    {

        private $tbl = 'material_issuances';
        private $fields = array(
            'issuance_type' => array(
                "create" => "ALTER TABLE `material_issuances` ADD COLUMN `issuance_type` INT UNSIGNED NULL DEFAULT '1' AFTER `warehouse_id`",
                "delete" => "ALTER TABLE `material_issuances` DROP COLUMN `issuance_type`"
            ),
        );

        public function up()
        {
            if ($this->db->table_exists($this->tbl)) {
                foreach ($this->fields as $field => $queries) {
                    if (!$this->db->field_exists($field, $this->tbl)) {
                        $this->db->query($queries['create']);
                    }
                }
            }
        }

        public function down()
        {
            if ($this->db->table_exists($this->tbl)) {
                foreach ($this->fields as $field => $queries) {
                    if ($this->db->field_exists($field, $this->tbl)) {
                        $this->db->query($queries['delete']);
                    }
                }
            }
        }
    }
