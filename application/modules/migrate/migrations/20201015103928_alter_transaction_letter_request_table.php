<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_letter_request_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transaction_letter_request')) {
            if (!$this->db->field_exists('approved_image', 'transaction_letter_request')) {
                $this->db->query("ALTER TABLE `transaction_letter_request` ADD COLUMN `approved_image` VARCHAR(255) DEFAULT NULL  AFTER `image`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transaction_letter_request')) {
            if($this->db->field_exists('approved_image', 'transaction_letter_request')) {
                $this->dbforge->drop_column('transaction_letter_request', 'approved_image');
            }
        }
    }
}
