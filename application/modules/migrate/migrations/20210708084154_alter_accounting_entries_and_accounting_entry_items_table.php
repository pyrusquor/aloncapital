<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_entries_and_accounting_entry_items_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('accounting_entries')) {

            if ($this->db->field_exists('created_by', 'accounting_entries')) {

                $this->db->query("ALTER TABLE `accounting_entries` MODIFY COLUMN `created_by` INT(10) UNSIGNED");
            }
        }

        if ($this->db->table_exists('accounting_entry_items')) {

            if ($this->db->field_exists('created_by', 'accounting_entry_items')) {

                $this->db->query("ALTER TABLE `accounting_entry_items` MODIFY COLUMN `created_by` INT(10) UNSIGNED");
            }
        }
    }

    public function down()
    {
    }
}
