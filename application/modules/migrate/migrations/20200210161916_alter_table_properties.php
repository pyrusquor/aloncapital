<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_properties extends CI_Migration
{

    public function up()
    {
       $this->db->query("ALTER TABLE `properties`
		ADD `total_lot_price` double(20,2) NOT NULL AFTER `lot_price_per_sqm`,
		ADD `progress` double(20,2) NOT NULL AFTER `status`,
		ADD `sales_arm_id` int(11) NOT NULL AFTER `progress`,
		ADD `tct_number` varchar(50) NOT NULL AFTER `sales_arm_id`,
		ADD `cts_number` varchar(50) NOT NULL AFTER `tct_number`,
		ADD `cts_amount` double(20,2) NOT NULL AFTER `cts_number`,
		ADD `doas_amount` double(20,2) NOT NULL AFTER `cts_amount`,
		ADD `date_registered` datetime NOT NULL AFTER `doas_amount`,
		ADD `cts_notarized_date` datetime NOT NULL AFTER `date_registered`,
		ADD `doas_notarized_date` datetime NOT NULL AFTER `cts_notarized_date`;");
    }

    public function down()
    {
    	$this->db->query("ALTER TABLE `properties`
		DROP `total_lot_price`,
		DROP `progress`,
		DROP `sales_arm_id`,
		DROP `tct_number`,
		DROP `cts_number`,
		DROP `cts_amount`,
		DROP `doas_amount`,
		DROP `date_registered`,
		DROP `cts_notarized_date`,
		DROP `doas_notarized_date`;");
    }
}


