<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_sub_warehouse_table extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'sub_warehouse_name' => array(
                'name' => 'name',
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false
            )
        );


        if ($this->db->table_exists('sub_warehouse')) {

            if (!$this->db->field_exists('name', 'sub_warehouse')) {
                $this->dbforge->modify_column('sub_warehouse', $fields);
            }

        }
    }

    public function down()
    {
        $fields = array(
            'name' => array(
                'name' => 'sub_warehouse_name',
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false
            )
        );

        if ($this->db->table_exists('sub_warehouse')) {

            if (!$this->db->field_exists('sub_warehouse_name', 'sub_warehouse')) {
                $this->dbforge->modify_column('sub_warehouse', $fields);
            }

        }

    }
}
