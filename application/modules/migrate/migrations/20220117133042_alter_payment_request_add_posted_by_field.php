<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_payment_request_add_posted_by_field extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('posted_by', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD `posted_by` int(11) NULL after `land_inventory_id`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('posted_by', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` DROP COLUMN `posted_by`");
        }
    }
}
