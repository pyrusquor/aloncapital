<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_projects extends CI_Migration
{

    public function up()
    {
        if ( $this->db->table_exists('projects') ) {

			$this->dbforge->drop_table('projects');
        }

        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'NULL' => FALSE
            ),
            'company_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => FALSE
            ),
            'image' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'NULL' => FALSE
            ),
            'type' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => FALSE
            ),
            'embedded_map' => array(
                'type' => 'TEXT',
                'NULL' => TRUE
            ),
            'max_price' => array(
                'type' => 'DECIMAL(20,2)',
                'NULL' => TRUE
            ),
            'min_price' => array(
                'type' => 'DECIMAL(20,2)',
                'NULL' => TRUE
            ),
            'location' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'NULL' => FALSE
            ),
            'project_start_date' => array(
                'type' => 'DATE',
                'NULL' => TRUE
            ),
            'dpc_palc_date' => array(
                'type' => 'DATE',
                'NULL' => TRUE
            ),
            'cr_lts_date' => array(
                'type' => 'DATE',
                'NULL' => TRUE
            ),
            'cr_lts_date' => array(
                'type' => 'DATE',
                'NULL' => TRUE
            ),
            'titling_date' => array(
                'type' => 'DATE',
                'NULL' => TRUE
            ),
            'project_area' => array(
                'type' => 'DECIMAL(22,2)',
                'NULL' => TRUE
            ),
            'saleable_area' => array(
                'type' => 'DECIMAL(22,2)',
                'NULL' => TRUE
            ),
            'no_saleable_unit' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE
            ),
            'coc_date' => array(
                'type' => 'DATE',
                'NULL' => TRUE
            ),
            'etd_date' => array(
                'type' => 'DATE',
                'NULL' => TRUE
            ),
            'lts_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
                'NULL' => TRUE
            ),
            'pd_units' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE
            ),
            'bp_units' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => '1',
                'NULL' => FALSE
            ),
            'is_active' => array(
                'type' => 'TINYINT',
                'constraint' => '1',
                'NULL' => TRUE
            ),
            'created_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'created_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'updated_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'updated_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'deleted_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'deleted_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('projects', TRUE);
    }

    public function down()
    {
        if ( $this->db->table_exists('projects') ) {

			$this->dbforge->drop_table("projects");
		}
    }
}
