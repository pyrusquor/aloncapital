<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_cheque_voucher_table extends CI_Migration
{

    private $tbl = "cheque_vouchers";

    private $fields = array(
        'id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'auto_increment' => TRUE,
            'NOT NULL' => TRUE,
        ),
        'payment_voucher_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'cheque_voucher_number' => array(
            'type' => 'VARCHAR',
            'constraint' => '50',
            'NULL' => TRUE,
        ),
        'cheque_number' => array(
            'type' => 'VARCHAR',
            'constraint' => '50',
            'NULL' => TRUE,
        ),
        'bank_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'remarks' => array(
            'type' => 'VARCHAR',
            'constraint' => '350',
            'NULL' => TRUE,
        ),
        'created_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'created_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'updated_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'updated_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'deleted_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'deleted_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
    );

    public function up()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->tbl, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
