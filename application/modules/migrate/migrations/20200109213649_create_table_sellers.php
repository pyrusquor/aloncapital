<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_sellers extends CI_Migration
{

    public function up()
    {
        if ( $this->db->table_exists('sellers') ) {

			$this->dbforge->drop_table('sellers');
        }

        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'NOT NULL' => FALSE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => FALSE
            ),
            'sales_group_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => FALSE
            ),
            'seller_position_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => FALSE
            ),
            'image' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => TRUE
            ),
            'last_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'NULL' => FALSE
            ),
            'first_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'NULL' => FALSE
            ),
            'middle_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'NULL' => TRUE
            ),
            'designation' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
                'NULL' => TRUE
            ),
            'birth_place' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'NULL' => TRUE
            ),
            'birth_date' => array(
                'type' => 'DATE',
                'NULL' => FALSE
            ),
            'gender' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'NULL' => TRUE
            ),
            'height' => array(
                'type' => 'DECIMAL',
                'constraint' => '11, 2',
                'NULL'  => TRUE
            ),
            'weight' => array(
                'type' => 'DECIMAL',
                'constraint' => '11, 2',
                'NULL' => TRUE

            ),
            'spouse_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'NULL' => TRUE
            ),
            'religion' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'NULL' => TRUE
            ),
            'tin' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
                'NULL' => TRUE
            ),
            'philhealth' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
                'NULL' => TRUE
            ),
            'hdmf' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
                'NULL' => TRUE
            ),
            'sss' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
                'NULL' => TRUE
            ),
            'citizenship' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'NULL' => TRUE
            ),
            'skill' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'NULL' => TRUE
            ),
            'hobby' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'NULL' => TRUE
            ),
            'language' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'NULL' => TRUE
            ),
            'is_active' => array(
                'type' => 'TINYINT',
                'constraint' => '1',
                'NULL' => FALSE
            ),
            'created_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'created_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'updated_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'updated_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'deleted_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'deleted_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('sellers', TRUE);
    }

    public function down()
    {
        if ( $this->db->table_exists('sellers') ) {

			$this->dbforge->drop_table("sellers");
		}
    }
}
