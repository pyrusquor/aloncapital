<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_entries_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('accounting_entries')) {
            if(!$this->db->field_exists('cancelled_at', 'accounting_entries')) {
                $this->db->query("ALTER TABLE `accounting_entries` ADD COLUMN `cancelled_at` DATETIME DEFAULT NULL AFTER `is_approve`");
            }
       }
    }

    public function down()
    {
        if($this->db->table_exists('accounting_entries')) {
            $this->dbforge->drop_column('accounting_entries', 'cancelled_at');
        }
    }
}
