<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_seller_settings_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('seller_settings'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'seller_rate' => array(
                    'type' => 'DECIMAL',
                    'constraint' => '11, 2',
                    'NULL' => TRUE
                ),
                'level' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '10',
                    'NULL' => TRUE
                ),
                'is_bypassable' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'NULL' => TRUE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=>TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=>TRUE,
                    'NULL'=>TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=>TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=>TRUE,
                    'NULL'=>TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=>TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=>TRUE,
                    'NULL'=>TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('seller_settings', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('seller_settings') ) {

			$this->dbforge->drop_table('seller_settings');
		}
    }
}
