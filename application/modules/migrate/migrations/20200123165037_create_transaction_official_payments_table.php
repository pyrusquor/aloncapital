<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_transaction_official_payments_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('transaction_official_payments'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'transaction_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'entry_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'ar_number' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => FALSE
                ),
                'or_number' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => FALSE
                ),
                'or_date' => array(
                    'type' => 'DATETIME',
                    'NULL' => FALSE
                ),
                'amount_paid' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'principal_amount' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'interest_amount' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'penalty_amount' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'payment_type_id' => array(
                    'type' => 'INT',
                    'constraint' => '50',
                    'NULL' => FALSE,
                    'comment' => '1=CASH,2=PDC,3=OTHERS'
                ),
                'post_dated_check_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE,
                ),
                'remarks' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => FALSE,
                ),
                'payment_date' => array(
                    'type' => 'DATETIME',
                    'NULL' => FALSE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('transaction_official_payments', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('transaction_official_payments') ) {

			$this->dbforge->drop_table('transaction_official_payments');
		}
    }
}
