<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_construction_template_items_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('construction_template_items')){

            if($this->db->field_exists('item_code', 'construction_template_items')){
                $this->dbforge->drop_column('construction_template_items', 'item_code');
            }

            if($this->db->field_exists('item', 'construction_template_items')){
                $this->dbforge->drop_column('construction_template_items', 'item');
            }

            if($this->db->field_exists('unit_of_measurement', 'construction_template_items')){
                $this->dbforge->drop_column('construction_template_items', 'unit_of_measurement');
            }

            if(!$this->db->field_exists('unit_of_measurement_id', 'construction_template_items')){
                $this->db->query("ALTER TABLE `construction_template_items` ADD COLUMN `unit_of_measurement_id` INT(11) UNSIGNED NULL AFTER item_id");
            }

            if($this->db->field_exists('unit_price', 'construction_template_items')){
                $this->dbforge->drop_column('construction_template_items', 'unit_price');
            }

            if($this->db->field_exists('subtotal', 'construction_template_items')){
                $this->dbforge->drop_column('construction_template_items', 'subtotal');
            }

            if($this->db->field_exists('grand_total', 'construction_template_items')){
                $this->dbforge->drop_column('construction_template_items', 'grand_total');
            }

            if(!$this->db->field_exists('unit_cost', 'construction_template_items')){
                $this->db->query("ALTER TABLE `construction_template_items` ADD COLUMN `unit_cost` DOUBLE(20, 2) NULL AFTER quantity");
            }

            if(!$this->db->field_exists('total_cost', 'construction_template_items')){
                $this->db->query("ALTER TABLE `construction_template_items` ADD COLUMN `total_cost` DOUBLE(20, 2) NULL AFTER unit_cost");
            }
            // 
            if(!$this->db->field_exists('item_group_id', 'construction_template_items')){
                $this->db->query("ALTER TABLE `construction_template_items` ADD COLUMN `item_group_id` INT(11) UNSIGNED NULL AFTER total_cost");
            }

            if(!$this->db->field_exists('item_type_id', 'construction_template_items')){
                $this->db->query("ALTER TABLE `construction_template_items` ADD COLUMN `item_type_id` INT(11) UNSIGNED NULL AFTER item_group_id");
            }

            if(!$this->db->field_exists('item_brand_id', 'construction_template_items')){
                $this->db->query("ALTER TABLE `construction_template_items` ADD COLUMN `item_brand_id` INT(11) UNSIGNED NULL AFTER item_type_id");
            }

            if(!$this->db->field_exists('item_class_id', 'construction_template_items')){
                $this->db->query("ALTER TABLE `construction_template_items` ADD COLUMN `item_class_id` INT(11) UNSIGNED NULL AFTER item_brand_id");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('construction_template_items')) {

            if(!$this->db->field_exists('item_code', 'construction_template_items')){
                $this->db->query("ALTER TABLE `construction_template_items` ADD COLUMN `item_code` VARCHAR(50) NULL AFTER item_id");
            }

            if(!$this->db->field_exists('item', 'construction_template_items')){
                $this->db->query("ALTER TABLE `construction_template_items` ADD COLUMN `item` VARCHAR(255) NULL AFTER item_code");
            }

            if(!$this->db->field_exists('unit_of_measurement', 'construction_template_items')){
                $this->db->query("ALTER TABLE `construction_template_items` ADD COLUMN `unit_of_measurement` VARCHAR(255) NULL AFTER item");
            }

            if($this->db->field_exists('unit_of_measurement_id', 'construction_template_items')){
                $this->dbforge->drop_column('construction_template_items', 'unit_of_measurement_id');
            }

            if(!$this->db->field_exists('unit_price', 'construction_template_items')){
                $this->db->query("ALTER TABLE `construction_template_items` ADD COLUMN `unit_price` DOUBLE(20, 2) NULL AFTER quantity");
            }

            if(!$this->db->field_exists('subtotal', 'construction_template_items')){
                $this->db->query("ALTER TABLE `construction_template_items` ADD COLUMN `subtotal` DOUBLE(20, 2) NULL AFTER unit_price");
            }

            if(!$this->db->field_exists('grand_total', 'construction_template_items')){
                $this->db->query("ALTER TABLE `construction_template_items` ADD COLUMN `grand_total` DOUBLE(20, 2) NULL AFTER subtotal");
            }

            if($this->db->field_exists('unit_cost', 'construction_template_items')){
                $this->dbforge->drop_column('construction_template_items', 'unit_cost');
            }

            if($this->db->field_exists('total_cost', 'construction_template_items')){
                $this->dbforge->drop_column('construction_template_items', 'total_cost');
            }

            if($this->db->field_exists('item_group_id', 'construction_template_items')){
                $this->dbforge->drop_column('construction_template_items', 'item_group_id');
            }

            if($this->db->field_exists('item_type_id', 'construction_template_items')){
                $this->dbforge->drop_column('construction_template_items', 'item_type_id');
            }

            if($this->db->field_exists('item_brand_id', 'construction_template_items')){
                $this->dbforge->drop_column('construction_template_items', 'item_brand_id');
            }

            if($this->db->field_exists('item_class_id', 'construction_template_items')){
                $this->dbforge->drop_column('construction_template_items', 'item_class_id');
            }
        }
    }
}
