<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_billing_logs extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transaction_billing_logs')) {

            if ($this->db->field_exists('interest_rate', 'transaction_billing_logs')) {
                $this->db->query("ALTER TABLE `transaction_billing_logs` MODIFY COLUMN `interest_rate` double(20,6) NOT NULL");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transaction_billing_logs')) {

            if ($this->db->field_exists('interest_rate', 'transaction_billing_logs')) {
                $this->db->query("ALTER TABLE `transaction_billing_logs` MODIFY COLUMN `interest_rate` double(20,2) NOT NULL");
            }
        }
    }
}
