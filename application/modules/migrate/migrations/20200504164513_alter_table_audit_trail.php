<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_audit_trail extends CI_Migration
{

    public function up()
    {

        if (!$this->db->field_exists('extra_tag', 'audit_trail')) {
            $this->db->query("ALTER TABLE `audit_trail` ADD COLUMN `extra_tag` varchar(255) NULL AFTER `notification_status`");
        }
        if (!$this->db->field_exists('icon', 'audit_trail')) {
            $this->db->query("ALTER TABLE `audit_trail` ADD COLUMN `icon` varchar(1024) NULL AFTER `extra_tag`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('extra_tag', 'audit_trail')) {
            $this->dbforge->drop_column('audit_trail', 'extra_tag');
        }

        if ($this->db->field_exists('icon', 'audit_trail')) {
            $this->dbforge->drop_column('audit_trail', 'icon');
        }
    }
}
