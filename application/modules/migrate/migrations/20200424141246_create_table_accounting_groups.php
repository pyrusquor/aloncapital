<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_accounting_groups extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => true,
                'NOT NULL' => false,
            ),
            'parent_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => false,
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'NULL' => false,
            ),
            'slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'affects_gross' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
            ),
            'account_title' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'company_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => false,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('accounting_groups', true);

        $this->load->model('accounting_groups/Accounting_groups_model', 'accounting_groups');
        // $this->accounting_groups->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('accounting_groups')) {

            $this->dbforge->drop_table('accounting_groups');
        }
    }
}
