<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_prospect_add_column extends CI_Migration
{

    public function up()
    {

        if ($this->db->table_exists('prospect')) {

            if (!$this->db->field_exists('other_mobile', 'prospect')) {

                $this->db->query("ALTER TABLE `prospect` ADD COLUMN `other_mobile` VARCHAR(11) DEFAULT NULL  AFTER `mobile_no`");
            }

            if (!$this->db->field_exists('civil_status_id', 'prospect')) {

                $this->db->query("ALTER TABLE `prospect` ADD COLUMN `civil_status_id` VARCHAR(11) DEFAULT NULL  AFTER `nationality`");
            }

        }
    }

    public function down()
    {

        if ($this->db->field_exists('other_mobile', 'prospect')) {

            $this->dbforge->drop_column('prospect', 'other_mobile');
        }

        if ($this->db->field_exists('civil_status_id', 'prospect')) {

            $this->dbforge->drop_column('prospect', 'civil_status_id');
        }

    }
}
