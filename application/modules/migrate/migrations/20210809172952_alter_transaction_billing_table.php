<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_billing_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transaction_billings')) {

            if (!$this->db->field_exists('consolidate_and_subdivide_id', 'transaction_billings')) {
                $this->db->query("ALTER TABLE `transaction_billings` ADD COLUMN `excess` double(20,2) DEFAULT 0.00 AFTER `period_term`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transaction_billings')) {

            if ($this->db->field_exists('consolidate_and_subdivide_id', 'transaction_billings')) {
                $this->dbforge->drop_column('transaction_billings', 'excess');
            }
        }
    }
}
