<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_staff_table extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('image', 'staff')) {
            $this->db->query("ALTER TABLE `staff` ADD COLUMN `image` varchar(255) NULL AFTER `position`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('image', 'staff')) {
            $this->dbforge->drop_column('staff', 'image');
        }
    }
}
