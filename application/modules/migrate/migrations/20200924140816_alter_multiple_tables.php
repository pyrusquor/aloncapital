<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_tables extends CI_Migration
{

    public function up()
    {
        $complaint_fields = array(
            'complaint_category_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => FALSE
            ),
            'complaint_sub_category_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => FALSE
            ),
        );

       if($this->db->table_exists('complaint_category_id')) {
           if($this->db->field_exists('period_id', 'complaint_tickets')) {
                $this->dbforge->modify_column('complaint_tickets', $complaint_fields);
           }
       }

        $inquiry_fields = array(
            'inquiry_category_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => FALSE
            ),
            'inquiry_sub_category_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => FALSE
            ),
        );

       if($this->db->table_exists('inquiry_tickets')) {
           if($this->db->field_exists('inquiry_category_id', 'inquiry_tickets')) {
                $this->dbforge->modify_column('inquiry_tickets', $inquiry_fields);
           }
       }

    }

    public function down()
    {
        $complaint_fields = array(
            'complaint_category_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE
            ),
            'complaint_sub_category_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE
            ),
        );

       if($this->db->table_exists('complaint_tickets')) {
           if($this->db->field_exists('complaint_category_id', 'complaint_tickets')) {
                $this->dbforge->modify_column('complaint_tickets', $complaint_fields);
           }
       }
       
       $inquiry_fields = array(
            'inquiry_category_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE
            ),
            'inquiry_sub_category_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE
            ),
        );

       if($this->db->table_exists('inquiry_tickets')) {
           if($this->db->field_exists('inquiry_category_id', 'inquiry_tickets')) {
                $this->dbforge->modify_column('inquiry_tickets', $inquiry_fields);
           }
       }
    }
}
