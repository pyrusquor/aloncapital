<?php defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_project_documents extends CI_Migration {

	function up () {

		if ( $this->db->table_exists('project_documents') ) {

			$this->dbforge->drop_table('project_documents');
		}

		$_fields	=	array(
									'id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => TRUE,
                    'comment'	=>	'project_documents Table ID'
									),
									'project_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'project_documents project_id | projects ID'
									),
									'document_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'project_documents document_id | documents ID'
									),
									'uploaded_document_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'project_documents uploaded_document_id | uploaded_documents ID'
									),
									'category_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'project_documents category_id | categories ID'
									),
									'start_date'	=>	array(
										'type'	=>	'DATETIME',
	                	'NULL'	=>	TRUE,
                    'comment'	=>	'project_documents start_date'
									),
									'owner_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'project_documents owner_id | owners ID'
									),
									'group_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'project_documents group_id | groups ID'
									),
									'dependency_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'project_documents dependency_id | dedpendencies ID'
									),
									'is_active'	=>	array(
										'type'	=>	'TINYINT',
										'constraint' => '1',
                    'NOT NULL' => TRUE,
                    'default'	=>	1,
                    'comment'	=>	'project_documents is_active 1:TRUE|FALSE:0'
									),
	                'created_by'	=>	array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'project_documents created_by'
	                ),
	                'created_at' => array(
	                	'type'	=>	'DATETIME',
	                	'NULL'	=>	TRUE,
                    'comment'	=>	'project_documents created_at'
	                ),
	                'updated_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'project_documents updated_by'
	                ),
	                'updated_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'project_documents updated_at'
	                ),
	                'deleted_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'project_documents deleted_by'
	                ),
	                'deleted_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'project_documents deleted_at'
	                )
								);

		$this->dbforge->add_field($_fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('project_documents', TRUE);
	}

	function down () {

		if ( $this->db->table_exists('project_documents') ) {

			$this->dbforge->drop_table("project_documents");
		}
	}
}