<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_sellers_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('sellers')) {
            if(!$this->db->field_exists('acct_number', 'sellers')) {

                $this->db->query("ALTER TABLE `sellers` ADD COLUMN `acct_number` varchar(64) COLLATE 'utf8_general_ci' DEFAULT NULL AFTER `sitio`");

            }
            if(!$this->db->field_exists('SID', 'sellers')) {

                $this->db->query("ALTER TABLE `sellers` ADD COLUMN `SID` INT(11) DEFAULT 0 NOT NULL AFTER `acct_number`");
            }

            if(!$this->db->field_exists('is_exported', 'sellers')) {
                $this->db->query("ALTER TABLE `sellers` ADD COLUMN `is_exported` INT(11) DEFAULT 0 NULL AFTER `SID`");
            }

            if(!$this->db->field_exists('other_mobile', 'sellers')) {
                $this->db->query("ALTER TABLE `sellers` ADD COLUMN `other_mobile` varchar(15) COLLATE 'utf8_general_ci' NOT NULL AFTER `mobile_no`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('sellers')) {
            $this->dbforge->drop_column('sellers', 'acct_number');
            $this->dbforge->drop_column('sellers', 'SID');
            $this->dbforge->drop_column('sellers', 'is_exported');
            $this->dbforge->drop_column('sellers', 'other_mobile');
        }
    }
}
