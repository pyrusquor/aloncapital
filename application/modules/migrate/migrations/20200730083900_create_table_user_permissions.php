<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_user_permissions extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('permissions')) {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NULL' => false,
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '80',
                    'NULL' => false,
                ),
                'is_active' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => false,
                    'DEFAULT' => '1',
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('permissions', true);
        }

        if (!$this->db->table_exists('user_permissions')) {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NULL' => false,
                ),
                'user_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'permission_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('user_permissions', true);
        }

        if (!$this->db->table_exists('module_permissions')) {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NULL' => false,
                ),
                'permission_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'module_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'is_create' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => false,
                    'DEFAULT' => '0',
                ),
                'is_read' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => false,
                    'DEFAULT' => '0',
                ),
                'is_update' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => false,
                    'DEFAULT' => '0',
                ),
                'is_delete' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => false,
                    'DEFAULT' => '0',
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('module_permissions', true);
        }
    }

    public function down()
    {
        if($this->db->table_exists('permissions')) {
            $this->dbforge->drop_table('permissions');
        }

        if($this->db->table_exists('user_permissions')) {
            $this->dbforge->drop_table('user_permissions');
        }

        if($this->db->table_exists('module_permissions')) {
            $this->dbforge->drop_table('module_permissions');
        }
    }
}
