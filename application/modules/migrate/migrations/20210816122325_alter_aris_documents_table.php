<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_aris_documents_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('aris_documents')) {

            if ($this->db->field_exists('PERCENTDPP', 'aris_documents')) {

                $this->db->query("ALTER TABLE `aris_documents` MODIFY COLUMN `PERCENTDPP` DECIMAL(10,10) UNSIGNED default 0.00 NOT NULL");
            }
        }
        if ($this->db->table_exists('aris_documents')) {

            if ($this->db->field_exists('PERCENTDP', 'aris_documents')) {

                $this->db->query("ALTER TABLE `aris_documents` MODIFY COLUMN `PERCENTDP` DECIMAL(10,10) UNSIGNED default 0.00 NULL");
            }
        }
        if ($this->db->table_exists('aris_documents')) {

            if ($this->db->field_exists('INTEREST_P', 'aris_documents')) {

                $this->db->query("ALTER TABLE `aris_documents` MODIFY COLUMN `INTEREST_P` DECIMAL(10,10) UNSIGNED default 0.00 NOT NULL");
            }
        }
        if ($this->db->table_exists('aris_documents')) {

            if ($this->db->field_exists('INTEREST', 'aris_documents')) {

                $this->db->query("ALTER TABLE `aris_documents` MODIFY COLUMN `INTEREST` DECIMAL(10,10) UNSIGNED default 0.00 NULL");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('aris_documents')) {

            if ($this->db->field_exists('PERCENTDPP', 'aris_documents')) {

                $this->db->query("ALTER TABLE `aris_documents` MODIFY COLUMN `PERCENTDPP` FLOAT UNSIGNED default 0.00 NOT NULL");
            }
        }
        if ($this->db->table_exists('aris_documents')) {

            if ($this->db->field_exists('PERCENTDP', 'aris_documents')) {

                $this->db->query("ALTER TABLE `aris_documents` MODIFY COLUMN `PERCENTDP` FLOAT UNSIGNED default 0.00 NOT NULL");
            }
        }
        if ($this->db->table_exists('aris_documents')) {

            if ($this->db->field_exists('INTEREST_P', 'aris_documents')) {

                $this->db->query("ALTER TABLE `aris_documents` MODIFY COLUMN `INTEREST_P` FLOAT UNSIGNED default 0.00 NOT NULL");
            }
        }
        if ($this->db->table_exists('aris_documents')) {

            if ($this->db->field_exists('INTEREST', 'aris_documents')) {

                $this->db->query("ALTER TABLE `aris_documents` MODIFY COLUMN `INTEREST` FLOAT UNSIGNED default 0.00 NOT NULL");
            }
        }
    }
}
