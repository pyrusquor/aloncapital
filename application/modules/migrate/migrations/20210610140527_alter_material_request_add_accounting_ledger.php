<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_material_request_add_accounting_ledger extends CI_Migration
{

    private $tbl = 'material_requests';
    private $fields = array(
        'accounting_ledger_id' => array(
            "create" => "ALTER TABLE `material_requests` ADD COLUMN `accounting_ledger_id` INT UNSIGNED NULL DEFAULT NULL",
            "delete" => "ALTER TABLE `material_requests` DROP COLUMN `accounting_ledger_id`"
        ),
    );

    public function up()
    {
        if ($this->db->table_exists($this->tbl)) {
            foreach ($this->fields as $field => $queries) {
                if (!$this->db->field_exists($field, $this->tbl)) {
                    $this->db->query($queries['create']);
                }
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            foreach ($this->fields as $field => $queries) {
                if ($this->db->field_exists($field, $this->tbl)) {
                    $this->db->query($queries['delete']);
                }
            }
        }
    }
}
