<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_taxes extends CI_Migration
{

    public function up()
    {
        if (!$this->db->table_exists('taxes')) {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NOT NULL' => false,
                ),
                'tax_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '250',
                    'NULL' => false,
                ),
                'tax_description' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => true,
                ),
                'tax_code' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => true,
                ),
                'tax_rate' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => false,
                ),
                'ledger_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('taxes', true);

            // $this->load->model('taxes/Contractors_model', 'taxes');

            // $this->taxes->insert_dummy();
        }
    }

    public function down()
    {
        if ($this->db->table_exists('taxes')) {

            $this->dbforge->drop_table('taxes');
        }
    }
}
