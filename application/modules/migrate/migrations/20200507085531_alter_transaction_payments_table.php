<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_payments_table extends CI_Migration
{

    public function up()
    { 
    	if(!$this->db->table_exists('standard_reports'))
        {
	    	$fields = array(
	            'id' => array(
	                'type' => 'INT',
	                'constraint' => '11',
	                'auto_increment' => true,
	                'NOT NULL' => false,
	            ),
	            'name' => array(
	                'type' => 'text',
	                'NULL' => true,
	            ),
	            'description' => array(
	                'type' => 'text',
	                'NULL' => true,
	            ),
	            'filters' => array(
	                'type' => 'text',
	                'NULL' => true,
	            ),
	            'signatory' => array(
	                'type' => 'text',
	                'NULL' => true,
	            ),
	            'slug' => array(
	                'type' => 'text',
	                'NULL' => true,
	            ),
	            'created_at' => array(
	                'type' => 'text',
	                'NULL' => true,
	            ),
	            'created_by' => array(
	                'type' => 'DATETIME',
	                'NULL' => true,
	            ),
	            'updated_at' => array(
	                'type' => 'DATETIME',
	                'NULL' => true,
	            ),
	            'updated_by' => array(
	                'type' => 'INT',
	                'unsigned' => true,
	                'NULL' => true,
	            ),
	            'deleted_at' => array(
	                'type' => 'DATETIME',
	                'NULL' => true,
	            ),
	            'deleted_by' => array(
	                'type' => 'INT',
	                'unsigned' => true,
	                'NULL' => true,
	            ),
	        );

	        $this->dbforge->add_field($fields);
	        $this->dbforge->add_key('id', true);
	        $this->dbforge->create_table('standard_reports', true);

	        // $this->load->model('standard_reports/Standard_report_model', 'M_s_report');
	        // $this->M_s_report->insert_default();
    	}

    	if(!$this->db->table_exists('standard_report_logs'))
        {
	    	$fields = array(
	            'id' => array(
	                'type' => 'INT',
	                'constraint' => '11',
	                'auto_increment' => true,
	                'NOT NULL' => false,
	            ),
	            'standard_report_id' => array(
	               'type' => 'INT',
	               'constraint' => '11',
	            ),
	            'filters' => array(
	                'type' => 'text',
	                'NULL' => true,
	            ),
	            'signatory' => array(
	                'type' => 'text',
	                'NULL' => true,
	            ),
	            'slug' => array(
	                'type' => 'text',
	                'NULL' => true,
	            ),
	            'created_at' => array(
	                'type' => 'text',
	                'NULL' => true,
	            ),
	            'created_by' => array(
	                'type' => 'DATETIME',
	                'NULL' => true,
	            ),
	            'updated_at' => array(
	                'type' => 'DATETIME',
	                'NULL' => true,
	            ),
	            'updated_by' => array(
	                'type' => 'INT',
	                'unsigned' => true,
	                'NULL' => true,
	            ),
	            'deleted_at' => array(
	                'type' => 'DATETIME',
	                'NULL' => true,
	            ),
	            'deleted_by' => array(
	                'type' => 'INT',
	                'unsigned' => true,
	                'NULL' => true,
	            ),
	        );

	        $this->dbforge->add_field($fields);
	        $this->dbforge->add_key('id', true);
	        $this->dbforge->create_table('standard_report_logs', true);
    	}
    }

    public function down()
    {
    	if ($this->db->table_exists('standard_reports')) {

            $this->dbforge->drop_table('standard_reports');
        }

        if ($this->db->table_exists('standard_report_logs')) {

            $this->dbforge->drop_table('standard_report_logs');
        }
    }
}
