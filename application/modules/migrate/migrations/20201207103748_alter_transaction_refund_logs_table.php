<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_refund_logs_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transaction_refund_logs')) {
            if(!$this->db->field_exists('refund_percentage', 'transaction_refund_logs')) {
                 $this->db->query("ALTER TABLE `transaction_refund_logs` ADD COLUMN `refund_percentage` VARCHAR(124) DEFAULT 0 AFTER `refund_amount`");
            }

            if(!$this->db->field_exists('processing_fee', 'transaction_refund_logs')) {
                 $this->db->query("ALTER TABLE `transaction_refund_logs` ADD COLUMN `processing_fee` DOUBLE(20, 2) DEFAULT 0 AFTER `refund_percentage`");
            }
            
            if(!$this->db->field_exists('misc_fee', 'transaction_refund_logs')) {
                 $this->db->query("ALTER TABLE `transaction_refund_logs` ADD COLUMN `misc_fee` DOUBLE(20, 2) DEFAULT 0 AFTER `processing_fee`");
            }

            if(!$this->db->field_exists('penalty', 'transaction_refund_logs')) {
                 $this->db->query("ALTER TABLE `transaction_refund_logs` ADD COLUMN `penalty` DOUBLE(20, 2) DEFAULT 0 AFTER `misc_fee`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transaction_refund_logs')) {
            $this->dbforge->drop_column('transaction_refund_logs', 'refund_percentage');
            $this->dbforge->drop_column('transaction_refund_logs', 'processing_fee');
            $this->dbforge->drop_column('transaction_refund_logs', 'misc_fee');
            $this->dbforge->drop_column('transaction_refund_logs', 'penalty');
        }
    }
}
