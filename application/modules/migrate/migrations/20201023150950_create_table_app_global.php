<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_app_global extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('app_global'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '254',
                    'NULL' => TRUE,
                ),
                'slug' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '254',
                    'NULL' => TRUE,
                ),
                'value' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '254',
                    'NULL' => TRUE,
                ),
                'value_type' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '24',
                    'NULL' => TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
			
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('app_global', TRUE);

            $sql = "INSERT INTO `app_global` (`name`, `slug`, `value`, `value_type`) VALUES
                ('SMS Api',	'sms-api',	'db1f73e7cea81c824500c682a9971e89',	1),
                ('SendGrid Api', 'sendgrid-api', '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', 1)";
            $this->db->query($sql);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('app_global') ) {

			$this->dbforge->drop_table('app_global');
        }
    }
}
