<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Atler_transaction_official_payments_table_add_rc_bank_name_and_rc_check_number extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transaction_official_payments')) {

            if (!$this->db->field_exists('rc_bank_name ', 'transaction_official_payments')) {
                $this->db->query("ALTER TABLE `transaction_official_payments` ADD COLUMN `rc_bank_name` VARCHAR(255) NULL AFTER period_count");
            }

            if (!$this->db->field_exists('rc_check_number  ', 'transaction_official_payments')) {
                $this->db->query("ALTER TABLE `transaction_official_payments` ADD COLUMN `rc_check_number` VARCHAR(50) NULL AFTER period_count");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transaction_official_payments')) {

            if ($this->db->field_exists('rc_bank_name', 'transaction_official_payments')) {
                $this->dbforge->drop_column('transaction_official_payments', 'rc_bank_name');
            }

            if ($this->db->field_exists('rc_check_number', 'transaction_official_payments')) {
                $this->dbforge->drop_column('transaction_official_payments', 'rc_check_number');
            }
        }
    }
}
