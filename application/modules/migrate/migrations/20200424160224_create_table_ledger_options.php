<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_ledger_options extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => true,
                'NOT NULL' => false,
            ),
            'company_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true,
            ),
            'ledger_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true,
            ),
            'op_balance' => array(
                'type' => 'decimal',
                'constraint' => '15,2',
                'NULL' => false,
                'DEFAULT' => "0.00",
            ),
            'op_balance_dc' => array(
                'type' => 'char',
                'constraint' => '1',
            ),
            'reconciliation' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
                'DEFAULT' => "0",
            ),
            'non_operating' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
                'DEFAULT' => "0",
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('ledger_options', true);

        // $this->load->model('accounting_groups/Accounting_groups_model', 'accounting_groups');
        // $this->accounting_groups->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('ledger_options')) {

            $this->dbforge->drop_table('ledger_options');
        }
    }
}
