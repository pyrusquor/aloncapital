<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Update_material_request_additional_relations extends CI_Migration
    {
        private $tbl = "material_requests";
        private $new_fields = array(
            'sub_project_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE,
            ),
            'amenity_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE,
            ),
            'sub_amenity_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE,
            ),
            'property_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE,
            ),
            'department_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE,
            ),
            'equipment_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE,
            ),
        );

        public function up()
        {
            $this->dbforge->add_column($this->tbl, $this->new_fields);
        }

        public function down()
        {
            foreach($this->new_fields as $field => $data){
                $this->dbforge->drop_column($this->tbl, $field);
            }
        }
    }
