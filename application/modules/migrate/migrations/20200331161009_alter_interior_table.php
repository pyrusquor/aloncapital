<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_interior_table extends CI_Migration
{

    public function up()
    {

	    if ( ! $this->db->field_exists('improved_price', 'house_model_interiors') ) {
	      	$this->db->query("ALTER TABLE `house_model_interiors` ADD COLUMN `improved_price` decimal(20,2) NULL AFTER `price`");
	    }

	    if ( ! $this->db->field_exists('fully_furnished_price', 'house_model_interiors') ) {
	      	$this->db->query("ALTER TABLE `house_model_interiors` ADD COLUMN `fully_furnished_price` decimal(20,2) NULL AFTER `improved_price`");
	    }

	    if ( ! $this->db->field_exists('improved_price', 'house_model_interior_price_logs') ) {
	      	$this->db->query("ALTER TABLE `house_model_interior_price_logs` ADD COLUMN `improved_price` decimal(20,2) NULL AFTER `price`");
	    }
	    
	    if ( ! $this->db->field_exists('fully_furnished_price', 'house_model_interior_price_logs') ) {
	      	$this->db->query("ALTER TABLE `house_model_interior_price_logs` ADD COLUMN `fully_furnished_price` decimal(20,2) NULL AFTER `improved_price`");
	    }

	    if ( ! $this->db->field_exists('zonal_value', 'projects') ) {
	      	$this->db->query("ALTER TABLE `projects` ADD COLUMN `zonal_value` text NULL AFTER `bp_units`");
	    }
	    
	    if ( ! $this->db->field_exists('fair_market_value', 'projects') ) {
	      	$this->db->query("ALTER TABLE `projects` ADD COLUMN `fair_market_value` text NULL AFTER `zonal_value`");
	    }

	    if ( ! $this->db->field_exists('max_selling_price', 'properties') ) {
	      	$this->db->query("ALTER TABLE `properties` ADD COLUMN `max_selling_price` text NULL AFTER `tax_declaration_building`");
	    }
	    
	    if ( ! $this->db->field_exists('tct_max_price_annotation', 'properties') ) {
	      	$this->db->query("ALTER TABLE `properties` ADD COLUMN `tct_max_price_annotation` text NULL AFTER `max_selling_price`");
	    }

	    if ( ! $this->db->field_exists('package_type_id', 'properties') ) {
	      	$this->db->query("ALTER TABLE `properties` ADD COLUMN `package_type_id` int(11) AFTER `tct_max_price_annotation`");
	    }

	    if ( ! $this->db->field_exists('commission_rate', 'sellers') ) {
	      	$this->db->query("ALTER TABLE `sellers` ADD COLUMN `commission_rate` double(20,2) NULL AFTER `upline_id`");
	    }

	    if ( ! $this->db->field_exists('default', 'fees') ) {
	      	$this->db->query("ALTER TABLE `fees` ADD COLUMN `default` int(11) AFTER `fees_type`");
	    }
    }

    public function down()
    {
    	if ( $this->db->field_exists('improved_price', 'house_model_interiors') ) {
			$this->dbforge->drop_column('house_model_interiors', 'improved_price');
		}

		if ( $this->db->field_exists('fully_furnished_price', 'house_model_interiors') ) {
			$this->dbforge->drop_column('house_model_interiors', 'fully_furnished_price');
		}

		if ( $this->db->field_exists('improved_price', 'house_model_interior_price_logs') ) {
			$this->dbforge->drop_column('house_model_interior_price_logs', 'improved_price');
		}

		if ( $this->db->field_exists('fully_furnished_price', 'house_model_interior_price_logs') ) {
			$this->dbforge->drop_column('house_model_interior_price_logs', 'fully_furnished_price');
		}

		if ( $this->db->field_exists('zonal_value', 'projects') ) {
			$this->dbforge->drop_column('projects', 'zonal_value');
		}

		if ( $this->db->field_exists('fair_market_value', 'projects') ) {
			$this->dbforge->drop_column('projects', 'fair_market_value');
		}

		if ( $this->db->field_exists('package_type_id', 'properties') ) {
			$this->dbforge->drop_column('properties', 'package_type_id');
		}

		if ( $this->db->field_exists('max_selling_price', 'properties') ) {
			$this->dbforge->drop_column('properties', 'max_selling_price');
		}

		if ( $this->db->field_exists('tct_max_price_annotation', 'properties') ) {
			$this->dbforge->drop_column('properties', 'tct_max_price_annotation');
		}

		if ( $this->db->field_exists('commission_rate', 'sellers') ) {
			$this->dbforge->drop_column('sellers', 'commission_rate');
		}

		if ( $this->db->field_exists('default', 'fees') ) {
			$this->dbforge->drop_column('fees', 'default');
		}
    }
}

