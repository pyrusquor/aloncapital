<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_tables extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('buyers')) {

            if (!$this->db->field_exists('sitio', 'buyers')) {

                $this->db->query("ALTER TABLE `buyers` ADD COLUMN `sitio` varchar(64) DEFAULT NULL  AFTER `brgyCode`");
            }
        }

        if ($this->db->table_exists('sellers')) {

            if (!$this->db->field_exists('sitio', 'sellers')) {

                $this->db->query("ALTER TABLE `sellers` ADD COLUMN `sitio` varchar(64) DEFAULT NULL  AFTER `brgyCode`");
            }
        }
    }

    public function down()
    {
        if ($this->db->field_exists('sitio', 'buyers')) {

            $this->dbforge->drop_column('buyers', 'sitio');
        }

        if ($this->db->field_exists('sitio', 'sellers')) {

            $this->dbforge->drop_column('sellers', 'sitio');
        }
    }
}
