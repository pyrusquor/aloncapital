<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_leasing_projects_table extends CI_Migration
{

    private $table = "leasing_projects";
    private $fields = array(

        "id" => array(
            "type" => "INT",
            "constraint" => "11",
            "unsigned" => TRUE,
            "auto_increment" => TRUE,
            "NOT NULL" => TRUE,
        ),

        'project_name' => array(
            "type" => "VARCHAR",
            'constraint' => '255',
            "NULL" => TRUE,
        ),

        'project_type_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'company_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'location' => array(
            "type" => "VARCHAR",
            'constraint' => '255',
            "NULL" => TRUE,
        ),

        'real_property_tax' => array(
            "type" => "FLOAT",
            "default" => 0
        ),

        'price_minimum' => array(
            "type" => "FLOAT",
            "default" => 0
        ),

        'price_maximum' => array(
            "type" => "FLOAT",
            "default" => 0
        ),

        'project_status_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'total_lot_area' => array(
            "type" => "FLOAT",
            "default" => 0
        ),

        'total_leasable_area' => array(
            "type" => "FLOAT",
            "default" => 0
        ),

        'leasable_units' => array(
            "type" => "INT",
            "default" => 0
        ),

        'total_gross_area' => array(
            "type" => "FLOAT",
            "default" => 0
        ),

        'number_of_floors' => array(
            "type" => "INT",
            "default" => 0
        ),

        'leasable_units_per_floor' => array(
            "type" => "INT",
            "default" => 0
        ),

        'leasable_office' => array(
            "type" => "INT",
            "default" => 0
        ),

        'total_beds' => array(
            "type" => "INT",
            "default" => 0
        ),

        'beds_per_floor' => array(
            "type" => "INT",
            "default" => 0
        ),

        'embedded_map' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),

        // File info
        'file_name' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),
        'full_path' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),

        // Audit Info
        "created_by" => array(
            "type" => "INT",
            "NOT NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "created_at" => array(
            "type" => "DATETIME",
            "NOT NULL" => TRUE,
        ),

        "updated_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "updated_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        "deleted_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "deleted_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

    );

    public function up()
    {
        if (!$this->db->table_exists($this->table)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->table, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->table)) {
            $this->dbforge->drop_table($this->table);
        }
    }
}
