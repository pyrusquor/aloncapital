<?php defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_documents_rename_columns_security_classification_and_issuing_party extends CI_Migration {

	function up () {

		if( $this->db->table_exists('documents') ) {

      if ( $this->db->field_exists('security_classification', 'documents') ) {

      	$this->db->query("ALTER TABLE documents CHANGE security_classification security_classification_id INT UNSIGNED DEFAULT NULL COMMENT 'documents issuing_party_id'");
      }

      if ( $this->db->field_exists('issuing_party', 'documents') ) {

      	$this->db->query("ALTER TABLE documents CHANGE issuing_party issuing_party_id INT UNSIGNED DEFAULT NULL COMMENT 'documents issuing_party_id'");
      }
    }
	}

	function down () {

		if ( $this->db->field_exists('security_classification_id', 'documents') ) {

			$this->db->query("ALTER TABLE documents CHANGE security_classification_id security_classification TEXT DEFAULT NULL COMMENT 'documents security_classification'");
		}

		if ( $this->db->field_exists('issuing_party_id', 'documents') ) {

			$this->db->query("ALTER TABLE documents CHANGE issuing_party_id issuing_party TEXT DEFAULT NULL COMMENT 'documents issuing_party'");
		}
	}
}