<?php defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_document_checklists extends CI_Migration {

	function up () {

		if ( $this->db->table_exists('document_checklists') ) {

			$this->dbforge->drop_table('document_checklists');
		}

		$_fields	=	array(
									'id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => TRUE,
                    'comment'	=>	'document_checklists Table ID'
									),
									'checklist_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NOT NULL' => TRUE,
                    'comment'	=>	'document_checklists checklist_id | checklists ID'
									),
									'document_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NOT NULL' => TRUE,
                    'comment'	=>	'document_checklists document_id | documents ID'
									),
									'is_active'	=>	array(
										'type'	=>	'TINYINT',
										'constraint' => '1',
                    'NOT NULL' => TRUE,
                    'default'	=>	1,
                    'comment'	=>	'document_checklists is_active 1:TRUE|FALSE:0'
									),
	                'created_by'	=>	array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'document_checklists created_by'
	                ),
	                'created_at' => array(
	                	'type'	=>	'DATETIME',
	                	'NULL'	=>	TRUE,
                    'comment'	=>	'document_checklists created_at'
	                ),
	                'updated_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'document_checklists updated_by'
	                ),
	                'updated_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'document_checklists updated_at'
	                ),
	                'deleted_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'document_checklists deleted_by'
	                ),
	                'deleted_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'document_checklists deleted_at'
	                )
								);

		$this->dbforge->add_field($_fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('document_checklists', TRUE);

		// $this->load->model('checklist/Document_checklist_model', 'M_document_checklist');
		// $this->M_document_checklist->insert_dummy();
	}

	function down () {

		if ( $this->db->table_exists('document_checklists') ) {

			$this->dbforge->drop_table("document_checklists");
		}
	}
}