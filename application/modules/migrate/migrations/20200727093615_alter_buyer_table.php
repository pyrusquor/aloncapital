<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_buyer_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('buyers')) {
            if (!$this->db->field_exists('country_id', 'buyers')) {
                $this->db->query("ALTER TABLE `buyers` ADD COLUMN `country_id` INT(11) NULL AFTER `social_media`");
            }
        }

    }

    public function down()
    {
        if ($this->db->field_exists('country_id', 'buyers')) {
            $this->dbforge->drop_column('buyers', 'country_id');
        }
    }
}
