<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_documents_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transaction_documents')) {
            if(!$this->db->field_exists('uploaded_document_id', 'transaction_documents')) {
                 $this->db->query("ALTER TABLE `transaction_documents` ADD COLUMN `uploaded_document_id` BIGINT(20) DEFAULT NULL AFTER `document_id`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transaction_documents')) {
            $this->dbforge->drop_column('transaction_documents', 'uploaded_document_id');
        }
    }
}
