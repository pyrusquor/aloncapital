<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_sellers_contact_info_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('seller_contact_infos'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'seller_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'present_address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE
                ),
                'home_address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE
                ),
                'living_quarters' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => TRUE
                ),
                'mobile_no' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '11',
                    'NULL' => TRUE
                ),
                'email' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE
                ),
                'landline' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '10',
                    'NULL' => TRUE
                ),
                'home_address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('seller_contact_infos', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('seller_contact_infos') ) {

			$this->dbforge->drop_table('seller_contact_infos');
		}
    }
}
