<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_official_payments extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transaction_official_payments')) {
            if(!$this->db->field_exists('adhoc_payment', 'transaction_official_payments')) {
                 $this->db->query("ALTER TABLE `transaction_official_payments` ADD COLUMN `adhoc_payment` DOUBLE(20,2) DEFAULT 0 AFTER `penalty_amount`");
            }
            if(!$this->db->field_exists('grand_total', 'transaction_official_payments')) {
                 $this->db->query("ALTER TABLE `transaction_official_payments` ADD COLUMN `grand_total` DOUBLE(20,2) DEFAULT 0 AFTER `adhoc_payment`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transaction_official_payments')) {
            $this->dbforge->drop_column('transaction_official_payments', 'adhoc_payment');
            $this->dbforge->drop_column('transaction_official_payments', 'grand_total');
        }
    }
}
