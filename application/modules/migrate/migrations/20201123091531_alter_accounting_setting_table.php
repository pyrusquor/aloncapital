<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_setting_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('accounting_settings')) {
            if(!$this->db->field_exists('company_id', 'accounting_settings')) {
                 $this->db->query("ALTER TABLE `accounting_settings` ADD COLUMN `company_id` INT(11) DEFAULT NULL AFTER `code`");
            }
            if(!$this->db->field_exists('project_id', 'accounting_settings')) {
                 $this->db->query("ALTER TABLE `accounting_settings` ADD COLUMN `project_id` INT(11) DEFAULT NULL AFTER `code`");
            }
            if(!$this->db->field_exists('entry_types_id', 'accounting_settings')) {
                 $this->db->query("ALTER TABLE `accounting_settings` ADD COLUMN `entry_types_id` INT(11) DEFAULT NULL AFTER `code`");
            }
            if(!$this->db->field_exists('module_types_id', 'accounting_settings')) {
                 $this->db->query("ALTER TABLE `accounting_settings` ADD COLUMN `module_types_id` INT(11) DEFAULT NULL AFTER `code`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('accounting_settings')) {
            $this->dbforge->drop_column('accounting_settings', 'company_id');
            $this->dbforge->drop_column('accounting_settings', 'project_id');
            $this->dbforge->drop_column('accounting_settings', 'entry_types_id');
            $this->dbforge->drop_column('accounting_settings', 'module_types_id');
        }
    }
}
