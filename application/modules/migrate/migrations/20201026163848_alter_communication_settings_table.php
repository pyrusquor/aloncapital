<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_communication_settings_table extends CI_Migration
{

    public function up()
    {
       if($this->db->table_exists('communication_settings')) {
           $sql = "INSERT INTO `communication_settings` (`id`, `name`, `days_to_send`, `days_to_send_type`, `period_id`, `setting_slug`, `sms`, `sms_template_id`, `email`, `email_template_id`, `created_at`, `created_by`) VALUES
           (1,	'First Warning',	1,	'after',	1,	'overdue_payments',	0,	5,	0,	6,	'2020-05-04 13:46:55',	1),
           (2,	'Second Warning',	3,	'after',	1,	'overdue_payments',	0,	7,	0,	8,	'2020-05-04 13:46:55',	1),
           (3,	'Third Warning',	7,	'after',	1,	'overdue_payments',	0,	9,	0,	10,	'2020-05-04 13:46:55',	1),
           (4,	'First Warning',	1,	'after',	2,	'overdue_payments',	1,	5,	1,	6,	'2020-05-04 13:46:55',	1),
           (5,	'Second Warning',	3,	'after',	2,	'overdue_payments',	1,	7,	1,	8,	'2020-05-04 13:46:55',	1),
           (6,	'Third Warning',	7,	'after',	2,	'overdue_payments',	1,	9,	1,	10,	'2020-05-04 13:46:55',	1),
           (7,	'First Warning',	3,	'after',	3,	'overdue_payments',	0,	3,	0,	4,	'2020-05-04 13:46:55',	1),
           (8,	'Second Warning',	5,	'after',	3,	'overdue_payments',	0,	3,	0,	4,	'2020-05-04 13:46:55',	1),
           (9,	'Third Warning',	10,	'after',	3,	'overdue_payments',	0,	3,	0,	4,	'2020-05-04 13:46:55',	1),
           (10,	'Reminder',	3,	'before',	3,	'upcoming_payments',	0,	3,	0,	4,	'2020-05-04 13:46:55',	1),
           (11,	'Reminder',	5,	'before',	3,	'upcoming_payments',	0,	3,	0,	4,	'2020-05-04 13:46:55',	1),
           (12,	'Reminder',	10,	'before',	3,	'upcoming_payments',	0,	3,	0,	4,	'2020-05-04 13:46:55',	1),
           (13,	'Reminder',	3,	'before',	2,	'upcoming_payments',	1,	3,	1,	4,	'2020-05-04 13:46:55',	1),
           (14,	'Reminder',	5,	'before',	2,	'upcoming_payments',	0,	3,	0,	4,	'2020-05-04 13:46:55',	1),
           (15,	'Reminder',	10,	'before',	2,	'upcoming_payments',	0,	3,	0,	4,	'2020-05-04 13:46:55',	1),
           (16,	'Update of Details',	1,	'after',	0,	'update_buyer_details',	0,	3,	0,	4,	'2020-05-04 13:46:55',	1),
           (17,	'Follow Up',	15,	'after',	0,	'document_followup',	0,	1,	0,	2,	'2020-05-04 13:46:55',	1),
           (18,	'Validated Reservation',	15,	'after',	0,	'reservation',	0,	1,	0,	2,	'2020-05-04 13:46:55',	1);";

            $this->db->query($sql);
       }
    }

    public function down()
    {
        if($this->db->table_exists('communication_settings')) {
            $sql = "DELETE FROM `communication_settings`";
            $this->db->query($sql);
        }
    }
}
