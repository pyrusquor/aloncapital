<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_warehouses_table_add_file_path_column extends CI_Migration
{

    public function up()
    {

        if (!$this->db->field_exists('file_path', 'warehouses')) {
            $this->db->query("ALTER TABLE `warehouses` ADD `file_path` TEXT NULL");
        }
    }

    public function down()
    {

        if ($this->db->field_exists('file_path', 'warehouses')) {
            $this->db->query("ALTER TABLE `warehouses` DROP COLUMN `file_path`");
        }
    }
}
