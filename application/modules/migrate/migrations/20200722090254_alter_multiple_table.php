<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('suppliers')) {
            if(!$this->db->field_exists('payment_type', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` ADD COLUMN `payment_type` VARCHAR(255) DEFAULT NULL  AFTER `image`");
            }

            if(!$this->db->field_exists('terms', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` ADD COLUMN `terms` INT(11) DEFAULT NULL  AFTER `payment_type`");
            }

            if(!$this->db->field_exists('vat_type', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` ADD COLUMN `vat_type` VARCHAR(50) DEFAULT NULL  AFTER `terms`");
            }

            if(!$this->db->field_exists('tax_type', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` ADD COLUMN `tax_type` VARCHAR(50) DEFAULT NULL  AFTER `vat_type`");
            }

            if(!$this->db->field_exists('bank_name', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` ADD COLUMN `bank_name` VARCHAR(124) DEFAULT NULL  AFTER `tax_type`");
            }

            if(!$this->db->field_exists('bank_account_number', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` ADD COLUMN `bank_account_number` VARCHAR(50) DEFAULT NULL  AFTER `bank_name`");
            }

            if(!$this->db->field_exists('cor_image', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` ADD COLUMN `cor_image` VARCHAR(250) DEFAULT NULL  AFTER `bank_account_number`");
            }

            if(!$this->db->field_exists('auth_payee', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` ADD COLUMN `auth_payee` VARCHAR(250) DEFAULT NULL  AFTER `cor_image`");
            }

            if(!$this->db->field_exists('cor_number', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` ADD COLUMN `cor_number` VARCHAR(50) DEFAULT NULL  AFTER `auth_payee`");
            }
        }

        if($this->db->table_exists('contractors')) {
            if(!$this->db->field_exists('payment_type', 'contractors')) {
                $this->db->query("ALTER TABLE `contractors` ADD COLUMN `payment_type` VARCHAR(255) DEFAULT NULL  AFTER `image`");
            }

            if(!$this->db->field_exists('terms', 'contractors')) {
                $this->db->query("ALTER TABLE `contractors` ADD COLUMN `terms` INT(11) DEFAULT NULL  AFTER `payment_type`");
            }

            if(!$this->db->field_exists('vat_type', 'contractors')) {
                $this->db->query("ALTER TABLE `contractors` ADD COLUMN `vat_type` VARCHAR(50) DEFAULT NULL  AFTER `terms`");
            }

            if(!$this->db->field_exists('tax_type', 'contractors')) {
                $this->db->query("ALTER TABLE `contractors` ADD COLUMN `tax_type` VARCHAR(50) DEFAULT NULL  AFTER `vat_type`");
            }

            if(!$this->db->field_exists('bank_name', 'contractors')) {
                $this->db->query("ALTER TABLE `contractors` ADD COLUMN `bank_name` VARCHAR(124) DEFAULT NULL  AFTER `tax_type`");
            }

            if(!$this->db->field_exists('bank_account_number', 'contractors')) {
                $this->db->query("ALTER TABLE `contractors` ADD COLUMN `bank_account_number` VARCHAR(50) DEFAULT NULL  AFTER `bank_name`");
            }

            if(!$this->db->field_exists('cor_image', 'contractors')) {
                $this->db->query("ALTER TABLE `contractors` ADD COLUMN `cor_image` VARCHAR(250) DEFAULT NULL  AFTER `bank_account_number`");
            }

            if(!$this->db->field_exists('auth_payee', 'contractors')) {
                $this->db->query("ALTER TABLE `contractors` ADD COLUMN `auth_payee` VARCHAR(250) DEFAULT NULL  AFTER `cor_image`");
            }

            if(!$this->db->field_exists('cor_number', 'contractors')) {
                $this->db->query("ALTER TABLE `contractors` ADD COLUMN `cor_number` VARCHAR(50) DEFAULT NULL  AFTER `auth_payee`");
            }
        }
    }

    public function down()
    {
        if ($this->db->field_exists('payment_type', 'suppliers')) {
            $this->dbforge->drop_column('suppliers', 'payment_type');
        }

        if ($this->db->field_exists('terms', 'suppliers')) {
            $this->dbforge->drop_column('suppliers', 'terms');
        }

        if ($this->db->field_exists('vat_type', 'suppliers')) {
            $this->dbforge->drop_column('suppliers', 'vat_type');
        }

        if ($this->db->field_exists('tax_type', 'suppliers')) {
            $this->dbforge->drop_column('suppliers', 'tax_type');
        }

        if ($this->db->field_exists('bank_name', 'suppliers')) {
            $this->dbforge->drop_column('suppliers', 'bank_name');
        }

        if ($this->db->field_exists('bank_account_number', 'suppliers')) {
            $this->dbforge->drop_column('suppliers', 'bank_account_number');
        }

        if ($this->db->field_exists('cor_image', 'suppliers')) {
            $this->dbforge->drop_column('suppliers', 'cor_image');
        }

        if ($this->db->field_exists('auth_payee', 'suppliers')) {
            $this->dbforge->drop_column('suppliers', 'auth_payee');
        }

        if ($this->db->field_exists('cor_number', 'suppliers')) {
            $this->dbforge->drop_column('suppliers', 'cor_number');
        }

        if ($this->db->field_exists('payment_type', 'contractors')) {
            $this->dbforge->drop_column('contractors', 'payment_type');
        }

        if ($this->db->field_exists('terms', 'contractors')) {
            $this->dbforge->drop_column('contractors', 'terms');
        }

        if ($this->db->field_exists('vat_type', 'contractors')) {
            $this->dbforge->drop_column('contractors', 'vat_type');
        }

        if ($this->db->field_exists('tax_type', 'contractors')) {
            $this->dbforge->drop_column('contractors', 'tax_type');
        }

        if ($this->db->field_exists('bank_name', 'contractors')) {
            $this->dbforge->drop_column('contractors', 'bank_name');
        }

        if ($this->db->field_exists('bank_account_number', 'contractors')) {
            $this->dbforge->drop_column('contractors', 'bank_account_number');
        }

        if ($this->db->field_exists('cor_image', 'contractors')) {
            $this->dbforge->drop_column('contractors', 'cor_image');
        }

        if ($this->db->field_exists('auth_payee', 'contractors')) {
            $this->dbforge->drop_column('contractors', 'auth_payee');
        }

        if ($this->db->field_exists('cor_number', 'contractors')) {
            $this->dbforge->drop_column('contractors', 'cor_number');
        }
    }
}
