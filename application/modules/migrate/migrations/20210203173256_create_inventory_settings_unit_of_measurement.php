<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_inventory_settings_unit_of_measurement extends CI_Migration
{

    protected $tbl = 'inventory_settings_unit_of_measurements';

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'NOT NULL' => FALSE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'code' => array(
                'type' => 'VARCHAR',
                'constraint' => '32',
                'NULL' => false,
            )
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl, TRUE);
    }

    public function down()
    {
        if($this->db->table_exists($this->tbl)){
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
