<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transactions_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transactions')) {
            if(!$this->db->field_exists('wht_rate', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` ADD COLUMN `wht_rate` INT(11) NULL AFTER `personnel_id`");
            }
        }
        if($this->db->table_exists('transactions')) {
            if(!$this->db->field_exists('wht_amount', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` ADD COLUMN `wht_amount` INT(11) NULL AFTER `wht_rate`");
            }
        }
        if($this->db->table_exists('transactions')) {
            if(!$this->db->field_exists('is_inclusive', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` ADD COLUMN `is_inclusive` INT(1) NULL AFTER `wht_amount`");
            }
        }
        if($this->db->table_exists('transactions')) {
            if(!$this->db->field_exists('vat_amount', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` ADD COLUMN `vat_amount` INT(1) NULL AFTER `is_inclusive`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transactions')) {
            $this->dbforge->drop_column('transactions', 'wht_rate');
        }
        if($this->db->table_exists('transactions')) {
            $this->dbforge->drop_column('transactions', 'is_inclusive');
        }
        if($this->db->table_exists('transactions')) {
            $this->dbforge->drop_column('transactions', 'wht_amount');
        }
        if($this->db->table_exists('transactions')) {
            $this->dbforge->drop_column('transactions', 'vat_amount');
        }
    }
}
