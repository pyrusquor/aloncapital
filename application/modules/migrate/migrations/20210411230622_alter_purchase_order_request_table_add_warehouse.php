<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_purchase_order_request_table_add_warehouse extends CI_Migration
{
    public function up()
    {
        if($this->db->table_exists('purchase_order_requests')) {
            if(!$this->db->field_exists('warehouse_id', 'purchase_order_requests')){
                $this->db->query("ALTER TABLE `purchase_order_requests` ADD COLUMN `warehouse_id` INT(11) NULL");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('purchase_order_requests')){
            if($this->db->field_exists('warehouse_id', 'purchase_order_requests')){
                $this->dbforge->drop_column('purchase_order_requests', 'warehouse_id');
            }
        }
    }
}
