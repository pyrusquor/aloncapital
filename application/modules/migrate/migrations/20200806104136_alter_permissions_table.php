<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_permissions_table extends CI_Migration
{

    public function up()
    {
       if($this->db->table_exists('permissions')) {
           if(!$this->db->field_exists('is_admin', 'permissions')) {
               $this->db->query("ALTER TABLE `permissions` ADD COLUMN `is_admin` INT(1) DEFAULT 0 AFTER `name`");

               $this->db->insert('permissions', array('id' => 1, 'name' => "Administrator", 'is_admin' => 1, 'created_by' => 1, 'created_at' => date("Y-m-d")));
               $this->db->insert('user_permissions', array('id' => 1, 'user_id' => 1, 'permission_id' => 1, 'created_by' => 1, 'created_at' => date("Y-m-d")));
           }
       }   

    }

    public function down()
    {
        if($this->db->table_exists('permissions')) {
            if($this->db->field_exists('is_admin', 'permissions')) {
                $this->dbforge->drop_column('permissions', 'is_admin');
                $this->db->query("DELETE FROM `permissions`");
                $this->db->query("DELETE FROM `user_permissions`");
            }
        }
    }
}
