<?php defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Drop_table_uploaded_document_create_table_land_uploaded_document extends CI_Migration {

	function up () {

		if ( $this->db->table_exists('uploaded_document') ) {

			$this->dbforge->drop_table('uploaded_document');
		}

		if ( $this->db->table_exists('land_uploaded_document') ) {

			$this->dbforge->drop_table('land_uploaded_document');
		}

		$_fields	=	array(
									'id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => TRUE,
                    'comment'	=>	'land_uploaded_document Table ID'
									),
									'land_inventory_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'land_uploaded_document land_inventory_id | land_inventories ID'
									),
									'document_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'land_uploaded_document document_id | documents ID'
									),
									'file_name'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_uploaded_document file_name'
									),
									'file_type'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_uploaded_document file_type'
									),
									'file_src'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_uploaded_document file_src'
									),
									'file_path'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_uploaded_document file_path'
									),
									'full_path'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_uploaded_document full_path'
									),
									'raw_name'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_uploaded_document raw_name'
									),
									'orig_name'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_uploaded_document orig_name'
									),
									'client_name'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_uploaded_document client_name'
									),
									'file_ext'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_uploaded_document file_ext'
									),
									'file_size'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'land_uploaded_document file_size'
									),
									'is_active'	=>	array(
										'type'	=>	'TINYINT',
										'constraint' => '1',
                    'NOT NULL' => TRUE,
                    'default'	=>	1,
                    'comment'	=>	'land_uploaded_document is_active 1:TRUE|FALSE:0'
									),
	                'created_by'	=>	array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_uploaded_document created_by'
	                ),
	                'created_at' => array(
	                	'type'	=>	'DATETIME',
	                	'NULL'	=>	TRUE,
                    'comment'	=>	'land_uploaded_document created_at'
	                ),
	                'updated_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_uploaded_document updated_by'
	                ),
	                'updated_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_uploaded_document updated_at'
	                ),
	                'deleted_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_uploaded_document deleted_by'
	                ),
	                'deleted_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_uploaded_document deleted_at'
	                )
								);

		$this->dbforge->add_field($_fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('land_uploaded_document', TRUE);
	}

	function down () {

		if ( $this->db->table_exists('land_uploaded_document') ) {

			$this->dbforge->drop_table("land_uploaded_document");
		}
	}
}