<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_sellers_table extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'designation' => array(
                'name' => 'seller_team_id',
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => false
            )
        );


        if ($this->db->table_exists('sellers')) {

            if (!$this->db->field_exists('seller_team_id', 'sellers')) {
                $this->dbforge->modify_column('sellers', $fields);
            }

        }
    }

    public function down()
    {
        $fields = array(
            'seller_team_id' => array(
                'name' => 'designation',
                'type' => 'VARCHAR',
                'constraint' => '20',
                'NULL' => false
            )
        );

        if ($this->db->table_exists('sellers')) {

            if (!$this->db->field_exists('designation', 'sellers')) {
                $this->dbforge->modify_column('sellers', $fields);
            }

        }

    }
}
