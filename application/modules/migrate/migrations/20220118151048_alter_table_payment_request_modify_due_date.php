<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_payment_request_modify_due_date extends CI_Migration
{

    public function up()
    {
        $this->db->query("ALTER TABLE `payment_requests` modify `due_date` date NULL");
    }

    public function down()
    {
        $this->db->query("ALTER TABLE `payment_requests` modify `due_date` datetime null");
    }
}
