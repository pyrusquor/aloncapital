<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Alter_items_add_type_id extends CI_Migration
    {
        private $tbl = "item";

        public function up()
        {
            if ($this->db->table_exists($this->tbl)) {
                if (!$this->db->field_exists('item_type_id', $this->tbl)) {
                    $this->db->query("ALTER TABLE `$this->tbl` ADD COLUMN `item_type_id` INT(11) DEFAULT NULL");
                }
            }
        }

        public function down()
        {
            if ($this->db->table_exists($this->tbl)) {
                if ($this->db->field_exists('item_type_id', $this->tbl)) {
                    $this->dbforge->drop_column($this->tbl, 'item_type_id');
                }
            }
        }
    }
