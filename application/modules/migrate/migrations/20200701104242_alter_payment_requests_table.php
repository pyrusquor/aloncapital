<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_payment_requests_table extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'reference' => array(
                'name' => 'reference',
                'type' => 'VARCHAR',
                'constraint' => '12',
                'NULL' => false
            )
        );

        if ($this->db->table_exists('payment_requests')) {

            if (!$this->db->field_exists('wht_amount', 'payment_requests')) {
                $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `wht_amount` double(20, 2) DEFAULT NULL  AFTER `total_due_amount`");
            }

            if (!$this->db->field_exists('wht_percentage', 'payment_requests')) {
                $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `wht_percentage` double(20, 2) DEFAULT NULL  AFTER `wht_amount`");
            }

            if ($this->db->field_exists('net_amount', 'payment_requests')) {

                $this->dbforge->drop_column('payment_requests', 'net_amount');
            }

            $this->dbforge->modify_column('payment_requests', $fields);

        }
    }

    public function down()
    {
        $fields = array(
            'reference' => array(
                'name' => 'reference',
                'type' => 'VARCHAR',
                'constraint' => '11',
                'NULL' => false
            )
        );

        if ($this->db->field_exists('wht_amount', 'payment_requests')) {

            $this->dbforge->drop_column('payment_requests', 'wht_amount');
        }

        if ($this->db->field_exists('wht_percentage', 'payment_requests')) {

            $this->dbforge->drop_column('payment_requests', 'wht_percentage');
        }

        if (!$this->db->field_exists('net_amount', 'payment_requests')) {

            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `net_amount` double(20, 2) DEFAULT NULL  AFTER `gross_amount`");
        }

        if ($this->db->table_exists('payment_requests')) {

            $this->dbforge->modify_column('payment_requests', $fields);
        }
    }
}
