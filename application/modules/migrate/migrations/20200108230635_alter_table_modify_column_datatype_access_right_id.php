<?php defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_modify_column_datatype_access_right_id extends CI_Migration {

	function up () {

		if( $this->db->table_exists('documents') ) {

      if ( $this->db->field_exists('access_right_id', 'documents') ) {

      	$this->db->query("ALTER TABLE documents MODIFY access_right_id TEXT NULL COMMENT 'documents access_right_id';");
      }
    }
	}

	function down () {

		if ( $this->db->field_exists('access_right_id', 'documents') ) {

			$this->db->query("ALTER TABLE documents MODIFY access_right_id INT UNSIGNED NULL COMMENT 'documents access_right_id'");
		}
	}
}