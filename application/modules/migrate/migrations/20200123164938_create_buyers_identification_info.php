<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_buyers_identification_info extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('buyer_identifications'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'type_of_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'id_number' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE
                ),
                'date_issued' => array(
                    'type' => 'DATETIME',
                    'NULL' => TRUE
                ),
                'date_expiration' => array(
                    'type' => 'DATETIME',
                    'NULL' => TRUE
                ),
                'place_issued' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '12551',
                    'NULL' => TRUE
                ),
                'file_id' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('buyer_identifications', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('buyer_identifications') ) {

			$this->dbforge->drop_table('buyer_identifications');
		}
    }
}
