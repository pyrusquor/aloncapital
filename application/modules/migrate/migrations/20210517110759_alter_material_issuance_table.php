<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_material_issuance_table extends CI_Migration
{

    public function up()
    {
       if($this->db->table_exists('material_issuances')){
           if(!$this->db->field_exists('reference', 'material_issuances')){
               $this->db->query("ALTER TABLE `material_issuances` ADD COLUMN `reference` VARCHAR(32) NOT NULL AFTER `id`");
           }
       }
    }

    public function down()
    {
        if($this->db->table_exists('material_issuances')){
            if($this->db->field_exists('reference', 'material_issuances')){
                $this->db->query("ALTER TABLE `material_issuances` DROP COLUMN `reference`");
            }
        }
    }
}
