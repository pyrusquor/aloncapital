<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_seller_group extends CI_Migration
{

    public function up()
    {
        if (!$this->db->table_exists('seller_group')) {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NOT NULL' => false,
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '250',
                    'NULL' => false,
                ),
                'code' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('seller_group', true);

            // $this->load->model('seller_group/Seller_teams_model', 'seller_group');

            // $this->seller_group->insert_dummy();
        }
    }

    public function down()
    {
        if ($this->db->table_exists('seller_group')) {

            $this->dbforge->drop_table('seller_group');
        }
    }
}
