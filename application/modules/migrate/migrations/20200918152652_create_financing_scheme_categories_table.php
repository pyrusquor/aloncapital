<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_financing_scheme_categories_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('financing_scheme_categories'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '64',
                    'NULL' => FALSE
                ),
                'description' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '254',
                    'NULL' => TRUE
                ),
                'is_active' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => TRUE,
                    'DEFAULT' => '1',
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
			
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('financing_scheme_categories', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('financing_scheme_categories') ) {

			$this->dbforge->drop_table('financing_scheme_categories');
        }
    }
}
