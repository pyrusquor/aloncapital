<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_virtual_map_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('virtual_map')) {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'x_axis' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE,
                ),
                'y_axis' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE,
                ),
                'path' => array(
                    'type' => 'text',
                    'NULL' => FALSE,
                ),
                'property_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
            
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('virtual_map', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('virtual_map') ) {

            $this->dbforge->drop_table('virtual_map');
        }
    }
}
