<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_letter_request_subjects_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('letter_request_subjects')){
            if(!$this->db->field_exists('reference', 'letter_request_subjects')){
                $this->db->query("ALTER TABLE `letter_request_subjects` ADD COLUMN `reference` VARCHAR(55) NULL AFTER id");
            }

            if(!$this->db->field_exists('transaction_id', 'letter_request_subjects')){
                $this->db->query("ALTER TABLE `letter_request_subjects` ADD COLUMN `transaction_id` INT(11) NULL AFTER `reference`");
            }

            if(!$this->db->field_exists('buyer_id', 'letter_request_subjects')){
                $this->db->query("ALTER TABLE `letter_request_subjects` ADD COLUMN `buyer_id` INT(11) NULL AFTER `transaction_id`");
            }

            if(!$this->db->field_exists('request_type_id', 'letter_request_subjects')){
                $this->db->query("ALTER TABLE `letter_request_subjects` ADD COLUMN `request_type_id` INT(11) NULL AFTER `buyer_id`");
            }

            if(!$this->db->field_exists('status', 'letter_request_subjects')){
                $this->db->query("ALTER TABLE `letter_request_subjects` ADD COLUMN `status` VARCHAR(255) NULL AFTER `request_type_id`");
            }

            if(!$this->db->field_exists('assignatory_id', 'letter_request_subjects')){
                $this->db->query("ALTER TABLE `letter_request_subjects` ADD COLUMN `assignatory_id` INT(11) NULL AFTER `status`");
            }

            if(!$this->db->field_exists('remarks', 'letter_request_subjects')){
                $this->db->query("ALTER TABLE `letter_request_subjects` ADD COLUMN `remarks` VARCHAR(255) NULL AFTER `assignatory_id`");
            }

            if(!$this->db->field_exists('file_id', 'letter_request_subjects')){
                $this->db->query("ALTER TABLE `letter_request_subjects` ADD COLUMN `file_id` VARCHAR(255) NULL AFTER `remarks`");
            }

            if(!$this->db->field_exists('requested_at', 'letter_request_subjects')){
                $this->db->query("ALTER TABLE `letter_request_subjects` ADD COLUMN `requested_at` DATETIME NULL AFTER `file_id`");
            }

            // if(!$this->db->field_exists('unit_of_measurement_id', 'letter_request_subjects')){
            //     $this->db->query("ALTER TABLE `letter_request_subjects` ADD COLUMN `unit_of_measurement_id` INT(11) UNSIGNED NULL AFTER item_id");
            // }
        }
    }

    public function down()
    {
        if($this->db->table_exists('letter_request_subjects')){
            if($this->db->field_exists('reference', 'letter_request_subjects')){
                $this->dbforge->drop_column('letter_request_subjects', 'reference');
            }

            if($this->db->field_exists('transaction_id', 'letter_request_subjects')){
                $this->dbforge->drop_column('letter_request_subjects', 'transaction_id');
            }

            if($this->db->field_exists('buyer_id', 'letter_request_subjects')){
                $this->dbforge->drop_column('letter_request_subjects', 'buyer_id');
            }

            if($this->db->field_exists('request_type_id', 'letter_request_subjects')){
                $this->dbforge->drop_column('letter_request_subjects', 'request_type_id');
            }

            if($this->db->field_exists('status', 'letter_request_subjects')){
                $this->dbforge->drop_column('letter_request_subjects', 'status');
            }

            if($this->db->field_exists('assignatory_id', 'letter_request_subjects')){
                $this->dbforge->drop_column('letter_request_subjects', 'assignatory_id');
            }

            if($this->db->field_exists('remarks', 'letter_request_subjects')){
                $this->dbforge->drop_column('letter_request_subjects', 'remarks');
            }

            if($this->db->field_exists('requested_at', 'letter_request_subjects')){
                $this->dbforge->drop_column('letter_request_subjects', 'requested_at');
            }

            if($this->db->field_exists('file_id', 'letter_request_subjects')){
                $this->dbforge->drop_column('letter_request_subjects', 'file_id');
            }

            if(!$this->db->field_exists('name', 'letter_request_subjects')){
                $this->db->query("ALTER TABLE `letter_request_subjects` ADD COLUMN `name` VARCHAR(255) NULL AFTER `id`");
            }
        }
    }
}
