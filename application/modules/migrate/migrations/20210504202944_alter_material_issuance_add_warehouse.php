<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_material_issuance_add_warehouse extends CI_Migration
{

    public function up()
    {
       if($this->db->table_exists('material_issuances')){
           if($this->db->field_exists('material_request_id', 'material_issuances')){
               $this->db->query("ALTER TABLE `material_issuances` CHANGE COLUMN `material_request_id` `material_request_id` INT UNSIGNED NULL AFTER `deleted_at`");
           }
           if(! $this->db->field_exists('warehouse_id', 'material_issuances')){
               $this->db->query("ALTER TABLE `material_issuances` ADD COLUMN `warehouse_id` INT UNSIGNED NULL DEFAULT NULL AFTER `material_request_id`");
           }
       }
    }

    public function down()
    {
        if($this->db->table_exists('material_issuances')){
            if($this->db->field_exists('warehouse_id', 'material_issuances')){
                $this->db->query("ALTER TABLE `material_issuances` DROP COLUMN `warehouse_id`");
            }
        }
    }
}
