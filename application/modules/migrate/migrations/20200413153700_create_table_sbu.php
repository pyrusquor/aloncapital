<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_sbu extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => true,
                'auto_increment' => true,
                'NOT NULL' => false,
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'company' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
            ),
            'code' => array(
                'type' => 'VARCHAR',
                'constraint' => '11',
                'NULL' => false,
            ),
            'is_active' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'created_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('sbu', true);

        $this->load->model('sbu/Sbu_model', 'sbu');
        $this->sbu->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('sbu')) {

            $this->dbforge->drop_table('sbu');
        }
    }
}
