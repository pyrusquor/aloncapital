<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_sellers_reference_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('seller_references'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'seller_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'contact_no' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'NULL' => TRUE
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('seller_references', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('seller_references') ) {

			$this->dbforge->drop_table('seller_references');
		}
    }
}
