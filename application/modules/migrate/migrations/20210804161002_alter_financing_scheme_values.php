<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_financing_scheme_values extends CI_Migration
{

    public function up()
    {
        if ($this->db->field_exists('period_rate_percentage', 'financing_scheme_values')) {
            $this->db->query("ALTER TABLE `financing_scheme_values` modify COLUMN `period_rate_percentage` double (20,6)");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('period_rate_percentage', 'financing_scheme_values')) {
            $this->db->query("ALTER TABLE `financing_scheme_values` modify COLUMN `period_rate_percentage` double (20,2)");
        }
    }
}
