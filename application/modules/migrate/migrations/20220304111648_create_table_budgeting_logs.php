<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_budgeting_logs extends CI_Migration
{

    private $tbl = "budgeting_logs";

    private $fields = array(
        'id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'auto_increment' => TRUE,
            'NOT NULL' => TRUE,
        ),
        'ledger_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'budgeting_setup_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'material_request_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'amount' => array(
            'type' => 'decimal',
            'constraint' => '15,2',
            'NULL' => false,
            'DEFAULT' => "0.00",
        ),
        'created_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'created_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'updated_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'updated_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'deleted_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'deleted_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
    );

    public function up()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_key('ledger_id');
            $this->dbforge->add_key('budgeting_setup_id');
            $this->dbforge->add_key('material_request_id');
            $this->dbforge->create_table($this->tbl, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
