<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_sellers_reservations_table_add_payment_status extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('payment_status', 'sellers_reservations')) {
            $this->db->query("ALTER TABLE `sellers_reservations` ADD `payment_status` int(1) default 1 AFTER `reservation_fee`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('payment_status', 'sellers_reservations')) {
            $this->db->query("ALTER TABLE `sellers_reservations` DROP COLUMN `payment_status`");
        }
    }
}
