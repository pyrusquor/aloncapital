<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transactions_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transactions')) {
            if (!$this->db->field_exists('due_date', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` ADD COLUMN `due_date` datetime DEFAULT NULL  AFTER `expiration_date`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transactions')) {
            if ($this->db->field_exists('due_date', 'transactions')) {
                $this->dbforge->drop_column('transactions', 'due_date');
            }
        }
    }
}
