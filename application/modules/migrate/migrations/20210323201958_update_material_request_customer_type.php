<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Update_material_request_customer_type extends CI_Migration
{
    private $tbl = "material_requests";
    private $new_fields = array(
        'customer_type' => array(
            'type' => 'VARCHAR',
            'constraint' => '32',
            'NULL' => TRUE,
            'default' => ''
        )
    );

    public function up()
    {
        $this->dbforge->add_column($this->tbl, $this->new_fields);
    }

    public function down()
    {
        foreach($this->new_fields as $field => $data){
            $this->dbforge->drop_column($this->tbl, $field);
        }
    }
}
