<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_land_inventories_table_add_subdivide_company_id_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('land_inventories')) {

            if (!$this->db->field_exists('subdivide_company_id', 'land_inventories')) {
                $this->db->query("ALTER TABLE `land_inventories` ADD COLUMN `subdivide_company_id` INT(11) NULL AFTER `remarks`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('land_inventories')) {

            if ($this->db->field_exists('subdivide_company_id', 'land_inventories')) {
                $this->dbforge->drop_column('land_inventories', 'subdivide_company_id');
            }
        }
    }
}
