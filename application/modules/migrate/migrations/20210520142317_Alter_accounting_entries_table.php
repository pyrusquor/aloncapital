<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_entries_table extends CI_Migration
{

    public function up(){
       
        if($this->db->table_exists('accounting_entries')){

			if ($this->db->field_exists('project_id', 'accounting_entries')){

				$this->dbforge->drop_column('accounting_entries', 'project_id');
            }

            if ($this->db->field_exists('property_id', 'accounting_entries')){

				$this->dbforge->drop_column('accounting_entries', 'property_id');
			}
        }

        $fields = array(
            'project_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NOT NULL' => TRUE,
                'default' => 0,
                'constraint' => 11,
            ),
            'property_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'constraint' => 11,
                'NOT NULL' => TRUE,
                'default' => 0,
            ),
        );

        $this->dbforge->add_column('accounting_entries', $fields);

        $this->dbforge->drop_table('accounting_entry_items_table',TRUE);

    }

    public function down(){

        if($this->db->table_exists('accounting_entries')){

			if ($this->db->field_exists('project_id', 'accounting_entries')){

				$this->dbforge->drop_column('accounting_entries', 'project_id');
            }

            if ($this->db->field_exists('property_id', 'accounting_entries')){

				$this->dbforge->drop_column('accounting_entries', 'property_id');
			}
        }
    }
}
