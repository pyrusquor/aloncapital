<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_properties_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('properties'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'project_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => FALSE
                ),
                'phase' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'cluster' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'lot' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'block' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'model_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'interior_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'lot_area' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'floor_area' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'lot_price_per_sqm' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'model_price' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'total_selling_price' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'status' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'is_active' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'NULL' => FALSE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('properties', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('properties') ) {

            $this->dbforge->drop_table('properties');
        }
    }
}
