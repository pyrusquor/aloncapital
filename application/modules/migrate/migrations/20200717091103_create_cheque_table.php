<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_cheque_table extends CI_Migration
{

    public function up()
    {
       if(!$this->db->table_exists('cheque')) {
           $fields = array(
               'id' => array(
                   'type' => 'INT',
                   'constraint' => '11',
                   'unsigned' => true,
                   'auto_increment' => true,
                   'NULL' => false,
               ),
               'bank_id' => array(
                   'type' => 'INT',
                   'constraint' => '11',
                   'NULL' => false,
               ),
               'branch' => array(
                   'type' => 'VARCHAR',
                   'constraint' => '250',
                   'NULL' => true,
               ),
               'amount' => array(
                   'type' => 'DECIMAL',
                   'constraint' => '20, 2',
                   'NULL' => true,
               ),
               'unique_number' => array(
                   'type' => 'INT',
                   'constraint' => '11',
                   'NULL' => true,
               ),
               'due_date' => array(
                   'type' => 'DATE',
                   'NULL' => true,
               ),
               'particulars' => array(
                   'type' => 'VARCHAR',
                   'constraint' => '255',
                   'NULL' => true,
               ),
               'status' => array(
                   'type' => 'INT',
                   'constraint' => '1',
                   'NULL' => true,
               ),
               'is_active' => array(
                   'type' => 'INT',
                   'constraint' => '1',
                   'NULL' => true,
               ),
               'created_at' => array(
                   'type' => 'DATETIME',
                   'NULL' => true,
               ),
               'created_by' => array(
                   'type' => 'INT',
                   'unsigned' => true,
                   'NULL' => true,
               ),
               'updated_at' => array(
                   'type' => 'DATETIME',
                   'NULL' => true,
               ),
               'updated_by' => array(
                   'type' => 'INT',
                   'unsigned' => true,
                   'NULL' => true,
               ),
               'deleted_at' => array(
                   'type' => 'DATETIME',
                   'NULL' => true,
               ),
               'deleted_by' => array(
                   'type' => 'INT',
                   'unsigned' => true,
                   'NULL' => true,
               ),
           );

           $this->dbforge->add_field($fields);
           $this->dbforge->add_key('id', true);
           $this->dbforge->create_table('cheque', true);
       }
    }

    public function down()
    {
        if($this->db->table_exists('cheque')) {
            $this->dbforge->drop_table('cheque');
        }
    }
}
