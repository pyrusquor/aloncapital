<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transactions_table_add_tpm_p_dp extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('tpm_p_dp', 'transactions')) {
            $this->db->query("ALTER TABLE `transactions` ADD `tpm_p_dp` float NULL AFTER `sales_recognize_entry_id`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('tpm_p_dp', 'transactions')) {
            $this->db->query("ALTER TABLE `transactions` DROP COLUMN `tpm_p_dp`");
        }
    }
}
