<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Add_staff_table extends CI_Migration
{

    public function up()
    {
		if(!$this->db->table_exists('staff'))
		{
			$fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
				'user_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
				'last_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => FALSE
                ),
                'first_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => FALSE
                ),
                'middle_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE
                ),
				'position' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE
                ),
				'birthday' => array(
                    'type' => 'DATE',
                    'NULL' => FALSE
                ),
				'contact' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'NULL' => FALSE
                ),
                'area' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => FALSE
                ),
                'city' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
				'group_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
				'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
			);
			
			$this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('staff', TRUE);
		}
    }

    public function down()
    {
		if ( $this->db->table_exists('staff') ) {

			$this->dbforge->drop_table('staff');
		}
    }
}
