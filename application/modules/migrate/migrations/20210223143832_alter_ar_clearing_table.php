<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_ar_clearing_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('ar_clearing')) {
            $this->dbforge->drop_column('ar_clearing', 'amount');
            $this->dbforge->drop_column('ar_clearing', 'payment_type');

            if(!$this->db->field_exists('period_count', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `period_count` INT(50) NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('period_id', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `period_id` INT(11) NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('payment_date', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `payment_date` datetime NULL AFTER `is_active`");
            }


            if(!$this->db->field_exists('or_date', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `or_date` datetime NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('is_waived', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `is_waived` INT(11) NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('penalty_amount', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `penalty_amount` INT(50) NULL AFTER `is_active`");
            }


            if(!$this->db->field_exists('amount_paid', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `amount_paid` INT(50) NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('payment_type_id', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `payment_type_id` INT(11) NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('post_dated_check_id', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `post_dated_check_id` INT(11) NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('cash_amount', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `cash_amount` INT(50) NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('check_deposit_amount', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `check_deposit_amount` INT(50) NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('receipt_type', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `receipt_type` INT(11) NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('OR_number', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `OR_number` VARCHAR(255) NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('adhoc_payment', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `adhoc_payment` VARCHAR(255) NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('grand_total', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `grand_total` INT(50) NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('remarks', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `remarks` VARCHAR(255) NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('transaction_payment_id', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `transaction_payment_id` VARCHAR(11) NULL AFTER `is_active`");
            }

            if(!$this->db->field_exists('payment_application', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `payment_application` VARCHAR(11) NULL AFTER `is_active`");
            }

        }
    }

    public function down()
    {
        if($this->db->table_exists('ar_clearing')) {
            if(!$this->db->field_exists('amount', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `amount` INT(50) NULL AFTER `id`");
            }

            if(!$this->db->field_exists('payment_type', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `payment_type` INT(11) NULL AFTER `amount`");
            }

            if($this->db->field_exists('period_count', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'period_count');
            }

            if($this->db->field_exists('period_id', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'period_id');
            }

            if($this->db->field_exists('payment_date', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'payment_date');
            }

            if($this->db->field_exists('or_date', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'or_date');
            }

            if($this->db->field_exists('is_waived', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'is_waived');
            }

            if($this->db->field_exists('penalty_amount', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'penalty_amount');
            }

            if($this->db->field_exists('amount_paid', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'amount_paid');
            }

            if($this->db->field_exists('payment_type_id', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'payment_type_id');
            }

            if($this->db->field_exists('post_dated_check_id', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'post_dated_check_id');
            }

            if($this->db->field_exists('cash_amount', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'cash_amount');
            }

            if($this->db->field_exists('check_deposit_amount', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'check_deposit_amount');
            }

            if($this->db->field_exists('receipt_type', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'receipt_type');
            }

            if($this->db->field_exists('OR_number', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'OR_number');
            }

            if($this->db->field_exists('adhoc_payment', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'adhoc_payment');
            }

            if($this->db->field_exists('grand_total', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'grand_total');
            }

            if($this->db->field_exists('remarks', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'remarks');
            }

            if($this->db->field_exists('transaction_payment_id', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'transaction_payment_id');
            }

            if($this->db->field_exists('payment_application', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'payment_application');
            }
            
        }
    }
}
