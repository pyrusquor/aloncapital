<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Update_material_request_status extends CI_Migration
{

    public function up()
    {
       if($this->db->table_exists('material_requests')) {
           if(!$this->db->field_exists('request_status', 'material_requests')){
               $this->db->query("ALTER TABLE `material_requests` ADD COLUMN `request_status` INT(2) DEFAULT 0 NOT NULL");
           }
       }
    }

    public function down()
    {
        if($this->db->table_exists('material_requests')){
            if($this->db->field_exists('request_status', 'material_requests')){
                $this->dbforge->drop_column('material_requests', 'request_status');
            }
        }
    }
}
