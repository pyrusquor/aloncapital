<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_aris_payments extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('aris_payments')) {
            if(!$this->db->field_exists('official_payment_id', 'aris_payments')) {
                $this->db->query("ALTER TABLE `aris_payments` ADD COLUMN `official_payment_id` INT(11) DEFAULT 0 AFTER `PaymentFor`");
            }
       }
    }

    public function down()
    {
        if($this->db->table_exists('aris_payments')) {
            $this->dbforge->drop_column('aris_payments', 'official_payment_id');
        }
    }
}
