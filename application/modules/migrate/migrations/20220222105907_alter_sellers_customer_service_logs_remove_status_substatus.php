<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_sellers_customer_service_logs_remove_status_substatus extends CI_Migration
{

    public function up()
    {

        if ($this->db->field_exists('status', 'customer_service_logs')) {
            $this->db->query("ALTER TABLE `customer_service_logs` DROP COLUMN `status`");
        }

        if ($this->db->field_exists('sub_status', 'customer_service_logs')) {
            $this->db->query("ALTER TABLE `customer_service_logs` DROP COLUMN `sub_status`");
        }
    }

    public function down()
    {
        if (!$this->db->field_exists('sub_status', 'customer_service_logs')) {
            $this->db->query("ALTER TABLE `customer_service_logs` ADD `sub_status` INT(2) NULL AFTER `forward_completion_date`");
        }

        if (!$this->db->field_exists('status', 'customer_service_logs')) {
            $this->db->query("ALTER TABLE `customer_service_logs` ADD `status` INT(2) NULL AFTER `forward_completion_date`");
        }
    }
}
