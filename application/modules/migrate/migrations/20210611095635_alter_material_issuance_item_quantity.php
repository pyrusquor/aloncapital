<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_material_issuance_item_quantity extends CI_Migration
{

    private $tbl = 'material_issuance_items';
    private $fields = array(
        'quantity' => array(
            "alter" => "ALTER TABLE `material_receiving_items` CHANGE COLUMN `quantity` `quantity` DECIMAL(20,2) UNSIGNED NULL DEFAULT 0",
            "undo" => "ALTER TABLE `material_receiving_items` CHANGE COLUMN `quantity` `quantity` INT UNSIGNED NULL DEFAULT 0"
        )
    );

    public function up()
    {
        if ($this->db->table_exists($this->tbl)) {
            foreach ($this->fields as $field => $queries) {
                if (!$this->db->field_exists($field, $this->tbl)) {
                    $this->db->query($queries['alter']);
                }
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            foreach ($this->fields as $field => $queries) {
                if ($this->db->field_exists($field, $this->tbl)) {
                    $this->db->query($queries['undo']);
                }
            }
        }
    }
}
