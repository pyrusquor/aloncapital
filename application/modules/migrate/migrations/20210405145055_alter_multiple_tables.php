<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_tables extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('construction_order_items')) {
            if(!$this->db->field_exists('order_id', 'construction_order_items')){
                $this->db->query("ALTER TABLE `construction_order_items` ADD COLUMN `order_id` INT(11) NOT NULL AFTER id");
            }
        }
        if($this->db->table_exists('construction_order_items')) {
            if(!$this->db->field_exists('item_id', 'construction_order_items')){
                $this->db->query("ALTER TABLE `construction_order_items` ADD COLUMN `item_id` INT(11) NOT NULL AFTER order_id");
            }
        }
        if($this->db->table_exists('construction_template_items')) {
            if(!$this->db->field_exists('item_id', 'construction_template_items')){
                $this->db->query("ALTER TABLE `construction_template_items` ADD COLUMN `item_id` INT(11) NOT NULL AFTER construction_template_id");
            }
        }
        if($this->db->table_exists('construction_orders')) {
            if(!$this->db->field_exists('template_id', 'construction_orders')){
                $this->db->query("ALTER TABLE `construction_orders` ADD COLUMN `template_id` INT(11) NOT NULL AFTER id");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('construction_order_items')){
            if($this->db->field_exists('order_id', 'construction_order_items')){
                $this->dbforge->drop_column('construction_order_items', 'order_id');
            }
        }
        if($this->db->table_exists('construction_order_items')){
            if($this->db->field_exists('item_id', 'construction_order_items')){
                $this->dbforge->drop_column('construction_order_items', 'item_id');
            }
        }
        if($this->db->table_exists('construction_template_items')){
            if($this->db->field_exists('item_id', 'construction_template_items')){
                $this->dbforge->drop_column('construction_template_items', 'item_id');
            }
        }
        if($this->db->table_exists('construction_order')){
            if($this->db->field_exists('template_id', 'construction_order')){
                $this->dbforge->drop_column('construction_order', 'template_id');
            }
        }
    }
}
