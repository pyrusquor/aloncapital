<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Alter_material_receiving_issuance_table extends CI_Migration
    {

        public function up()
        {
            if($this->db->table_exists('material_receiving_issuancess') && ! $this->db->table_exists('material_receiving_issuances')){
                $this->db->query("RENAME TABLE `material_receiving_issuancess` TO `material_receiving_issuances`;");
            }
        }

        public function down()
        {
            if($this->db->table_exists('material_receiving_issuances')  && ! $this->db->table_exists('material_receiving_issuancess')){
                $this->db->query("RENAME TABLE `material_receiving_issuances` TO `material_receiving_issuancess`;");
            }
        }
    }