<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_reports_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('accounting_reports')) {
            if(!$this->db->field_exists('company_id', 'accounting_reports')) {
                $this->db->query("ALTER TABLE `accounting_reports` ADD COLUMN `company_id` INT(11) DEFAULT 0 AFTER `slug`");
            }
       }
    }

    public function down()
    {
        if($this->db->table_exists('accounting_reports')) {
            $this->dbforge->drop_column('accounting_reports', 'company_id');
        }
    }
}
