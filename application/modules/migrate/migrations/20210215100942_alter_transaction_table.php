<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transactions')) {
            if(!$this->db->field_exists('aris_id', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` ADD COLUMN `aris_id` INT(11) NULL AFTER `vat_amount`");
            }
       }
    }

    public function down()
    {
        if($this->db->table_exists('transactions')) {
            $this->dbforge->drop_column('transactions', 'aris_id');
        }
    }
}
