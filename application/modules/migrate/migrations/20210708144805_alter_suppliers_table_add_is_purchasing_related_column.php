<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_suppliers_table_add_is_purchasing_related_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('suppliers')) {

            if (!$this->db->field_exists('is_purchasing_related', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` ADD COLUMN `is_purchasing_related` INT(2) NULL AFTER `cor_number`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('suppliers')) {

            if ($this->db->field_exists('is_purchasing_related', 'suppliers')) {
                $this->dbforge->drop_column('suppliers', 'is_purchasing_related');
            }
        }
    }
}
