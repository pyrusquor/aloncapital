<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_payment_request_items_table extends CI_Migration
{

    public function up()
    {
       if($this->db->table_exists('payment_request_items')) {
           $this->dbforge->rename_table('payment_request_items', 'payable_items');

           if (!$this->db->field_exists('payable_type', 'payable_items')) {

               $this->db->query("ALTER TABLE `payable_items` ADD COLUMN `payable_type` varchar(2) DEFAULT NULL  AFTER `item_id`");
           }
       }
    }

    public function down()
    {
        if($this->db->table_exists('payable_items')) {
            $this->dbforge->rename_table('payable_items', 'payment_request_items');
        }

        if ($this->db->field_exists('payable_type', 'payment_request_items')) {

            $this->dbforge->drop_column('payment_request_items', 'payable_type');
        }
    }
}
