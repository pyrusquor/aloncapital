<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_confirmation_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transaction_confirmation')) {
            if(!$this->db->field_exists('username', 'transaction_confirmation')) {
                $this->db->query("ALTER TABLE `transaction_confirmation` ADD COLUMN `username` VARCHAR(100) NULL AFTER `category`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transaction_confirmation')) {
            $this->dbforge->drop_column('transaction_confirmation', 'username');
        }
    }
}
