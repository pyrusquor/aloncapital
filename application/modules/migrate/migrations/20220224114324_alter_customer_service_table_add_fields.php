<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_customer_service_table_add_fields extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('last_name', 'customer_services')) {
            $this->db->query("ALTER TABLE `customer_services` ADD `last_name` VARCHAR(255) NULL AFTER `buyer_id`");
        }
        if (!$this->db->field_exists('first_name', 'customer_services')) {
            $this->db->query("ALTER TABLE `customer_services` ADD `first_name` VARCHAR(255) NULL AFTER `buyer_id`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('last_name', 'customer_services')) {
            $this->db->query("ALTER TABLE `customer_services` DROP COLUMN `last_name`");
        }
        if ($this->db->field_exists('first_name', 'customer_services')) {
            $this->db->query("ALTER TABLE `customer_services` DROP COLUMN `first_name`");
        }
    }
}
