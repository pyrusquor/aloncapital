<?php defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_house_model_interior_price_logs_add_columns extends CI_Migration {

	function up () {

		if( $this->db->table_exists('house_model_interior_price_logs') ) {

      if ( !$this->db->field_exists('date_effectivity', 'house_model_interior_price_logs') ) {

      	$this->db->query("ALTER TABLE `house_model_interior_price_logs` ADD COLUMN `date_effectivity` TEXT DEFAULT NULL COMMENT 'house_model_interior_price_logs date_effectivity' AFTER `price`");
      }
    }

    if( $this->db->table_exists('house_model_interior_price_logs') ) {

      if ( !$this->db->field_exists('is_multiply_floor_area', 'house_model_interior_price_logs') ) {

      	$this->db->query("ALTER TABLE `house_model_interior_price_logs` ADD COLUMN `is_multiply_floor_area` TINYINT DEFAULT NULL COMMENT 'house_model_interior_price_logs is_multiply_floor_area' AFTER `date_effectivity`");
      }
    }

    if( $this->db->table_exists('house_model_interior_price_logs') ) {

      if ( !$this->db->field_exists('is_multiply_lot_area', 'house_model_interior_price_logs') ) {

      	$this->db->query("ALTER TABLE `house_model_interior_price_logs` ADD COLUMN `is_multiply_lot_area` TINYINT DEFAULT NULL COMMENT 'house_model_interior_price_logs is_multiply_lot_area' AFTER `is_multiply_floor_area`");
      }
    }
	}

	function down () {

		if ( $this->db->field_exists('date_effectivity', 'house_model_interior_price_logs') ) {

			$this->dbforge->drop_column('house_model_interior_price_logs', 'date_effectivity');
		}

		if ( $this->db->field_exists('is_multiply_floor_area', 'house_model_interior_price_logs') ) {

			$this->dbforge->drop_column('house_model_interior_price_logs', 'is_multiply_floor_area');
		}

		if ( $this->db->field_exists('is_multiply_lot_area', 'house_model_interior_price_logs') ) {

			$this->dbforge->drop_column('house_model_interior_price_logs', 'is_multiply_lot_area');
		}
	}
}