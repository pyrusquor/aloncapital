<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_tables_payment_vouchers_requests extends CI_Migration
{

    public function up()
    {
        if ($this->db->field_exists('property_id', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'property_id');
        }
        if ($this->db->field_exists('project_id', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'project_id');
        }
        if ($this->db->field_exists('property_id', 'payment_vouchers')) {
            $this->dbforge->drop_column('payment_vouchers', 'property_id');
        }
        if ($this->db->field_exists('project_id', 'payment_vouchers')) {
            $this->dbforge->drop_column('payment_vouchers', 'project_id');
        }
    }

    public function down()
    {
        if (!$this->db->field_exists('property_id', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD `property_id` INT(11) NULL after date_requested");
        }
        if (!$this->db->field_exists('project_id', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD `project_id` INT(11) NULL after date_requested");
        }
        if (!$this->db->field_exists('property_id', 'payment_vouchers')) {
            $this->db->query("ALTER TABLE `payment_vouchers` ADD `property_id` INT(11) NULL after date_requested");
        }
        if (!$this->db->field_exists('project_id', 'payment_vouchers')) {
            $this->db->query("ALTER TABLE `payment_vouchers` ADD `project_id` INT(11) NULL after date_requested");
        }

    }
}
