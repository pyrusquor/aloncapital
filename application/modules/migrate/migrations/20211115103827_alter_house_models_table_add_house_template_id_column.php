<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_house_models_table_add_house_template_id_column extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('house_template_id', 'house_models')) {
            $this->db->query("ALTER TABLE `house_models` ADD `house_template_id` int(11) AFTER `sub_type_id`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('house_template_id', 'house_models')) {
            $this->db->query("ALTER TABLE `house_models` DROP COLUMN `house_template_id`");
        }
    }
}
