<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_entries_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('accounting_entries')) {

            if (!$this->db->field_exists('payment_request_id', 'accounting_entries')) {

                $this->db->query("ALTER TABLE `accounting_entries` ADD COLUMN `payment_request_id` int(11) DEFAULT NULL  AFTER `payee_type_id`");
            }
        }

       
    }

    public function down()
    {
        if ($this->db->field_exists('payment_request_id', 'accounting_entries')) {

            $this->dbforge->drop_column('accounting_entries', 'payment_request_id');
        }

    }
}
