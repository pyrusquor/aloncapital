<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_payment_request_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->field_exists('department_id', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'department_id');
        }

        if ($this->db->field_exists('staff_id', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'staff_id');
        }
    }

    public function down()
    {
        if (!$this->db->field_exists('department_id', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `department_id` INT(11) NULL AFTER `date_requested`");
        }

        if (!$this->db->field_exists('staff_id', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `staff_id` INT(11) NULL AFTER `department_id`");
        }
    }
}
