<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_transaction_post_dated_checks_add_column extends CI_Migration
{

    public function up()
    {

        if ($this->db->table_exists('transaction_post_dated_checks')) {

            if (!$this->db->field_exists('branch', 'transaction_post_dated_checks')) {

                $this->db->query("ALTER TABLE `transaction_post_dated_checks` ADD COLUMN `branch` varchar(50) DEFAULT NULL  AFTER `bank_id`");
            }

        }

    }

    public function down()
    {

        if ($this->db->field_exists('branch', 'transaction_post_dated_checks')) {

            $this->dbforge->drop_column('transaction_post_dated_checks', 'branch');
        }

    }
}
