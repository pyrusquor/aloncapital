<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_column_supervisor_source_info_table extends CI_Migration
{

    public function up()
    {

        if($this->db->table_exists('seller_source_infos'))
        {	
            if ($this->db->field_exists('supervisor', 'seller_source_infos'))
			{
				$this->dbforge->drop_column('seller_source_infos', 'supervisor');
            }
          

            if (!$this->db->field_exists('seller_type', 'seller_source_infos'))
            {
                $this->db->query("ALTER TABLE `seller_source_infos` ADD COLUMN `supervisor` VARCHAR(100) NULL AFTER `recruiter`");
            }
        }

    }

    public function down()
    {
        if($this->db->table_exists('seller_source_infos'))
		{
			if ($this->db->field_exists('supervisor', 'seller_source_infos'))
			{
				$this->dbforge->drop_column('seller_source_infos', 'supervisor');
            }
          
		}
    }
}
