<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_payment_requests_table_add_land_inventory_id_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('payment_requests')) {

            if (!$this->db->field_exists('land_inventory_id', 'payment_requests')) {
                $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `land_inventory_id` INT(11) NULL AFTER `particulars`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('payment_requests')) {

            if ($this->db->field_exists('land_inventory_id', 'payment_requests')) {
                $this->dbforge->drop_column('payment_requests', 'land_inventory_id');
            }
        }
    }
}
