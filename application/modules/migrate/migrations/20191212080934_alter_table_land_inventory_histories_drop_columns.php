<?php defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_land_inventory_histories_drop_columns extends CI_Migration {

	function up () {

		if( $this->db->table_exists('land_inventory_histories') ) {

			if ( $this->db->field_exists('number_of_lot_to_subdivide', 'land_inventory_histories') ) {

				$this->dbforge->drop_column('land_inventory_histories', 'number_of_lot_to_subdivide');
			}

			if ( $this->db->field_exists('project', 'land_inventory_histories') ) {

				$this->dbforge->drop_column('land_inventory_histories', 'project');
			}
    }
	}

	function down () {}
}