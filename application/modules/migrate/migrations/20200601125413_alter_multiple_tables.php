<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_tables extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('regCode', 'buyers')) {
        $this->db->query("ALTER TABLE `buyers` ADD COLUMN `regCode` INT(11) NULL AFTER `social_media`");
        }

        if (!$this->db->field_exists('provCode', 'buyers')) {
            $this->db->query("ALTER TABLE `buyers` ADD COLUMN `provCode` INT(11) NULL AFTER `regCode`");
        }

        if (!$this->db->field_exists('citymunCode', 'buyers')) {
            $this->db->query("ALTER TABLE `buyers` ADD COLUMN `citymunCode` INT(11) NULL AFTER `provCode`");
        }

        if (!$this->db->field_exists('brgyCode', 'buyers')) {
            $this->db->query("ALTER TABLE `buyers` ADD COLUMN `brgyCode` INT(11) NULL AFTER `citymunCode`");
        }

        if (!$this->db->field_exists('regCode', 'staff')) {
            $this->db->query("ALTER TABLE `staff` ADD COLUMN `regCode` INT(11) NULL AFTER `city`");
        }

        if (!$this->db->field_exists('provCode', 'staff')) {
            $this->db->query("ALTER TABLE `staff` ADD COLUMN `provCode` INT(11) NULL AFTER `regCode`");
        }

        if (!$this->db->field_exists('citymunCode', 'staff')) {
            $this->db->query("ALTER TABLE `staff` ADD COLUMN `citymunCode` INT(11) NULL AFTER `provCode`");
        }

        if (!$this->db->field_exists('brgyCode', 'staff')) {
            $this->db->query("ALTER TABLE `staff` ADD COLUMN `brgyCode` INT(11) NULL AFTER `citymunCode`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('regCode', 'buyers')) {
        $this->dbforge->drop_column('buyers', 'regCode');
        }
        if ($this->db->field_exists('provCode', 'buyers')) {
            $this->dbforge->drop_column('buyers', 'provCode');
        }
        if ($this->db->field_exists('citymunCode', 'buyers')) {
            $this->dbforge->drop_column('buyers', 'citymunCode');
        }
        if ($this->db->field_exists('brgyCode', 'buyers')) {
            $this->dbforge->drop_column('buyers', 'brgyCode');
        }
        if ($this->db->field_exists('regCode', 'staff')) {
            $this->dbforge->drop_column('staff', 'regCode');
        }
        if ($this->db->field_exists('provCode', 'staff')) {
            $this->dbforge->drop_column('staff', 'provCode');
        }
        if ($this->db->field_exists('citymunCode', 'staff')) {
            $this->dbforge->drop_column('staff', 'citymunCode');
        }
        if ($this->db->field_exists('brgyCode', 'staff')) {
            $this->dbforge->drop_column('staff', 'brgyCode');
        }
    }
}
