<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_material_request_table_add_status_column extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('status', 'material_requests')) {
            $this->db->query("ALTER TABLE `material_requests` ADD `status` int(10) NULL");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('status', 'material_requests')) {
            $this->db->query("ALTER TABLE `material_requests` DROP COLUMN `status`");
        }
    }
}
