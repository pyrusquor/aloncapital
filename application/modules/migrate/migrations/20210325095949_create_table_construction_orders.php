<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_construction_orders extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('construction_orders')) {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'company_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'cor_number' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE,
                ),
                'customer_code' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE,
                ),
                'order_to_construct' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'completion_certificate' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '255',
                    'NULL'=> TRUE,
                ),
                'instruction_number' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'filed_date' => array(
                    'type' => 'DATETIME',
                    'NULL' => TRUE,
                ),
                'delivery_date' => array(
                    'type' => 'DATETIME',
                    'NULL' => TRUE,
                ),
                'agent' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '255',
                    'NULL'=> TRUE,
                ),
                'warehouse_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'note' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '255',
                    'NULL'=> TRUE,
                ),
                'complete_date' => array(
                    'type' => 'DATETIME',
                    'NULL' => TRUE,
                ),
                'turnover_date' => array(
                    'type' => 'DATETIME',
                    'NULL' => TRUE,
                ),
                'warranty_start' => array(
                    'type' => 'DATETIME',
                    'NULL' => TRUE,
                ),
                'warranty_end' => array(
                    'type' => 'DATETIME',
                    'NULL' => TRUE,
                ),
                'project_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'sub_project_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'block' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '5',
                    'NULL'=> TRUE,
                ),
                'lot' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '5',
                    'NULL'=> TRUE,
                ),
                'status' => array(
                    'type' => 'INT',
                    'constraint' => '3',
                    'NULL' => TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
            
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('construction_orders', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('construction_orders') ) {

            $this->dbforge->drop_table('construction_orders');
        }
    }
}
