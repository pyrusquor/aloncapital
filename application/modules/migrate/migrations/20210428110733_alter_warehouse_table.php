<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_warehouse_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('warehouse')){

            if($this->db->field_exists('sbu_id', 'warehouse')){
                $this->db->query("ALTER TABLE `warehouse` CHANGE COLUMN `sbu_id` `sbu_id` INT(11) UNSIGNED NULL AFTER warehouse_code");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('construction_template_items')) {

            if($this->db->field_exists('sbu_id', 'warehouse')){
                $this->db->query("ALTER TABLE `warehouse` CHANGE COLUMN `sbu_id` `sbu_id` VARCHAR(255) NOT NULL AFTER warehouse_code");
            }
        }
    }
}
