<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_tables extends CI_Migration
{

    public function up()
    {

        if (!$this->db->field_exists('transaction_id', 'inquiry_tickets')) {
            $this->db->query("ALTER TABLE `inquiry_tickets` ADD `transaction_id` INT(11) null after `status`");
        }
        if (!$this->db->field_exists('transaction_id', 'complaint_tickets')) {
            $this->db->query("ALTER TABLE `complaint_tickets` ADD `transaction_id` INT(11) null after `status`");
        }
    }

    public function down()
    {

        if ($this->db->field_exists('transaction_id', 'inquiry_tickets')) {
            $this->db->query("ALTER TABLE `inquiry_tickets` DROP COLUMN `transaction_id`");
        }
        if ($this->db->field_exists('transaction_id', 'complaint_tickets')) {
            $this->db->query("ALTER TABLE `complaint_tickets` DROP COLUMN `transaction_id`");
        }
    }
}
