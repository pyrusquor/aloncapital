<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Create_canvassing_table extends CI_Migration
    {

        private $tbl = "canvassings";
        private $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'NOT NULL' => FALSE
            ),
            'created_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'created_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'updated_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'updated_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'deleted_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'deleted_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'company_id'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'approving_staff_id'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'requesting_staff_id'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'warehouse_id'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'accounting_ledger_id'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'purchase_order_request_id'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'status' => array(
                'type' => 'INT',
                'constraint' => '2',
                'default' => 0
            ),
            'reference' => array(
                'type' => 'VARCHAR',
                'constraint' => '24',
                'NULL' => FALSE
            ),
        );
        public function up()
        {
            if(!$this->db->table_exists($this->tbl)) {
                $this->dbforge->add_field($this->fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($this->tbl, TRUE);
            }
        }

        public function down()
        {
            if($this->db->table_exists($this->tbl)){
                $this->dbforge->drop_table($this->tbl);
            }
        }
    }
