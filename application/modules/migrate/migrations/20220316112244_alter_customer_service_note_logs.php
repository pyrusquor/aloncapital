<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_customer_service_note_logs extends CI_Migration
{

    public function up()
    {
        $this->db->query("ALTER TABLE `customer_service_note_logs` MODIFY column `note` text null");
    }

    public function down()
    {
        $this->db->query("ALTER TABLE `customer_service_note_logs` MODIFY column `note` varchar(254) null");
    }
}
