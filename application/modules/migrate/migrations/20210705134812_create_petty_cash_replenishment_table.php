<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_petty_cash_replenishment_table extends CI_Migration
{

    private $tbl = "petty_cash_replenishments";

    private $fields = array(
        'id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'auto_increment' => TRUE,
            'NOT NULL' => TRUE,
        ),
        'reference' => array(
            'type' => 'VARCHAR',
            'constraint' => '50',
            'NULL' => TRUE
        ),
        'accounting_entry_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'amount' => array(
            'type' => 'decimal',
            'constraint' => '15,2',
            'NULL' => false,
            'DEFAULT' => "0.00",
        ),
        'payee_type_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'payee_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'remarks' => array(
            'type' => 'VARCHAR',
            'constraint' => '250',
            'NULL' => TRUE
        ),
        'replenished_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'department_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'journal_type_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'created_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'created_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'updated_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'updated_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'deleted_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'deleted_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
    );

    public function up()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->tbl, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
