<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_material_receiving_item extends CI_Migration
{

    private $tbl = "material_receiving_items";
    private $fields = array(
        'id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'auto_increment' => TRUE,
            'NOT NULL' => FALSE
        ),
        'material_receiving_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NOT NULL' => TRUE
        ),
        'created_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'created_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'updated_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'updated_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'deleted_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'deleted_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'purchase_order_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'purchase_order_item_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'supplier_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'warehouse_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'purchase_order_request_item_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'purchase_order_request_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'material_request_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'item_group_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'item_type_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'item_brand_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'item_class_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'item_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'unit_of_measurement_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'item_brand_name' => array(
            'type' => 'VARCHAR',
            'constraint' => 255,
            'NULL' => TRUE,
            'default' => NULL
        ),
        'item_class_name' => array(
            'type' => 'VARCHAR',
            'constraint' => 255,
            'NULL' => TRUE,
            'default' => NULL
        ),
        'item_name' => array(
            'type' => 'VARCHAR',
            'constraint' => 255,
            'NULL' => TRUE,
            'default' => NULL
        ),
        'unit_of_measurement_name' => array(
            'type' => 'VARCHAR',
            'constraint' => 255,
            'NULL' => TRUE,
            'default' => NULL
        ),
        'item_group_name' => array(
            'type' => 'VARCHAR',
            'constraint' => 255,
            'NULL' => TRUE,
            'default' => NULL
        ),
        'item_type_name' => array(
            'type' => 'VARCHAR',
            'constraint' => 255,
            'NULL' => TRUE,
            'default' => NULL
        ),
        'status' => array(
            'type' => 'INT',
            'constraint' => 2,
            'NULL' => TRUE,
            'default' => 1
        ),
        'unit_cost' => array(
            'type' => 'DOUBLE',
            'constraint' => '20,2',
            'NULL' => FALSE
        ),
        'total_cost' => array(
            'type' => 'DOUBLE',
            'constraint' => '20,2',
            'NULL' => FALSE
        ),
        'quantity' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
    );

    public function up()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->tbl, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
