<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_accounting_groups extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('created_by', 'accounting_groups')) {
            $this->db->query("ALTER TABLE `accounting_groups` ADD COLUMN `created_by` int(1) NULL AFTER `company_id`");

        }
    }

    public function down()
    {
        
        if ( $this->db->field_exists('created_by', 'accounting_groups') ) {
            $this->dbforge->drop_column('accounting_groups', 'created_by');
        }
    }
}
