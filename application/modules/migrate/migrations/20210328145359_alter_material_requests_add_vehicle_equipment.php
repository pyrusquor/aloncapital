<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Alter_material_requests_add_vehicle_equipment extends CI_Migration
    {

        private $tbl = "material_requests";

        public function up()
        {
            if ($this->db->table_exists($this->tbl)) {
                if (!$this->db->field_exists('vehicle_equipment_id', $this->tbl)) {
                    $this->db->query("ALTER TABLE `$this->tbl` ADD COLUMN `vehicle_equipment_id` INT(11) DEFAULT NULL");
                }
            }
        }

        public function down()
        {
            if ($this->db->table_exists($this->tbl)) {
                if ($this->db->field_exists('vehicle_equipment_id', $this->tbl)) {
                    $this->dbforge->drop_column($this->tbl, 'vehicle_equipment_id');
                }
            }
        }
    }
