<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Add_construction_order_labor_info_table extends CI_Migration
{

    private $table = "construction_order_labor_info";
    private $fields = array(

        "id" => array(
            "type" => "INT",
            "constraint" => "11",
            "unsigned" => TRUE,
            "auto_increment" => TRUE,
            "NOT NULL" => TRUE,
        ),

        "construction_order_id" => [
            "type" => "int",
            "constraint" => "11",
            "null" => false,
            'unsigned' => true
        ],

        "laborer_id" => [
            "type" => "int",
            "constraint" => "11",
            "null" => false,
            'unsigned' => true
        ],

        "date" => [
            "type" => 'date',
            'null' => false,
            'default' => NOW
        ],

        "time_in" => [
            "type" => 'time',
            'null' => false
        ],

        "time_out" => [
            "type" => 'time',
            'null' => false
        ],

        "project_id" => [
            "type" => "int",
            "constraint" => "11",
            "null" => false,
            'unsigned' => true
        ],

        "property_id" => [
            "type" => "int",
            "constraint" => "11",
            "null" => true,
            'unsigned' => true
        ],

        "task" => [
            "type" => 'varchar',
            "constraint" => '255',
            "null" => TRUE
        ],

        // Audit Info
        "created_by" => array(
            "type" => "INT",
            "NOT NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "created_at" => array(
            "type" => "DATETIME",
            "NOT NULL" => TRUE,
        ),

        "updated_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "updated_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        "deleted_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "deleted_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

    );

    public function up()
    {
        if (!$this->db->table_exists($this->table)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_key('construction_order_id');
            $this->dbforge->add_key('project_id');
            $this->dbforge->add_key('property_id');
            $this->dbforge->create_table($this->table, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->table)) {
            $this->dbforge->drop_table($this->table);
        }
    }
}
