<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_payment_voucher_items extends CI_Migration
{

    public function up()
    {
        if (!$this->db->table_exists('payment_voucher_items')) {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NOT NULL' => false,
                ),
                'payment_voucher_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'transaction_commission_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('payment_voucher_items', true);
        }
    }

    public function down()
    {
        if ($this->db->table_exists('payment_voucher_items')) {

            $this->dbforge->drop_table('payment_voucher_items');
        }
    }
}
