<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_suppliers_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('suppliers')) {

            if ($this->db->field_exists('mobile_number', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` CHANGE COLUMN `mobile_number` `mobile_number` VARCHAR(50) NULL");
            }

            if ($this->db->field_exists('alternate_mobile_number', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` CHANGE COLUMN `alternate_mobile_number` `alternate_mobile_number` VARCHAR(50) NULL");
            }

            if ($this->db->field_exists('landline_number', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` CHANGE COLUMN `landline_number` `landline_number` VARCHAR(50) NULL");
            }

            if ($this->db->field_exists('sales_mobile_number', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` CHANGE COLUMN `sales_mobile_number` `sales_mobile_number` VARCHAR(50) NULL");
            }

            if ($this->db->field_exists('finance_mobile_number', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` CHANGE COLUMN `finance_mobile_number` `finance_mobile_number` VARCHAR(50) NULL");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('suppliers')) {

            if ($this->db->field_exists('mobile_number', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` CHANGE COLUMN `mobile_number` `mobile_number` INT(50) NOT NULL");
            }

            if ($this->db->field_exists('alternate_mobile_number', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` CHANGE COLUMN `alternate_mobile_number` `alternate_mobile_number` INT(50) NOT NULL");
            }

            if ($this->db->field_exists('landline_number', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` CHANGE COLUMN `landline_number` `landline_number` INT(50) NOT NULL");
            }

            if ($this->db->field_exists('sales_mobile_number', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` CHANGE COLUMN `sales_mobile_number` `sales_mobile_number` INT(50) NOT NULL");
            }

            if ($this->db->field_exists('finance_mobile_number', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` CHANGE COLUMN `finance_mobile_number` `finance_mobile_number` INT(50) NOT NULL");
            }
        }
    }
}
