<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_payment_voucher_table extends CI_Migration
{

    public function up()
    {   
        $fields = array(
            'remarks' => array(
                'name' => 'particulars',
                'type' => 'VARCHAR',
                'constraint' => '250',
                'NULL' => false
            )
        );


        if ($this->db->table_exists('payment_vouchers')) {

            $this->dbforge->modify_column('payment_vouchers', $fields);

            if (!$this->db->field_exists('accounting_entry_id', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `accounting_entry_id` INT(11) DEFAULT NULL  AFTER `reference`");
            }

            if (!$this->db->field_exists('date_requested', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `date_requested` datetime DEFAULT NULL  AFTER `reference`");
            }

            if (!$this->db->field_exists('origin_id', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `origin_id` INT(11) DEFAULT NULL  AFTER `reference`");
            }

            if (!$this->db->field_exists('project_id', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `project_id` INT(11) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('property_id', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `property_id` INT(11) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('approving_department_id', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `approving_department_id` INT(11) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('approving_staff_id', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `approving_staff_id` INT(11) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('prepared_by', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `prepared_by` INT(11) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('requesting_department_id', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `requesting_department_id` INT(11) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('requesting_staff_id', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `requesting_staff_id` INT(11) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('gross_amount', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `gross_amount` DECIMAL(15,2) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('net_amount', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `net_amount` DECIMAL(15,2) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('total_due_amount', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `total_due_amount` DECIMAL(15,2) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('wht_amount', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `wht_amount` DECIMAL(15,2) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('wht_percentage', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `wht_percentage` INT(11) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('tax_amount', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `tax_amount` DECIMAL(15,2) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('tax_percentage', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `tax_percentage` INT(11) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('payment_voucher_id', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `payment_voucher_id` INT(11) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('is_paid', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `is_paid` INT(11) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('is_complete', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `is_complete` INT(11) DEFAULT NULL  AFTER `reference`");
            }
            if (!$this->db->field_exists('due_date', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `due_date` datetime DEFAULT NULL  AFTER `reference`");
            }
        }
    }

    public function down()
    {

        
        $fields = array(
            'particulars' => array(
                'name' => 'remarks',
                'type' => 'VARCHAR',
                'constraint' => '250',
                'NULL' => false
            )
        );

        if ($this->db->table_exists('payment_vouchers')) {

            $this->dbforge->modify_column('payment_vouchers', $fields);

        }

        if ($this->db->field_exists('accounting_entry_id', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'accounting_entry_id');
        }
        if ($this->db->field_exists('origin_id', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'origin_id');
        }
        if ($this->db->field_exists('date_requested', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'date_requested');
        }
        if ($this->db->field_exists('project_id', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'project_id');
        }
        if ($this->db->field_exists('property_id', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'property_id');
        }
        if ($this->db->field_exists('approving_department_id', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'approving_department_id');
        }
        if ($this->db->field_exists('approving_staff_id', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'approving_staff_id');
        }
        if ($this->db->field_exists('prepared_by', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'prepared_by');
        }
        if ($this->db->field_exists('requesting_department_id', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'requesting_department_id');
        }
        if ($this->db->field_exists('requesting_staff_id', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'requesting_staff_id');
        }
        if ($this->db->field_exists('gross_amount', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'gross_amount');
        }
        if ($this->db->field_exists('net_amount', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'net_amount');
        }
        if ($this->db->field_exists('total_due_amount', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'total_due_amount');
        }
        if ($this->db->field_exists('wht_amount', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'wht_amount');
        }
        if ($this->db->field_exists('wht_percentage', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'wht_percentage');
        }
        if ($this->db->field_exists('tax_amount', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'tax_amount');
        }
        if ($this->db->field_exists('tax_percentage', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'tax_percentage');
        }
        if ($this->db->field_exists('payment_voucher_id', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'payment_voucher_id');
        }
        if ($this->db->field_exists('is_paid', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'is_paid');
        }
        if ($this->db->field_exists('is_complete', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'is_complete');
        }
        if ($this->db->field_exists('due_date', 'payment_vouchers')) {

            $this->dbforge->drop_column('payment_vouchers', 'due_date');
        }
    }
}
