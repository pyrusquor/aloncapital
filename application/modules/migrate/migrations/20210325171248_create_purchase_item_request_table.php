<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_purchase_item_request_table extends CI_Migration
{
    private $tbl = "purchase_order_request_items";
    private $fields = array(
        'id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'auto_increment' => TRUE,
            'NOT NULL' => FALSE
        ),
        'item_group_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'NULL' => TRUE,
            'unsigned' => TRUE,
            'default' => '0'
        ),
        'item_type_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'NULL' => TRUE,
            'unsigned' => TRUE,
            'default' => '0'
        ),
        'item_brand_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'NULL' => TRUE,
            'unsigned' => TRUE,
            'default' => '0'
        ),
        'item_class_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'NULL' => TRUE,
            'unsigned' => TRUE,
            'default' => '0'
        ),
        'item_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'NULL' => TRUE,
            'unsigned' => TRUE,
            'default' => '0'
        ),
        'unit_of_measurement_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'NULL' => TRUE,
            'unsigned' => TRUE,
            'default' => '0'
        ),
        'material_request_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'NULL' => TRUE,
            'unsigned' => TRUE,
            'default' => '0'
        ),
        'purchase_order_request_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'NULL' => TRUE,
            'unsigned' => TRUE,
            'default' => '0'
        ),
        'item_brand_name' => array(
            'type' => 'VARCHAR',
            'constraint' => '255',
            'NULL' => TRUE,
            'default' => ''
        ),
        'item_class_name' => array(
            'type' => 'VARCHAR',
            'constraint' => '255',
            'NULL' => TRUE,
            'default' => ''
        ),
        'item_name' => array(
            'type' => 'VARCHAR',
            'constraint' => '255',
            'NULL' => TRUE,
            'default' => ''
        ),
        'unit_of_measurement_name' => array(
            'type' => 'VARCHAR',
            'constraint' => '255',
            'NULL' => TRUE,
            'default' => ''
        ),
        'item_group_name' => array(
            'type' => 'VARCHAR',
            'constraint' => '255',
            'NULL' => TRUE,
            'default' => ''
        ),
        'item_type_name' => array(
            'type' => 'VARCHAR',
            'constraint' => '255',
            'NULL' => TRUE,
            'default' => ''
        ),
        'quantity' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => FALSE,
        ),
        'unit_cost' => array(
            'type' => 'DOUBLE',
            'constraint' => '20,2',
            'NULL' => FALSE
        ),
        'total_cost' => array(
            'type' => 'DOUBLE',
            'constraint' => '20,2',
            'NULL' => FALSE
        ),
        'created_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'created_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'updated_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'updated_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'deleted_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'deleted_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
    );

    public function up()
    {
       if(!$this->db->table_exists($this->tbl)){
           $this->dbforge->add_field($this->fields);
           $this->dbforge->add_key('id', TRUE);
           $this->dbforge->create_table($this->tbl, TRUE);
       }
    }

    public function down()
    {
        if($this->db->table_exists($this->tbl)){
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
