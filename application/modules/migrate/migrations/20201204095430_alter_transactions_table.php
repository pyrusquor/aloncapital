<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transactions_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transactions')) {
            if(!$this->db->field_exists('personnel_id', 'transactions')) {
                 $this->db->query("ALTER TABLE `transactions` ADD COLUMN `personnel_id` INT(11) DEFAULT NULL AFTER `is_active`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transactions')) {
            $this->dbforge->drop_column('transactions', 'personnel_id');
        }
    }
}
