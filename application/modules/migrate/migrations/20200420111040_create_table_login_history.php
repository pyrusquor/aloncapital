<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_login_history extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => true,
                'auto_increment' => true,
                'NOT NULL' => false,
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => false,
            ),
            'ip_address' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'created_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('login_history', true);

        $this->load->model('login_history/Login_history_model', 'login_history');
        // $this->login_history->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('login_history')) {

            $this->dbforge->drop_table('login_history');
        }
    }
}
