<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_payment_vouchers_table_add_land_inventory_id_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('payment_vouchers')) {

            if (!$this->db->field_exists('land_inventory_id', 'payment_vouchers')) {
                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `land_inventory_id` INT(11) NULL AFTER `particulars`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('payment_vouchers')) {

            if ($this->db->field_exists('land_inventory_id', 'payment_vouchers')) {
                $this->dbforge->drop_column('payment_vouchers', 'land_inventory_id');
            }
        }
    }
}
