<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_construction_order_items extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('construction_order_items')) {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'item_code' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE,
                ),
                'item_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE,
                ),
                'unit_of_measurement' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE,
                ),
                'quantity' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'amenity' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '255',
                    'NULL'=> TRUE,
                ),
                'sub_amenity' => array(
                    'type'=>'VARCHAR',
                    'constraint' => '255',
                    'NULL'=> TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
            
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('construction_order_items', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('construction_order_items') ) {

            $this->dbforge->drop_table('construction_order_items');
        }
    }
}
