<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_warehouse_inventory_logs_add_material_movement extends CI_Migration
{
    private $tbl = 'warehouse_inventory_logs';
    private $fields = ['material_receiving_id', 'material_releasing_id'];

    public function up()
    {
       if( $this->db->table_exists($this->tbl) ){
           foreach($this->fields as $field){
               if(! $this->db->field_exists($field, $this->tbl)){
                   $this->db->query("ALTER TABLE `$this->tbl`	ADD COLUMN `$field` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `current_available_quantity`");
               }
           }
       }
    }

    public function down()
    {
        if( $this->db->table_exists($this->tbl) ){
            foreach($this->fields as $field){
                if($this->db->field_exists($field, $this->tbl)){
                    $this->db->query("ALTER TABLE `$this->tbl` DROP COLUMN `$field`");
                }
            }
        }
    }
}
