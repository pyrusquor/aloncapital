<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Replace_subwarehouse_table extends CI_Migration
{

    protected $tbl = "sub_warehouses";
    protected $fields = array(

        "id" => array(

            "type" => "INT",


            "constraint" => "11",


            "unsigned" => True,


            "auto_increment" => True,


            "NOT NULL" => True,


        ),

        "created_by" => array(

            "type" => "INT",


            "NULL" => True,


            "unsigned" => True,


        ),

        "created_at" => array(

            "type" => "DATETIME",


            "NULL" => True,


        ),

        "updated_by" => array(

            "type" => "INT",


            "NULL" => True,


            "unsigned" => True,


        ),

        "updated_at" => array(

            "type" => "DATETIME",


            "NULL" => True,


        ),

        "deleted_by" => array(

            "type" => "INT",


            "NULL" => True,


            "unsigned" => True,


        ),

        "deleted_at" => array(

            "type" => "DATETIME",


            "NULL" => True,


        ),

        "name" => array(

            "type" => "VARCHAR",


            "constraint" => "255",


            "NULL" => True,


        ),

        "warehouse_code" => array(

            "type" => "VARCHAR",


            "constraint" => "32",


            "NULL" => True,


        ),

        "warehouse_id" => array(

            "type" => "INT",


            "constraint" => "11",


            "NULL" => True,


        ),

        "telephone_number" => array(

            "type" => "VARCHAR",


            "constraint" => "15",


            "NULL" => True,


        ),

        "fax_number" => array(

            "type" => "VARCHAR",


            "constraint" => "15",


            "NULL" => True,


        ),

        "mobile_number" => array(

            "type" => "VARCHAR",


            "constraint" => "15",


            "NULL" => True,


        ),

        "address" => array(

            "type" => "VARCHAR",


            "constraint" => "512",


            "NULL" => True,


        ),

        "contact_person" => array(

            "type" => "VARCHAR",


            "constraint" => "255",


            "NULL" => True,


        ),

        "email" => array(

            "type" => "VARCHAR",


            "constraint" => "512",


            "NULL" => True,


        ),

        "is_active" => array(

            "type" => "BOOL",


            "NULL" => True,


            "default" => True,


        ),

    );

    public function up()
    {
        if ($this->db->table_exists('sub_warehouse')) {
            $this->db->query("RENAME TABLE `sub_warehouse` TO `__sub_warehouse`");
        }
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->tbl, TRUE);
        }
        $data = $this->db->get('__sub_warehouse')->result();
        $fields = $this->db->list_fields('__sub_warehouse');
        foreach($data as $d){
            $__data = [];
            foreach($fields as $f){
                if($this->db->field_exists($f, $this->tbl) & $f != "id"){
                    $__data[$f] = $d->$f;
                }
            }
            $this->db->insert($this->tbl, $__data);
        }
    }

    public function down()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
            $this->db->query("RENAME TABLE `__sub_warehouse` TO `sub_warehouse`");
        }
    }
}
