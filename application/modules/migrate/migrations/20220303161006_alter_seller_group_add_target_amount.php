<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_seller_group_add_target_amount extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('target_amount', 'seller_group')) {
            $this->db->query("ALTER TABLE `seller_group` ADD `target_amount` decimal(20,2) NULL AFTER `code`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('target_amount', 'seller_group')) {
            $this->db->query("ALTER TABLE `seller_group` DROP COLUMN `target_amount`");
        }
    }
}
