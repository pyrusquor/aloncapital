<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_payment_vouchers_add_cheque_voucher_id extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('payment_vouchers')) {

            if (!$this->db->field_exists('cheque_voucher_id', 'payment_vouchers')) {
                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `cheque_voucher_id` INT(11) NULL AFTER `land_inventory_id`");
            }
        }
        if ($this->db->table_exists('cheque_vouchers')) {

            if (!$this->db->field_exists('reference', 'cheque_vouchers')) {
                $this->db->query("ALTER TABLE `cheque_vouchers` ADD COLUMN `reference` VARCHAR(56) NULL AFTER `id`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('payment_vouchers')) {
            if ($this->db->field_exists('cheque_voucher_id', 'payment_vouchers')) {
                $this->dbforge->drop_column('payment_vouchers', 'cheque_voucher_id');
            }
        }
        if ($this->db->table_exists('cheque_vouchers')) {
            if ($this->db->field_exists('reference', 'cheque_vouchers')) {
                $this->dbforge->drop_column('cheque_vouchers', 'reference');
            }
        }
    }
}
