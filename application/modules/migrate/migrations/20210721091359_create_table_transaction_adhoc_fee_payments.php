<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_transaction_adhoc_fee_payments extends CI_Migration
{
    private $tbl = "transaction_adhoc_fee_payments";
    private $fields = array(

        "id" => array(
            "type" => "INT",
            "constraint" => "11",
            "unsigned" => True,
            "auto_increment" => True,
            "NOT NULL" => True,
        ),

        'transaction_id' => array(
            'type' => 'INT',
            'NULL' => false
        ),

        'adhoc_schedule' => array(
            'type' => 'INT',
            'NULL' => false
        ),

        'adhoc_fee' => array(
            'type' => 'INT',
            'NULL' => false
        ),

        "payment_date" => array(
            "type" => "DATETIME",
            "NULL" => False,
        ),

        "receipt_date" => array(
            "type" => "DATETIME",
            "NULL" => True,
        ),

        "waive_penalty" => array(
            "type" => "INT",
            'constraint' => '1',
            'default' => '1',
            "NULL" => FALSE,
        ),

        'penalty_amount' => array(
            'type' => 'DOUBLE',
            'constraint' => '20,2',
            'default' => '0.00',
            'NULL' => FALSE
        ),

        "payment_type_id" => array(
            "type" => "INT",
            'constraint' => '2',
            "NULL" => FALSE,
        ),

        'amount_paid' => array(
            'type' => 'DOUBLE',
            'constraint' => '20,2',
            'NULL' => FALSE
        ),

        'cash_amount' => array(
            'type' => 'DOUBLE',
            'constraint' => '20,2',
            'NULL' => TRUE
        ),

        'cheque_amount' => array(
            'type' => 'DOUBLE',
            'constraint' => '20,2',
            'NULL' => TRUE
        ),

        "receipt_type" => array(
            "type" => "INT",
            'constraint' => '2',
            "NULL" => FALSE,
        ),

        'receipt_number' => array(
            'type' => 'VARCHAR',
            'constraint' => '50',
            'NULL' => TRUE
        ),

        "remarks" => array(
            "type" => "TEXT",
            "NOT NULL" => True,
        ),

        "created_by" => array(
            "type" => "INT",
            "NOT NULL" => True,
            "unsigned" => True,
        ),

        "created_at" => array(
            "type" => "DATETIME",
            "NOT NULL" => True,
        ),

        "updated_by" => array(
            "type" => "INT",
            "NULL" => True,
            "unsigned" => True,
        ),

        "updated_at" => array(
            "type" => "DATETIME",
            "NULL" => True,
        ),

        "deleted_by" => array(
            "type" => "INT",
            "NULL" => True,
            "unsigned" => True,
        ),

        "deleted_at" => array(
            "type" => "DATETIME",
            "NULL" => True,
        ),

    );
    public function up()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->tbl, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
