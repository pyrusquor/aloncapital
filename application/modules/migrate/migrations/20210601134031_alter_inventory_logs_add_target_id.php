<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_inventory_logs_add_target_id extends CI_Migration
{
    private $tbl = 'warehouse_inventory_logs';
    private $fields = array(
        'destination_warehouse_id' => array(
            "create" => "ALTER TABLE `warehouse_inventory_logs` ADD COLUMN `destination_warehouse_id` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `warehouse_id`",
            "delete" => "ALTER TABLE `warehouse_inventory_logs` DROP COLUMN `destination_warehouse_id`"
        ),
        'source_warehouse_id' => array(
            "create" => "ALTER TABLE `warehouse_inventory_logs` ADD COLUMN `source_warehouse_id` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `warehouse_id`",
            "delete" => "ALTER TABLE `warehouse_inventory_logs` DROP COLUMN `source_warehouse_id`"
        ),
    );

    public function up()
    {
        if($this->db->table_exists($this->tbl)){
            foreach($this->fields as $field => $queries){
                if(! $this->db->field_exists($field, $this->tbl)){
                    $this->db->query($queries['create']);
                }
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists($this->tbl)){
            foreach($this->fields as $field => $queries){
                if($this->db->field_exists($field, $this->tbl)){
                    $this->db->query($queries['delete']);
                }
            }
        }
    }
}
