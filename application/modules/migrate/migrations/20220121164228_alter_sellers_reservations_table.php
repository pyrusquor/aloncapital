<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_sellers_reservations_table extends CI_Migration
{

    public function up()
    {
        $this->db->query("ALTER TABLE `sellers_reservations` modify `expected_reservation_date` datetime NULL");
        $this->db->query("ALTER TABLE `sellers_reservations` modify `expiration_date` datetime NULL");
    }

    public function down()
    {
        $this->db->query("ALTER TABLE `sellers_reservations` modify `expected_reservation_date` date null");
        $this->db->query("ALTER TABLE `sellers_reservations` modify `expiration_date` date null");
    }
}
