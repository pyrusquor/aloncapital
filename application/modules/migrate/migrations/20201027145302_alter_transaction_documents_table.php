<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_documents_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transaction_documents')) {
            $this->dbforge->drop_column('transaction_documents', 'name');
        }
    }

    public function down()
    {
        if($this->db->table_exists('transaction_documents')) {
            if(!$this->db->field_exists('name', 'transaction_documents')) {
                 $this->db->query("ALTER TABLE `transaction_documents` ADD COLUMN `name` VARCHAR(255) DEFAULT NULL AFTER `category_id`");
            }
        }
    }
}
