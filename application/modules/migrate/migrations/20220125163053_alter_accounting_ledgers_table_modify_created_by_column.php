<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_ledgers_table_modify_created_by_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->field_exists('created_by', 'accounting_ledgers')) {
            $this->db->query("ALTER TABLE `accounting_ledgers` modify `created_by` INT(10) UNSIGNED NULL");
        }
    }

    public function down()
    {
        // 
    }
}
