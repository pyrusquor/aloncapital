<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_item_abc extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => true,
                'auto_increment' => true,
                'NOT NULL' => false,
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'abc_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '5',
                'NULL' => false,
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => '1024',
                'NULL' => false,
            ),
            'is_active' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'created_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('item_abc', true);

        $this->load->model('item_abc/Item_abc_model', 'item_abc');
        $this->item_abc->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('item_abc')) {

            $this->dbforge->drop_table('item_abc');
        }
    }
}
