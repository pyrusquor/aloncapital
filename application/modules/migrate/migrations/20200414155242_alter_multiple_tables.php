<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_tables extends CI_Migration
{

    public function up()
    {

        if(!$this->db->table_exists('transaction_billing_logs'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'transaction_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'transaction_payment_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'period_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'period_amount' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'period_term' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'effectivity_date' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'starting_balance' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,8',
                    'NULL' => FALSE
                ),
                'ending_balance' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,8',
                    'NULL' => FALSE
                ),
                'interest_rate' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'slug' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => FALSE
                ),
                'remarks' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => FALSE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('transaction_billing_logs', TRUE);
        }	


        if(!$this->db->table_exists('transaction_penalty_logs'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'transaction_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'transaction_official_payment_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'is_waived' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'amount' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,8',
                    'NULL' => FALSE
                ),
                'days_lapsed' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('transaction_penalty_logs', TRUE);
        }

        if(!$this->db->table_exists('transaction_status_logs'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'transaction_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'previous_status' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'current_status' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'type' => array(
                    'type' => 'CHAR',
                    'constraint' => '50',
                    'NULL' => FALSE
                ),
                'remarks' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => FALSE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('transaction_status_logs', TRUE);
        }


        if(!$this->db->table_exists('transaction_refund_logs'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'transaction_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'refund_amount' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'status' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'remarks' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => FALSE
                ),
                'date_cancellation' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => FALSE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('transaction_refund_logs', TRUE);
        }


        if ( ! $this->db->field_exists('process', 'transaction_payments') ) {
	      	$this->db->query("ALTER TABLE `transaction_payments` ADD COLUMN `process` varchar(255) NULL AFTER `next_payment_date`");
	    }

	    if ( ! $this->db->field_exists('deleted_reason', 'transaction_payments') ) {
	      	$this->db->query("ALTER TABLE `transaction_payments` ADD COLUMN `deleted_reason` decimal(20,2) NULL AFTER `process`");
	    }


        if ( ! $this->db->field_exists('general_status', 'transactions') ) {
            $this->db->query("ALTER TABLE `transactions` ADD COLUMN `general_status` int(10) NOT NULL DEFAULT '1' AFTER `remarks`");
        }

        if ( ! $this->db->field_exists('collection_status', 'transactions') ) {
            $this->db->query("ALTER TABLE `transactions` ADD COLUMN `collection_status` int(10) NOT NULL DEFAULT '1' AFTER `general_status`");
        }

        if ( ! $this->db->field_exists('documentation_status', 'transactions') ) {
            $this->db->query("ALTER TABLE `transactions` ADD COLUMN `documentation_status` int(10) NOT NULL DEFAULT '1'  NULL AFTER `collection_status`");
        }
    }

    public function down()
    {
    	if ( $this->db->table_exists('transaction_billing_logs') ) {
			$this->dbforge->drop_table('transaction_billing_logs');
		}

		if ( $this->db->table_exists('transaction_penalty_logs') ) {
			$this->dbforge->drop_table('transaction_penalty_logs');
		}

        if ( $this->db->table_exists('transaction_status_logs') ) {
            $this->dbforge->drop_table('transaction_status_logs');
        }

        if ( $this->db->table_exists('transaction_refund_logs') ) {
            $this->dbforge->drop_table('transaction_refund_logs');
        }
        
	    if ( $this->db->field_exists('process', 'transaction_payments') ) {
			$this->dbforge->drop_column('transaction_payments', 'process');
		}

		if ( $this->db->field_exists('deleted_reason', 'transaction_payments') ) {
			$this->dbforge->drop_column('transaction_payments', 'deleted_reason');
		}

        if ( $this->db->field_exists('general_status', 'transactions') ) {
            $this->dbforge->drop_column('transactions', 'general_status');
        }

        if ( $this->db->field_exists('collection_status', 'transactions') ) {
            $this->dbforge->drop_column('transactions', 'collection_status');
        }

        if ( $this->db->field_exists('documentation_status', 'transactions') ) {
            $this->dbforge->drop_column('transactions', 'documentation_status');
        }
    }
}
