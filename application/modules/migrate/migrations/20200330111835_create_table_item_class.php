<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_item_class extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'NOT NULL' => FALSE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => FALSE
            ),
            'class_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '5',
                'NULL' => FALSE
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => '1024',
                'NULL' => FALSE
            ),
            'is_active' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => FALSE
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('item_class', TRUE);

        // $this->load->model('item_class/Item_class_model', 'item_class');
        // $this->item_class->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('item_class')) {

            $this->dbforge->drop_table('item_class');
        }
    }
}
