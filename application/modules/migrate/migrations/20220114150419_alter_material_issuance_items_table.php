<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_material_issuance_items_table extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('material_request_id', 'material_issuance_items')) {
            $this->db->query("ALTER TABLE `material_issuance_items` ADD `material_request_id` int(10) NULL");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('material_request_id', 'material_issuance_items')) {
            $this->db->query("ALTER TABLE `material_issuance_items` DROP COLUMN `material_request_id`");
        }
    }
}
