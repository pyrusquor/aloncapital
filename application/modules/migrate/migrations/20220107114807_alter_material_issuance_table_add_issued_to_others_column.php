<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_material_issuance_table_add_issued_to_others_column extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('issued_to_others', 'material_issuances')) {
            $this->db->query("ALTER TABLE `material_issuances` ADD `issued_to_others` int(10) NULL");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('issued_to_others', 'material_issuances')) {
            $this->db->query("ALTER TABLE `material_issuances` DROP COLUMN `issued_to_others`");
        }
    }
}
