<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_document_attachments_table extends CI_Migration
{

    protected $tbl = "document_attachments";
    protected $fields = array(

        "id" => array(

            "type" => "INT",


            "constraint" => "11",


            "unsigned" => True,


            "auto_increment" => True,


            "NOT NULL" => True,


        ),

        "created_by" => array(

            "type" => "INT",


            "NOT NULL" => True,


            "unsigned" => True,


        ),

        "created_at" => array(

            "type" => "DATETIME",


            "NOT NULL" => True,


        ),

        "updated_by" => array(

            "type" => "INT",


            "NULL" => True,


            "unsigned" => True,


        ),

        "updated_at" => array(

            "type" => "DATETIME",


            "NULL" => True,


        ),

        "deleted_by" => array(

            "type" => "INT",


            "NULL" => True,


            "unsigned" => True,


        ),

        "deleted_at" => array(

            "type" => "DATETIME",


            "NULL" => True,


        ),

        "path" => array(

            "type" => "TEXT",


            "NOT NULL" => True,


        ),

        "filename" => array(

            "type" => "TEXT",


            "NOT NULL" => True,


        ),

        "object_type" => array(

            "type" => "varchar",


            "constraint" => "128",


            "NOT NULL" => True,


        ),

        "object_type_id" => array(

            "type" => "INT",


            "constraint" => "11",


            "NOT NULL" => True,


            "UNSIGNED" => True,


        ),

    );

    public function up()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->tbl, TRUE);
        }

    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
