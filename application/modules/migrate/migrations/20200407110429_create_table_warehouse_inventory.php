<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_warehouse_inventory extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => true,
                'auto_increment' => true,
                'NOT NULL' => false,
            ),
            'company' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'sbu' => array(
                'type' => 'VARCHAR',
                'constraint' => '5',
                'NULL' => false,
            ),
            'warehouse' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'item_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'item_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'current_physical_count' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'is_active' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'created_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('warehouse_inventory', true);

        $this->load->model('warehouse_inventory/Warehouse_inventory_model', 'warehouse_inventory');
        $this->warehouse_inventory->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('warehouse_inventory')) {

            $this->dbforge->drop_table('warehouse_inventory');
        }
    }
}
