<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_construction_orders_table_add_columns extends CI_Migration
{

    public function up()
    {
        // Add
        if (!$this->db->field_exists('proposed_start_date', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` ADD `proposed_start_date` DATE NULL AFTER `status`");
        }

        if (!$this->db->field_exists('proposed_finish_date', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` ADD `proposed_finish_date` DATE NULL AFTER `status`");
        }

        if (!$this->db->field_exists('actual_start_date', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` ADD `actual_start_date` DATE NULL AFTER `status`");
        }

        if (!$this->db->field_exists('actual_finish_date', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` ADD `actual_finish_date` DATE NULL AFTER `status`");
        }

        if (!$this->db->field_exists('construction_method', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` ADD `construction_method` INT(2) NULL AFTER `status`");
        }

        if (!$this->db->field_exists('furnishing_type', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` ADD `furnishing_type` INT(2) NULL AFTER `status`");
        }

        // if (!$this->db->field_exists('remarks', 'construction_orders')) {
        //     $this->db->query("ALTER TABLE `construction_orders` ADD `remarks` VARCHAR(255) NULL AFTER `status`");
        // }

        // Drop
        if ($this->db->field_exists('turnover_date', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` DROP COLUMN `turnover_date`");
        }

        if ($this->db->field_exists('warranty_start', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` DROP COLUMN `warranty_start`");
        }

        if ($this->db->field_exists('warranty_end', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` DROP COLUMN `warranty_end`");
        }
    }

    public function down()
    {
        // Drop
        if ($this->db->field_exists('proposed_start_date', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` DROP COLUMN `proposed_start_date`");
        }

        if ($this->db->field_exists('proposed_finish_date', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` DROP COLUMN `proposed_finish_date`");
        }

        if ($this->db->field_exists('actual_start_date', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` DROP COLUMN `actual_start_date`");
        }

        if ($this->db->field_exists('actual_finish_date', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` DROP COLUMN `actual_finish_date`");
        }

        if ($this->db->field_exists('construction_method', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` DROP COLUMN `construction_method`");
        }

        if ($this->db->field_exists('furnishing_type', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` DROP COLUMN `furnishing_type`");
        }

        // if ($this->db->field_exists('remarks', 'construction_orders')) {
        //     $this->db->query("ALTER TABLE `construction_orders` DROP COLUMN `remarks`");
        // }

        // Add
        if (!$this->db->field_exists('turnover_date', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` ADD `turnover_date` DATETIME NULL AFTER `status`");
        }

        if (!$this->db->field_exists('warranty_start', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` ADD `warranty_start` DATETIME NULL AFTER `status`");
        }

        if (!$this->db->field_exists('warranty_end', 'construction_orders')) {
            $this->db->query("ALTER TABLE `construction_orders` ADD `warranty_end` DATETIME NULL AFTER `status`");
        }
    }
}
