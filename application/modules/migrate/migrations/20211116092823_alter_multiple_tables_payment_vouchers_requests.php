<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_tables_payment_vouchers_requests extends CI_Migration
{

    public function up()
    {
        if (!$this->db->table_exists('payment_request_origins')) {
            $this->dbforge->rename_table('payment_request_items', 'payment_request_origins');
        }
        if (!$this->db->table_exists('payment_voucher_origins')) {
            $this->dbforge->rename_table('payment_voucher_items', 'payment_voucher_origins');
        }
    }

    public function down()
    {
        if ($this->db->table_exists('payment_request_origins')) {
            $this->dbforge->rename_table('payment_request_origins', 'payment_request_items');
        }
        if ($this->db->table_exists('payment_voucher_origins')) {
            $this->dbforge->rename_table('payment_voucher_origins', 'payment_voucher_items');
        }
    }
}
