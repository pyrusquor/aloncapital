<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_item_group extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'NOT NULL' => FALSE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => FALSE
            ),
            'group_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '5',
                'NULL' => FALSE
            ),
            'account_number' => array(
                'type' => 'INT',
                'constraint' => '8',
                'NULL' => FALSE
            ),
            'status' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => FALSE
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('item_group', TRUE);

        // $this->load->model('item_group/Item_group_model', 'item_group');
        // $this->item_group->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('item_group')) {

            $this->dbforge->drop_table('item_group');
        }
    }
}
