<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_tables extends CI_Migration
{

    public function up()
    {
       if($this->db->table_exists('suppliers')) {
           if($this->db->field_exists('terms', 'suppliers')) {
               $this->dbforge->drop_column('suppliers', 'terms');
           }
       }

       if($this->db->table_exists('contractors')) {
           if($this->db->field_exists('terms', 'contractors')) {
               $this->dbforge->drop_column('contractors', 'terms');
           }
       }
    }

    public function down()
    {
        if($this->db->table_exists('suppliers')) {
            if (!$this->db->field_exists('terms', 'suppliers')) {
                $this->db->query("ALTER TABLE `suppliers` ADD COLUMN `terms` INT(11) DEFAULT NULL  AFTER `payment_type`");
            }
        }

        if($this->db->table_exists('contractors')) {
            if (!$this->db->field_exists('terms', 'contractors')) {
                $this->db->query("ALTER TABLE `contractors` ADD COLUMN `terms` INT(11) DEFAULT NULL  AFTER `payment_type`");
            }
        }
    }
}
