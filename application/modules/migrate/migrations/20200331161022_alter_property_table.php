<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_property_table extends CI_Migration
{

    public function up()
    {
       if ( ! $this->db->field_exists('package_type_id', 'properties') ) {
	      	$this->db->query("ALTER TABLE `properties` ADD COLUMN `package_type_id` int(11) NULL AFTER `interior_id`");
	    }

	    if ( ! $this->db->field_exists('social_media', 'buyers') ) {
	      	$this->db->query("ALTER TABLE `buyers` ADD COLUMN `social_media` text NULL AFTER `landline`");
	    }
    }

    public function down()
    {
    	if ( $this->db->field_exists('package_type_id', 'properties') ) {
			$this->dbforge->drop_column('properties', 'package_type_id');
		}

		if ( $this->db->field_exists('social_media', 'buyers') ) {
			$this->dbforge->drop_column('buyers', 'social_media');
		}


    }
}
