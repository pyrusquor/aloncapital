<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_ledgers_table_add_account_number_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('accounting_ledgers')) {

            if (!$this->db->field_exists('account_number  ', 'accounting_ledgers')) {
                $this->db->query("ALTER TABLE `accounting_ledgers` ADD COLUMN `account_number` VARCHAR(50) NULL AFTER company_id");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('accounting_ledgers')) {

            if ($this->db->field_exists('account_number', 'accounting_ledgers')) {
                $this->dbforge->drop_column('accounting_ledgers', 'account_number');
            }
        }
    }
}
