<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_reconciliation extends CI_Migration
{

    public function up()
    {
        if (!$this->db->table_exists('reconciliation')) {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NOT NULL' => false,
                ),
                'bank_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'month_year' => array(
                    'type' => 'DATETIME',
                    'NULL' => false,
                ),
                'ending_balance' => array(
                    'type' => 'DECIMAL',
                    'constraint' => '15, 2',
                    'default' => '0.00'
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('reconciliation', true);
        }
    }

    public function down()
    {
        if ($this->db->table_exists('reconciliation')) {

            $this->dbforge->drop_table('reconciliation');
        }
    }
}
