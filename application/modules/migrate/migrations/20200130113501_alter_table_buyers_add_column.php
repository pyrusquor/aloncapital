<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_buyers_add_column extends CI_Migration
{

   function up () {

		if( $this->db->table_exists('buyers') ) {

	      	if ( !$this->db->field_exists('other_mobile', 'buyers') ) {

	      		$this->db->query("ALTER TABLE `buyers` ADD COLUMN `other_mobile` VARCHAR(11) DEFAULT NULL  AFTER `mobile_no`");
	      	}

	      	if ( !$this->db->field_exists('civil_status_id', 'buyers') ) {

	      		$this->db->query("ALTER TABLE `buyers` ADD COLUMN `civil_status_id` VARCHAR(11) DEFAULT NULL  AFTER `nationality`");
	      	}
	      	
	    }
	}

	function down () {

		if ( $this->db->field_exists('other_mobile', 'buyers') ) {

			$this->dbforge->drop_column('buyers', 'other_mobile');
		}

		if ( $this->db->field_exists('civil_status_id', 'buyers') ) {

			$this->dbforge->drop_column('buyers', 'civil_status_id');
		}
		
	}
}
