<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_fees extends CI_Migration
{

    public function up()
    {
       if( $this->db->table_exists('fees') ) {

	      	// if ( $this->db->field_exists('name', 'fees') ) {

	      		$this->db->query("ALTER TABLE `fees` CHANGE `name` `name` varchar(50) NOT NULL AFTER `id`");
	      	// }
	      	
	    }
    }

    public function down()
    {
    	if ( $this->db->field_exists('name', 'fees') ) {

	      		$this->db->query("ALTER TABLE `fees` CHANGE `name` `name` int(11) NOT NULL AFTER `id`");

			
		}
    }
}
