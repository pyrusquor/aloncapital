<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_table extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'bank_name' => array(
                'name' => 'bank_id',
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true
            )
        );

       if($this->db->table_exists('suppliers')) {
           if($this->db->field_exists('bank_name', 'suppliers')) {
                $this->dbforge->modify_column('suppliers', $fields);
           }
       }

       if($this->db->table_exists('contractors')) {
           if($this->db->field_exists('bank_name', 'contractors')) {
                $this->dbforge->modify_column('contractors', $fields);
           }
       }
    }

    public function down()
    {
        $fields = array(
            'bank_id' => array(
                'name' => 'bank_name',
                'type' => 'VARCHAR',
                'constraint' => '124',
                'NULL' => true
            )
        );

       if($this->db->table_exists('suppliers')) {
           if($this->db->field_exists('bank_id', 'suppliers')) {
                $this->dbforge->modify_column('suppliers', $fields);
           }
       }

       if($this->db->table_exists('contractors')) {
           if($this->db->field_exists('bank_id', 'contractors')) {
                $this->dbforge->modify_column('contractors', $fields);
           }
       }
    }
}
