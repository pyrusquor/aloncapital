<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_commission_setup extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('commission_setup')) {
            if (!$this->db->field_exists('wht_percentage', 'commission_setup')) {
                $this->db->query("ALTER TABLE `commission_setup` ADD COLUMN `wht_percentage` INT(100) DEFAULT 0  AFTER `name`");
            }
        }
        
        if($this->db->table_exists('commission_setup')) {
            if (!$this->db->field_exists('is_active', 'commission_setup')) {
                $this->db->query("ALTER TABLE `commission_setup` ADD COLUMN `is_active` INT(1) DEFAULT 1  AFTER `wht_percentage`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('commission_setup')) {
            if($this->db->field_exists('wht_percentage', 'commission_setup')) {
                $this->dbforge->drop_column('commission_setup', 'wht_percentage');
            }
        }

        if($this->db->table_exists('commission_setup')) {
            if($this->db->field_exists('is_active', 'commission_setup')) {
                $this->dbforge->drop_column('commission_setup', 'is_active');
            }
        }
    }
}
