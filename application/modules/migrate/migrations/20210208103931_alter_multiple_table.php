<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_table extends CI_Migration
{

    public function up()
    {
       if($this->db->table_exists('buyers')) {
            if(!$this->db->field_exists('aris_id', 'buyers')) {
                $this->db->query("ALTER TABLE `buyers` ADD COLUMN `aris_id` INT(11) NULL AFTER `is_exported`");
            }
       }
    
       if($this->db->table_exists('sellers')) {
            if(!$this->db->field_exists('aris_id', 'sellers')) {
                $this->db->query("ALTER TABLE `sellers` ADD COLUMN `aris_id` INT(11) NULL AFTER `is_exported`");
            }
       }
    }

    public function down()
    {
        if($this->db->table_exists('buyers')) {
            $this->dbforge->drop_column('buyers', 'aris_id');
        }

        if($this->db->table_exists('sellers')) {
            $this->dbforge->drop_column('sellers', 'aris_id');
        }
    }
}
