<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_aris_documents_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('aris_documents')) {
            if(!$this->db->field_exists('last_date_synced', 'aris_documents')) {
                $this->db->query("ALTER TABLE `aris_documents` ADD COLUMN `last_date_synced` DATETIME NULL AFTER `deleted_by`");
            }
       }
    }

    public function down()
    {
        if($this->db->table_exists('aris_documents')) {
            $this->dbforge->drop_column('aris_documents', 'last_date_synced');
        }
    }
}
