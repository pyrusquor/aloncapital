<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_commissions_table_add_columns extends CI_Migration
{
    public function up()
    {
        if ($this->db->table_exists('transaction_commissions')) {

            if (!$this->db->field_exists('released_at', 'transaction_commissions')) {
                $this->db->query("ALTER TABLE `transaction_commissions` ADD COLUMN `released_at` DATETIME AFTER payment_voucher_id");
            }

            if (!$this->db->field_exists('processed_at', 'transaction_commissions')) {
                $this->db->query("ALTER TABLE `transaction_commissions` ADD COLUMN `processed_at` DATETIME AFTER payment_voucher_id");
            }

            if (!$this->db->field_exists('requested_at', 'transaction_commissions')) {
                $this->db->query("ALTER TABLE `transaction_commissions` ADD COLUMN `requested_at` DATETIME AFTER payment_voucher_id");
            }

            if (!$this->db->field_exists('requirements_completed_at', 'transaction_commissions')) {
                $this->db->query("ALTER TABLE `transaction_commissions` ADD COLUMN `requirements_completed_at` DATETIME AFTER payment_voucher_id");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transaction_commissions')) {

            if ($this->db->field_exists('released_at', 'transaction_commissions')) {
                $this->dbforge->drop_column('transaction_commissions', 'released_at');
            }

            if ($this->db->field_exists('processed_at', 'transaction_commissions')) {
                $this->dbforge->drop_column('transaction_commissions', 'processed_at');
            }

            if ($this->db->field_exists('requested_at', 'transaction_commissions')) {
                $this->dbforge->drop_column('transaction_commissions', 'requested_at');
            }

            if ($this->db->field_exists('requirements_completed_at', 'transaction_commissions')) {
                $this->dbforge->drop_column('transaction_commissions', 'requirements_completed_at');
            }
        }
    }
}
