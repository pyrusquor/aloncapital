<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_documents_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transaction_documents')) {
            if(!$this->db->field_exists('transaction_document_stage_id', 'transaction_documents')) {
                 $this->db->query("ALTER TABLE `transaction_documents` ADD COLUMN `transaction_document_stage_id` INT(11) DEFAULT NULL AFTER `has_file`");
            }
            if(!$this->db->field_exists('transaction_status', 'transaction_documents')) {
                 $this->db->query("ALTER TABLE `transaction_documents` ADD COLUMN `transaction_status` INT(11) DEFAULT NULL AFTER `has_file`");
            }
            if(!$this->db->field_exists('remarks', 'transaction_documents')) {
                 $this->db->query("ALTER TABLE `transaction_documents` ADD COLUMN `remarks` VARCHAR(255) DEFAULT NULL AFTER `has_file`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transaction_documents')) {
            $this->dbforge->drop_column('transaction_documents', 'transaction_document_stage_id');
            $this->dbforge->drop_column('transaction_documents', 'transaction_status');
            $this->dbforge->drop_column('transaction_documents', 'remarks');
        }
    }
}
