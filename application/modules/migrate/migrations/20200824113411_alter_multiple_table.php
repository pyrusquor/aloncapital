<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_table extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'period_id' => array(
                'name' => 'category_type',
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true
            )
        );

       if($this->db->table_exists('accounting_settings')) {
           if($this->db->field_exists('period_id', 'accounting_settings')) {
                $this->dbforge->modify_column('accounting_settings', $fields);
           }
       }

       if($this->db->table_exists('accounting_settings')) {
        if (!$this->db->field_exists('origin_id', 'accounting_settings')) {
            $this->db->query("ALTER TABLE `accounting_settings` ADD COLUMN `origin_id` int(1) DEFAULT NULL  AFTER `id`");
        }
    }
    }

    public function down()
    {
        $fields = array(
            'category_type' => array(
                'name' => 'period_id',
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true
            )
        );

       if($this->db->table_exists('accounting_settings')) {
           if($this->db->field_exists('category_type', 'accounting_settings')) {
                $this->dbforge->modify_column('accounting_settings', $fields);
           }
       }

       if($this->db->table_exists('accounting_settings')) {
        if($this->db->field_exists('origin_id', 'accounting_settings')) {
            $this->dbforge->drop_column('accounting_settings', 'origin_id');
        }
    }
    }
}
