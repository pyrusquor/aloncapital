<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_contractors extends CI_Migration
{

    public function up()
    {
        if (!$this->db->table_exists('contractors')) {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NOT NULL' => false,
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'code' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'type' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'payment_terms' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'delivery_terms' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '250',
                    'NULL' => false,
                ),
                'alternate_address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '250',
                    'NULL' => false,
                ),
                'mobile_number' => array(
                    'type' => 'INT',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'alternate_mobile_number' => array(
                    'type' => 'INT',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'landline_number' => array(
                    'type' => 'INT',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'fax_number' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'email_address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'tin_number' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'sales_contact_person' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'sales_email_address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'sales_mobile_number' => array(
                    'type' => 'INT',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'finance_contact_person' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'finance_email_address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'finance_mobile_number' => array(
                    'type' => 'INT',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'image' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '250',
                    'NULL' => false,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('contractors', true);

            // $this->load->model('contractors/Contractors_model', 'contractors');

            // $this->contractors->insert_dummy();
        }
    }

    public function down()
    {
        if ($this->db->table_exists('contractors')) {

            $this->dbforge->drop_table('contractors');
        }
    }
}
