<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_cashier_table extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'payee_type' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 50,
            ),
        );
        if ($this->db->table_exists('cashier')) {
            
            if ($this->db->field_exists('payee_type', 'cashier')) {
                $this->dbforge->modify_column('cashier', $fields);
            }
        }
    }

    public function down()
    {
        $fields = array(
            'payee_type' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
            ),
        );
        if ($this->db->table_exists('cashier')) {
            
            if ($this->db->field_exists('payee_type', 'cashier')) {
                $this->dbforge->modify_column('cashier', $fields);
            }
        }
    }
}
