<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_ar_clearing_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('ar_clearing')) {

            if ($this->db->field_exists('grand_total', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` MODIFY COLUMN `grand_total` double(20,2) DEFAULT 0.00");
            }
            if ($this->db->field_exists('adhoc_payment', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` MODIFY COLUMN `adhoc_payment` double(20,2) DEFAULT 0.00");
            }
            if ($this->db->field_exists('check_deposit_amount', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` MODIFY COLUMN `check_deposit_amount` double(20,2) DEFAULT 0.00");
            }
            if ($this->db->field_exists('cash_amount', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` MODIFY COLUMN `cash_amount` double(20,2) DEFAULT 0.00");
            }
            if ($this->db->field_exists('amount_paid', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` MODIFY COLUMN `amount_paid` double(20,2) DEFAULT 0.00");
            }
            if ($this->db->field_exists('penalty_amount', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` MODIFY COLUMN `penalty_amount` double(20,2) DEFAULT 0.00");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('ar_clearing')) {

            if ($this->db->field_exists('grand_total', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` MODIFY COLUMN `grand_total` INT(50) NULL");
            }
            if ($this->db->field_exists('adhoc_payment', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` MODIFY COLUMN `adhoc_payment` INT(50) NULL");
            }
            if ($this->db->field_exists('check_deposit_amount', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` MODIFY COLUMN `check_deposit_amount` INT(50) NULL");
            }
            if ($this->db->field_exists('cash_amount', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` MODIFY COLUMN `cash_amount` INT(50) NULL");
            }
            if ($this->db->field_exists('amount_paid', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` MODIFY COLUMN `amount_paid` INT(50) NULL");
            }
            if ($this->db->field_exists('penalty_amount', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` MODIFY COLUMN `penalty_amount` INT(50) NULL");
            }
        }
    }
}
