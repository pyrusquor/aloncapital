<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Add_ticket_log_uploaded_documents_table extends CI_Migration
{
    private $table_name = 'ticket_log_uploaded_documents';

    private $fields = array(
        'id' => array(
            'type' => 'BIGINT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'auto_increment' => TRUE,
            'NOT NULL' => TRUE,
        ),
        'type' => array(
            'type' => 'VARCHAR',
            'constraint' => '100',
            'NULL' => TRUE,
        ),
        'ticket_log_id' => array(
            'type' => 'BIGINT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'file_name' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),
        'file_type' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),
        'file_src' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),
        'file_path' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),
        'full_path' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),
        'raw_name' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),
        'orig_name' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),
        'client_name' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),
        'file_ext' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),
        'file_size' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),
        'created_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'created_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'updated_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'updated_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'deleted_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'deleted_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        )
    );

    function up()
    {
        if (!$this->db->table_exists($this->table_name)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->table_name, TRUE);
        }
    }

    function down()
    {
        if ($this->db->table_exists($this->table_name)) {

            $this->dbforge->drop_table($this->table_name);
        }
    }
}
