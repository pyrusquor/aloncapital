<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transactions_table_add_sales_group_id extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('sales_group_id', 'transactions')) {
            $this->db->query("ALTER TABLE `transactions` ADD `sales_group_id` int(11) NULL AFTER `sales_recognize_entry_id`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('sales_group_id', 'transactions')) {
            $this->db->query("ALTER TABLE `transactions` DROP COLUMN `sales_group_id`");
        }
    }
}
