<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_cashier_table extends CI_Migration
{

    private $tbl = 'cashier';
    private $fields = array(
        'id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'auto_increment' => TRUE,
            'NOT NULL' => FALSE
        ),
        'created_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'created_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'updated_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'updated_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'deleted_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'deleted_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'company_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => FALSE,
        ),
        'payee_type' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => FALSE,
        ),
        'payee_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => FALSE,
        ),
        'payment_date' => array(
            'type' => 'DATETIME',
            'NULL' => FALSE,
        ),
        'payment_type_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => FALSE,
        ),
        'payment_amount' => array(
            'type' => 'DOUBLE',
            'constraint' => '20,2',
            'NULL' => FALSE,
        ),
        'bank' => array(
            'type' => 'VARCHAR',
            'constraint' => '50',
            'NULL' => FALSE,
        ),
        'cheque_number' => array(
            'type' => 'VARCHAR',
            'constraint' => '50',
            'NULL' => FALSE,
        ),
        'receipt_type' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => FALSE,
        ),
        'receipt_number' => array(
            'type' => 'VARCHAR',
            'constraint' => '50',
            'NULL' => FALSE,
        ),
        'remarks' => array(
            'type' => 'VARCHAR',
            'constraint' => '250',
            'NULL' => FALSE,
        )
    );

    public function up()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->tbl, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
