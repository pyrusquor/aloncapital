<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Update_canvassing_item_add_company extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('canvassing_items')) {
            if(!$this->db->field_exists('company_id', 'canvassing_items')){
                $this->db->query("ALTER TABLE `canvassing_items` ADD COLUMN `company_id` INT(11) DEFAULT 0 NULL");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('canvassing_items')){
            if($this->db->field_exists('company_id', 'canvassing_items')){
                $this->dbforge->drop_column('canvassing_items', 'company_id');
            }
        }
    }
}
