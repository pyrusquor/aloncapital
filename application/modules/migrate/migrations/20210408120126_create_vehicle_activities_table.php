<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_vehicle_activities_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('vehicle_activities')) {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'activity_type_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'activity_number' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'driver_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'date' => array(
                    'type'=>'DATE',
                    'NULL'=> TRUE,
                ),
                'vehicle_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'fleet' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
            
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('vehicle_activities', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('vehicle_activities') ) {

            $this->dbforge->drop_table('vehicle_activities');
        }
    }
}
