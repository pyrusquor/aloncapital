<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_past_due_notices_table_add_columns extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transaction_past_due_notices')) {

            if (!$this->db->field_exists('is_email_sent', 'transaction_past_due_notices')) {
                $this->db->query("ALTER TABLE `transaction_past_due_notices` ADD COLUMN `is_email_sent` INT(11) NULL DEFAULT 0 AFTER `is_void`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transaction_past_due_notices')) {

            if ($this->db->field_exists('is_email_sent', 'transaction_past_due_notices')) {
                $this->dbforge->drop_column('transaction_past_due_notices', 'is_email_sent');
            }
        }
    }
}
