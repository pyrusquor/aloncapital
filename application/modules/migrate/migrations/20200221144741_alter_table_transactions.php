<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_transactions extends CI_Migration
{

    public function up()
    {
	     $this->db->query("ALTER TABLE `transactions` CHANGE `buyer_id` `seller_id` int(11) unsigned NOT NULL AFTER `buyer_id`, ADD FOREIGN KEY (`seller_id`) REFERENCES `sellers` (`id`)");
	     $this->db->query("ALTER TABLE `transaction_properties` ADD `lot_area` double(20,2) NOT NULL AFTER `property_id`, ADD `floor_area` double(20,2) NOT NULL AFTER `lot_area`, ADD `lot_price_per_sqm` double(20,2) NOT NULL AFTER `floor_area`");
	     $this->db->query("ALTER TABLE `transactions` CHANGE `reference` `reference` varchar(50) COLLATE 'utf8_general_ci' NOT NULL AFTER `id`, ADD `expiration_date` datetime NOT NULL AFTER `financing_scheme_id`");
         $this->db->query("ALTER TABLE `transaction_fees` CHANGE `fess_amount_rate` `fees_amount_rate` double(20,2) NOT NULL AFTER `fees_percentage_rate`");
         $this->db->query("ALTER TABLE `transactions` ADD `property_id` int(11) unsigned NOT NULL AFTER `seller_id`, ADD `reservation_date` datetime NOT NULL AFTER `financing_scheme_id`");
         $this->db->query("ALTER TABLE `transactions` ADD `project_id` int(11) unsigned NOT NULL AFTER `seller_id`");
         $this->db->query("ALTER TABLE `transactions` ADD `remarks` varchar(255) NOT NULL AFTER `expiration_date`");
	     $this->db->query("ALTER TABLE `transaction_payments` ADD `due_date` datetime NOT NULL AFTER `is_paid`, ADD `next_payment_date` datetime NOT NULL AFTER `due_date`");
         
    }

    public function down()
    {

    }
}
