<?php defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_documents extends CI_Migration {

	function up () {

		if ( $this->db->table_exists('documents') ) {

			$this->dbforge->drop_table('documents');
		}

		$_fields	=	array(
									'id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => TRUE,
                    'comment'	=>	'documents Table ID'
									),
									'name'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => FALSE,
                    'comment'	=>	'documents name'
									),
									'description'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'documents description'
									),
									'classification_id'	=>	array(
										'type'	=>	'INT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'documents classification_id'
									),
									'lead_time'	=>	array(
										'type'	=>	'INT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'documents lead_time'
									),
									'owner_id'	=>	array(
										'type'	=>	'INT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'documents owner_id'
									),
									'processor'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'documents processor'
									),
									'is_retention'	=>	array(
										'type'	=>	'TINYINT',
										'constraint' => '1',
                    'NULL' => TRUE,
                    'comment'	=>	'documents is_retention 1:TRUE|FALSE:0'
									),
									'is_need_hard_copy'	=>	array(
										'type'	=>	'TINYINT',
										'constraint' => '1',
                    'NULL' => TRUE,
                    'comment'	=>	'documents is_need_hard_copy 1:TRUE|FALSE:0'
									),
									'access_right_id'	=>	array(
										'type'	=>	'INT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'documents access_right_id'
									),
									'security_classification'	=>	array(
										'type'	=>	'INT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'documents security_classification'
									),
									'issuing_party'	=>	array(
										'type'	=>	'INT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'documents issuing_party'
									),
									'relevant_policies'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'documents relevant_policies'
									),
	                'created_by'	=>	array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'documents created_by'
	                ),
	                'created_at' => array(
	                	'type'	=>	'DATETIME',
	                	'NULL'	=>	TRUE,
                    'comment'	=>	'documents created_at'
	                ),
	                'updated_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'documents updated_by'
	                ),
	                'updated_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'documents updated_at'
	                ),
	                'deleted_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'documents deleted_by'
	                ),
	                'deleted_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'documents deleted_at'
	                )
								);

		$this->dbforge->add_field($_fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('documents', TRUE);

		// $this->load->model('document/Document_model', 'M_document');
		// $this->M_document->insert_dummy();
	}

	function down () {

		if ( $this->db->table_exists('documents') ) {

			$this->dbforge->drop_table("documents");
		}
	}
}