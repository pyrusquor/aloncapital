<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_transactions_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('transactions'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'reference' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'period_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'buyer_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'client_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'financing_scheme_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'is_active' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'NULL' => FALSE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('transactions', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('transactions') ) {

			$this->dbforge->drop_table('transactions');
		}
    }
}
