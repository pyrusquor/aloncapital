<?php defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_land_inventories_project_id_to_project extends CI_Migration {

	function up () {

		if( $this->db->table_exists('land_inventories') ) {

			if ( $this->db->field_exists('project_id', 'land_inventories') ) {

				$this->dbforge->drop_column('land_inventories', 'project_id');
			}

      if ( !$this->db->field_exists('project', 'land_inventories') ) {

      	$this->db->query("ALTER TABLE `land_inventories` ADD COLUMN `project` TEXT DEFAULT NULL COMMENT 'land_inventories project' AFTER `number_of_lot_to_subdivide`");
      }
    }
	}

	function down () {

		if ( $this->db->field_exists('project', 'land_inventories') ) {

			$this->dbforge->drop_column('land_inventories', 'project');
		}
	}
}