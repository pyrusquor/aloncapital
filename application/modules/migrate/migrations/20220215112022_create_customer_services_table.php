<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_customer_services_table extends CI_Migration
{

    private $table = "customer_services";
    private $fields = array(

        "id" => array(
            "type" => "INT",
            "constraint" => "11",
            "unsigned" => TRUE,
            "auto_increment" => TRUE,
            "NOT NULL" => TRUE,
        ),

        'reference' => array(
            "type" => "VARCHAR",
            'constraint' => '13',
            "NULL" => TRUE,
        ),

        'landline' => array(
            "type" => "VARCHAR",
            'constraint' => '50',
            "NULL" => TRUE,
        ),

        'mobile' => array(
            "type" => "VARCHAR",
            'constraint' => '50',
            "NULL" => TRUE,
        ),

        'email' => array(
            "type" => "VARCHAR",
            'constraint' => '100',
            "NULL" => TRUE,
        ),

        'project_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'property_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'phase' => array(
            "type" => "VARCHAR",
            'constraint' => '100',
            "NULL" => TRUE,
        ),

        'block' => array(
            "type" => "VARCHAR",
            'constraint' => '100',
            "NULL" => TRUE,
        ),

        'lot' => array(
            "type" => "VARCHAR",
            'constraint' => '100',
            "NULL" => TRUE,
        ),

        'house_model_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'buyer_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'department_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "turn_over_date" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        "warranty_date" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        'warranty' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "constraint" => "1",
        ),

        'category_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'description' => array(
            "type" => "VARCHAR",
            'constraint' => '254',
            "NULL" => TRUE,
        ),

        'notes' => array(
            "type" => "TEXT",
            "NULL" => TRUE,
        ),

        'status' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "constraint" => "2",
            "DEFAULT" => 1
        ),

        'sub_status' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "constraint" => "2",
        ),

        "closed_on" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        'forwarded_to' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "forwarded_on" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        'forwarded_status' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
            "constraint" => "2",
        ),

        "forward_completion_date" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        'transaction_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        // File info
        'file_path' => array(
            'type' => 'TEXT',
            'NULL' => TRUE,
        ),

        // Audit Info
        "created_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "created_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        "updated_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "updated_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        "deleted_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "deleted_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

    );

    public function up()
    {
        if (!$this->db->table_exists($this->table)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->table, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->table)) {
            $this->dbforge->drop_table($this->table);
        }
    }
}
