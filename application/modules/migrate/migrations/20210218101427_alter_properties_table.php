<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_properties_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('properties')) {
            if(!$this->db->field_exists('adjusted_price', 'properties')) {
                $this->db->query("ALTER TABLE `properties` ADD COLUMN `adjusted_price` INT(11) DEFAULT 0 AFTER `total_selling_price`");
            }
       }
    }

    public function down()
    {
        if($this->db->table_exists('properties')) {
            $this->dbforge->drop_column('properties', 'adjusted_price');
        }
    }
}
