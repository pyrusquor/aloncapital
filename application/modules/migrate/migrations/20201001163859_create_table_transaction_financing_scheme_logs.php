<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_transaction_financing_scheme_logs extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('transaction_financing_scheme_logs'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'transaction_id' => array(
                    'type' => 'int',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'prev_financing_scheme_id' => array(
                    'type' => 'int',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'new_financing_scheme_id' => array(
                    'type' => 'int',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'period_amount' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20, 2',
                    'NULL' => FALSE,
                ),
                'interest_rate' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20, 6',
                    'NULL' => FALSE,
                ),
                'period_term' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE,
                ),
                'effectivity_date' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'period_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE,
                ),
                'remarks' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
			
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('transaction_financing_scheme_logs', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('transaction_financing_scheme_logs') ) {

			$this->dbforge->drop_table('transaction_financing_scheme_logs');
        }
    }
}
