<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_setting_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('accounting_settings')) {
            $this->dbforge->drop_column('accounting_settings', 'name');
            $this->dbforge->drop_column('accounting_settings', 'description');
        }
        $fields = array(
            'origin_id' => array(
                'name' => 'period_id',
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => false
            ),
        );

        if($this->db->table_exists('accounting_settings')) {
            $this->dbforge->modify_column('accounting_settings', $fields);
       }
    }

    public function down()
    {

        $fields = array(
            'period_id' => array(
                'name' => 'origin_id',
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false
            ),
        );

        if($this->db->table_exists('accounting_settings')) {
            $this->dbforge->modify_column('accounting_settings', $fields);
       }

       if($this->db->table_exists('accounting_settings')) {
        if(!$this->db->field_exists('name', 'accounting_settings')) {
             $this->db->query("ALTER TABLE `accounting_settings` ADD COLUMN `name` VARCHAR(80) AFTER `code`");
        }
        if(!$this->db->field_exists('description', 'accounting_settings')) {
             $this->db->query("ALTER TABLE `accounting_settings` ADD COLUMN `description` VARCHAR(255) AFTER `code`");
        }

    }

    }
}
