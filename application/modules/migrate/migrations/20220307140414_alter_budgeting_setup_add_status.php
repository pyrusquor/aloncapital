<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_budgeting_setup_add_status extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('status', 'budgeting_setup')) {
            $this->db->query("ALTER TABLE `budgeting_setup` ADD `status` int(1) NULL AFTER `year`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('status', 'budgeting_setup')) {
            $this->db->query("ALTER TABLE `budgeting_setup` DROP COLUMN `status`");
        }
    }
}
