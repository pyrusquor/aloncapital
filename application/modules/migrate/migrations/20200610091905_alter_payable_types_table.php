<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_payable_types_table extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('created_at', 'payable_types')) {
            $this->db->query("ALTER TABLE `payable_types` ADD COLUMN `created_at` datetime NULL AFTER `content`");
        }
        if (!$this->db->field_exists('created_by', 'payable_types')) {
            $this->db->query("ALTER TABLE `payable_types` ADD COLUMN `created_by` INT(11) NULL AFTER `created_at`");
        }
        if (!$this->db->field_exists('updated_at', 'payable_types')) {
            $this->db->query("ALTER TABLE `payable_types` ADD COLUMN `updated_at` datetime NULL AFTER `created_by`");
        }
        if (!$this->db->field_exists('updated_by', 'payable_types')) {
            $this->db->query("ALTER TABLE `payable_types` ADD COLUMN `updated_by` INT(11) NULL AFTER `updated_at`");
        }
        if (!$this->db->field_exists('deleted_at', 'payable_types')) {
            $this->db->query("ALTER TABLE `payable_types` ADD COLUMN `deleted_at` datetime NULL AFTER `updated_by`");
        }
        if (!$this->db->field_exists('deleted_by', 'payable_types')) {
            $this->db->query("ALTER TABLE `payable_types` ADD COLUMN `deleted_by` INT(11) NULL AFTER `deleted_at`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('created_at', 'payable_types')) {
            $this->dbforge->drop_column('payable_types', 'created_at');
        }
        if ($this->db->field_exists('created_by', 'payable_types')) {
            $this->dbforge->drop_column('payable_types', 'created_by');
        }
        if ($this->db->field_exists('updated_at', 'payable_types')) {
            $this->dbforge->drop_column('payable_types', 'updated_at');
        }
        if ($this->db->field_exists('updated_by', 'payable_types')) {
            $this->dbforge->drop_column('payable_types', 'updated_by');
        }
        if ($this->db->field_exists('deleted_at', 'payable_types')) {
            $this->dbforge->drop_column('payable_types', 'deleted_at');
        }
        if ($this->db->field_exists('deleted_by', 'payable_types')) {
            $this->dbforge->drop_column('payable_types', 'deleted_by');
        }
    }
}
