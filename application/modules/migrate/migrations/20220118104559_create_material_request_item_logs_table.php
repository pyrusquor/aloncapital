<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_material_request_item_logs_table extends CI_Migration
{

    private $table = "material_request_item_logs";
    private $fields = array(

        "id" => array(
            "type" => "INT",
            "constraint" => "11",
            "unsigned" => TRUE,
            "auto_increment" => TRUE,
            "NOT NULL" => TRUE,
        ),

        'item_brand_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'item_class_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'item_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'unit_of_measurement_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'material_request_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'quantity' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'unit_cost' => array(
            'type' => 'DOUBLE',
            'constraint' => '20,2',
            'NULL' => FALSE
        ),

        'total_cost' => array(
            'type' => 'DOUBLE',
            'constraint' => '20,2',
            'NULL' => FALSE
        ),

        'request_reason' => array(
            "type" => "VARCHAR",
            'constraint' => '255',
            "NULL" => TRUE,
        ),

        'item_group_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'item_type_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'description' => array(
            "type" => "VARCHAR",
            'constraint' => '255',
            "NULL" => TRUE,
        ),

        "log_time" => array(
            "type" => "DATETIME",
            "NOT NULL" => TRUE,
        ),

        'material_request_item_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        // Audit Info
        "created_by" => array(
            "type" => "INT",
            "NOT NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "created_at" => array(
            "type" => "DATETIME",
            "NOT NULL" => TRUE,
        ),

        "updated_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "updated_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        "deleted_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "deleted_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

    );

    public function up()
    {
        if (!$this->db->table_exists($this->table)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->table, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->table)) {
            $this->dbforge->drop_table($this->table);
        }
    }
}
