<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_laborers_add_rates extends CI_Migration
{

    public function up()
    {

        if (!$this->db->field_exists('day_rate', 'laborers')) {
            $this->db->query("ALTER TABLE `laborers` ADD `day_rate` double(20,2) null after `designation`");
        }
        if (!$this->db->field_exists('hour_rate', 'laborers')) {
            $this->db->query("ALTER TABLE `laborers` ADD `hour_rate` double(20,2) null after `designation`");
        }
    }

    public function down()
    {

        if ($this->db->field_exists('day_rate', 'laborers')) {
            $this->db->query("ALTER TABLE `laborers` DROP COLUMN `day_rate`");
        }
        if ($this->db->field_exists('hour_rate', 'laborers')) {
            $this->db->query("ALTER TABLE `laborers` DROP COLUMN `hour_rate`");
        }
    }
}
