<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_tables extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('construction_orders')) {
            $this->dbforge->drop_column('construction_orders', 'lot');
        }

        $fields = array(
            'block' => array(
                'name' => 'property_id',
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => false
            ),
        );

        if($this->db->table_exists('construction_orders')) {
            $this->dbforge->modify_column('construction_orders', $fields);
       }
    }

    public function down()
    {
        $fields = array(
            'property_id' => array(
                'name' => 'block',
                'type' => 'VARCHAR',
                'constraint' => '5',
                'NULL' => false
            ),
        );

        if($this->db->table_exists('construction_orders')) {
            $this->dbforge->modify_column('construction_orders', $fields);
        }

        if($this->db->table_exists('construction_orders')) {
            if(!$this->db->field_exists('lot', 'construction_orders')) {
                $this->db->query("ALTER TABLE `construction_orders` ADD COLUMN `lot` VARCHAR(5) AFTER `block`");
            }
        }
    }
}
