<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Update_defaults_multiple_tables extends CI_Migration
{

    public function up()
    {
       if($this->db->table_exists('material_requests')) {
           if($this->db->field_exists('request_status', 'material_requests')){
               $this->db->query("ALTER TABLE `material_requests` CHANGE COLUMN `request_status` `request_status` INT(2) NOT NULL DEFAULT '1'");
           }
       }
        if($this->db->table_exists('purchase_order_requests')) {
            if($this->db->field_exists('request_status', 'purchase_order_requests')){
                $this->db->query("ALTER TABLE `purchase_order_requests` CHANGE COLUMN `request_status` `request_status` INT(2) NOT NULL DEFAULT '1'");
            }
        }
        if($this->db->table_exists('canvassings')) {
            if($this->db->field_exists('status', 'canvassings')){
                $this->db->query("ALTER TABLE `canvassings` CHANGE COLUMN `status` `status` INT(2) NOT NULL DEFAULT '1'");
            }
        }
        if($this->db->table_exists('purchase_orders')) {
            if($this->db->field_exists('status', 'purchase_orders')){
                $this->db->query("ALTER TABLE `purchase_orders` CHANGE COLUMN `status` `status` INT(2) NOT NULL DEFAULT '1'");
            }
        }
    }

    public function down()
    {

    }
}
