<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_accounting_entries extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('accounting_entries')) {
            if ($this->db->field_exists('payee_type', 'accounting_entries')) {

                $this->dbforge->drop_column('accounting_entries', 'payee_type');
            }
        }
        if ($this->db->table_exists('accounting_entries')) {

            if (!$this->db->field_exists('payee_type', 'accounting_entries')) {

                $this->db->query("ALTER TABLE `accounting_entries` ADD COLUMN `payee_type` varchar(50) DEFAULT NULL  AFTER `payment_date`");
            }

        }

    }

    public function down()
    {

        if ($this->db->table_exists('accounting_entries')) {

            if (!$this->db->field_exists('payee_type', 'accounting_entries')) {

                $this->db->query("ALTER TABLE `accounting_entries` ADD COLUMN `payee_type` varchar(50) DEFAULT NULL  AFTER `payment_date`");
            }
        }

        if ($this->db->field_exists('payee_type', 'accounting_entries')) {

            $this->dbforge->drop_column('accounting_entries', 'payee_type');
        }

    }
}
