<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_accounting_entries extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => true,
                'NOT NULL' => false,
            ),
            'or_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '12',
                'NULL' => true,
            ),
            'invoice_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '12',
                'NULL' => true,
            ),
            'journal_type' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true,
            ),
            'payment_date' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'payee_type' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true,
            ),
            'payee_type_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true,
            ),
            'remarks' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => true,
            ),
            'cr_total' => array(
                'type' => 'decimal',
                'constraint' => '15,2',
                'NULL' => false,
                'DEFAULT' => "0.00",
            ),
            'dr_total' => array(
                'type' => 'decimal',
                'constraint' => '15,2',
                'NULL' => false,
                'DEFAULT' => "0.00",
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'created_by' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('accounting_entries', true);

        // $this->load->model('accounting_entries/Accounting_entries_model', 'M_accounting_entries');
        // $this->M_accounting_entries->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('accounting_entries')) {

            $this->dbforge->drop_table('accounting_entries');
        }
    }
}
