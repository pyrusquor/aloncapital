<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transactions_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transactions')) {
            if (!$this->db->field_exists('is_recognized', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` ADD COLUMN `is_recognized` int(1) DEFAULT NULL  AFTER `id`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transactions')) {
            if ($this->db->field_exists('is_recognized', 'transactions')) {
                $this->dbforge->drop_column('transactions', 'is_recognized');
            }
        }
    }
}
