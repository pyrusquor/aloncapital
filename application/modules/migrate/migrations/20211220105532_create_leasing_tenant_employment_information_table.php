<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_leasing_tenant_employment_information_table extends CI_Migration
{

    private $table = "leasing_tenant_employment_information";
    private $fields = array(

        "id" => array(
            "type" => "INT",
            "constraint" => "11",
            "unsigned" => TRUE,
            "auto_increment" => TRUE,
            "NOT NULL" => TRUE,
        ),

        'leasing_tenant_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'occupation_type_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'industry_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'occupation_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'designation' => array(
            "type" => "VARCHAR",
            'constraint' => '255',
            "NULL" => TRUE,
        ),

        'employer' => array(
            "type" => "VARCHAR",
            'constraint' => '255',
            "NULL" => TRUE,
        ),

        'gross_salary' => array(
            "type" => "FLOAT",
            "NULL" => TRUE,
        ),

        'location_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'address' => array(
            "type" => "VARCHAR",
            'constraint' => '255',
            "NULL" => TRUE,
        ),

        // Audit Info
        "created_by" => array(
            "type" => "INT",
            "NOT NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "created_at" => array(
            "type" => "DATETIME",
            "NOT NULL" => TRUE,
        ),

        "updated_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "updated_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        "deleted_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "deleted_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

    );

    public function up()
    {
        if (!$this->db->table_exists($this->table)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->table, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->table)) {
            $this->dbforge->drop_table($this->table);
        }
    }
}
