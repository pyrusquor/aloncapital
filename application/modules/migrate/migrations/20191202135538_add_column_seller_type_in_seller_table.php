<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Add_column_seller_type_in_seller_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('sellers'))
		{	
			if (!$this->db->field_exists('seller_type', 'sellers'))
			{
				$this->db->query("ALTER TABLE `sellers` ADD COLUMN `seller_type` VARCHAR(50) NULL AFTER `sales_group_id`");
			}
		}
    }

    public function down()
    {
        if($this->db->table_exists('sellers'))
		{
			if ($this->db->field_exists('seller_type', 'sellers'))
			{
				$this->dbforge->drop_column('sellers', 'seller_type');
			}
		}
    }
}
