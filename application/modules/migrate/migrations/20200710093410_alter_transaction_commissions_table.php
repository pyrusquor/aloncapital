<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_commissions_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transaction_commissions')) {

            if (!$this->db->field_exists('payment_request_id', 'transaction_commissions')) {

                $this->db->query("ALTER TABLE `transaction_commissions` ADD COLUMN `payment_request_id` INT(11) DEFAULT NULL  AFTER `remarks`");
            }

            if (!$this->db->field_exists('payment_voucher_id', 'transaction_commissions')) {

                $this->db->query("ALTER TABLE `transaction_commissions` ADD COLUMN `payment_voucher_id` INT(11) DEFAULT NULL  AFTER `payment_request_id`");
            }

        }
    }

    public function down()
    {
        if ($this->db->field_exists('payment_request_id', 'transaction_commissions')) {

            $this->dbforge->drop_column('transaction_commissions', 'payment_request_id');
        }

        if ($this->db->field_exists('payment_voucher_id', 'transaction_commissions')) {

            $this->dbforge->drop_column('transaction_commissions', 'payment_voucher_id');
        }
    }
}
