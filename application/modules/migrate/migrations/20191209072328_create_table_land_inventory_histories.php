<?php defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_land_inventory_histories extends CI_Migration {

	function up () {

		if ( $this->db->table_exists('land_inventory_histories') ) {

			$this->dbforge->drop_table('land_inventory_histories');
		}

		$_fields	=	array(
									'id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => TRUE,
                    'comment'	=>	'land_inventory_histories Table ID'
									),
									'land_inventory_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories Table land_inventory_id'
									),
									'consolidate_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories Table ID|consolidate_id'
									),
									'subdivide_id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventories Table ID|subdivide_id'
									),
									'number_of_lot_to_subdivide'	=>	array(
										'type'	=>	'TEXT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventory_histories number_of_lot_to_subdivide'
									),
									'project'	=>	array(
										'type'	=>	'TEXT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'land_inventory_histories project'
									),
	                'created_by'	=>	array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventory_histories created_by'
	                ),
	                'created_at' => array(
	                	'type'	=>	'DATETIME',
	                	'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventory_histories created_at'
	                ),
	                'updated_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventory_histories updated_by'
	                ),
	                'updated_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventory_histories updated_at'
	                ),
	                'deleted_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventory_histories deleted_by'
	                ),
	                'deleted_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'land_inventory_histories deleted_at'
	                )
								);

		$this->dbforge->add_field($_fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('land_inventory_histories', TRUE);
	}

	function down () {

		if ( $this->db->table_exists('land_inventory_histories') ) {

			$this->dbforge->drop_table("land_inventory_histories");
		}
	}
}