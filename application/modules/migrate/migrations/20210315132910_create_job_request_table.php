<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_job_request_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('job_requests')) {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'number' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE,
                ),
                'company_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'project_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'description' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE,
                ),
                'staff_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'status' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
            
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('job_requests', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('job_requests') ) {

            $this->dbforge->drop_table('job_requests');
        }
    }
}
