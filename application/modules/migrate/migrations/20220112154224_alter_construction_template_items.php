<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_construction_template_items extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('stage_id', 'construction_template_items')) {
            $this->db->query("ALTER TABLE construction_template_items ADD `stage_id` int(11) null AFTER `item_class_id`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('stage_id', 'construction_template_items')) {
            $this->db->query("ALTER TABLE construction_template_items DROP COLUMN `stage_id`");
        }
    }
}
