<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_tables extends CI_Migration
{

    public function up()
    {

        if ($this->db->table_exists('accounting_entries')) {

            if (!$this->db->field_exists('company_id', 'accounting_entries')) {

                $this->db->query("ALTER TABLE `accounting_entries` ADD COLUMN `company_id` INT(11) DEFAULT NULL  AFTER `id`");
            }
            
        }

        if (!$this->db->table_exists('accounting_ledger_options')) {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NOT NULL' => false,
                ),
                'company_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => true,
                ),
                'ledger_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => true,
                ),
                'op_balance' => array(
                    'type' => 'DECIMAL',
                    'constraint' => '15,2',
                    'NULL' => false,
                ),
                'op_balance_dc' => array(
                    'type' => 'CHAR',
                    'constraint' => '1',
                    'NULL' => false,
                ),
                'reconciliation' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => false,
                ),
                'non_operating' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => false,
                ),
                'created' => array(
                    'type' => 'datetime',
                    'NULL' => false,
                ),
                'updated' => array(
                    'type' => 'datetime',
                    'NULL' => true,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('accounting_ledger_options', true);

        }


        if (!$this->db->table_exists('accounting_reports')) {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NOT NULL' => false,
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => false,
                ),
                'description' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => false,
                ),
                'filters' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => false,
                ),
                'signatory' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => false,
                ),
                'slug' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => false,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('accounting_reports', true);

        }
        if (!$this->db->table_exists('accounting_report_logs')) {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NOT NULL' => false,
                ),
                'accounting_report_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'filters' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => false,
                ),
                'signatory' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => false,
                ),
                'slug' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => false,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('accounting_report_logs', true);

        }
        if (!$this->db->table_exists('sales_checklists')) {

            $fields = array(
                'id' => array(
                    'type' => 'BIGINT',
                    'constraint' => '20',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NOT NULL' => false,
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => true,
                ),
                'category_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => true,
                ),
                'description' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => true,
                ),
                'is_active' => array(
                    'type' => 'tinyint',
                    'constraint' => '1',
                    'NULL' => true,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('sales_checklists', true);

        }
        if (!$this->db->table_exists('sales_checklist_stages')) {

            $fields = array(
                'id' => array(
                    'type' => 'int',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NOT NULL' => false,
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => false,
                ),
                'category_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'description' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => false,
                ),
                'sales_checklist_id' => array(
                    'type' => 'bigint',
                    'constraint' => '20',
                    'unsigned' => true,
                    'NULL' => false,
                ),
                'order' => array(
                    'type' => 'int',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('sales_checklist_stages', true);

        }
        if (!$this->db->table_exists('sales_checklist_stages_documents')) {

            $fields = array(
                'id' => array(
                    'type' => 'bigint',
                    'constraint' => '20',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NOT NULL' => false,
                ),
                'sales_checklist_stage_id' => array(
                    'type' => 'bigint',
                    'constraint' => '20',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'document_id' => array(
                    'type' => 'bigint',
                    'constraint' => '20',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'required_copies' => array(
                    'type' => 'int',
                    'constraint' => '10',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'days_to_finish' => array(
                    'type' => 'int',
                    'constraint' => '10',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'days_to_expire' => array(
                    'type' => 'int',
                    'constraint' => '10',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'is_required' => array(
                    'type' => 'tinyint',
                    'constraint' => '1',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'upload_document_id' => array(
                    'type' => 'bigint',
                    'constraint' => '20',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'category_id' => array(
                    'type' => 'bigint',
                    'constraint' => '20',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'start_date' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'owner_id' => array(
                    'type' => 'bigint',
                    'constraint' => '20',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'group_id' => array(
                    'type' => 'bigint',
                    'constraint' => '20',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'dependency_id' => array(
                    'type' => 'bigint',
                    'constraint' => '20',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'is_required' => array(
                    'type' => 'tinyint',
                    'constraint' => '1',
                    "NULL" => false
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('sales_checklist_stages_documents', true);

        }
       
    }

    public function down()
    {

        if ($this->db->table_exists('accounting_entries')) {
            if ($this->db->field_exists('company_id', 'accounting_entries')) {
    
                $this->dbforge->drop_column('accounting_entries', 'company_id');
            }
        }

        if ($this->db->table_exists('accounting_ledger_options')) {

            $this->dbforge->drop_table('accounting_ledger_options');
        }
        if ($this->db->table_exists('accounting_reports')) {

            $this->dbforge->drop_table('accounting_reports');
        }
        if ($this->db->table_exists('accounting_report_logs')) {

            $this->dbforge->drop_table('accounting_report_logs');
        }
        if ($this->db->table_exists('sales_checklists')) {

            $this->dbforge->drop_table('sales_checklists');
        }
        if ($this->db->table_exists('sales_checklist_stages')) {

            $this->dbforge->drop_table('sales_checklist_stages');
        }
        if ($this->db->table_exists('sales_checklist_stages_documents')) {

            $this->dbforge->drop_table('sales_checklist_stages_documents');
        }


    }
}
