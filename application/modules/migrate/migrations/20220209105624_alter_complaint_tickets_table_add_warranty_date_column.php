<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_complaint_tickets_table_add_warranty_date_column extends CI_Migration
{

    public function up()
    {

        if (!$this->db->field_exists('warranty_date', 'complaint_tickets')) {
            $this->db->query("ALTER TABLE `complaint_tickets` ADD `warranty_date` DATETIME NULL AFTER `turn_over_date`");
        }
    }

    public function down()
    {

        if ($this->db->field_exists('warranty_date', 'complaint_tickets')) {
            $this->db->query("ALTER TABLE `complaint_tickets` DROP COLUMN `warranty_date`");
        }
    }
}
