<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_payment_request_table extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('status', 'payment_requests')) {
            $this->db->query("ALTER TABLE `payment_requests` ADD COLUMN `status` int(1) DEFAULT 0 after is_complete");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('status', 'payment_requests')) {
            $this->dbforge->drop_column('payment_requests', 'status');
        }
    }
}
