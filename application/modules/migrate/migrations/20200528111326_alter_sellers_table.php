<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_sellers_table extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('regCode', 'sellers')) {
            $this->db->query("ALTER TABLE `sellers` ADD COLUMN `regCode` INT(11) NULL AFTER `commission_rate`");
        }

        if (!$this->db->field_exists('provCode', 'sellers')) {
            $this->db->query("ALTER TABLE `sellers` ADD COLUMN `provCode` INT(11) NULL AFTER `regCode`");
        }

        if (!$this->db->field_exists('citymunCode', 'sellers')) {
            $this->db->query("ALTER TABLE `sellers` ADD COLUMN `citymunCode` INT(11) NULL AFTER `provCode`");
        }

        if (!$this->db->field_exists('brgyCode', 'sellers')) {
            $this->db->query("ALTER TABLE `sellers` ADD COLUMN `brgyCode` INT(11) NULL AFTER `citymunCode`");
        }

        if (!$this->db->field_exists('social_media', 'sellers')) {

            $this->db->query("ALTER TABLE `sellers` ADD COLUMN `social_media` VARCHAR(255) DEFAULT NULL  AFTER `commission_rate`");
        }

        if (!$this->db->field_exists('civil_status_id', 'sellers')) {

            $this->db->query("ALTER TABLE `sellers` ADD COLUMN `civil_status_id` VARCHAR(11) DEFAULT NULL  AFTER `social_media`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('regCode', 'sellers')) {
            $this->dbforge->drop_column('sellers', 'regCode');
        }
        if ($this->db->field_exists('provCode', 'sellers')) {
            $this->dbforge->drop_column('sellers', 'provCode');
        }
        if ($this->db->field_exists('citymunCode', 'sellers')) {
            $this->dbforge->drop_column('sellers', 'citymunCode');
        }
        if ($this->db->field_exists('brgyCode', 'sellers')) {
            $this->dbforge->drop_column('sellers', 'brgyCode');
        }
        if ($this->db->field_exists('social_media', 'sellers')) {
            $this->dbforge->drop_column('sellers', 'social_media');
        }
        if ($this->db->field_exists('civil_status_id', 'sellers')) {
            $this->dbforge->drop_column('sellers', 'civil_status_id');
        }
    }
}
