<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_budget_setup_table extends CI_Migration
{
    private $tbl = "budgeting_setup";

    private $fields = array(
        'id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'auto_increment' => TRUE,
            'NOT NULL' => TRUE,
        ),
        'project_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'property_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'land_inventory_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'department_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'ledger_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'amount' => array(
            'type' => 'decimal',
            'constraint' => '15,2',
            'NULL' => false,
            'DEFAULT' => "0.00",
        ),
        'month' => array(
            'type' => 'INT',
            'constraint' => '2',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'year' => array(
            'type' => 'INT',
            'constraint' => '4',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'created_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'created_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'updated_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'updated_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
        'deleted_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE
        ),
        'deleted_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE
        ),
    );

    public function up()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_key('project_id');
            $this->dbforge->add_key('property_id');
            $this->dbforge->add_key('land_inventory_id');
            $this->dbforge->add_key('department_id');
            $this->dbforge->add_key('ledger_id');
            $this->dbforge->create_table($this->tbl, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
