<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_official_payment extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transaction_official_payments')) {
            if(!$this->db->field_exists('period_count', 'transaction_official_payments')) {
                $this->db->query("ALTER TABLE `transaction_official_payments` ADD COLUMN `period_count` INT(11) DEFAULT 1 NOT NULL AFTER `payment_date`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transaction_official_payments')) {
            $this->dbforge->drop_column('transaction_official_payments', 'period_count');
        }
    }
}
