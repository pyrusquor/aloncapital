<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_discount_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transaction_discounts')) {
            if(!$this->db->field_exists('deduct_to', 'transaction_discounts')) {
                $this->db->query("ALTER TABLE `transaction_discounts` ADD COLUMN `deduct_to` INT(11) NULL AFTER `discount_amount_rate`");
            }

            if(!$this->db->field_exists('remarks', 'transaction_discounts')) {
                $this->db->query("ALTER TABLE `transaction_discounts` ADD COLUMN `remarks` VARCHAR(255) NULL AFTER `deduct_to`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transaction_discounts')) {
            $this->dbforge->drop_column('transaction_discounts', 'deduct_to');
            $this->dbforge->drop_column('transaction_discounts', 'remarks');
        }
    }
}
