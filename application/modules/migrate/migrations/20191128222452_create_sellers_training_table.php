<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_sellers_training_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('seller_trainings'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'seller_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'trainor' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'year' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '4',
                    'NULL' => TRUE
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('seller_trainings', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('seller_trainings') ) {

			$this->dbforge->drop_table('seller_trainings');
		}
    }
}
