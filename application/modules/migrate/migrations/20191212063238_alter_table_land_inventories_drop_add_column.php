<?php defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_land_inventories_drop_add_column extends CI_Migration {

	function up () {

		if( $this->db->table_exists('land_inventories') ) {

			if ( $this->db->field_exists('company', 'land_inventories') ) {

				$this->dbforge->drop_column('land_inventories', 'company');
			}

      if ( !$this->db->field_exists('company_id', 'land_inventories') ) {

      	$this->db->query("ALTER TABLE `land_inventories` ADD COLUMN `company_id` BIGINT DEFAULT NULL COMMENT 'land_inventories company_id | companies ID' AFTER `tax_declaration`");
      }

      if ( !$this->db->field_exists('project_id', 'land_inventories') ) {

      	$this->db->query("ALTER TABLE `land_inventories` ADD COLUMN `project_id` BIGINT DEFAULT NULL COMMENT 'land_inventories project_id | projects ID' AFTER `company_id`");
      }

      if ( !$this->db->field_exists('number_of_lot_to_subdivide', 'land_inventories') ) {

      	$this->db->query("ALTER TABLE `land_inventories` ADD COLUMN `number_of_lot_to_subdivide` INT DEFAULT NULL COMMENT 'land_inventories number_of_lot_to_subdivide' AFTER `lot_number`");
      }
    }
	}

	function down () {

		if ( !$this->db->field_exists('company', 'land_inventories') ) {

    	$this->db->query("ALTER TABLE `land_inventories` ADD COLUMN `company` TEXT DEFAULT NULL COMMENT 'land_inventories company' AFTER `tax_declaration`");
    }

		if ( $this->db->field_exists('company_id', 'land_inventories') ) {

			$this->dbforge->drop_column('land_inventories', 'company_id');
		}

		if ( $this->db->field_exists('project_id', 'land_inventories') ) {

			$this->dbforge->drop_column('land_inventories', 'project_id');
		}

		if ( $this->db->field_exists('number_of_lot_to_subdivide', 'land_inventories') ) {

			$this->dbforge->drop_column('land_inventories', 'number_of_lot_to_subdivide');
		}
	}
}