<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_penalty_logs extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transaction_penalty_logs')) {
            if(!$this->db->field_exists('transaction_payment_id', 'transaction_penalty_logs')) {
                $this->db->query("ALTER TABLE `transaction_penalty_logs` ADD COLUMN `transaction_payment_id` INT(11) NULL AFTER `transaction_official_payment_id`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transaction_penalty_logs')) {
            $this->dbforge->drop_column('transaction_penalty_logs', 'transaction_payment_id');
        }
    }
}
