<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_properties_table_add_columns extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('properties')) {

            if (!$this->db->field_exists('raw_land_cost', 'properties')) {
                $this->db->query("ALTER TABLE `properties` ADD COLUMN `raw_land_cost` VARCHAR(50) AFTER is_active");
            }

            if (!$this->db->field_exists('development_cost', 'properties')) {
                $this->db->query("ALTER TABLE `properties` ADD COLUMN `development_cost` VARCHAR(50) AFTER is_active");
            }

            if (!$this->db->field_exists('construction_in_progress_cost', 'properties')) {
                $this->db->query("ALTER TABLE `properties` ADD COLUMN `construction_in_progress_cost` VARCHAR(50) AFTER is_active");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('properties')) {

            if ($this->db->field_exists('raw_land_cost', 'properties')) {
                $this->dbforge->drop_column('properties', 'raw_land_cost');
            }

            if ($this->db->field_exists('development_cost', 'properties')) {
                $this->dbforge->drop_column('properties', 'development_cost');
            }

            if ($this->db->field_exists('construction_in_progress_cost', 'properties')) {
                $this->dbforge->drop_column('properties', 'construction_in_progress_cost');
            }
        }
    }
}
