<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_audit_trail extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('remarks', 'audit_trail')) {
            $this->db->query("ALTER TABLE `audit_trail` ADD COLUMN `remarks` varchar(1024) NULL AFTER `new_record`");

        }
        if (!$this->db->field_exists('notification_status', 'audit_trail')) {
            $this->db->query("ALTER TABLE `audit_trail` ADD COLUMN `notification_status` int(1) DEFAULT 0 AFTER `remarks`");

        }
    }

    public function down()
    {
        if ($this->db->field_exists('remarks', 'audit_trail')) {
            $this->dbforge->drop_column('audit_trail', 'remarks');
        }

        if ($this->db->field_exists('notification_status', 'audit_trail')) {
            $this->dbforge->drop_column('audit_trail', 'notification_status');
        }
    }
}
