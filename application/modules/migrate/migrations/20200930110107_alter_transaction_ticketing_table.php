<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_ticketing_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transaction_ticketing')) {
            if (!$this->db->field_exists('is_viewed', 'transaction_ticketing')) {
                $this->db->query("ALTER TABLE `transaction_ticketing` ADD COLUMN `is_viewed` int(1) DEFAULT NULL  AFTER `name`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transaction_ticketing')) {
            if ($this->db->field_exists('is_viewed', 'transaction_ticketing')) {
                $this->dbforge->drop_column('transaction_ticketing', 'is_viewed');
            }
        }
    }
}
