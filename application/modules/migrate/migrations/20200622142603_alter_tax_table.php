<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_tax_table extends CI_Migration
{

    public function up()
    {
       $fields = array(
           'tax_name' => array(
               'name' => 'name',
               'type' => 'VARCHAR',
               'constraint' => '250',
               'NULL' => false
           ),
           'tax_description' => array (
               'name' => 'description',
               'type' => 'VARCHAR',
               'constraint' => '255'
           ),
           'tax_code' => array(
               'name' => 'code',
               'type' => 'VARCHAR',
               'constraint' => '255',
               'NULL' => false
           ),
           'tax_rate' => array(
               'name' => 'rate',
               'type' => 'VARCHAR',
               'constraint' => '255',
               'DEFAULT' => '0',
               'NULL' => false
           )
       );

       if ($this->db->table_exists('taxes')) {
           $this->dbforge->modify_column('taxes', $fields);
       }
    }

    public function down()
    {
        $fields = array(
            'name' => array(
                'name' => 'tax_name',
                'type' => 'VARCHAR',
                'constraint' => '250',
                'NULL' => false
            ),
            'description' => array (
                'name' => 'tax_description',
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'code' => array(
                'name' => 'tax_code',
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false
            ),
            'rate' => array(
                'name' => 'tax_rate',
                'type' => 'VARCHAR',
                'constraint' => '255',
                'DEFAULT' => '0',
                'NULL' => false
            )
        );

        if ($this->db->table_exists('taxes')) {
            $this->dbforge->modify_column('taxes', $fields);
        }
    }
}
