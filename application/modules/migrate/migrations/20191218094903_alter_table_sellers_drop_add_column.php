<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_sellers_drop_add_column extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('sellers'))
		{
			if ($this->db->field_exists('seller_position_id', 'sellers'))
			{
				$this->dbforge->drop_column('sellers', 'seller_position_id');
            }
            
        }
        
        if($this->db->table_exists('sellers'))
		{	
			if (!$this->db->field_exists('seller_type', 'sellers'))
			{
				$this->db->query("ALTER TABLE `sellers` ADD COLUMN `seller_type` VARCHAR(11) NOT NULL AFTER `sales_group`");
			}
		}
    }

    public function down()
    {
        if($this->db->table_exists('sellers'))
		{
			if ($this->db->field_exists('seller_type', 'sellers'))
			{
				$this->dbforge->drop_column('sellers', 'seller_type');
			}
		}
    }
}
