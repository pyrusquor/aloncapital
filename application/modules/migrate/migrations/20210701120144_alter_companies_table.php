<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_companies_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('companies')) {

            if (!$this->db->field_exists('footer', 'companies')) {

                $this->db->query("ALTER TABLE `companies` ADD COLUMN `footer` VARCHAR(250) NULL AFTER `image`");
            }
            if (!$this->db->field_exists('header', 'companies')) {

                $this->db->query("ALTER TABLE `companies` ADD COLUMN `header` VARCHAR(250) NULL AFTER `image`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('companies')) {

            if ($this->db->field_exists('header', 'companies')) {

                $this->dbforge->drop_column('companies', 'header');
            }
            if ($this->db->field_exists('footer', 'companies')) {

                $this->dbforge->drop_column('companies', 'footer');
            }
        }
    }
}
