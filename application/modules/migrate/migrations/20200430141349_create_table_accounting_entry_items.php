<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_accounting_entry_items extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => true,
                'NOT NULL' => false,
            ),
            'accounting_entry_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true,
            ),
            'ledger_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true,
            ),
            'ledger_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true,
            ),
            'amount' => array(
                'type' => 'decimal',
                'constraint' => '15,2',
                'NULL' => false,
                'DEFAULT' => "0.00",
            ),
            'dc' => array(
                'type' => 'char',
                'constraint' => '1',
            ),
            'payee_type' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true,
            ),
            'payee_type_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true,
            ),
            'is_reconciled' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'created_by' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('accounting_entry_items', true);

        // $this->load->model('accounting_entries/Accounting_entries_model', 'M_accounting_entries');
        // $this->M_accounting_entries->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('accounting_entry_items')) {

            $this->dbforge->drop_table('accounting_entry_items');
        }
    }
}
