<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_properties_add_null extends CI_Migration
{

    public function up()
    {
       $this->db->query("ALTER TABLE `properties`
		CHANGE `phase` `phase` int(11) NULL AFTER `name`,
		CHANGE `cluster` `cluster` int(11) NULL AFTER `phase`,
		CHANGE `floor_area` `floor_area` double(20,2) NULL AFTER `lot_area`,
		CHANGE `model_price` `model_price` double(20,2) NULL AFTER `total_lot_price`,
		CHANGE `sales_arm_id` `sales_arm_id` int(11) NULL AFTER `progress`,
		CHANGE `tct_number` `tct_number` varchar(50) COLLATE 'utf8_general_ci' NULL AFTER `sales_arm_id`,
		CHANGE `cts_number` `cts_number` varchar(50) COLLATE 'utf8_general_ci' NULL AFTER `tct_number`,
		CHANGE `cts_amount` `cts_amount` double(20,2) NULL AFTER `cts_number`,
		CHANGE `doas_amount` `doas_amount` double(20,2) NULL AFTER `cts_amount`,
		CHANGE `date_registered` `date_registered` datetime NULL AFTER `doas_amount`,
		CHANGE `cts_notarized_date` `cts_notarized_date` datetime NULL AFTER `date_registered`,
		CHANGE `doas_notarized_date` `doas_notarized_date` datetime NULL AFTER `cts_notarized_date`;");
    }

    public function down()
    {

    }
}
