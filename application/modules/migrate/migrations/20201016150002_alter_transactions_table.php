<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transactions_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transactions')) {
            if (!$this->db->field_exists('commission_setup_id', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` ADD COLUMN `commission_setup_id` INT(1) DEFAULT NULL  AFTER `financing_scheme_id`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transactions')) {
            if($this->db->field_exists('commission_setup_id', 'transactions')) {
                $this->dbforge->drop_column('transactions', 'commission_setup_id');
            }
        }
    }
}
