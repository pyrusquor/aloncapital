<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_seller_reservations_table extends CI_Migration
{

    public function up()
    {

        if ($this->db->table_exists('sellers_reservations')) {

            if (!$this->db->field_exists('reservation_status', 'sellers_reservations')) {

                $this->db->query("ALTER TABLE `sellers_reservations` ADD COLUMN `reservation_status` INT(11) DEFAULT NULL  AFTER `id`");
            }

        }
       
    }

    public function down()
    {

        if ($this->db->field_exists('reservation_status', 'sellers_reservations')) {

            $this->dbforge->drop_column('sellers_reservations', 'reservation_status');
        }

    }
}
