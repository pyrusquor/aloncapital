<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_payment_request_items extends CI_Migration
{

    public function up()
    {
        if (!$this->db->table_exists('payment_request_items')) {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NOT NULL' => false,
                ),
                'payment_request_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'item_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '250',
                    'NULL' => false,
                ),
                'unit_price' => array(
                    'type' => 'DECIMAL',
                    'constraint' => '15, 2',
                    'DEFAULT' => '0.00',
                ),
                'quantity' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                    'DEFAULT' => '1'
                ),
                'tax_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => true,
                ),
                'rate' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'default' => '0'
                ),
                'tax_amount' => array(
                    'type' => 'DECIMAL',
                    'constraint' => '15, 2',
                    'DEFAULT' => '0.00',
                ),
                'total_amount' => array(
                    'type' => 'DECIMAL',
                    'constraint' => '15, 2',
                    'DEFAULT' => '0.00',
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('payment_request_items', true);

            // $this->load->model('seller_group/Seller_teams_model', 'seller_group');

            // $this->seller_group->insert_dummy();
        }
    }

    public function down()
    {
        if ($this->db->table_exists('payment_request_items')) {

            $this->dbforge->drop_table('payment_request_items');
        }
    }
}
