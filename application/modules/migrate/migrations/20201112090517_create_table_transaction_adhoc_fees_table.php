<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_transaction_adhoc_fees_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('transaction_adhoc_fees')) {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'transaction_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'fee_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'amount' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20, 2',
                    'NULL' => TRUE,
                ),
                'value_type' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '254',
                    'NULL' => TRUE,
                ),
                'percentage' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20, 2',
                    'NULL'
                ),
                'terms' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'effectivity_date' => array(
                    'type' => 'DATETIME',
                    'NULL' => TRUE,
                ),
                'is_active' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
            
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('transaction_adhoc_fees', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('transaction_adhoc_fees') ) {

            $this->dbforge->drop_table('transaction_adhoc_fees');
        }
    }
}