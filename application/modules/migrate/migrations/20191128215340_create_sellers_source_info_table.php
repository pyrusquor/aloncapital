<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_sellers_source_info_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('seller_source_infos'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'seller_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'recruiter' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'supervisor' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE
                ),
                'net_worth' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'to_report' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'NULL' => TRUE
                ),
                'to_attend' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'NULL' => TRUE
                ),
                'commission_based' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'NULL' => TRUE
                ),
                'is_member' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'NULL' => TRUE
                ),
                'group_id' => array(
                    'type' => 'TINYINT',
                    'constraint' => '11',
                    'NULL' => TRUE
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('seller_source_infos', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('seller_source_infos') ) {

			$this->dbforge->drop_table('seller_source_infos');
		}
    }
}
