<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_projects_add_column extends CI_Migration
{

    public function up()
    {

        if ($this->db->table_exists('projects')) {

            if (!$this->db->field_exists('project_code', 'projects')) {

                $this->db->query("ALTER TABLE `projects` ADD COLUMN `project_code` varchar(50) DEFAULT NULL  AFTER `name`");
            }

        }

    }

    public function down()
    {

        if ($this->db->field_exists('project_code', 'projects')) {

            $this->dbforge->drop_column('projects', 'project_code');
        }

    }
}
