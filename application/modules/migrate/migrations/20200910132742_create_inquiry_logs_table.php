<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_inquiry_logs_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('inquiry_ticket_logs'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'inquiry_id' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => TRUE
                ),
                'remarks' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '1024',
                    'NULL' => TRUE
                ),
                'status' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => TRUE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
			
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('inquiry_ticket_logs', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('inquiry_ticket_logs') ) {

			$this->dbforge->drop_table('inquiry_ticket_logs');
        }
    }
}
