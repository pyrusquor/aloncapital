<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_inquiry_tickets_table_add_warranty_date_column extends CI_Migration
{

    public function up()
    {

        if (!$this->db->field_exists('warranty_date', 'inquiry_tickets')) {
            $this->db->query("ALTER TABLE `inquiry_tickets` ADD `warranty_date` DATETIME NULL AFTER `turn_over_date`");
        }
    }

    public function down()
    {

        if ($this->db->field_exists('warranty_date', 'inquiry_tickets')) {
            $this->db->query("ALTER TABLE `inquiry_tickets` DROP COLUMN `warranty_date`");
        }
    }
}
