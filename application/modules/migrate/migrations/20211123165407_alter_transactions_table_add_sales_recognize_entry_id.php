<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transactions_table_add_sales_recognize_entry_id extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('sales_recognize_entry_id', 'transactions')) {
            $this->db->query("ALTER TABLE `transactions` ADD `sales_recognize_entry_id` int(11) AFTER `sales_recognize_threshold_date`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('sales_recognize_entry_id', 'transactions')) {
            $this->db->query("ALTER TABLE `transactions` DROP COLUMN `sales_recognize_entry_id`");
        }
    }
}
