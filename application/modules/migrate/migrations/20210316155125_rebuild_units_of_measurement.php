<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Rebuild_units_of_measurement extends CI_Migration
{

    protected $tbl = 'inventory_settings_unit_of_measurements';
    protected $fields = [
        'input_unit_of_measure',
        'input_quantity',
        'output_quantity',
        'output_quantity',
    ];

    public function up()
    {
        foreach ($this->fields as $field){
            if($this->db->table_exists($this->tbl)) {
                if ($this->db->field_exists($field, $this->tbl)) {
                    $this->dbforge->drop_column($this->tbl, $field);
                }
            }
        }
    }

    public function down()
    {

    }
}
