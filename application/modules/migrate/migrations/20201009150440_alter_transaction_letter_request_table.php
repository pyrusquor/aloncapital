<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_letter_request_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transaction_letter_request')) {
            if (!$this->db->field_exists('status', 'transaction_letter_request')) {
                $this->db->query("ALTER TABLE `transaction_letter_request` ADD COLUMN `status` INT(1) DEFAULT NULL  AFTER `image`");
            }
            if (!$this->db->field_exists('approve_date', 'transaction_letter_request')) {
                $this->db->query("ALTER TABLE `transaction_letter_request` ADD COLUMN `approve_date` datetime DEFAULT NULL  AFTER `image`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transaction_letter_request')) {
            if ($this->db->field_exists('status', 'transaction_letter_request')) {
                $this->dbforge->drop_column('transaction_letter_request', 'status');
            }
            if ($this->db->field_exists('approve_date', 'transaction_letter_request')) {
                $this->dbforge->drop_column('transaction_letter_request', 'approve_date');
            }
        }
    }
}
