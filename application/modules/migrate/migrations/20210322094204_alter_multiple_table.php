<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('job_orders')) {
            if(!$this->db->field_exists('job_request_id', 'job_orders')) {
                $this->db->query("ALTER TABLE `job_orders` ADD COLUMN `job_request_id` INT(11) DEFAULT NULL AFTER `approved_by`");
            }
       }
    }

    public function down()
    {
        if($this->db->table_exists('job_orders')) {
            $this->dbforge->drop_column('job_orders', 'job_request_id');
        }
    }
}
