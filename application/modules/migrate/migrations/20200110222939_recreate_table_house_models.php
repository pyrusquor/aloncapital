<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Recreate_table_house_models extends CI_Migration
{

    public function up()
    {
        if ( $this->db->table_exists('house_models') ) {

			$this->dbforge->drop_table('house_models');
        }

        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'NOT NULL' => FALSE
            ),
            'project_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => FALSE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'NULL' => FALSE
            ),
            'code' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'NULL' => FALSE
            ),
            'no_bedroom' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE
            ),
            'no_bathroom' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE
            ),
            'no_floor' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE
            ),
            'no_ac' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE
            ),
            'color' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
                'NULL' => TRUE
            ),
            'floor_area' => array(
                'type' => 'DECIMAL(20,2)',
                'NULL' => FALSE
            ),
            'lot_area' => array(
                'type' => 'DECIMAL(20,2)',
                'NULL' => FALSE
            ),
            'reservation_fee' => array(
                'type' => 'DECIMAL(20,2)',
                'NULL' => FALSE
            ),
            'furnishing_fee' => array(
                'type' => 'DECIMAL(20,2)',
                'NULL' => FALSE
            ),
            'created_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'created_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'updated_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'updated_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'deleted_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'deleted_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('house_models', TRUE);
    }

    public function down()
    {
        if ( $this->db->table_exists('house_models') ) {

			$this->dbforge->drop_table("house_models");
		}
    }
}
