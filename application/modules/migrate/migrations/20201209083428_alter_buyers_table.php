<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_buyers_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('buyers')) {
            if(!$this->db->field_exists('CID', 'buyers')) {
                 $this->db->query("ALTER TABLE `buyers` ADD COLUMN `CID` INT(11) DEFAULT 0 NOT NULL AFTER `is_active`");
            }
            if(!$this->db->field_exists('PID', 'buyers')) {
                 $this->db->query("ALTER TABLE `buyers` ADD COLUMN `PID` INT(11) DEFAULT 0 NOT NULL AFTER `CID`");
            }
            if(!$this->db->field_exists('is_exported', 'buyers')) {
                 $this->db->query("ALTER TABLE `buyers` ADD COLUMN `is_exported` INT(11) DEFAULT 0 NOT NULL AFTER `PID`");
            }
            if(!$this->db->field_exists('spouse_name', 'buyers')) {
                 $this->db->query("ALTER TABLE `buyers` ADD COLUMN `spouse_name` varchar(50) COLLATE 'utf8_general_ci' DEFAULT NULL AFTER `middle_name`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('buyers')) {
            $this->dbforge->drop_column('buyers', 'CID');
            $this->dbforge->drop_column('buyers', 'PID');
            $this->dbforge->drop_column('buyers', 'is_exported');
            $this->dbforge->drop_column('buyers', 'spouse_name');
        }
    }
}
