<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_documents_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transaction_documents')) {
            if(!$this->db->field_exists('category_id', 'transaction_documents')) {
                 $this->db->query("ALTER TABLE `transaction_documents` ADD COLUMN `category_id` INT(11) DEFAULT NULL AFTER `document_id`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transaction_documents')) {
            $this->dbforge->drop_column('transaction_documents', 'category_id');
        }
    }
}
