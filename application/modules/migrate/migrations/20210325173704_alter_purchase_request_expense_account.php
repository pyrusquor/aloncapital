<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_purchase_request_expense_account extends CI_Migration
{
    private $tbl = "purchase_order_requests";
    private $new_fields = array(
        'accounting_ledger_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'NULL' => TRUE,
        )
    );

    public function up()
    {
        $this->dbforge->add_column($this->tbl, $this->new_fields);
    }

    public function down()
    {
        foreach($this->new_fields as $field => $data){
            $this->dbforge->drop_column($this->tbl, $field);
        }
    }
}
