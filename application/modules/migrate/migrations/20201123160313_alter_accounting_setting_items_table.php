<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_setting_items_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('accounting_setting_items')) {
            if(!$this->db->field_exists('origin_id', 'accounting_setting_items')) {
                 $this->db->query("ALTER TABLE `accounting_setting_items` ADD COLUMN `origin_id` INT(11) DEFAULT NULL AFTER `accounting_entry`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('accounting_setting_items')) {
            $this->dbforge->drop_column('accounting_setting_items', 'origin_id');
        }
    }
}
