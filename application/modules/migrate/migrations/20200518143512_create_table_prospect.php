<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_prospect extends CI_Migration
{

    public function up()
    {
        if (!$this->db->table_exists('prospect')) {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => true,
                    'auto_increment' => true,
                    'NOT NULL' => false,
                ),
                'user_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'type_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                    'COMMENT' => '0=Person, 1=Company',
                ),
                'last_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'first_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => false,
                ),
                'middle_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => true,
                ),
                'birth_place' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => true,
                ),
                'birth_date' => array(
                    'type' => 'DATE',
                    'NULL' => false,
                ),
                'gender' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                    'COMMENT' => '1=MALE,2=FEMALE',
                ),
                'nationality' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'housing_membership_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => false,
                ),
                'bis_file' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => false,
                ),
                'present_address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => true,
                ),
                'mailing_address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => true,
                ),
                'business_address' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => true,
                ),
                'mobile_no' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '11',
                    'NULL' => true,
                ),
                'email' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => true,
                ),
                'landline' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '10',
                    'NULL' => true,
                ),
                'is_active' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'NULL' => false,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'NULL' => true,
                ),
                'deleted_by' => array(
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => true,
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('prospect', true);
        }
    }

    public function down()
    {
        if ($this->db->table_exists('prospect')) {

            $this->dbforge->drop_table('prospect');
        }
    }
}
