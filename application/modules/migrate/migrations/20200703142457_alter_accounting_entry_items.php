<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_entry_items extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'payee_type' => array(
                'type' => 'VARCHAR',
                'constraint' => '11',
                'NULL' => false
            )
        );

        if ($this->db->table_exists('accounting_entry_items')) {

            $this->dbforge->modify_column('accounting_entry_items', $fields);

        }
    }

    public function down()
    {
        $fields = array(
            'payee_type' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true
            )
        );

        if ($this->db->table_exists('accounting_entry_items')) {

            $this->dbforge->modify_column('accounting_entry_items', $fields);

        }
    }
}
