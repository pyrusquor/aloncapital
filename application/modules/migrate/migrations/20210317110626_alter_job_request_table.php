<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_job_request_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('job_requests')) {
            if(!$this->db->field_exists('request_type', 'job_requests')) {
                $this->db->query("ALTER TABLE `job_requests` ADD COLUMN `request_type` INT(11) DEFAULT NULL AFTER `status`");
            }
       }
    }

    public function down()
    {
        if($this->db->table_exists('job_requests')) {
            $this->dbforge->drop_column('job_requests', 'request_type');
        }
    }
}
