<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_land_inventory_histories_table_add_consolidate_and_subdivide_id_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('land_inventory_histories')) {

            if (!$this->db->field_exists('consolidate_and_subdivide_id', 'land_inventory_histories')) {
                $this->db->query("ALTER TABLE `land_inventory_histories` ADD COLUMN `consolidate_and_subdivide_id` TEXT AFTER `subdivide_id`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('land_inventory_histories')) {

            if ($this->db->field_exists('consolidate_and_subdivide_id', 'land_inventory_histories')) {
                $this->dbforge->drop_column('land_inventory_histories', 'consolidate_and_subdivide_id');
            }
        }
    }
}
