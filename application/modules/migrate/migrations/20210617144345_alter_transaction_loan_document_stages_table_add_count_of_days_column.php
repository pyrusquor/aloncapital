<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_loan_document_stages_table_add_count_of_days_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transaction_loan_document_stages')) {

            if (!$this->db->field_exists('count_of_days', 'transaction_loan_document_stages')) {
                $this->db->query("ALTER TABLE `transaction_loan_document_stages` ADD COLUMN `count_of_days` INT(10) NULL AFTER `description`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transaction_loan_document_stages')) {

            if ($this->db->field_exists('count_of_days', 'transaction_loan_document_stages')) {
                $this->dbforge->drop_column('transaction_loan_document_stages', 'count_of_days');
            }
        }
    }
}
