<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_transaction_letter_request_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('transaction_letter_request'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'transaction_id' => array(
                    'type' => 'int',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'request_date' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'subject' => array(
                    'type' => 'int',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'request_body' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE,
                ),
                'image' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
			
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('transaction_letter_request', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('transaction_letter_request') ) {

			$this->dbforge->drop_table('transaction_letter_request');
        }
    }
}
