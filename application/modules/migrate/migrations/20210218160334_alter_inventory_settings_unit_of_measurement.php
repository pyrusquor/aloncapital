<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Alter_inventory_settings_unit_of_measurement extends CI_Migration
    {
        protected $tbl = 'inventory_settings_unit_of_measurements';
        protected $fields = array(
            'input_unit_of_measure' => array(
                'type' => 'VARCHAR',
                'constraint' => 120,
                'NULL' => false

            ),
            'input_quantity' => array(
                'type' => 'FLOAT',
                'NULL' => false
            ),
            'output_unit_of_measure' => array(
                'type' => 'VARCHAR',
                'constraint' => 120,
                'NULL' => false

            ),
            'output_quantity' => array(
                'type' => 'FLOAT',
                'NULL' => false
            ),
        );

        private function check($field)
        {
            if ($this->db->table_exists($this->tbl)) {
                if (!$this->db->field_exists($field, $this->tbl)) {
                    return true;
                }
            }
            return false;
        }

        public function up()
        {
            foreach($this->fields as $key => $field){
                if($this->check($key)){
                    $type = $field['type'];

                    if($type == 'VARCHAR'){
                        $type = $type . '(' . $field['constraint'] . ')';
                    }

                    if($field['NULL']){
                        $null = "NULL";
                    }else{
                        $null = "NOT NULL";
                    }
                    $this->db->query("ALTER TABLE `$this->tbl` ADD COLUMN `$key` $type");
                }
            }

        }

        public function down()
        {
            if($this->db->table_exists($this->tbl)){
                foreach($this->fields as $key => $field){
                    $this->dbforge->drop_column($this->tbl, $field);
                }
            }
        }
    }
