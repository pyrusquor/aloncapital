<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_sellers_reservations_table_add_buyer_id extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('buyer_id', 'sellers_reservations')) {
            $this->db->query("ALTER TABLE `sellers_reservations` ADD `buyer_id` int(11) AFTER `seller_id`");
        }
        if (!$this->db->field_exists('reservation_fee', 'sellers_reservations')) {
            $this->db->query("ALTER TABLE `sellers_reservations` ADD `reservation_fee` decimal(20,2) AFTER `buyer_id`");
        }
        if (!$this->db->field_exists('remarks', 'sellers_reservations')) {
            $this->db->query("ALTER TABLE `sellers_reservations` ADD `remarks` text AFTER `reservation_fee`");
        }
        if (!$this->db->field_exists('seller_group', 'sellers_reservations')) {
            $this->db->query("ALTER TABLE `sellers_reservations` ADD `seller_group` int(11) AFTER `remarks`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('buyer_id', 'sellers_reservations')) {
            $this->db->query("ALTER TABLE `sellers_reservations` DROP COLUMN `buyer_id`");
        }
        if ($this->db->field_exists('reservation_fee', 'sellers_reservations')) {
            $this->db->query("ALTER TABLE `sellers_reservations` DROP COLUMN `reservation_fee`");
        }
        if ($this->db->field_exists('remarks', 'sellers_reservations')) {
            $this->db->query("ALTER TABLE `sellers_reservations` DROP COLUMN `remarks`");
        }
        if ($this->db->field_exists('seller_group', 'sellers_reservations')) {
            $this->db->query("ALTER TABLE `sellers_reservations` DROP COLUMN `seller_group`");
        }
    }
}
