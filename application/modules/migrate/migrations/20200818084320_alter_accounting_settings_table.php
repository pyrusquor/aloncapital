<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_settings_table extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'code' => array(
                'name' => 'period_id',
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => true
            )
        );

       if($this->db->table_exists('accounting_settings')) {
           if($this->db->field_exists('code', 'accounting_settings')) {
                $this->dbforge->modify_column('accounting_settings', $fields);
           }
       }
    }

    public function down()
    {
        $fields = array(
            'period_id' => array(
                'name' => 'code',
                'type' => 'VARCHAR',
                'constraint' => '50',
                'NULL' => true
            )
        );

       if($this->db->table_exists('accounting_settings')) {
           if($this->db->field_exists('period_id', 'accounting_settings')) {
                $this->dbforge->modify_column('accounting_settings', $fields);
           }
       }
    }
}
