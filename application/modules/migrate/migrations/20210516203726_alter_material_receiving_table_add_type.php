<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Alter_material_receiving_table_add_type extends CI_Migration
    {

        public function up()
        {
            if($this->db->table_exists('material_receivings')){
                $this->db->query("ALTER TABLE `material_receivings`	ADD COLUMN `receiving_type` SMALLINT(2) UNSIGNED NOT NULL DEFAULT '2' COMMENT 'null, Direct In, Standard Receiving Process, Transfer' AFTER `input_tax`");
            }
        }

        public function down()
        {
            if($this->db->table_exists('material_receivings')){
                if($this->db->field_exists('receiving_type', 'material_receivings')){
                    $this->db->query('ALTER TABLE `material_receivings` DROP COLUMN `receiving_type`');
                }
            }
        }
    }