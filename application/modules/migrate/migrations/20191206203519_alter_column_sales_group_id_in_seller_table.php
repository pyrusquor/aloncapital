<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_column_sales_group_id_in_seller_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('sellers'))
		{
			if ($this->db->field_exists('sales_group_id', 'sellers'))
			{
                $fields = array(
                    'sales_group_id' => array(
                        'name' => 'sales_group',
                        'type' => 'VARCHAR',
                        'constraint' => '10',
                    ),
                );
                $this->dbforge->modify_column('sellers', $fields);
            }
        }

    }

    public function down()
    {
        if($this->db->table_exists('sellers'))
		{
			if ($this->db->field_exists('sales_group', 'sellers'))
			{
				$this->dbforge->drop_column('sellers', 'sales_group');
            }
          
		}
    }
}
