<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_payment_vouchers_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('payment_vouchers')) {

            if (!$this->db->field_exists('payable_type_id', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `payable_type_id` int(11) DEFAULT NULL  AFTER `reference`");
            }
        }
       
    }

    public function down()
    {

        if ($this->db->table_exists('payment_vouchers')) {
            if ($this->db->field_exists('payable_type_id', 'payment_vouchers')) {
                $this->dbforge->drop_column('payment_vouchers', 'payable_type_id');
            }
        }

    }
}
