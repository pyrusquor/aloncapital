<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Rebuild_inventory_conversion extends CI_Migration
{
    protected $tbl = 'inventory_settings_conversions';
    protected $fields = array(
        'id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'auto_increment' => TRUE,
            'NOT NULL' => FALSE
        ),
        'name' => array(
            'type' => 'VARCHAR',
            'constraint' => '255',
            'NULL' => false,
        ),
        'created_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'created_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'updated_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'updated_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'deleted_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'deleted_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'input_unit_of_measure' => array(
            'type' => 'VARCHAR',
            'constraint' => 120,
            'NULL' => false

        ),
        'input_quantity' => array(
            'type' => 'FLOAT',
            'NULL' => false
        ),
        'output_unit_of_measure' => array(
            'type' => 'VARCHAR',
            'constraint' => 120,
            'NULL' => false

        ),
        'output_quantity' => array(
            'type' => 'FLOAT',
            'NULL' => false
        ),
    );

    public function up()
    {
        $this->dbforge->add_field($this->fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl, TRUE);
    }

    public function down()
    {
        if($this->db->table_exists($this->tbl)){
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
