<?php
	$_checklist_id	=	isset($_list['id']) && $_list['id'] ? $_list['id'] : '';
	$_description		=	isset($_list['description']) && $_list['description'] ? $_list['description'] : '';
	$_name					=	isset($_list['name']) && $_list['name'] ? $_list['name'] : '';
	$_created_at		=	isset($_list['created_at']) && $_list['created_at'] ? date_format(date_create($_list['created_at']), 'F j, Y') : '';
	$_updated_at		=	isset($_list['updated_at']) && $_list['updated_at'] ? date_format(date_create($_list['updated_at']), 'F j, Y') : '';
?>

<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Checklist Template</h3>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<button type="button" class="btn btn-sm btn-elevate btn-label-success" id="_update_document_checklist">
					<i class="fa fa-sync"></i> Save
				</button>
				<a href="<?php echo site_url('checklist');?>" class="btn btn-label-instagram btn-sm btn-elevate">
					<i class="fa fa-reply"></i> Back
				</a>
			</div>
		</div>
	</div>
</div>

<div class="row __head">
	<div class="col-md-12">
		<!-- General Information -->
		<div class="kt-portlet">
			<div class="accordion accordion-solid accordion-toggle-svg" id="accord_general_information">
				<div class="card">
					<div class="card-header" id="head_general_information">
						<div class="card-title" data-toggle="collapse" data-target="#general_information" aria-expanded="true" aria-controls="general_information">
							General Information <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
									<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
								</g>
							</svg>
						</div>
					</div>
					<div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
					<div id="general_information" class="collapse show" aria-labelledby="head_general_information" data-parent="#accord_general_information">
						<div class="card-body">
							<div class="kt-notification-v2">
								<div class="row">
									<div class="col-sm-6">
										<div class="row">
											<div class="col-sm-12">
												<span href="#" class="kt-notification-v2__item">
													<div class="kt-notification-v2__item-icon">
														<i class="flaticon2-layers kt-font-danger"></i>
													</div>
													<div class="kt-notification-v2__itek-wrapper">
														<div class="kt-notification-v2__item-title">
															<h6 class="kt-portlet__head-title kt-font-primary">Checklist Name</h6>
														</div>
														<div class="kt-notification-v2__item-desc">
															<h5 class="kt-portlet__head-title kt-font-dark">
																<?php echo isset($_name) && $_name ? $_name : '';?>
															</h5>
														</div>
													</div>
												</span>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<span href="#" class="kt-notification-v2__item">
													<div class="kt-notification-v2__item-icon">
														<i class="flaticon2-clip-symbol kt-font-warning"></i>
													</div>
													<div class="kt-notification-v2__itek-wrapper">
														<div class="kt-notification-v2__item-title">
															<h6 class="kt-portlet__head-title kt-font-primary">Created By</h6>
														</div>
														<div class="kt-notification-v2__item-desc">
															<h5 class="kt-portlet__head-title kt-font-dark">
																<?php echo isset($_created_at) && $_created_at ? $_created_at : '';?>
															</h5>
														</div>
													</div>
												</span>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="row">
											<div class="col-sm-12">
												<span href="#" class="kt-notification-v2__item">
													<div class="kt-notification-v2__item-icon">
														<i class="flaticon2-indent-dots kt-font-danger"></i>
													</div>
													<div class="kt-notification-v2__itek-wrapper">
														<div class="kt-notification-v2__item-title">
															<h6 class="kt-portlet__head-title kt-font-primary">Description</h6>
														</div>
														<div class="kt-notification-v2__item-desc">
															<h5 class="kt-portlet__head-title kt-font-dark">
																<?php echo isset($_description) && $_description ? $_description : '';?>
															</h5>
														</div>
													</div>
												</span>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<span href="#" class="kt-notification-v2__item">
													<div class="kt-notification-v2__item-icon">
														<i class="flaticon2-pen kt-font-warning"></i>
													</div>
													<div class="kt-notification-v2__itek-wrapper">
														<div class="kt-notification-v2__item-title">
															<h6 class="kt-portlet__head-title kt-font-primary">Last Updated By</h6>
														</div>
														<div class="kt-notification-v2__item-desc">
															<h5 class="kt-portlet__head-title kt-font-dark">
																<?php echo isset($_updated_at) && $_updated_at ? $_updated_at : '';?>
															</h5>
														</div>
													</div>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<!-- Document Repository -->
		<div class="kt-portlet">
			<div class="accordion accordion-solid accordion-toggle-svg" id="accord_document_repository">
				<div class="card">
					<div class="card-header" id="head_document_repository">
						<div class="card-title" data-toggle="collapse" data-target="#document_repository" aria-expanded="true" aria-controls="document_repository">
							Drag-and-Drop from the Document Repository <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
									<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
								</g>
							</svg>
						</div>
					</div>
					<div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
					<div id="document_repository" class="collapse show" aria-labelledby="head_document_repository" data-parent="#accord_document_repository">
						<div class="card-body">
							<ol class="list-group connectedSortable" id="_documents">
								<?php foreach ( $_documents as $_dkey => $_document ): ?>

									<?php
										$_label	=	'';
										$_name	=	isset($_document['name']) && $_document['name'] ? $_document['name'] : '';
										if ( $_name ) {

											$_label	=	$_name;
										}												
										$_id	=	isset($_document['id']) && $_document['id'] ? $_document['id'] : '';
										if ( $_id ) {

											$_label	.= ' <span class="kt-font-danger">( '.$_id.' )</span>';
										}
									?>

									<?php if ( isset($_selected_ids) && $_selected_ids ): ?>

										<?php if ( !in_array($_id, $_selected_ids) ): ?>

											<li class="list-group-item _document_checklist" id="<?php echo @$_id?>">
												<?php echo @$_label; ?>
											</li>
										<?php else: continue;?>
										<?php endif; ?>
									<?php else: ?>

										<li class="list-group-item _document_checklist" id="<?php echo @$_id?>">
											<?php echo @$_label; ?>
										</li>
									<?php endif; ?>
								<?php endforeach; ?>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<!-- Selected Doucments -->
		<div class="kt-portlet">
			<div class="accordion accordion-solid accordion-toggle-svg" id="accord_selected_documents">
				<div class="card">
					<div class="card-header" id="head_selected_documents">
						<div class="card-title" data-toggle="collapse" data-target="#selected_documents" aria-expanded="true" aria-controls="selected_documents">
							Selected Document List <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
									<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
								</g>
							</svg>
						</div>
					</div>
					<div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
					<div id="selected_documents" class="collapse show" aria-labelledby="head_selected_documents" data-parent="#accord_selected_documents">
						<div class="card-body">
							<ol class="list-group connectedSortable" id="_document_checklists" data-id="<?php echo $_checklist_id;?>">
								<?php foreach ( $_documents as $_dkey => $_document ): ?>

									<?php
										$_label	=	'';
										$_name	=	isset($_document['name']) && $_document['name'] ? $_document['name'] : '';
										if ( $_name ) {

											$_label	=	$_name;
										}												
										$_id	=	isset($_document['id']) && $_document['id'] ? $_document['id'] : '';
										if ( $_id ) {

											$_label	.= ' <span class="kt-font-danger">( '.$_id.' )</span>';
										}
									?>

									<?php if ( isset($_selected_ids) && $_selected_ids ): ?>

										<?php if ( in_array($_id, $_selected_ids) ): ?>

											<li class="list-group-item _document_checklist" id="<?php echo @$_id?>">
												<?php echo @$_label; ?>
											</li>
										<?php else: continue;?>
										<?php endif; ?>
									<?php endif; ?>
								<?php endforeach; ?>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>