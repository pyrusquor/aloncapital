<?php
	$_description	=	isset($_list['description']) && $_list['description'] ? $_list['description'] : '';
	$_category_id	=	isset($_list['category_id']) && $_list['category_id'] ? $_list['category_id'] : '';
	$_name				=	isset($_list['name']) && $_list['name'] ? $_list['name'] : '';
?>

<div class="kt-section kt-section--first">
	<div class="form-group row">
		<div class="col-sm-6 offset-sm-3">
			<div class="form-group">
				<label class="form-control-label">Checklist Name <span class="kt-font-danger">*</span></label>
				<div class="kt-input-icon  kt-input-icon--left">
					<input type="text" name="name" class="form-control" id="name" placeholder="Checklist Name" value="<?php echo set_value('name', @$_name);?>" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-book"></i></span></span>
				</div>
				<?php echo form_error('name'); ?>
				<span class="form-text text-muted"></span>
			</div>
			<div class="form-group">
				<label class="form-control-label">Category Name</label>
				<div class="kt-input-icon  kt-input-icon--left">
					<select class="form-control" name="category_id" id="category_id">
						<option value="">Select Category Name</option>
						<option value="1" <?php echo set_select('category_id', '1', ($_category_id === '1' ? TRUE : FALSE));?>>General</option>
						<option value="2" <?php echo set_select('category_id', '2', ($_category_id === '2' ? TRUE : FALSE));?>>Land Inventory</option>
						<option value="3" <?php echo set_select('category_id', '3', ($_category_id === '3' ? TRUE : FALSE));?>>Licensing</option>
						<option value="4" <?php echo set_select('category_id', '4', ($_category_id === '4' ? TRUE : FALSE));?>>Sales</option>
						<option value="5" <?php echo set_select('category_id', '5', ($_category_id === '5' ? TRUE : FALSE));?>>Construction</option>
						<option value="6" <?php echo set_select('category_id', '6', ($_category_id === '6' ? TRUE : FALSE));?>>Financing</option>
					</select>
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sitemap"></i></span></span>
				</div>
				<?php echo form_error('category_id'); ?>
				<span class="form-text text-muted"></span>
			</div>
			<div class="form-group">
				<label class="form-control-label">Description</label>
				<div class="kt-input-icon  kt-input-icon--left">
					<input type="text" name="description" class="form-control" id="description" placeholder="Description" value="<?php echo set_value('description', @$_description);?>" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-comments"></i></span></span>
				</div>
				<?php echo form_error('description'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
</div>