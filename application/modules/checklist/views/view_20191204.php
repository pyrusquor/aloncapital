<?php
	$_description	=	isset($_list['description']) && $_list['description'] ? $_list['description'] : '';
	$_name				=	isset($_list['name']) && $_list['name'] ? $_list['name'] : '';
	$_created_at	=	isset($_list['created_at']) && $_list['created_at'] ? date_format(date_create($_list['created_at']), 'F j, Y') : '';
	$_updated_at	=	isset($_list['updated_at']) && $_list['updated_at'] ? date_format(date_create($_list['updated_at']), 'F j, Y') : '';
?>

<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Checklist Template</h3>
		</div>
	</div>
</div>

<!-- begin::Portlet-->
<div class="kt-portlet">
	<!-- <div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				Checklist Template
			</h3>
		</div>
	</div> -->

	<div class="kt-portlet__body">
		<div class="kt-section kt-section--first">
			<div class="kt-section__body">				
				<div class="kt-portlet kt-portlet--bordered">
					<div class="kt-portlet__body">
						<!-- <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div> -->
						<h3 class="kt-section__title">GENERAL INFORMATION</h3>
						<div class="kt-section__body">
							<div class="kt-notification-v2">
								<div class="row">
									<div class="col-lg-6">
										<div class="row">
											<div class="col-lg-12">
												<span href="#" class="kt-notification-v2__item">
													<div class="kt-notification-v2__item-icon">
														<i class="flaticon2-layers kt-font-danger"></i>
													</div>
													<div class="kt-notification-v2__itek-wrapper">
														<div class="kt-notification-v2__item-title">
															<h6 class="kt-portlet__head-title kt-font-primary">Checklist Name</h6>
														</div>
														<div class="kt-notification-v2__item-desc">
															<h5 class="kt-portlet__head-title kt-font-dark">
																<?php echo isset($_name) && $_name ? $_name : '';?>
															</h5>
														</div>
													</div>
												</span>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<span href="#" class="kt-notification-v2__item">
													<div class="kt-notification-v2__item-icon">
														<i class="flaticon2-clip-symbol kt-font-warning"></i>
													</div>
													<div class="kt-notification-v2__itek-wrapper">
														<div class="kt-notification-v2__item-title">
															<h6 class="kt-portlet__head-title kt-font-primary">Created By</h6>
														</div>
														<div class="kt-notification-v2__item-desc">
															<h5 class="kt-portlet__head-title kt-font-dark">
																<?php echo isset($_created_at) && $_created_at ? $_created_at : '';?>
															</h5>
														</div>
													</div>
												</span>
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="row">
											<div class="col-lg-12">
												<span href="#" class="kt-notification-v2__item">
													<div class="kt-notification-v2__item-icon">
														<i class="flaticon2-indent-dots kt-font-danger"></i>
													</div>
													<div class="kt-notification-v2__itek-wrapper">
														<div class="kt-notification-v2__item-title">
															<h6 class="kt-portlet__head-title kt-font-primary">Description</h6>
														</div>
														<div class="kt-notification-v2__item-desc">
															<h5 class="kt-portlet__head-title kt-font-dark">
																<?php echo isset($_description) && $_description ? $_description : '';?>
															</h5>
														</div>
													</div>
												</span>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<span href="#" class="kt-notification-v2__item">
													<div class="kt-notification-v2__item-icon">
														<i class="flaticon2-pen kt-font-warning"></i>
													</div>
													<div class="kt-notification-v2__itek-wrapper">
														<div class="kt-notification-v2__item-title">
															<h6 class="kt-portlet__head-title kt-font-primary">Last Updated By</h6>
														</div>
														<div class="kt-notification-v2__item-desc">
															<h5 class="kt-portlet__head-title kt-font-dark">
																<?php echo isset($_updated_at) && $_updated_at ? $_updated_at : '';?>
															</h5>
														</div>
													</div>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="kt-notification-v2">
								<a href="#" class="kt-notification-v2__item">
									<div class="kt-notification-v2__item-icon">
										<i class="flaticon-bell kt-font-success"></i>
									</div>
									<div class="kt-notification-v2__itek-wrapper">
										<div class="kt-notification-v2__item-title">
											5 new user generated report
										</div>
										<div class="kt-notification-v2__item-desc">
											Reports based on sales
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification-v2__item">
									<div class="kt-notification-v2__item-icon">
										<i class="flaticon2-box kt-font-danger"></i>
									</div>
									<div class="kt-notification-v2__itek-wrapper">
										<div class="kt-notification-v2__item-title">
											2 new items submited
										</div>
										<div class="kt-notification-v2__item-desc">
											by Grog John
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification-v2__item">
									<div class="kt-notification-v2__item-icon">
										<i class="flaticon-psd kt-font-brand"></i>
									</div>
									<div class="kt-notification-v2__itek-wrapper">
										<div class="kt-notification-v2__item-title">
											79 PSD files generated
										</div>
										<div class="kt-notification-v2__item-desc">
											Reports based on sales
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification-v2__item">
									<div class="kt-notification-v2__item-icon">
										<i class="flaticon2-supermarket kt-font-warning"></i>
									</div>
									<div class="kt-notification-v2__itek-wrapper">
										<div class="kt-notification-v2__item-title">
											$2900 worth producucts sold
										</div>
										<div class="kt-notification-v2__item-desc">
											Total 234 items
										</div>
									</div>
								</a>
								<a href="#" class="kt-notification-v2__item">
									<div class="kt-notification-v2__item-icon">
										<i class="flaticon-paper-plane-1 kt-font-success"></i>
									</div>
									<div class="kt-notification-v2__itek-wrapper">
										<div class="kt-notification-v2__item-title">
											4.5h-avarage response time
										</div>
										<div class="kt-notification-v2__item-desc">
											Fostest is Barry
										</div>
									</div>
								</a>
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<a href="<?php echo site_url('checklist');?>" class="btn btn-secondary btn-sm">
				<i class="fa fa-reply"></i> Back
			</a>
		</div>
	</div>
</div>
<!--end::Portlet