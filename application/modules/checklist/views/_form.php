<?php
	$_description	=	isset($_list['description']) && $_list['description'] ? $_list['description'] : '';
	$_category_id	=	isset($_list['category_id']) && $_list['category_id'] ? $_list['category_id'] : '';
	$loan_document_stage_id				=	isset($_list['loan_document_stage_id']) && $_list['loan_document_stage_id'] ? $_list['loan_document_stage_id'] : '';
	$transaction_document_stage_id				=	isset($_list['transaction_document_stage_id']) && $_list['transaction_document_stage_id'] ? $_list['transaction_document_stage_id'] : '';
	$_name				=	isset($_list['name']) && $_list['name'] ? $_list['name'] : '';
?>

<div class="kt-section kt-section--first">
	<div class="form-group row">
		<div class="col-sm-6 offset-sm-3">
			<div class="form-group">
				<label class="form-control-label">Checklist Name <code>*</code></label>
				<div class="kt-input-icon">
					<input type="text" name="name" class="form-control" id="name" placeholder="Checklist Name" value="<?php echo set_value('name', @$_name);?>" autocomplete="off">
				</div>
				<?php echo form_error('name'); ?>
				<span class="form-text text-muted"></span>
			</div>
			<div class="form-group">
				<label class="form-control-label">Category Name</label>
				<div class="kt-input-icon">
					<?php echo form_dropdown('category_id', Dropdown::get_static('document_checklist_category'), set_value('category_id', @$_category_id), 'class="form-control" id="category_id"'); ?>
				</div>
				<?php echo form_error('category_id'); ?>
				<span class="form-text text-muted"></span>
			</div>


			<div class="form-group">
				<label class="form-control-label">Transaction Category Stage</label>
				<div class="kt-input-icon">
					<select class="form-control"  id="transaction_category_stage_id">
						<option value="0">Select Option</option>
						<option value="1">Lots Only</option>
						<option value="2">H&L / Condo</option>
						<option value="3">Both</option>
					</select>

				</div>
				<?php echo form_error('transaction_category_stage_id'); ?>
				<span class="form-text text-muted"></span>
			</div>

			<div class="form-group">
				<label class="form-control-label">Transaction Stage</label>
				<div class="kt-input-icon">
					
					<select class="form-control suggests" data-param="transaction_category_stage_id" data-module="transaction_document_stages" id="transaction_document_stage_id" name="transaction_document_stage_id">
						<option value="0">Select Option</option>
						<?php if ($transaction_document_stage_id): ?>
							<option value="<?=$transaction_document_stage_id;?>" selected>
								<?=get_value_field($transaction_document_stage_id,'transaction_document_stages','name');?>
							</option>
						<?php endif ?>
					</select>

				</div>
				<?php echo form_error('transaction_document_stage_id'); ?>
				<span class="form-text text-muted"></span>
			</div>

			<div class="form-group">
				<label class="form-control-label">Loan Category Stage</label>
				<div class="kt-input-icon">
					
					<select class="form-control"  id="loan_category_stage_id">
						<option value="0">Select Option</option>
						<option value="1">HDMF Window 2</option>
						<option value="2">HDMF Retail Window</option>
						<option value="3">Bank</option>
						
					</select>

				</div>
				<?php echo form_error('transaction_category_stage_id'); ?>
				<span class="form-text text-muted"></span>
			</div>

			<div class="form-group">
				<label class="form-control-label">Loan Document Stage</label>
				<div class="kt-input-icon">
					
					<select class="form-control suggests" data-param="loan_category_stage_id" data-module="transaction_loan_document_stages" id="loan_document_stage_id" name="loan_document_stage_id">
						<option value="0">Select Option</option>
						<?php if ($loan_document_stage_id): ?>
							<option value="<?=$loan_document_stage_id;?>" selected>
								<?=get_value_field($loan_document_stage_id,'transaction_loan_document_stages','name');?>
							</option>
						<?php endif ?>
					</select>

				</div>
				<?php echo form_error('loan_document_stage_id'); ?>
				<span class="form-text text-muted"></span>
			</div>


			<div class="form-group">
				<label class="form-control-label">Description</label>
				<div class="kt-input-icon">
					<input type="text" name="description" class="form-control" id="description" placeholder="Description" value="<?php echo set_value('description', @$_description);?>" autocomplete="off">
				</div>
				<?php echo form_error('description'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
</div>