<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 
 */
class Checklist_model extends MY_Model
{

	public $primary_key	=	'id'; // you MUST mention the primary key
	public $protected		=	['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $fillable		=	[
		'name',
		'category_id',
		'description',
		'loan_document_stage_id',
		'transaction_document_stage_id',
		'is_reservation_document',
		'is_active',
	]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $table				=	'checklists'; // you MUST mention the table name
	public $rules				=	[];
	public $_fields			=	[
		'name'	=>	array(
			'field'	=>	'name',
			'label'	=>	'Checklist Name',
			'rules'	=>	'trim|required|min_length[2]'
		),
		'category_id'	=>	array(
			'field'	=>	'category_id',
			'label'	=>	'Category Name',
			'rules'	=>	'trim|required',
		),
		'description'	=>	array(
			'field'	=>	'description',
			'label'	=>	'Checklist Description',
			'rules'	=>	'trim|min_length[2]'
		)
	];

	function __construct()
	{

		parent::__construct();

		$this->soft_deletes = TRUE;

		$this->return_as = 'array';

		$this->rules['insert']	=	$this->_fields;
		$this->rules['update']	=	$this->_fields;
	}

	function find_all($_where = FALSE, $_columns = FALSE, $_row = FALSE)
	{

		$_return	=	FALSE;

		if ($_where) {

			if (is_array($_where)) {

				foreach ($_where as $key => $_wang) {

					$this->db->where($key, $_wang);
				}
			} else {

				$this->db->where('id', $_where);
			}
		} else {

			$this->db->where('deleted_at IS NULL');
		}

		if ($_columns) {

			if (is_array($_columns)) {

				foreach ($_columns as $key => $_col) {

					$this->db->select($_col);
				}
			} else {

				$this->db->select($_columns);
			}
		} else {

			$this->db->select('*');
		}

		$this->db->order_by('name', 'asc');

		$_query	=	$this->db->get($this->table);
		if ($_row) {

			$_return	=	$_query->num_rows() > 0 ? $_query->row() :  FALSE;
		} else {

			$_return	=	$_query->num_rows() > 0 ? $_query->result() :  FALSE;
		}

		return $_return;
	}

	function _get_checklist_info()
	{

		$_return	=	FALSE;

		$_sql	=	"
							SELECT  c.`id`,
								c.`name`,
								c.`description`,
								c.`loan_document_stage_id`,
								c.`transaction_document_stage_id`,
								IF ( COUNT(d.`id`) > 0, COUNT(d.`id`), 'None' ) AS number_of_documents,
								c.`category_id`,
								c.`updated_at`,
								c.`created_at`
							FROM checklists AS c
							LEFT JOIN document_checklists AS dc ON dc.`checklist_id` = c.`id`
							LEFT JOIN documents AS d ON d.`id` = dc.`document_id`
							WHERE c.`deleted_at` IS NULL
								AND d.`deleted_at` IS NULL
							GROUP BY c.`id`
						";

		$_query	=	$this->db->query($_sql);

		$_return	=	$_query->num_rows() > 0 ? $_query->result_array() : FALSE;

		return $_return;
	}

	function insert_dummy()
	{

		require APPPATH . '/third_party/faker/autoload.php';

		$_faker	=	Faker\Factory::create();

		$_datas	=	[];

		for ($i = 1; $i <= 5; $i++) {

			array_push($_datas, [
				'name'				=>	$_faker->name,
				'category_id'	=>	rand(1, 6),
				'description'	=>	$_faker->address,
				'created_by'	=>	rand(1, 6),
				'created_at'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'updated_by'	=>	rand(1, 6),
				'updated_at'	=>	date('Y-m-d H:i:s', mt_rand(1, time()))
			]);
		}

		$this->db->insert_batch($this->table, $_datas);
	}
}
