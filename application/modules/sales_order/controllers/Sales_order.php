<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales_order extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $sales_order_models = array(
            'sales_order/Sales_order_model' => 'M_sales_order',
            'item/Item_model' => 'M_item',
            'sales_order_items/Sales_order_items_model' => 'M_sales_order_items',
        );

        // Load models
        $this->load->model($sales_order_models);
        

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper
        $this->load->helper('form');

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_sales_order->fillable;
        $this->_table_columns = $this->M_sales_order->__get_columns();

        $this->u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $this->additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_sales_order->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_sales_order->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }

        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showSalesOrders()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'number' => true,
            'customer_type' => true,
            'customer_id' => true,
            'prepared_by' => true,
            'prepared_date' => true,
            'approved_by' => true,
            'approved_date' => true,
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        $sales_orders = $this->M_sales_order->with_staff()->with_user()->as_array()->get_all();
        $data = [];

        if ($sales_orders) {

            foreach($sales_orders as $key => $value) {
                $customer = $this->M_sales_order->get_customer_by_type($value['customer_id'] , $value['customer_type']);
                $sales_orders[$key]['customer_id'] = $customer['name'];

                $sales_orders[$key]['prepared_by'] = @$value['user']['first_name'] . " " . @$value['user']['middle_name'] . " " . @$value['user']['last_name'];

                $sales_orders[$key]['approved_by'] = @$value['staff']['first_name'] . " " . @$value['staff']['middle_name'] . " " . @$value['staff']['last_name'];

                $sales_orders[$key]['prepared_date'] = view_date($value['created_at']);
                $sales_orders[$key]['approved_date'] = view_date($value['approved_date']);
            }

            foreach ($sales_orders as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                $_column = $_REQUEST['order'][0]['column'] - 1;
                $_dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($x, $y) use ($_column, $_dir) {

                    // echo "<pre>";
                    // print_r($x) ;echo "<br>";
                    // echo $_column;echo "<br>";

                    $x = array_slice($x, $_column, 1);

                    // vdebug($x);

                    $x = array_pop($x);

                    $y = array_slice($y, $_column, 1);
                    $y = array_pop($y);

                    if ($_dir === 'asc') {

                        return $x > $y ? true : false;
                    } else {

                        return $x < $y ? true : false;
                    }
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );

        }

        echo json_encode($output);
        exit();
    }

    public function form($id = false)
    {

        $method = "Create";
        if ($id) {$method = "Update";}

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $info = $this->input->post();

            $sales_order_items = $info['sales_order_items'];

            $data['number'] = $info['number'];
            $data['customer_type'] = $info['customer_type'];
            $data['customer_id'] = $info['customer_id'];
            $data['prepared_by'] = $this->user->id;
            $data['approved_by'] = $info['approved_by'];
            $data['approved_date'] = NOW;

            if($id) {

                $sales_order_items_filter = ["sales_order_id" => $id];
            
                $this->view_data['sales_order_items'] = $this->M_sales_order_items->with_item()->get_all($sales_order_items_filter);

                $sales_order_id = $this->M_sales_order->update($data, $id);

                foreach ($sales_order_items as $key => $item) {
                    $sales_order_item['sales_order_id'] = $sales_order_id;
                    $sales_order_item['item_code'] = $item['item_code'];
                    $sales_order_item['item_id'] = $item['item'];
                    $sales_order_item['unit_of_measure'] = $item['unit_of_measure'];
                    $sales_order_item['quantity'] = $item['quantity'];
                    $sales_order_item['unit_price'] = $item['unit_price'];
                    $sales_order_item['subtotal'] = $item['subtotal'];
                    $sales_order_item['grandtotal'] = $info['grandtotal'];
    
                    $this->M_sales_order_items->update($sales_order_item + $this->additional);
                }

            } else {

                $sales_order_id = $this->M_sales_order->insert($data);

                foreach ($sales_order_items as $key => $item) {
                    $sales_order_item['sales_order_id'] = $sales_order_id;
                    $sales_order_item['item_code'] = $item['item_code'];
                    $sales_order_item['item_id'] = $item['item'];
                    $sales_order_item['unit_of_measure'] = $item['unit_of_measure'];
                    $sales_order_item['quantity'] = $item['quantity'];
                    $sales_order_item['unit_price'] = $item['unit_price'];
                    $sales_order_item['subtotal'] = $item['subtotal'];
                    $sales_order_item['grandtotal'] = $info['grandtotal'];
    
                    $this->M_sales_order_items->insert($sales_order_item + $this->additional);
                }
            }


            if ($result === false) {

                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {

                // Success
                $this->notify->success('Sales Order successfully' . " " . $method, 'sales_order');
            }
        }

        if ($id) {
            $this->view_data['info'] = $info = $this->M_sales_order->with_staff()->with_user()->get($id);

        }

        $this->view_data['method'] = $method;
        $this->view_data['sales_order_number'] = uniqidSO();

        $this->template->build('form', $this->view_data);
    }

    public function view($id = false)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['sales_orders'] = $this->M_sales_order->with_staff()->with_user()->get($id);

            $sales_order_items_filter = ["sales_order_id" => $id];
            
            $this->view_data['sales_order_items'] = $this->M_sales_order_items->with_item()->get_all($sales_order_items_filter);

            if ($this->view_data['sales_orders']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_sales_order->get($id);
            if ($list) {

                $deleted = $this->M_sales_order->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Sales Order Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_sales_order->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Sales Order/s Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }
    

    public function get_item() {

        if($post = $this->input->post()) {

            $id = $post['id'];

            $item = $this->M_item->get($id);

            $output = array(
                'data' => $item
            );

            echo json_encode($output);
        }
        
    }

    public function get_all_item() {
        $items = $this->M_item->get_all();

        $output = array(
            'data' => $items
        );

        echo json_encode($output);
    }

    public function printable($id = false,$debug = 0)
    {
        if ($id) {

            $this->view_data['info'] = $info = $this->M_sales_order->with_requests()->with_staff()->with_user()->get($id);

            $this->view_data['customer'] = $this->M_sales_order->get_customer_by_type($info['customer_id'] , $info['customer_type']);

            if($debug){
                vdebug($this->view_data);
            }
            
            $this->template->build('printable', $this->view_data);

            $generateHTML = $this->load->view('printable', $this->view_data, true);

            echo $generateHTML;die();

            pdf_create($generateHTML, 'generated_form');

        } else {

            show_404();
        }

    }

}