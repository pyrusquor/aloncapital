<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Sales_order_model extends MY_Model
{
    public $table = 'sales_orders'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'number', 
        'customer_type', 
        'customer_id', 
        'prepared_by', 
        'approved_by', 
        'approved_date', 
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['staff'] = array('foreign_model' => 'staff/Staff_model', 'foreign_table' => 'staff', 'foreign_key' => 'id', 'local_key' => 'approved_by');

        $this->has_one['user'] = array('foreign_model' => 'user/User_model', 'foreign_table' => 'users', 'foreign_key' => 'id', 'local_key' => 'prepared_by');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function get_customer_by_type($id = 0, $type = "") {

        $this->db->from($type . " as table");
		$this->db->where("table.id = ", $id);
		$this->db->where("table.deleted_at IS NULL");
		$query = $this->db->get();
        
		$result = $query->result_array()[0];

        $customer = "";

        if($type == "sellers" || $type == "buyers" || $type == "staff") {
            $customer['name'] = @$result['first_name'] . " " . @$result['middle_name'] . " " . @$result['last_name'];
            $customer['address'] = @$result['present_address'];
        } else {
            $customer['name'] = @$result['name'];
        }

        return $customer;

    }

}
