<!DOCTYPE html>
<html>
    <head>
        <title><?php echo "Sales Order - " . $info['number'] ?></title>
        <!--begin::Global Theme Styles(used by all pages) -->
        <link
            href="<?=base_url();?>assets/css/demo1/style.bundle.css"
            rel="stylesheet"
            type="text/css"
        />
        <!--end::Global Theme Styles -->

        <!--begin::Layout Skins(used by all pages) -->
        <link
            href="<?=base_url();?>assets/css/demo1/skins/header/base/light.css"
            rel="stylesheet"
            type="text/css"
        />
        <link
            href="<?=base_url();?>assets/css/demo1/skins/header/menu/light.css"
            rel="stylesheet"
            type="text/css"
        />
        <link
            href="<?=base_url();?>assets/css/demo1/skins/brand/dark.css"
            rel="stylesheet"
            type="text/css"
        />
        <link
            href="<?=base_url();?>assets/css/demo1/skins/aside/dark.css"
            rel="stylesheet"
            type="text/css"
        />
        <link
            rel="shortcut icon"
            href="<?=base_url();?>assets/media/logos/favicon.ico"
        />
        <!--end::Layout Skins -->
        <style>
            body {
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                font-size: 13px;
                line-height: 1.42857143;
                color: #333;
                background-color: #fff;
            }

            .table {
                width: 100%;
                border-collapse: collapse;
                border-spacing: 0;
                margin-bottom: 15px;
            }

            .table > thead > tr > th,
            .table > tbody > tr > td,
            .table > tbody > tr > th,
            .table > tfoot > tr > td,
            .table > tfoot > tr > th {
                padding: 5px 8px;
            }

            .table > thead > tr > th {
                border-bottom: 2px solid #00529c;
            }

            .table > tfoot > tr > td {
                border-top: 2px solid #00529c;
            }

            .table > tfoot {
                border-top: 2px solid #ddd;
            }

            .table > tbody {
                border-bottom: 1px solid #fff;
            }

            .table.table-striped > thead > tr > th {
                background: #00529c;
                color: #fff;
                border: 0;
                padding: 12px 8px;
                text-transform: uppercase;
            }

            .table.table-striped > thead > tr > th a {
                color: #fff;
                font-weight: 400;
            }

            .table.table-striped > thead > tr:nth-child(2) > th {
                background: #0075de;
            }

            .table.table-striped td {
                border: 0;
                vertical-align: middle;
            }

            .table-striped > tbody > tr:nth-of-type(odd) {
                background: #fff;
            }

            .table-striped > tbody > tr:nth-of-type(even) {
                background: #f1f1f1;
            }

            .color-bluegreen {
                color: #169f98;
            }

            .color-white {
                color: #fff;
            }

            .peso_currency {
                font-family: DejaVu Sans;
            }

            .bg-bluegreen {
                background-color: #169f98;
            }

            .text-center {
                text-align: center;
            }

            .text-left {
                text-align: left;
            }

            .text-right {
                text-align: right;
            }

            .margin0 {
                margin: 0;
            }

            .padding10 {
                padding: 10px;
            }

            p {
                margin: 0 0 15px;
            }

            .alert {
                padding: 15px;
                margin-bottom: 20px;
                border: 1px solid transparent;
                border-radius: 4px;
            }

            .alert-warning {
                background-color: #fcf8e3;
                border-color: #faebcc;
                color: #8a6d3b;
            }

            input[type="text"],
            select.form-control {
                background: transparent;
                border: none;
                border-bottom: 1px solid #000000;
                -webkit-box-shadow: none;
                box-shadow: none;
                border-radius: 0;
            }

            input[type="text"]:focus,
            select.form-control:focus {
                -webkit-box-shadow: none;
                box-shadow: none;
            }
        </style>
    </head>

    <body>
        <!-- CONTENT -->
        <div
            class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid"
        >
            <!--begin:: Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body m-5 p-5">
                    <h3 class="text-center">
                        PUEBLO DE PANAY, INC.
                    </h3>
                    <p class="text-center">Punta Dulog Commercial Complex, St. Joseph Avenue, Pueblo de Panay Township, Lawaan, Roxas City, Capiz</p>
                    <p class="text-center">Tel/Fax# (036) 6212-808</p>

                    <h4 class="text-center text-danger">
                        SALES ORDER
                    </h4>

                    <div class="row">

                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    Document #: <?php echo $info['number'] ?>
                                </div>
                                <div class="col-lg-12">
                                    Customer Type:  <?php echo $info['customer_type'] ?>
                                </div>
                                <div class="col-lg-12">
                                    Customer:  <?php echo $customer['name'] ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    Address: <?php echo $customer['address'] ?>
                                </div>
                                <div class="col-lg-12">
                                    Ship To: 
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    Date. : <?php echo view_date(date(NOW)) ?>
                                </div>
                                <div class="col-lg-12">
                                    Delivery :
                                </div>
                                <div class="col-lg-12">
                                    Warehouse :
                                </div>
                                <div class="col-lg-12">
                                    Note :
                                </div>
                            </div>
                        </div>
                        

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
