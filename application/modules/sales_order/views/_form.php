<?php
    $number            =    isset($info['number']) && $info['number'] ? $info['number'] : '';
    $approved_by            =    isset($info['approved_by']) && $info['approved_by'] ? $info['approved_by'] : '';
    $staff_name = $info['staff'] ? $info['staff']['first_name'] . " " . $info['staff']['last_name'] : '';
    ?>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Sales Order No. <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="number" value="<?php echo $number ?  $number : $sales_order_number ?>" placeholder="Sales No." readonly>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span></span>
            </div>
            <?php echo form_error('number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Customer Type <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('customer_type', Dropdown::get_static('customer_type'), set_value('customer_type', @$customer_type), 'class="form-control" id="customer_type" required'); ?>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Customer <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-type="person"  id="customer_id" name="customer_id" required>
                <option value="">Select Customer</option>
                <?php if ($payee_name): ?>
                <option value="<?php echo $customer_id; ?>" selected><?php echo $payee_name; ?></option>
                <?php endif ?>
            </select>

            <?php echo form_error('customer_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Approved By <span class="kt-font-danger">*</span></label>
                <select class="form-control suggests" data-module="staff"  data-type="person" id="approved_by" name="approved_by" required>
                    <option value="">Select Approver</option>
                    <?php if ($staff_name): ?>
                    <option value="<?php echo $approved_by; ?>" selected><?php echo $staff_name; ?></option>
                    <?php endif?>
                </select>
            
            <?php echo form_error('approved_by'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<?php if (!empty($sales_order_items)): ?>
<?php foreach ($sales_order_items as $key => $sales_order_items): ?>
<?php
    $sales_order_item_id = isset($sales_order_items['id']) && $sales_order_items['id'] ? $sales_order_items['id'] : '';
    $item_id = isset($sales_order_items['item_id']) && $sales_order_items['item_id'] ? $sales_order_items['item_id'] : '';
    $item_code = isset($sales_order_items['item_code']) && $sales_order_items['item_code'] ? $sales_order_items['item_code'] : '';
    $unit_of_measure = isset($sales_order_items['unit_of_measure']) && $sales_order_items['unit_of_measure'] ? $sales_order_items['unit_of_measure'] : '';
    $quantity = isset($sales_order_items['quantity']) && $sales_order_items['quantity'] ? $sales_order_items['quantity'] : '';
    $unit_price = isset($sales_order_items['unit_price']) && $sales_order_items['unit_price'] ? $sales_order_items['unit_price'] : '';
    $subtotal = isset($sales_order_items['subtotal']) && $sales_order_items['subtotal'] ? $sales_order_items['subtotal'] : '';
    
    ?>
<div id="sales_order_items_form_repeater">
<div data-repeater-list="sales_order_items[<?php echo $sales_order_item_id; ?>]">
    <div data-repeater-item="sales_order_items[<?php echo $sales_order_item_id; ?>]" class="row border-top mt-1 pt-3">
        <div class="col-sm-2 d-none">
                <div class="form-group">
                    <label>ID <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon">
                        <input class="form-control" name="sales_order_items[<?php echo $sales_order_item_id ?>][id]"
                            value="<?php echo set_value('sales_order_item_id', $sales_order_item_id); ?>" placeholder="Type"
                            autocomplete="off" />

                    </div>
                </div>
            </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label>Item <span class="kt-font-danger">*</span></label>
                    <select class="form-control populateItems" 
                    value="<?php echo set_value('item_id', $item_id); ?>" name="sales_order_items[<?php echo $sales_order_item_id ?>][item]" id="item_el">
                    </select>
                <?php echo form_error('item'); ?>
                <span class="form-text text-muted"></span>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label>Item Code <span class="kt-font-danger">*</span></label>
                <div class="kt-input-icon">
                    <input type="text" class="form-control" 
                    value="<?php echo set_value('item_code', $item_code); ?>" name="sales_order_items[<?php echo $sales_order_item_id ?>][item_code]" readonly>
                </div>
                <?php echo form_error('item_code'); ?>
                <span class="form-text text-muted"></span>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label>Unit of Measure <span class="kt-font-danger">*</span></label>
                <div class="kt-input-icon">
                    <input type="text" class="form-control" 
                    value="<?php echo set_value('unit_of_measure', $unit_of_measure); ?>" name="sales_order_items[<?php echo $sales_order_item_id ?>][unit_of_measure]">
                </div>
                <?php echo form_error('unit_of_measure'); ?>
                <span class="form-text text-muted"></span>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label>Quantity <span class="kt-font-danger">*</span></label>
                <div class="kt-input-icon">
                    <input type="number" class="form-control" 
                    value="<?php echo set_value('quantity', $quantity); ?>" name="sales_order_items[<?php echo $sales_order_item_id ?>][quantity]">
                </div>
                <?php echo form_error('quantity'); ?>
                <span class="form-text text-muted"></span>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label>Unit Price <span class="kt-font-danger">*</span></label>
                <div class="kt-input-icon">
                    <input type="text" class="form-control" 
                    value="<?php echo set_value('unit_price', $unit_price); ?>" name="sales_order_items[<?php echo $sales_order_item_id ?>][unit_price]" readonly>
                </div>
                <?php echo form_error('unit_price'); ?>
                <span class="form-text text-muted"></span>
            </div>
        </div>
        <div class="col-sm-1">
            <div class="form-group">
                <label>Subtotal <span class="kt-font-danger">*</span></label>
                <div class="kt-input-icon">
                    <input type="text" class="form-control" 
                    value="<?php echo set_value('subtotal', $subtotal); ?>" name="sales_order_items[<?php echo $sales_order_item_id ?>][subtotal]" readonly>
                </div>
                <?php echo form_error('subtotal'); ?>
                <span class="form-text text-muted"></span>
            </div>
        </div>
        <div class="col-sm-1 align-self-center">

            <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold remove_item">
                <i class="la la-trash-o"></i>
            </a>

        </div>
    </div>
</div>
<?php endforeach;?>
<?php else: ?>
<div id="sales_order_items_form_repeater">
    <div data-repeater-list="sales_order_items">
        <div data-repeater-item="sales_order_items" class="row border-top mt-1 pt-3">
            <div class="col-sm-2">
                <div class="form-group">
                    <label>Item <span class="kt-font-danger">*</span></label>
                    <select class="form-control populateItems"  name="sales_order_items[item]" id="item_el">
                    </select>
                    <?php echo form_error('item'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label>Item Code <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control" name="sales_order_items[item_code]" readonly>
                    </div>
                    <?php echo form_error('item_code'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label>Unit of Measure <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control" name="sales_order_items[unit_of_measure]">
                    </div>
                    <?php echo form_error('unit_of_measure'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label>Quantity <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon">
                        <input type="number" class="form-control" name="sales_order_items[quantity]">
                    </div>
                    <?php echo form_error('quantity'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label>Unit Price <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control" name="sales_order_items[unit_price]" readonly>
                    </div>
                    <?php echo form_error('unit_price'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-1">
                <div class="form-group">
                    <label>Subtotal <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control" name="sales_order_items[subtotal]" readonly>
                    </div>
                    <?php echo form_error('subtotal'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-1 align-self-center">

                <a href="javascript:;" data-repeater-delete=""
                    class="btn-sm btn btn-label-danger btn-bold remove_item">
                    <i class="la la-trash-o"></i>
                </a>

            </div>
        </div>
    </div>
    <?php endif;?>
    <?php if (empty($sales_order_items)): ?>
    <div class="row mt-3">
        <div class="offset-2"></div>
        <div class="offset-2"></div>
        <div class="offset-2"></div>
        <div class="offset-2"></div>
        <div class="offset-2"></div>
        <div class="col-sm-2">
            <div class="form-group">
                <label>Grandtotal <span class="kt-font-danger">*</span></label>
                <div class="kt-input-icon">
                    <input type="text" class="form-control" name="grandtotal" readonly id="grandtotal">
                </div>
                <?php echo form_error('grandtotal'); ?>
                <span class="form-text text-muted"></span>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last row">
        <div class="offset-10"></div>
        <div class="col-lg-2">
            <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
            <i class="la la-plus"></i> Add Sales Order Item
            </a>
        </div>
    </div>
</div>
<?php endif?>