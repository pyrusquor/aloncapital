<?php
    $id = isset($sales_orders['id']) && $sales_orders['id'] ? $sales_orders['id'] : '';
    $number = isset($sales_orders['number']) && $sales_orders['number'] ? $sales_orders['number'] : '';
    $customer_type = isset($sales_orders['customer_type']) && $sales_orders['customer_type'] ? $sales_orders['customer_type'] : '';
    $customer_id = isset($sales_orders['customer_id']) && $sales_orders['customer_id'] ? $sales_orders['customer_id'] : '';
    $prepared_by = isset($sales_orders['user']) && $sales_orders['user'] ? @$sales_orders['user']['first_name'] . " " . @$sales_orders['user']['middle_name'] . " " . @$sales_orders['user']['last_name']  : '';
    $approved_by = isset($sales_orders['staff']) && $sales_orders['staff'] ? @$sales_orders['staff']['first_name'] . " " . @$sales_orders['staff']['middle_name'] . " " . @$sales_orders['staff']['last_name']  : '';
    $created_by = isset($sales_orders['created_by']) && $sales_orders['created_by'] ? $sales_orders['created_by'] : '';
    $created_at = isset($sales_orders['created_at']) && $sales_orders['created_at'] ? $sales_orders['created_at'] : '';
    $approved_date = isset($sales_orders['approved_date']) && $sales_orders['approved_date'] ? $sales_orders['approved_date'] : '';
    
    $customer = $this->M_sales_order->get_customer_by_type($customer_id , $customer_type);
    
    
    
    ?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Accounting Entries</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('sales_order/update/' . $id); ?>"
                    class="btn btn-label-success btn-elevate btn-sm">
                <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('sales_order'); ?>"
                    class="btn btn-label-instagram btn-sm btn-elevate">
                <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Sales Order Number
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $number; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Customer Type
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $customer_type; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Customer
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $customer['name']; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Prepared By
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $prepared_by; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Prepared Date
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold"><?php echo view_date($created_at); ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Approved By
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $approved_by; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Approved Date
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold"><?php echo view_date($approved_date); ?>
                                        </p>
                                    </div>
                                </div>
                                <!--end::Form-->
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
                <!--end::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__body">
                        <!--begin::Portlet-->
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        Sales Order Items
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <?php if ($sales_order_items): //vdebug($sales_order_items); ?>
                                <?php foreach ($sales_order_items as $key =>  $item): ?>
                                <?php
                                    $item_code = isset($item['item_code']) && $item['item_code'] ? $item['item_code'] : "";
                                    $item_id = isset($item['item_id']) && $item['item_id'] ? $item['item_id'] : "";
                                    $unit_of_measure = isset($item['unit_of_measure']) && $item['unit_of_measure'] ? $item['unit_of_measure'] : "";
                                    $quantity = isset($item['quantity']) && $item['quantity'] ? $item['quantity'] : "";
                                    $unit_price = isset($item['unit_price']) && $item['unit_price'] ? $item['unit_price'] : "";
                                    $subtotal = isset($item['subtotal']) && $item['subtotal'] ? $item['subtotal'] : "";
                                    $grandtotal = isset($item['grandtotal']) && $item['grandtotal'] ? $item['grandtotal'] : "";
                                    $item_name = isset($item['item']) && $item['item'] ? $item['item']['name'] : "";
                                    ?>
                                <div class="form-group form-group-xs row border-bottom pb-3 mb-3">
                                    <div class="col">
                                        <div href="#" class="kt-notification-v2__item">
                                            <div class="kt-notification-v2__itek-wrapper">
                                                <div class="kt-notification-v2__item-title">
                                                    <h6 class="kt-portlet__head-title kt-font-primary">
                                                        Item
                                                    </h6>
                                                </div>
                                                <div class="kt-notification-v2__item-desc">
                                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                                        <?php echo $item_name; ?>
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div href="#" class="kt-notification-v2__item">
                                            <div class="kt-notification-v2__itek-wrapper">
                                                <div class="kt-notification-v2__item-title">
                                                    <h6 class="kt-portlet__head-title kt-font-primary">
                                                        Item Code
                                                    </h6>
                                                </div>
                                                <div class="kt-notification-v2__item-desc">
                                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                                        <?php echo $item_code; ?>
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div href="#" class="kt-notification-v2__item">
                                            <div class="kt-notification-v2__itek-wrapper">
                                                <div class="kt-notification-v2__item-title">
                                                    <h6 class="kt-portlet__head-title kt-font-primary">
                                                        Unit of Measure
                                                    </h6>
                                                </div>
                                                <div class="kt-notification-v2__item-desc">
                                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                                        <?php echo $unit_of_measure ?>
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div href="#" class="kt-notification-v2__item">
                                            <div class="kt-notification-v2__itek-wrapper">
                                                <div class="kt-notification-v2__item-title">
                                                    <h6 class="kt-portlet__head-title kt-font-primary">
                                                        Quantity
                                                    </h6>
                                                </div>
                                                <div class="kt-notification-v2__item-desc">
                                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                                        <?php echo $quantity ?>
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div href="#" class="kt-notification-v2__item">
                                            <div class="kt-notification-v2__itek-wrapper">
                                                <div class="kt-notification-v2__item-title">
                                                    <h6 class="kt-portlet__head-title kt-font-primary">
                                                        Unit Price
                                                    </h6>
                                                </div>
                                                <div class="kt-notification-v2__item-desc">
                                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                                        <?php echo $unit_price; ?>
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div href="#" class="kt-notification-v2__item">
                                            <div class="kt-notification-v2__itek-wrapper">
                                                <div class="kt-notification-v2__item-title">
                                                    <h6 class="kt-portlet__head-title kt-font-primary">
                                                        Subtotal
                                                    </h6>
                                                </div>
                                                <div class="kt-notification-v2__item-desc">
                                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                                        <?php echo $subtotal; ?>
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach;?>
                                <?php else: ?>
                                <h4 class="text-center font-italic">No sales order items found</h4>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- begin:: Footer -->