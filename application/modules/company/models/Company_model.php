<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Company_model extends MY_Model {

	public $table = 'companies'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = ['name', 'location', 'email', 'year_started', 'contact_number', 'website', 'tin', 'sss', 'philhealth', 'hdmf', 'sec_number', 'inc_date', 'bir_reg_date', 'image','header','footer']; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [
		'name' => array(
			'field'=>'name',
			'label'=>'Name',
			'rules'=>'trim|required'
		),
		'location' => array(
			'field'=>'location',
			'label'=>'Location',
			'rules'=>'trim|required'
		),
		'email' => array(
			'field'=>'email',
			'label'=>'Email',
			'rules'=>'trim|valid_email',
			'errors' => array ('trim' => 'Error message for rule "trim" for field email',
					'valid_email' => 'Error message for rule "valid_email" for field email')
		),
		'year_started' => array(
			'field'=>'year_started',
			'label'=>'Year Started',
			'rules'=>'trim'
		),
		'contact_number' => array(
			'field'=>'contact_number',
			'label'=>'Contact Number',
			'rules'=>'trim'
		),
		'website' => array(
			'field'=>'website',
			'label'=>'Website',
			'rules'=>'trim'
		),
		'tin' => array(
			'field'=>'tin',
			'label'=>'TIN',
			'rules'=>'trim'
		),
		'sss' => array(
			'field'=>'sss',
			'label'=>'SSS',
			'rules'=>'trim'
		),
		'philhealth' => array(
			'field'=>'philhealth',
			'label'=>'PhilHealth',
			'rules'=>'trim'
		),
		'hdmf' => array(
			'field'=>'hdmf',
			'label'=>'HDMF',
			'rules'=>'trim'
		),
		'sec_number' => array(
			'field'=>'sec_number',
			'label'=>'SEC Number',
			'rules'=>'trim'
		),
		'inc_date' => array(
			'field'=>'inc_date',
			'label'=>'Incorporation Date',
			'rules'=>'trim'
		),
		'bir_reg_date' => array(
			'field'=>'bir_reg_date',
			'label'=>'BIR Registration Date',
			'rules'=>'trim'
		),
		'image' => array(
			'field'=>'image',
			'label'=>'Company Logo',
			'rules'=>'trim'
		),
		'header' => array(
			'field'=>'image',
			'label'=>'Company Header',
			'rules'=>'trim'
		),
		'footer' => array(
			'field'=>'image',
			'label'=>'Company Header',
			'rules'=>'trim'
		)
	];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;

		$this->has_many['project'] = array('foreign_model'=>'project/Project_model','foreign_table'=>'projects','foreign_key'=>'company_id','local_key'=>'id');

	}

	function get_columns () 
	{

		$_return	=	FALSE;

		if ( $this->fillable ) {

			$_return	=	$this->fillable;
		}

		return $_return;
	}

	public function insert_dummy()
	{
		require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

		$data = [];

        for($x = 0; $x < 20; $x++)
        {
            array_push($data,array(
				'name'=> $faker->name,
				'location' => $faker->address,
				'email' => $faker->email,
				'year_started' => $faker->year,
				'contact_number' => $faker->phoneNumber,
				'website' => $faker->url
            ));
		}
		$this->db->insert_batch($this->table, $data);

	}
}