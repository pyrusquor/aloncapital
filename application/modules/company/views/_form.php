<?php
    $name            =    isset($company['name']) && $company['name'] ? $company['name'] : '';
    $location        =    isset($company['location']) && $company['location'] ? $company['location'] : '';
    $email            =    isset($company['email']) && $company['email'] ? $company['email'] : '';
    $year_started    =    isset($company['year_started']) && $company['year_started'] ? $company['year_started'] : '';
    $contact_number    =    isset($company['contact_number']) && $company['contact_number'] ? $company['contact_number'] : '';
    $tin            =    isset($company['tin']) && $company['tin'] ? $company['tin'] : '';
    $sss            =    isset($company['sss']) && $company['sss'] ? $company['sss'] : '';
    $philhealth        =    isset($company['philhealth']) && $company['philhealth'] ? $company['philhealth'] : '';
    $hdmf        =    isset($company['hdmf']) && $company['hdmf'] ? $company['hdmf'] : '';
    $sec_number        =    isset($company['sec_number']) && $company['sec_number'] ? $company['sec_number'] : '';
    $website        =    isset($company['website']) && $company['website'] ? $company['website'] : '';
    $inc_date        =    isset($company['inc_date']) && $company['inc_date'] ? $company['inc_date'] : '';
    $bir_reg_date    =    isset($company['bir_reg_date']) && $company['bir_reg_date'] ? $company['bir_reg_date'] : '';
    $image    =    isset($company['image']) && $company['image'] ? $company['image'] : '';
    $header    =    isset($company['header']) && $company['header'] ? $company['header'] : '';
    $footer    =    isset($company['footer']) && $company['footer'] ? $company['footer'] : '';
?>


<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Company Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Company Name" autocomplete="off">
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Year Started</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control yearPicker" name="year_started" value="<?php echo set_value('year_started', $year_started); ?>" placeholder="Year Started" autocomplete="off">
            </div>
            <?php echo form_error('year_started'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Location (Full Address) <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="location" value="<?php echo set_value('location', $location); ?>" placeholder="Full Address" autocomplete="off">
            </div>
            <?php echo form_error('location'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Contact Number</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="contact_number" value="<?php echo set_value('contact_number', $contact_number); ?>" placeholder="Contact" autocomplete="off">
            </div>
            <?php echo form_error('contact_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Email Address</label>
            <div class="kt-input-icon">
                <input type="email" class="form-control" name="email" value="<?php echo set_value('email', $email); ?>" placeholder="Email Address" autocomplete="off">
            </div>
            <?php echo form_error('email'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Website</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="website" value="<?php echo set_value('website', $website); ?>" placeholder="Website" autocomplete="off">
            </div>
            <?php echo form_error('website'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>TIN</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="tin" value="<?php echo set_value('tin', $tin); ?>" placeholder="TIN" autocomplete="off">
            </div>
            <?php echo form_error('website'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>SSS</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="sss" value="<?php echo set_value('sss', $sss); ?>" placeholder="SSS" autocomplete="off">
            </div>
            <?php echo form_error('sss'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>PhilHealth</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="philhealth" value="<?php echo set_value('philhealth', $philhealth); ?>" placeholder="PhilHealth" autocomplete="off">
            </div>
            <?php echo form_error('philhealth'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>HDMF</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="hdmf" value="<?php echo set_value('hdmf', $hdmf); ?>" placeholder="HDMF" autocomplete="off">
            </div>
            <?php echo form_error('hdmf'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>SEC Number</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="sec_number" value="<?php echo set_value('sec_number', $sec_number); ?>" placeholder="SEC Number" autocomplete="off">
            </div>
            <?php echo form_error('sec_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Incorporation Date</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control compDatepicker" readonly name="inc_date" value="<?php echo set_value('inc_date', $inc_date); ?>" placeholder="Incorporation Date" autocomplete="off">
            </div>
            <?php echo form_error('inc_date'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>BIR Registration Date</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control compDatepicker" readonly name="bir_reg_date" value="<?php echo set_value('bir_reg_date', $bir_reg_date); ?>" placeholder="BIR Registration Date" autocomplete="off">
            </div>
            <?php echo form_error('bir_reg_date'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Company Logo</label>
            <div>
                <?php if(get_image('company', 'images', $image)): ?>
                    <img class="kt-widget__img" src="<?php echo base_url(get_image('company', 'images', $image)); ?>" width="90px" height="90px"/>
                <?php endif; ?>
                <span class="btn btn-sm">
                    <input type="file" name="company_image" class="" aria-invalid="false">
                </span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-sm-6">
        <div class="form-group">
            <label>Company Header</label>
            <div>
                <?php if(get_image('company', 'header', $header)): ?>
                    <img class="kt-widget__img" src="<?php echo base_url(get_image('company', 'header', $header)); ?>" width="950px" height="150px"/>
                <?php endif; ?>
                <span class="btn btn-sm">
                    <input type="file" name="company_header" class="" aria-invalid="false">
                </span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<div class="row">


<div class="col-sm-6">
        <div class="form-group">
            <label>Company Footer</label>
            <div>
                <?php if(get_image('company', 'footer', $footer)): ?>
                    <img class="kt-widget__img" src="<?php echo base_url(get_image('company', 'footer', $footer)); ?>" width="950px" height="150px"/>
                <?php endif; ?>
                <span class="btn btn-sm">
                    <input type="file" name="company_footer" class="" aria-invalid="false">
                </span>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>


<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit">
</div>

<!-- <div class="kt-section">
    <div class="kt-section__body">
        <h5>Additional Custom Fields</h5>
    </div>
</div> -->