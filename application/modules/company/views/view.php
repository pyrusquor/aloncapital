<?php
$id			=	isset($company['id']) && $company['id'] ? $company['id'] : '';
$name			=	isset($company['name']) && $company['name'] ? $company['name'] : '';
$location		=	isset($company['location']) && $company['location'] ? $company['location'] : '';
$email			=	isset($company['email']) && $company['email'] ? $company['email'] : 'Not Available';
$year_started	=	isset($company['year_started']) && $company['year_started'] ? $company['year_started'] : 'Not Available';
$contact_number	=	isset($company['contact_number']) && $company['contact_number'] ? $company['contact_number'] : 'Not Available';
$tin			=	isset($company['tin']) && $company['tin'] ? $company['tin'] : 'Not Available';
$sss			=	isset($company['sss']) && $company['sss'] ? $company['sss'] : 'Not Available';
$philhealth		=	isset($company['philhealth']) && $company['philhealth'] ? $company['philhealth'] : 'Not Available';
$hdmf		=	isset($company['hdmf']) && $company['hdmf'] ? $company['hdmf'] : 'Not Available';
$sec_number		=	isset($company['sec_number']) && $company['sec_number'] ? $company['sec_number'] : 'Not Available';
$website		=	isset($company['website']) && $company['website'] ? $company['website'] : 'Not Available';
$inc_date		=	isset($company['inc_date']) && $company['inc_date'] ? $company['inc_date'] : 'Not Available';
$bir_reg_date	=	isset($company['bir_reg_date']) && $company['bir_reg_date'] ? $company['bir_reg_date'] : 'Not Available';
$header    =    isset($company['header']) && $company['header'] ? $company['header'] : '';
$footer    =    isset($company['footer']) && $company['footer'] ? $company['footer'] : '';
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">View Company</h3>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<a href="<?php echo site_url('company/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
					<i class="fa fa-edit"></i> Edit
				</a>
				<a href="<?php echo site_url('company');?>" class="btn btn-label-instagram btn-sm btn-elevate">
					<i class="fa fa-reply"></i> Back
				</a>
			</div>
		</div>
	</div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-md-5">

			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							General Information
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">

					<!--begin::Form-->
					<div class="kt-widget13">
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Company Name
							</span>
							<span class="kt-widget13__text kt-widget13__text--bold">
								<?php echo $name; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc kt-align-right">
								Year Started:
							</span>
							<span class="kt-widget13__text">
								<?php echo $year_started; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Location (Full Address)
							</span>
							<span class="kt-widget13__text kt-widget13__text--bold">
								<?php echo $location; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Contact Number:
							</span>
							<span class="kt-widget13__text">
								<?php echo $contact_number; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Emal Address:
							</span>
							<span class="kt-widget13__text">
								<?php echo $email; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Website:
							</span>
							<span class="kt-widget13__text">
								<?php echo $website; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								TIN:
							</span>
							<span class="kt-widget13__text">
								<?php echo $tin; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								SSS:
							</span>
							<span class="kt-widget13__text">
								<?php echo $sss; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								PhilHealth:
							</span>
							<span class="kt-widget13__text">
								<?php echo $philhealth; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								HDMF:
							</span>
							<span class="kt-widget13__text">
								<?php echo $hdmf; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								SEC Number:
							</span>
							<span class="kt-widget13__text">
								<?php echo $sec_number; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Incorporation Date:
							</span>
							<span class="kt-widget13__text">
								<?php echo $inc_date; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								BIR REgistration Date:
							</span>
							<span class="kt-widget13__text">
								<?php echo $bir_reg_date; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Company Header:
							</span>
							<span class="kt-widget13__text">
							<?php if(get_image('company', 'header', $header)): ?>
                    <img class="kt-widget__img" src="<?php echo base_url(get_image('company', 'header', $header)); ?>" width="200px" height="32px"/>
                <?php endif; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Company Footer:
							</span>
							<span class="kt-widget13__text">
							<?php if(get_image('company', 'footer', $footer)): ?>
                    <img class="kt-widget__img" src="<?php echo base_url(get_image('company', 'footer', $footer)); ?>" width="200px" height="32px"/>
                <?php endif; ?>
							</span>
						</div>

					</div>

					<!--end::Form-->
				</div>
			</div>
			<!--end::Portlet-->

		</div>
		<div class="col-md-7">

			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Projects
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">

					<table class="table table-striped- table-bordered table-hover table-checkable" id="company_projects">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Address</th>
								<th>Project Type</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php if( ! empty($company['project']) ): ?>
								<?php foreach ($company['project'] as $key => $value): ?>
									<tr>
										<td><?php echo $value['id']; ?></td>
										<td><?php echo $value['name']; ?></td>
										<td><?php echo $value['location']; ?></td>
										<td><?php echo Dropdown::get_static('project_type', @$value['type'], 'view');?></td>
										<td><?php echo Dropdown::get_static('project_status', @$value['status'], 'view');?></td>
										<td nowrap>
											<a href="<?php echo base_url('project/view/'.$value['id']); ?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
												<i class="la la-eye"></i>
											</a>
										</td>
									</tr>
								<?php endforeach; ?>
							<?php endif; ?>

						</tbody>
					</table>
				</div>
			</div>

			<!--end::Portlet-->
		</div>
	</div>
</div>
<!-- begin:: Footer -->