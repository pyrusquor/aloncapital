<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Company extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('company/Company_model', 'M_company');

        // Format Helper
        $this->load->helper(['format', 'images']);
    }

    public function get_all_companies()
    {
        $data = $this->M_company->as_array()->get_all();
        $output = array(
            'data' => $data,
        );
        echo json_encode($output);
    }

    public function index()
    {

        $db_columns = $this->M_company->get_columns();
        if ($db_columns) {

            $column = [];
            foreach ($db_columns as $key => $dbclm) {

                $name = isset($dbclm) && $dbclm ? $dbclm : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {

                    if (strpos($name, '_id') !== false) {

                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {

                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {

                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {

                    continue;
                }
            }

            $ccount = count($column);
            $cceil = ceil(($ccount / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $ccompany = $this->M_company->count_rows();
            if ($ccompany) {

                $this->view_data['total'] = $ccompany;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function create()
    {
        if ($this->input->post()) {
            $company_info = $this->input->post();
            // Image Upload
            $upload_path = './assets/uploads/company/images';
            $config = array();
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 5000;
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config, 'company_image');
            $this->company_image->initialize($config);
            $config['upload_path'] = './assets/uploads/company/header';
            $this->load->library('upload', $config, 'company_header');
            $this->company_header->initialize($config);
            $config['upload_path'] = './assets/uploads/company/footer';
            $this->load->library('upload', $config, 'company_footer');
            $this->company_footer->initialize($config);

            if (!empty($_FILES['company_image']['name'])) {

                if (!$this->company_image->do_upload('company_image')) {
                    $this->notify->error($this->company_image->display_errors(), 'company/create');
                } else {
                    $img_data = $this->company_image->data();
                    $company_info['image'] = $img_data['file_name'];
                }
            }
            if (!empty($_FILES['company_header']['name'])) {

                if (!$this->company_header->do_upload('company_header')) {
                    $this->notify->error($this->company_header->display_errors(), 'company/create');
                } else {
                    $header_data = $this->company_header->data();
                    $company_info['header'] = $header_data['file_name'];
                }
            }
            if (!empty($_FILES['company_footer']['name'])) {

                if (!$this->company_footer->do_upload('company_footer')) {
                    $this->notify->error($this->company_footer->display_errors(), 'company/create');
                } else {
                    $footer_data = $this->company_footer->data();
                    $company_info['footer'] = $footer_data['file_name'];
                }
            }

            $result = $this->M_company->insert($company_info);

            if ($result === false) {

                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {

                // Success
                $this->notify->success('Company successfully created.', 'company');
            }
        }

        $this->template->build('create');
    }

    public function update($id = false)
    {
        $this->css_loader->queue("vendors/general/select2/dist/css/select2.css");
        $this->js_loader->queue("vendors/general/select2/dist/js/select2.full.js");
        if ($id) {

            $this->view_data['company'] = $data = $this->M_company->get($id);

            if ($data) {

                if ($this->input->post()) {

                    $result = $this->M_company->from_form()->update(null, $data['id']);
                    if ($result) {

                        // Image Upload
                        $upload_path = './assets/uploads/company/images';
                        $config = array();
                        $config['upload_path'] = $upload_path;
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = 5000;
                        $config['encrypt_name'] = TRUE;

                        $this->load->library('upload', $config, 'company_image');
                        $this->company_image->initialize($config);
                        $config['upload_path'] = './assets/uploads/company/header';
                        $this->load->library('upload', $config, 'company_header');
                        $this->company_header->initialize($config);
                        $config['upload_path'] = './assets/uploads/company/footer';
                        $this->load->library('upload', $config, 'company_footer');
                        $this->company_footer->initialize($config);

                        if (!empty($_FILES['company_image']['name'])) {

                            if (!$this->company_image->do_upload('company_image')) {
                                $this->notify->error($this->company_image->display_errors(), 'company/create');
                            } else {
                                $img_data = $this->company_image->data();
                                $additional_fields['image'] = $img_data['file_name'];
                            }
                        }
                        if (!empty($_FILES['company_header']['name'])) {

                            if (!$this->company_header->do_upload('company_header')) {
                                $this->notify->error($this->company_header->display_errors(), 'company/create');
                            } else {
                                $header_data = $this->company_header->data();
                                $additional_fields['header'] = $header_data['file_name'];
                            }
                        }
                        if (!empty($_FILES['company_footer']['name'])) {

                            if (!$this->company_footer->do_upload('company_footer')) {
                                $this->notify->error($this->company_footer->display_errors(), 'company/create');
                            } else {
                                $footer_data = $this->company_footer->data();
                                $additional_fields['footer'] = $footer_data['file_name'];
                            }
                        }

                        $additional_fields['updated_by'] = $this->session->userdata('user_id');
                        $additional_fields['updated_at'] = date('Y-m-d');

                        $company_result = $this->M_company->from_form(NULL, $additional_fields)->update(NULL, $data['id']);

                        if ($company_result === false) {

                            // Validation
                            $this->notify->error('Oops something went wrong.');
                        } else {

                            // Success
                            $this->notify->success('Company successfully updated.', 'company');
                        }
                    } else {
                        $this->notify->error('Oops something went wrong.');
                    }
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function view($id = false)
    {
        if ($id) {

            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['company'] = $this->M_company->with_project('fields: id, name, location, type, status')->get($id);

            if ($this->view_data['company']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_company->get($id);
            if ($list) {

                $deleted = $this->M_company->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Company successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function showCompanies()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'name' => true,
            'location' => true,
            'contact_number' => true,
            'year_started' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        $companies = $this->M_company->as_array()->get_all();
        $data = [];

        if ($companies) {

            foreach ($companies as $key => $value) {

                $companies[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

                $companies[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
            }

            foreach ($companies as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                $_column = $_REQUEST['order'][0]['column'] - 1;
                $_dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($x, $y) use ($_column, $_dir) {

                    // echo "<pre>";
                    // print_r($x) ;echo "<br>";
                    // echo $_column;echo "<br>";

                    $x = array_slice($x, $_column, 1);

                    // vdebug($x);

                    $x = array_pop($x);

                    $y = array_slice($y, $_column, 1);
                    $y = array_pop($y);

                    if ($_dir === 'asc') {

                        return $x > $y ? true : false;
                    } else {

                        return $x < $y ? true : false;
                    }
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_company->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Company successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function get_table_schema()
    {
        $query = $this->db->query('SHOW COLUMNS FROM companies');
        $queryResultArray = $query->result_array();
        $array = array();
        $company_fillable_fields = $this->M_company->fillable;
        $ctr = 1;
        foreach ($queryResultArray as $field) {
            if (in_array($field['Field'], $company_fillable_fields)) {
                $det = array();
                $det['no'] = $ctr;
                $det['name'] = $field['Field'];
                $det['type'] = $field['Type'];
                $det['format'] = '';
                $det['option'] = '';
                $det['required'] = ($field['Null'] == 'NO') ? 'Yes' : 'No';

                $array[] = $det;
                $ctr++;
            }
        }

        $_response = [];

        $_total['_displays'] = 0;
        $_total['_records'] = 0;

        $_columns = [
            'no' => true,
            'name' => true,
            'type' => true,
            'format' => true,
            'option' => true,
            'required' => true,
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {

            $_columns = [];

            foreach ($_REQUEST['columnsDef'] as $_dkey => $_def) {

                $_columns[$_def] = true;
            }
        }

        $_checklists = $array;

        if ($_checklists) {

            $_datas = [];

            foreach ($_checklists as $_ckkey => $_ck) {

                $_datas[] = $this->filterArray($_ck, $_columns);
            }

            $_total['_displays'] = $_total['_records'] = count($_datas);

            if (isset($_REQUEST['search'])) {

                $_datas = $this->filterKeyword($_datas, $_REQUEST['search']);

                $_total['_displays'] = $_datas ? count($_datas) : 0;
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {

                foreach ($_REQUEST['columns'] as $_ckey => $_clm) {

                    if ($_clm['search']) {

                        $_datas = $this->filterKeyword($_datas, $_clm['search'], $_clm['data']);

                        $_total['_displays'] = $_datas ? count($_datas) : 0;
                    }
                }
            }

            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                $_column = $_REQUEST['order'][0]['column'];
                $_dir = $_REQUEST['order'][0]['dir'];

                usort($_datas, function ($x, $y) use ($_column, $_dir) {

                    $x = array_slice($x, $_column, 1);
                    $x = array_pop($x);

                    $y = array_slice($y, $_column, 1);
                    $y = array_pop($y);

                    if ($_dir === 'asc') {

                        return $x > $y ? true : false;
                    } else {

                        return $x < $y ? true : false;
                    }
                });
            }

            if (isset($_REQUEST['length'])) {

                $_datas = array_splice($_datas, $_REQUEST['start'], $_REQUEST['length']);
            }

            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {

                $_temp = $_datas;
                $_datas = [];

                foreach ($_temp as $key => $_tmp) {

                    $_datas[] = array_values($_tmp);
                }
            }

            $_secho = 0;
            if (isset($_REQUEST['sEcho'])) {

                $_secho = intval($_REQUEST['sEcho']);
            }

            $_response = [
                'iTotalDisplayRecords' => $_total['_displays'],
                'iTotalRecords' => $_total['_records'],
                'sColumns' => '',
                'sEcho' => $_secho,
                'data' => $_datas,
            ];
        }

        echo json_encode($_response);
    }

    public function export_csv()
    {
        $date_file_name = date('F d Y', strtotime(date("Y-m-d H:i:s")));
        $date_title_name = date('F d, Y', strtotime(date("Y-m-d H:i:s")));

        $file = FCPATH . 'assets/excel_templates/csv_company.csv';
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->setPreCalculateFormulas(true);

        $baseRow = 1;
        $baseColumn = 'A';

        $query = $this->db->query('SHOW COLUMNS FROM companies');
        $queryResultArray = $query->result_array();
        $array = array();
        $company_fillable_fields = $this->M_company->fillable;

        if ($this->input->post('status') == '1') {
            array_push($company_fillable_fields, "id");
        }

        foreach ($queryResultArray as $field) {
            if (in_array($field['Field'], $company_fillable_fields)) {
                $objPHPExcel->getActiveSheet()->setCellValue($baseColumn . $baseRow, $field['Field']);
                $baseColumn++;
            }
        }

        // reset bases
        $baseRow++;
        $baseColumn = 'A';

        if ($this->input->post('status') == '1') {

            $alldata = $this->M_company->as_array()->get_all();

            if ($alldata) {
                foreach ($alldata as $p) {
                    foreach ($queryResultArray as $field) {
                        if (in_array($field['Field'], $company_fillable_fields)) {
                            $objPHPExcel->getActiveSheet()->setCellValue($baseColumn . $baseRow, $p[$field['Field']]);
                            $baseColumn++;
                        }
                    }
                    // reset bases
                    $baseRow++;
                    $baseColumn = 'A';
                    if ($this->input->post('status') == 0) {
                        break;
                    }
                }
            }
        }

        $file_name = "Company CSV Template.csv";

        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="' . $file_name . '"');
        // Write file to the browser
        $objWriter->save('php://output');
    }

    public function import()
    {
        // print_r($_FILES);
        // print_r($this->input->post()); exit;

        $file = $_FILES['file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        // print_r($sheetData); exit;
        $err = 0;

        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        'name' => $upload_data[1],
                        'location' => $upload_data[2],
                        'email' => $upload_data[3],
                        'year_started' => $upload_data[4],
                        'contact_number' => $upload_data[5],
                        'tin' => $upload_data[6],
                        'sss' => $upload_data[7],
                        'philhealth' => $upload_data[8],
                        'hdmf' => $upload_data[9],
                        'sec_number' => $upload_data[10],
                        'website' => $upload_data[11],
                        'inc_date' => $upload_data[12],
                        'bir_reg_date' => $upload_data[13],
                    );

                    $company_id = $upload_data[0];
                    $company = $this->M_company->get($company_id);

                    if ($company) {
                        $result = $this->M_company->update($fields, $company_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            'name' => $upload_data[0],
                            'location' => $upload_data[1],
                            'email' => $upload_data[2],
                            'year_started' => $upload_data[3],
                            'contact_number' => $upload_data[4],
                            'tin' => $upload_data[5],
                            'sss' => $upload_data[6],
                            'philhealth' => $upload_data[7],
                            'hdmf' => $upload_data[8],
                            'sec_number' => $upload_data[9],
                            'website' => $upload_data[10],
                            'inc_date' => $upload_data[11],
                            'bir_reg_date' => $upload_data[12],
                        );

                        $result = $this->M_company->insert($fields);
                    }
                }
                if ($result === false) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'company');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'company');
        die();
    }

    public function export()
    {

        $db_columns = [];
        $alphas = [];
        $data = [];

        $titles[] = '#';

        $start = 3;
        $row = 2;
        $no = 1;

        $companies = $this->M_company->get_all();
        if ($companies) {

            foreach ($companies as $ckey => $comp) {

                $data[$comp['id']]['#'] = $no;

                $no++;
            }

            $_filename = 'list_of_companies' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();
            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {
                        $db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
                }
            } else {

                $db_columns = $this->M_company->get_columns();

                if (!$db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
                }
            }

            if ($db_columns) {

                foreach ($db_columns as $key => $dbclmn) {

                    $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                    if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {

                        $titles[] = $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';

                        foreach ($companies as $ckey => $comp) {

                            $data[$comp['id']][$_title] = isset($comp[$name]) && $comp[$name] ? $comp[$name] : '';
                        }
                    } else {

                        continue;
                    }
                }

                for ($i = 65; $i < (65 + count($titles)); $i++) {

                    $alphas[] = chr($i);
                }

                $_xls_columns = array_combine($alphas, $titles);
                $_lastAlpha = end($alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title = ucwords(strtolower($_column));

                    $_objSheet->setCellValue($_xkey . $row, $_title);
                }

                $_objSheet->setTitle('List of Companies');
                $_objSheet->setCellValue('A1', 'LIST OF COMPANIES');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                foreach ($companies as $key => $comp) {

                    $comp_id = isset($comp['id']) && $comp['id'] ? $comp['id'] : '';

                    foreach ($alphas as $_akey => $_alpha) {

                        $_value = isset($data[$comp_id][$_xls_columns[$_alpha]]) && $data[$comp_id][$_xls_columns[$_alpha]] ? $data[$comp_id][$_xls_columns[$_alpha]] : '';

                        $_objSheet->setCellValue($_alpha . $start, $_value);
                    }

                    $start++;
                }

                foreach ($alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
                }

                $_style = array(
                    'font' => array(
                        'bold' => true,
                        'size' => 10,
                        'name' => 'Verdana',
                    ),
                );

                $_objSheet->getStyle('A1:' . $_lastAlpha . $row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
            }
        } else {

            $this->notify->error('No Record Found', 'document');
        }
    }
}
