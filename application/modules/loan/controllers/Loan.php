<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Loan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('loan/Loan_model', 'M_Loan');
        $this->load->model('loan/Loan_payment_model', 'M_Loan_payment');
        $this->load->model('loan/Loan_disbursements_model', 'M_Loan_disbursement');
        $this->load->model('applicant/Applicant_model', 'M_applicant');

        $this->_table_fillables = $this->M_Loan->fillable;
        $this->_table_columns = $this->M_Loan->__get_columns();
        $this->load->library('communication_library');
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Loan->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Loan->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showLoans()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            /* ==================== begin: Add model fields ==================== */
            'reference' => true,
            'applicant_id' => true,
            'application_at' => true,
            'loan_amount' => true,
            'loan_period' => true,
            'status' => true,
            'status_id' => true,
            'terms' => true,
            'created_by' => true,
            'updated_by' => true
            /* ==================== end: Add model fields ==================== */
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        if ($this->ion_auth->is_buyer()) {
            $buyer_id = get_value_field($this->session->userdata['user_id'], 'applicants', 'id', 'user_id');
            // $this->M_Loan->where("created_by", $buyer_id);
            $this->M_Loan->where("applicant_id", $buyer_id);
        }
        $loan = $this->M_Loan->with_applicant()->order_by('id', 'DESC')->as_array()->get_all();
        // vdebug($loan);
        $data = [];

        if ($loan) {

            foreach ($loan as $key => $value) {

                $type = Dropdown::get_static('applicant_type',@$value['applicant']['type_id'],"view");

                $loan[$key]['applicant_id'] = '<div><a href="/applicant/view/' . $value['applicant_id'] . '" target="_blank">' . get_person_name($value['applicant_id'], "applicants") . '</a><br>'.$type.'</div>';

                $loan[$key]['application_at'] = '<div>' . ($value['application_at'] ? view_date($value['application_at']) : '') . '</div>';

                $loan[$key]['loan_amount'] = money_php($value['loan_amount']);

                $loan[$key]['loan_period'] = ($value['loan_period']." Mo(s)");
                $stat = Dropdown::get_static('loan_status',$value['status'],"view");
                $loan[$key]['status'] = "<span class='stat_".$stat."'>".Dropdown::get_static('loan_status',$value['status'],"view")."</span><br>".substr($value['status_remarks'], 0,75)."...";

                $loan[$key]['status_id'] = $value['status'];

                $loan[$key]['terms'] = Dropdown::get_static('loan_terms',$value['terms'],"view");

                $loan[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                $loan[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
            }

            foreach ($loan as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'];
                $dir = $_REQUEST['order'][0]['dir'];
                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    public function create()
    {
        if ($this->input->post()) {
            $_input = $this->input->post();
            $_input['reference'] = uniqidReal();

             // Image Upload
            $upload_path = './assets/uploads/applicant/files';
            $config = array();
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'pdf|png|jpeg|jpg|docx|xls|zip|pptx';
            $config['max_size'] = 5000;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config, 'applicant_files');
            $this->applicant_files->initialize($config);

            // vdebug($_FILES);

            if (!empty($_FILES['coe_bir_certificate']['name'])) {
                if (!$this->applicant_files->do_upload('coe_bir_certificate')) {
                    $this->notify->error($this->applicant_files->display_errors(), 'loan/create');
                    // vdebug($this->applicant_files->display_errors());
                } else {
                    $img_data = $this->applicant_files->data();
                    $_input['coe_bir_certificate'] = $img_data['file_name'];
                    // vdebug($img_data);
                }
            }

            if (!empty($_FILES['latest_financial']['name'])) {
                if (!$this->applicant_files->do_upload('latest_financial')) {
                    $this->notify->error($this->applicant_files->display_errors(), 'loan/create');
                    // vdebug($this->applicant_files->display_errors());
                } else {
                    $img_data = $this->applicant_files->data();
                    $_input['latest_financial'] = $img_data['file_name'];
                }
            }

            
            if (!empty($_FILES['latest_payslip_itr_company']['name'])) {
                if (!$this->applicant_files->do_upload('latest_payslip_itr_company')) {
                    $this->notify->error($this->applicant_files->display_errors(), 'loan/create');
                    // vdebug($this->applicant_files->display_errors());
                } else {
                    $img_data = $this->applicant_files->data();
                    $_input['latest_payslip_itr_company'] = $img_data['file_name'];
                }
            }

            if (!empty($_FILES['latest_annual_proprietor_borrower']['name'])) {
                if (!$this->applicant_files->do_upload('latest_annual_proprietor_borrower')) {
                    $this->notify->error($this->applicant_files->display_errors(), 'loan/create');
                    // vdebug($this->applicant_files->display_errors());
                } else {
                    $img_data = $this->applicant_files->data();
                    $_input['latest_annual_proprietor_borrower'] = $img_data['file_name'];
                }
            }

             if (!empty($_FILES['latest_bank_statement']['name'])) {
                if (!$this->applicant_files->do_upload('latest_bank_statement')) {
                    $this->notify->error($this->applicant_files->display_errors(), 'loan/create');
                    // vdebug($this->applicant_files->display_errors());
                } else {
                    $img_data = $this->applicant_files->data();
                    $_input['latest_bank_statement'] = $img_data['file_name'];
                }
            }


            if (!empty($_FILES['government_id']['name'])) {
                if (!$this->applicant_files->do_upload('government_id')) {
                    $this->notify->error($this->applicant_files->display_errors(), 'loan/create');
                    // vdebug($this->applicant_files->display_errors());
                } else {
                    $img_data = $this->applicant_files->data();
                    $_input['government_id'] = $img_data['file_name'];
                }
            }

            if (!empty($_FILES['proof_of_billing']['name'])) {
                if (!$this->applicant_files->do_upload('proof_of_billing')) {
                    $this->notify->error($this->applicant_files->display_errors(), 'loan/create');
                    // vdebug($this->applicant_files->display_errors());
                } else {
                    $img_data = $this->applicant_files->data();
                    $_input['proof_of_billing'] = $img_data['file_name'];
                }
            }

            if (!empty($_FILES['checking_account_details']['name'])) {
                if (!$this->applicant_files->do_upload('checking_account_details')) {
                    $this->notify->error($this->applicant_files->display_errors(), 'loan/create');
                    // vdebug($this->applicant_files->display_errors());
                } else {
                    $img_data = $this->applicant_files->data();
                    $_input['checking_account_details'] = $img_data['file_name'];
                }
            }

            $additional = [
                'created_by' => $this->user->id,
                'created_at' => NOW
            ];

            $loan_id = $this->M_Loan->insert($_input + $additional);
            $source = get_value_field($_input['applicant_id'],'applicants','source_referral');

            $post['first_name'] = get_value_field($input['applicant_id'],'applicants','first_name');
            $post['middle_name'] = get_value_field($input['applicant_id'],'applicants','middle_name');
            $post['last_name'] = get_value_field($input['applicant_id'],'applicants','last_name');
            $post['email'] = get_value_field($input['applicant_id'],'applicants','email');

            $_u_full_name = $post['first_name'] . " " .$post['middle_name'] . " " .$post['last_name'];
    
            $recipient['subject'] = "Alon Capital - Loan Application";
            $recipient['name'] = $_u_full_name;
            $recipient['email'] = $post['email'];

            $content = "Your loan application is already submitted. Please wait for update. Thank you.";

            // $content = $this->load->view('emails/registration',$recipient,true);

            // $this->communication_library->send_email($recipient, $content);
            if ($source == 5) {
                $this->send_loan_info($loan_id);
            }

            if ($result === false) {
                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {
                // Success
                $this->notify->success('Loan application is successfully submitted.', 'loan');
            }
        }

        $this->template->build('create');
    }

    public function process_loan_schedule($loan_id = 0,$debug = 0, $reprocess = 0){

        if ($reprocess) {
            // $this->db->where('loan_id',$loan_id);
            // $this->M_Loan_payment->delete(array('loan_id' => $loan_id));
        }

        $loan = $this->M_Loan->get($loan_id);
        $loan_period = $loan['loan_period'];
        $loan_amount = $loan['loan_amount'];
        $loan_interest = $loan['loan_interest'];
        $date = ( $loan['loan_effectivity_date'] ? $loan['loan_effectivity_date'] : date('Y-m-d'));
        $monthly_payment = ( $loan['loan_amount'] / $loan_period );
        $factor_rate = 2;
        $sum_amount = 0;
        $loan_term = $loan['terms'];
        if ($loan_term == 1) { // lumpsum
            $loan_period = $loan_period - 1;
            for( $i = 1; $i <= ( $loan_period ); $i++) {
                $countterm = $i;

                /* MONTHLY BREAKDOWN START */
                $breakdown['monthly'][$i]['month'] = $i;
            
                if( $i == 1 ) {
                    $breakdown['monthly'][$i]['beginning_balance'] = round( $loan_amount, $factor_rate );
                } else {
                    $breakdown['monthly'][$i]['beginning_balance'] = round( $breakdown['monthly'][($i - 1)]['ending_balance'], $factor_rate );
                }

                $interest =  $loan_amount * $this->compute_percentage( $loan_interest );
                
                $breakdown['monthly'][$i]['interest'] = $interest;

                if ($loan_period == $countterm) {
                    $breakdown['monthly'][$i]['principal'] = $loan_amount;
                    $breakdown['monthly'][$i]['total_amount'] = $loan_amount + $interest;
                } else {
                    $breakdown['monthly'][$i]['principal'] = 0;
                    $breakdown['monthly'][$i]['total_amount'] = $monthly_payment + $interest;
                }
                    
                $breakdown['monthly'][$i]['ending_balance'] = round( $breakdown['monthly'][$i]['beginning_balance'] - $monthly_payment, $factor_rate );

                $due_date = add_months_to_date($date, ($i-1));
                $breakdown['monthly'][$i]['due_date'] = $due_date;

                $next_payment_date = add_months_to_date($due_date, 1);
                $breakdown['monthly'][$i]['next_payment_date'] = $next_payment_date;
                /* MONTHLY BREAKDOWN END */

                $additional = [
                    'created_by' => $this->user->id,
                    'created_at' => NOW,
                ];

                if (!$debug) {

                    $payment['loan_id'] = $loan_id;
                    $payment['total_amount'] = 0 + $interest;
                    $payment['principal_amount'] = 0;
                    $payment['interest_amount'] = $interest;
                    $payment['beginning_balance'] =  $loan['loan_amount'];
                    $payment['ending_balance'] =  $loan['loan_amount'];
                    $payment['period_id'] = 1;
                    $payment['particulars'] = ordinal($countterm) . ' Payment';
                    $payment['is_paid'] = 0;
                    $payment['is_complete'] = 0;
                    $payment['due_date'] = $due_date;
                    $payment['next_payment_date'] = $next_payment_date;
                    $this->M_Loan_payment->insert($payment + $additional);
                }

                if ($breakdown['monthly'][$i]['ending_balance'] < 0) {
                    break;
                }
            }

            $due_date = add_months_to_date($date, ($i-1));
            $payment['loan_id'] = $loan_id;
            $payment['total_amount'] =  $loan_amount;
            $payment['principal_amount'] = $loan_amount;
            $payment['interest_amount'] = 0;
            $payment['beginning_balance'] =  $loan_amount;
            $payment['ending_balance'] =  0;
            $payment['period_id'] = 1;
            $payment['particulars'] = ordinal($countterm + 1) . ' Payment';
            $payment['is_paid'] = 0;
            $payment['is_complete'] = 0;
            $payment['due_date'] = $due_date;
            $payment['next_payment_date'] = $next_payment_date;
            $this->M_Loan_payment->insert($payment + $additional);

        } else if ($loan_term == 2) { // installment
            for( $i = 1; $i <= ( $loan_period ); $i++) {
                $countterm = $i;

                /* MONTHLY BREAKDOWN START */
                $breakdown['monthly'][$i]['month'] = $i;
            
                if( $i == 1 ) {
                    $breakdown['monthly'][$i]['beginning_balance'] = round( $loan_amount, $factor_rate );
                } else {
                    $breakdown['monthly'][$i]['beginning_balance'] = round( $breakdown['monthly'][($i - 1)]['ending_balance'], $factor_rate );
                }

                $interest = round( $breakdown['monthly'][$i]['beginning_balance'] * $this->compute_percentage( $loan_interest ) , $factor_rate );
                
                $breakdown['monthly'][$i]['interest'] = $interest;

                $breakdown['monthly'][$i]['principal'] = $monthly_payment;

                $breakdown['monthly'][$i]['total_amount'] = $monthly_payment + $interest;
                    
                $breakdown['monthly'][$i]['ending_balance'] = round( $breakdown['monthly'][$i]['beginning_balance'] - $monthly_payment, $factor_rate );

                $due_date = add_months_to_date($date, ($i-1));
                $breakdown['monthly'][$i]['due_date'] = $due_date;

                $next_payment_date = add_months_to_date($due_date, 1);
                $breakdown['monthly'][$i]['next_payment_date'] = $next_payment_date;
                /* MONTHLY BREAKDOWN END */

                $additional = [
                    'created_by' => $this->user->id,
                    'created_at' => NOW,
                ];

                if (!$debug) {
                    $payment['loan_id'] = $loan_id;
                    $payment['total_amount'] =  $monthly_payment + $interest;
                    $payment['principal_amount'] =$monthly_payment;
                    $payment['interest_amount'] = $interest;
                    $payment['beginning_balance'] =  $breakdown['monthly'][$i]['beginning_balance'];
                    $payment['ending_balance'] =  $breakdown['monthly'][$i]['ending_balance'] ;
                    $payment['period_id'] = 1;
                    $payment['particulars'] = ordinal($countterm) . ' Payment';
                    $payment['is_paid'] = 0;
                    $payment['is_complete'] = 0;
                    $payment['due_date'] = $due_date;
                    $payment['next_payment_date'] = $next_payment_date;
                    $this->M_Loan_payment->insert($payment + $additional);
                }

                if ($breakdown['monthly'][$i]['ending_balance'] < 0) {
                    break;
                }
            }
        } else if ($loan_term == 3) { // salary loan
            $loan_period = 2;
            for( $i = 1; $i <= ( $loan_period ); $i++) {
                $countterm = $i;

                /* MONTHLY BREAKDOWN START */
                $breakdown['monthly'][$i]['month'] = $i;
            
                if( $i == 1 ) {
                    $breakdown['monthly'][$i]['beginning_balance'] = round( $loan_amount, $factor_rate );
                } else {
                    $breakdown['monthly'][$i]['beginning_balance'] = round( $breakdown['monthly'][($i - 1)]['ending_balance'], $factor_rate );
                }

                $interest =  $loan_amount * $this->compute_percentage( $loan_interest );
                
                $breakdown['monthly'][$i]['interest'] = $interest;

                $breakdown['monthly'][$i]['principal'] = 0;

                $breakdown['monthly'][$i]['total_amount'] = $monthly_payment + $interest;
                    
                $breakdown['monthly'][$i]['ending_balance'] = round( $breakdown['monthly'][$i]['beginning_balance'] - $monthly_payment, $factor_rate );

                $due_date = add_months_to_date($date, ($i-1));
                $breakdown['monthly'][$i]['due_date'] = $due_date;

                $next_payment_date = add_months_to_date($due_date, 1);
                $breakdown['monthly'][$i]['next_payment_date'] = $next_payment_date;
                /* MONTHLY BREAKDOWN END */

                $additional = [
                    'created_by' => $this->user->id,
                    'created_at' => NOW,
                ];

                if (!$debug) {

                    $payment['loan_id'] = $loan_id;
                    $payment['total_amount'] = 0 + $interest;
                    $payment['principal_amount'] = 0;
                    $payment['interest_amount'] = $interest;
                    $payment['beginning_balance'] =  $loan['loan_amount'];
                    $payment['ending_balance'] =  $loan['loan_amount'];
                    $payment['period_id'] = 1;
                    $payment['particulars'] = ordinal($countterm) . ' Payment';
                    $payment['is_paid'] = 0;
                    $payment['is_complete'] = 0;
                    $payment['due_date'] = $due_date;
                    $payment['next_payment_date'] = $next_payment_date;
                    $this->M_Loan_payment->insert($payment + $additional);
                }

                if ($breakdown['monthly'][$i]['ending_balance'] < 0) {
                    break;
                }
            }
        }

        if ($reprocess) {
            redirect('loan/view/' . $loan_id);
        }

        return true;

        if ($debug) {
           vdebug($breakdown);
        }
    }
    public function update($id = false,$debug = 0)
    {
        if ($id) {

            $this->view_data['loan'] = $data = $this->M_Loan->with_applicant()->get($id);

            if ($data) {

                if ($this->input->post()) {

                    $_input = $this->input->post();
                    $_input['updated_by'] = $this->session->userdata['user_id'];
                    $_input['reference'] = uniqidReal();

                    $result = $this->M_Loan->from_form()->update($_input, $data['id']);

                    if ($result === false) {

                        // Validation
                        $this->notify->error('Oops something went wrong.');
                    } else {

                        // Success
                        $this->notify->success('Successfully Updated.', 'loan');
                    }
                }

                if ($debug) {
                    vdebug($this->view_data);
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function disburse($id = false,$debug = 0)
    {
        if ($id) {

            $this->view_data['loan'] = $data = $this->M_Loan->with_applicant()->get($id);

            if ($data) {

                if ($this->input->post()) {

                    $_input = $this->input->post();

                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW
                    ];
                    $this->M_Loan_disbursement->insert($_input + $additional);

                    if ($result === false) {

                        // Validation
                        $this->notify->error('Oops something went wrong.');
                    } else {

                        // Success
                        $this->notify->success('Disbursement Sucessfully Created.', 'loan');
                    }
                }

                if ($debug) {
                    vdebug($this->view_data);
                }

                $this->template->build('disbursed', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function update_status()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';
        $loan_id = $this->input->post('loan_id');
        $status = $this->input->post('status_id');
        $remarks = $this->input->post('remarks');
        $terms = $this->input->post('scheme');
        $loan_effectivity_date = $this->input->post('loan_effectivity_date');
        $loan_interest_rate = $this->input->post('loan_interest_rate');

        if ($loan_id) {

            $update = [
                'status' => $status,
                'status_remarks' => $remarks,
                'terms' => $terms,
                'loan_effectivity_date' => $loan_effectivity_date,
                'loan_interest' => $loan_interest_rate,
                'updated_by' => $this->user->id,
                'updated_at' => NOW,
            ];

            $applicant_id = get_value_field($loan_id,'loans','applicant_id');

            $post['first_name'] = get_value_field($applicant_id,'applicants','first_name');
            $post['middle_name'] = get_value_field($applicant_id,'applicants','middle_name');
            $post['last_name'] = get_value_field($applicant_id,'applicants','last_name');
            $post['email'] = get_value_field($applicant_id,'applicants','email');

            $_u_full_name = $post['first_name'] . " " .$post['middle_name'] . " " .$post['last_name'];
    
            $recipient['subject'] = "Alon Capital - Loan Application";
            $recipient['name'] = $_u_full_name;
            $recipient['email'] = $post['email'];

            if ($status == 2) {
                $content = "You're loan application is being reviewed. Please wait for update. Thank you.";
            } else if ($status == 3) {
                $content = "You're loan application is disapproved. Please check your account for information. Thank you.";
            } else if ($status == 4) {
                $content = "Your loan application has been approved. Please wait for our representative to call you on the next steps. Thank you";
                $this->process_loan_schedule($loan_id);
            }

            $this->communication_library->send_email($recipient, $content);

            $id = $this->M_Loan->update($update, $loan_id);


            if ($id) {
                $response['status'] = 1;
                $response['message'] = $remarks;
            } else {
                $response['status'] = 0;
                $response['message'] = 'Error!';
            }
        } else {
            $response['status'] = 0;
            $response['message'] = 'Error!';
        }

        echo json_encode($response);
        exit();
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Loan->get($id);
            if ($list) {

                $deleted = $this->M_Loan->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Class Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {
                    $data = [
                        'deleted_by' => $this->session->userdata['user_id']
                    ];
                    $this->db->update('item_group', $data, array('id' => $value));
                    $deleted = $this->M_Loan->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Type Sucessfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = FALSE,$debug = FALSE)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['loan'] = $this->M_Loan->with_applicant()->with_payments()->get($id);

            if ($this->view_data['loan']) {
                if ($debug) {
                    vdebug($this->view_data);
                }
                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        /* ==================== begin: Add model fields ==================== */
                        'bank_id' =>  $upload_data[1],
                        'bank' =>  $upload_data[2],
                        'branch' =>  $upload_data[3],
                        'amount' =>  $upload_data[4],
                        'unique_number' =>  $upload_data[5],
                        'due_date' =>  $upload_data[6],
                        'particulars' =>  $upload_data[7],
                        'status' =>  $upload_data[8],
                        'is_active' =>  $upload_data[9],
                        /* ==================== end: Add model fields ==================== */
                    );

                    $loan_id = $upload_data[0];
                    $loan = $this->M_Loan->get($loan_id);

                    if ($loan) {
                        $result = $this->M_Loan->update($fields, $loan_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'bank_id' =>  $upload_data[0],
                            'bank' =>  $upload_data[1],
                            'branch' =>  $upload_data[2],
                            'amount' =>  $upload_data[3],
                            'unique_number' =>  $upload_data[4],
                            'due_date' =>  $upload_data[5],
                            'particulars' =>  $upload_data[6],
                            'status' =>  $upload_data[7],
                            'is_active' =>  $upload_data[8],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Loan->insert($fields);
                    } else {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'bank_id' =>  $upload_data[1],
                            'bank' =>  $upload_data[2],
                            'branch' =>  $upload_data[3],
                            'amount' =>  $upload_data[4],
                            'unique_number' =>  $upload_data[5],
                            'due_date' =>  $upload_data[6],
                            'particulars' =>  $upload_data[7],
                            'status' =>  $upload_data[8],
                            'is_active' =>  $upload_data[9],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Loan->insert($fields);
                    }
                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'loan');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'loan');
        die();
    }

    public function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued    =    $this->input->post('update_existing_data');

            $_is_update    =    $_ued === '1' ? TRUE : FALSE;

            $_alphas        =    [];
            $_datas            =    [];

            $_titles[]    =    'id';

            $_start    =    3;
            $_row        =    2;

            $_filename    =    'Loan CSV Template.csv';

            $_fillables    =    $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'loan');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                    $_titles[]    =    $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group    =    $this->M_Loan->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title]    =    isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas            =    $this->__get_excel_columns(count($_titles));
            $_xls_columns    =    array_combine($_alphas, $_titles);
            $_firstAlpha    =    reset($_alphas);
            $_lastAlpha        =    end($_alphas);

            $_objSheet    =    $this->excel->getActiveSheet();
            $_objSheet->setTitle('Loan');
            $_objSheet->setCellValue('A1', 'CHEQUE');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style    =    array(
                'font'  => array(
                    'bold'    =>    TRUE,
                    'size'    =>    10,
                    'name'    =>    'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    function export()
    {

        $_db_columns    =    [];
        $_alphas            =    [];
        $_datas                =    [];

        $_titles[]    =    '#';

        $_start    =    3;
        $_row        =    2;
        $_no        =    1;

        $loans    =    $this->M_Loan->as_array()->get_all();
        if ($loans) {

            foreach ($loans as $_lkey => $loan) {

                $_datas[$loan['id']]['#']    =    $_no;

                $_no++;
            }

            $_filename    =    'list_of_loans_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet    =    $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column    =    $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey]    =    isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'loan');
                }
            } else {

                $_filename    =    'list_of_loans_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns    =    $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'loan');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name    =    isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($loans as $_lkey => $loan) {

                                $_datas[$loan['id']][$_title]    =    isset($loan[$_column]) && ($loan[$_column] !== '') ? Dropdown::get_static('bool', $loan[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($loans as $_lkey => $loan) {

                                if ($_name === 'status') {

                                    $_datas[$loan['id']][$_title]    =    isset($loan[$_name]) && $loan[$_name] ? Dropdown::get_static('inventory_status', $loan[$_name], 'view') : '';
                                } else {

                                    $_datas[$loan['id']][$_title]    =    isset($loan[$_name]) && $loan[$_name] ? $loan[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas    =    $this->__get_excel_columns(count($_titles));

                $_xls_columns    =    array_combine($_alphas, $_titles);
                $_firstAlpha    =    reset($_alphas);
                $_lastAlpha        =    end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title    =    ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Loan');
                $_objSheet->setCellValue('A1', 'LIST OF CHEQUE');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style    =    array(
                    'font'  => array(
                        'bold'    =>    TRUE,
                        'size'    =>    10,
                        'name'    =>    'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'loan');
            }
        } else {

            $this->notify->error('No Record Found', 'loan');
        }
    }

    public function compute_percentage( $value = 0 ) {
    
        if( $value ) {

            return $value / 100;

        }

    }


    public function test_email($view = 0)
    {

        $_u_full_name = "Charles Hernandez";
    
        $recipient['subject'] = "Alon Capital - Loan Application";
        $recipient['name'] = $_u_full_name;
        $recipient['email'] = "charleskennethhernandez@gmail.com";

        if ($view) {
            
            $this->load->view('emails/registration');

        } else {
            
            $content = $this->load->view('emails/registration',$recipient,true);
            $this->communication_library->send_email($recipient, $content);

        }
        
    }


    public function send_loan_info($loan_id = 0, $view = 0, $debug = 0)
    {   

        $recipient['email'] = "luigi.santos@voffice.com.ph";
        // $recipient['email'] = "charleskennethhernandez@gmail.com";
        $recipient['subject'] = "Loan Application from Alon Capital";

        $data['loan'] = $this->M_Loan->get($loan_id);

        $data['applicant'] = $this->M_applicant
                ->with_region()
                ->with_province()
                ->with_city()
                ->with_barangay()
                ->with_employment()
                ->with_identifications()
                ->get($data['loan']['applicant_id']);

        
        if ($debug) {
            vdebug($data);
        }

        if ($view) {
            $this->load->view('emails/loan',$data);
        } else {
            $content = $this->load->view('emails/loan',$data,true);
            $this->communication_library->send_email($recipient, $content);
        }

    }

}
