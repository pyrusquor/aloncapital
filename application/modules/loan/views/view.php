<?php
    $id = isset($loan['id']) && $loan['id'] ? $loan['id'] : '';
    $applicant_id = isset($loan['applicant_id']) && $loan['applicant_id'] ? $loan['applicant_id'] : '';
    $application_at = isset($loan['application_at']) && $loan['application_at'] ? view_date($loan['application_at']) : date('Y-m-d H:i:s');
    $loan_amount = isset($loan['loan_amount']) && $loan['loan_amount'] ? money_php($loan['loan_amount']) : '';
    $loan_period = isset($loan['loan_period']) && $loan['loan_period'] ? $loan['loan_period']." Mo(s)" : '';
    $terms = isset($loan['terms']) && $loan['terms'] ? Dropdown::get_static('loan_terms',$loan['terms'],'view') : '';
    $remarks = isset($loan['remarks']) && $loan['remarks'] ? $loan['remarks'] : '';
    $status_remarks = isset($loan['status_remarks']) && $loan['status_remarks'] ? $loan['status_remarks'] : '';
    $status = isset($loan['status']) && $loan['status'] ? Dropdown::get_static('loan_status',$loan['status'],'view') : '';
    $applicant_name = isset($loan['applicant']) && $loan['applicant'] ? $loan['applicant']['first_name']." ".$loan['applicant']['last_name'] : '';
    $type = isset($loan['applicant']) && $loan['applicant'] ? $loan['applicant']['type_id'] : '';
    $file_path = "https://aloncapitalcorp.com/lms/assets/uploads/applicant/files/";
    $ygg_file_path = "https://aloncapital.yggdrasil.ph/assets/uploads/applicant/files/";

    $payments = isset($loan['payments']) && $loan['payments'] ? $loan['payments'] : array();

    $this->loan_library->initiate($id);

    $total_interest_amount = $this->loan_library->get_amount(0,8);
    $total_paid_interest = $this->loan_library->get_amount_paid(3,1,0);
    $total_paid_principal = $this->loan_library->get_amount_paid(2,1,0);
    

    // $this->loan_library->total_payment

?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Loan</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('loan/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('loan');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Loan Application Data
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <div class="row">

                                <!--begin::Form-->
                                    <div class="kt-widget13 col-md-6">
                                        <!-- ==================== begin: Add fields details  ==================== -->
                                        <div class="kt-widget13__item">
                                            <span class="kt-widget13__desc">
                                                Applicant Name
                                            </span>
                                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $applicant_name; ?></span>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <span class="kt-widget13__desc">
                                                Application Date
                                            </span>
                                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $application_at; ?></span>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <span class="kt-widget13__desc">
                                                Loan Amount
                                            </span>
                                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $loan_amount; ?></span>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <span class="kt-widget13__desc">
                                                Loan Duration (mos)
                                            </span>
                                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo ($loan_period); ?></span>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <span class="kt-widget13__desc">
                                                Loan Repayment
                                            </span>
                                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $terms; ?></span>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <span class="kt-widget13__desc">
                                                Loan Purpose
                                            </span>
                                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $remarks; ?></span>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <span class="kt-widget13__desc">
                                                Loan Status
                                            </span>
                                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $status; ?></span>
                                        </div>
                                        <!-- ==================== end: Add model details ==================== -->
                                    </div>
                                <!--end::Form-->

                                <!--begin::Form-->
                                    <div class="kt-widget13 col-md-6">
                                        <!-- ==================== begin: Add fields details  ==================== -->
                                        <div class="kt-widget13__item">
                                            <span class="kt-widget13__desc">
                                                Total Principal Amount
                                            </span>
                                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $loan_amount; ?></span>
                                        </div>

                                        <!-- ==================== begin: Add fields details  ==================== -->
                                        <div class="kt-widget13__item">
                                            <span class="kt-widget13__desc">
                                                Total Interest Amount
                                            </span>
                                        <span class="kt-widget13__text kt-widget13__text--bold"><?php echo money_php($total_interest_amount); ?></span>
                                        </div>

                                        <!-- ==================== begin: Add fields details  ==================== -->
                                        <div class="kt-widget13__item">
                                            <span class="kt-widget13__desc">
                                                Total Paid Principal
                                            </span>
                                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo money_php($total_paid_principal); ?></span>
                                        </div>

                                        <!-- ==================== begin: Add fields details  ==================== -->
                                        <div class="kt-widget13__item">
                                            <span class="kt-widget13__desc">
                                                Total Paid Interest
                                            </span>
                                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo money_php($total_paid_interest); ?></span>
                                        </div>
                                         <div class="kt-widget13__item">
                                            <span class="kt-widget13__desc">
                                                Remarks
                                            </span>
                                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo ($status_remarks); ?></span>
                                        </div>
                                    </div>
                                <!--end::Form-->

                            </div>

                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Loan Application Files
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                        <!-- coe_bir_certificate
                        latest_financial
                        latest_payslip_itr_company
                        latest_annual_proprietor_borrower
                        latest_bank_statement -->
                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <?php if ($type == 1){ ?>
                                    
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Company Registration Documents (DTI/GIS/Articles of Incorporation)
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['coe_bir_certificate']): ?>
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['coe_bir_certificate'])) ? $file_path.$loan['coe_bir_certificate']  : $ygg_file_path.$loan['coe_bir_certificate'];?>">File</a>
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>

                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Latest Audited Financial Statements
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['latest_financial']): ?>
                                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_financial'];?>">File</a> -->
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_financial'])) ? $file_path.$loan['latest_financial']  : $ygg_file_path.$loan['latest_financial'];?>">File</a>
                                            
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Latest Income Tax Returns
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['latest_payslip_itr_company']): ?>
                                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_payslip_itr_company'];?>">File</a> -->
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_payslip_itr_company'])) ? $file_path.$loan['latest_payslip_itr_company']  : $ygg_file_path.$loan['latest_payslip_itr_company'];?>">File</a>
                                            
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                       Business Permits
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['latest_annual_proprietor_borrower']): ?>
                                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_annual_proprietor_borrower'];?>">File</a> -->
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_annual_proprietor_borrower'])) ? $file_path.$loan['latest_annual_proprietor_borrower']  : $ygg_file_path.$loan['latest_annual_proprietor_borrower'];?>">File</a>
                                            
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Bank Statement for the Past 3 Months for both Proprietor and Company
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['latest_bank_statement']): ?>
                                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_bank_statement'];?>">File</a> -->
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_bank_statement'])) ? $file_path.$loan['latest_bank_statement']  : $ygg_file_path.$loan['latest_bank_statement'];?>">File</a>
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>
                                </div>

                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                       Government IDs
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['government_id']): ?>
                                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['government_id'];?>">File</a> -->
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['government_id'])) ? $file_path.$loan['government_id']  : $ygg_file_path.$loan['government_id'];?>">File</a>
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>
                                </div>

                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                       Proof of Billing
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['proof_of_billing']): ?>
                                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['proof_of_billing'];?>">File</a> -->
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['proof_of_billing'])) ? $file_path.$loan['proof_of_billing']  : $ygg_file_path.$loan['proof_of_billing'];?>">File</a>
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>
                                </div>

                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                       Checking Account Details (for post-dated checks)
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['checking_account_details']): ?>
                                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['checking_account_details'];?>">File</a> -->
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['checking_account_details'])) ? $file_path.$loan['checking_account_details']  : $ygg_file_path.$loan['checking_account_details'];?>">File</a>
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>
                                </div>

                                <?php } else if($type == 2) { ?>

                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Latest Annual Income Tax Return
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['latest_annual_proprietor_borrower']): ?>
                                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_annual_proprietor_borrower'];?>">File</a> -->
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_annual_proprietor_borrower'])) ? $file_path.$loan['latest_annual_proprietor_borrower']  : $ygg_file_path.$loan['latest_annual_proprietor_borrower'];?>">File</a>
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Bank Statement for the Past 3 Months
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['latest_bank_statement']): ?>
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_bank_statement'])) ? $file_path.$loan['latest_bank_statement']  : $ygg_file_path.$loan['latest_bank_statement'];?>">File</a>
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Certificate of Employment
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['coe_bir_certificate']): ?>
                                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['coe_bir_certificate'];?>">File</a> -->
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['coe_bir_certificate'])) ? $file_path.$loan['coe_bir_certificate']  : $ygg_file_path.$loan['coe_bir_certificate'];?>">File</a>
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Latest Payslip
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['latest_payslip_itr_company']): ?>
                                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_payslip_itr_company'];?>">File</a> -->
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_payslip_itr_company'])) ? $file_path.$loan['latest_payslip_itr_company']  : $ygg_file_path.$loan['latest_payslip_itr_company'];?>">File</a>
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>
                                </div>

                                 <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                       Government IDs
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['government_id']): ?>
                                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['government_id'];?>">File</a> -->
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['government_id'])) ? $file_path.$loan['government_id']  : $ygg_file_path.$loan['government_id'];?>">File</a>
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>
                                </div>

                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                       Proof of Billing
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['proof_of_billing']): ?>
                                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['proof_of_billing'];?>">File</a> -->
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['proof_of_billing'])) ? $file_path.$loan['proof_of_billing']  : $ygg_file_path.$loan['proof_of_billing'];?>">File</a>
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>
                                </div>

                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                       Checking Account Details (for post-dated checks)
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($loan['checking_account_details']): ?>
                                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['checking_account_details'];?>">File</a> -->
                                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['checking_account_details'])) ? $file_path.$loan['checking_account_details']  : $ygg_file_path.$loan['checking_account_details'];?>">File</a>
                                        <?php else: ?>
                                            N/A
                                        <?php endif ?>
                                    </span>
                                </div>

                                <?php } else { ?>

                                    <div class="kt-widget13__item">
                                        <span class="kt-widget13__desc">
                                            Latest Payslip
                                        </span>
                                            <span class="kt-widget13__text kt-widget13__text--bold">
                                                <?php if ($loan['latest_payslip_itr_company']): ?>
                                                <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_payslip_itr_company'];?>">File</a> -->
                                                <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_payslip_itr_company'])) ? $file_path.$loan['latest_payslip_itr_company']  : $ygg_file_path.$loan['latest_payslip_itr_company'];?>">File</a>
                                            <?php else: ?>
                                                N/A
                                            <?php endif ?>
                                        </span>
                                    </div>
                                    <div class="kt-widget13__item">
                                        <span class="kt-widget13__desc">
                                            Latest 1 Month Bank Statement
                                        </span>
                                        <span class="kt-widget13__text kt-widget13__text--bold">
                                            <span class="kt-widget13__text kt-widget13__text--bold">
                                                <?php if ($loan['latest_bank_statement']): ?>
                                                <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_bank_statement'];?>">File</a> -->
                                                <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_bank_statement'])) ? $file_path.$loan['latest_bank_statement']  : $ygg_file_path.$loan['latest_bank_statement'];?>">File</a>
                                            <?php else: ?>
                                                N/A
                                            <?php endif ?>
                                        </span>
                                    </div>
                                <?php } ?>

                                <!-- ==================== end: Add model details ==================== -->
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>
    </div>


    <div class="row">
    <div class="col-sm-12">
        <!-- General Information -->
        <div class="kt-portlet">
            <div class="accordion accordion-solid accordion-toggle-svg" id="accord_general_information">
                <div class="card">
                    <div class="card-header" id="head_general_information">
                        <div class="card-title" data-toggle="collapse" data-target="#general_information" aria-expanded="true" aria-controls="general_information">
                            Loan Schedule <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                </g>
                            </svg>
                        </div>

                    </div>


                    <div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
                    <div id="general_information" class="collapse show" aria-labelledby="head_general_information" data-parent="#accord_general_information">
                        <div class="card-body">

                            <div class="form-group form-group-xs row" style="overflow:scroll;">
                                <table class="table table-striped- table-bordered table-hover" id="m_schedule">
                                    <thead>
                                        <tr>
                                            <th>Due Date</th>
                                            <th>Principal</th>
                                            <th>Interest</th>
                                            <th>Initial Amount Due</th>

                                            <th>Beginning Balance</th>
                                            <th>Ending Balance</th>
                                           
                                                
                                            <th>Paid Principal</th>
                                            <th>Paid Interest</th>
                                            <th>Paid Penalty</th>

                                            <th>Total Amount Due</th>
                                            
                                            <!-- <th>Rebate Amount</th> -->
                                            <th>Payment Date</th>
                                            <th>Amount Paid</th>
                                            <th>Receipt #</th>
                                            <th>Particulars</th>
                                            <th>Remarks</th>
                                            <?php if(!$this->ion_auth->is_buyer()): ?><th>Actions</th> <?php endif ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if ($payments) :

                                                $previous_official_id = 0;
                                                $receipt_count = 0;$end_receipt =0;
                                                $previous_balance = 0;
                                                $prev_paids = 0;
                                                $prev_or = 0;
                                                $prev_period_id = 0;
                                                $or_number = '';
                                                $previous_period = 0;
                                                $previous_payment = 0;
                                                $previous_amount_due = 0;
                                                $t_amt_due = 0;
                                                $payment_to_complete = 0;
                                                

                                                foreach ($payments as $key => $payment) { 
                                                    $e = 0; $end_receipt = 0; 
                                                    $status = "";
                                                    $penalty = 0;
                                                    $rebate = "";

                                                    $prev_key = $key > 0 ? $key - 1 : 0;
                                                    $gcash = "";

                                                    

                                                    if (is_due($payment['due_date'])) {
                                                        $status = "table-danger";
                                                    }

                                                    if (($payment['is_paid']) && ($payment['is_complete'])) {
                                                        $status = "table-success";
                                                        $gcash = "hide";
                                                    } else if (($payment['is_paid']) && ($payment['is_complete'] == 0)) {
                                                        $status = "table-warning";
                                                    }


                                                    if ( ($payment['is_paid']) && ($payment['is_complete']) && !isset($payment['official_payments']) && ($previous_official_id)) {

                                                        $CI = &get_instance();

                                                        $p['loan_id'] = $loan_id;
                                                        $p['id >'] = $previous_official_id;
                                                        $p['period_id'] = $payment['period_id'];
                                                        $p['deleted_at'] = NULL;
                                                        $CI->db->where($p);
                                                        $CI->db->limit(1);
                                                        $r = $CI->db->get('loan_official_payments')->result_array();
                                                        $payment['official_payments'] = $r;
                                                        $e = 1;

                                                        if (!$receipt_count) {
                                                            $receipt_count = 1;
                                                        }

                                                    }

                                                    $total_amount_paid = [];
                                                    $period_count = 0;

                                                    $penalty =  "";
                                                    $amount_due = "";
                                                    $payment_date = "";
                                                    $amount_paid = "";
                                                    $receipt = "";
                                                    $remarks = "";
                                                    $paid_principal = '';
                                                    $paid_interest = '';

                                                    $beginning_balance = $payment['beginning_balance'];
                                                    $ending_balance = $payment['ending_balance'];
                                                    $principal = $payment['principal_amount'];
                                                    $interest  = $payment['interest_amount'];
                                                    $og_total_amount = $payment['total_amount'];
                                                    

                                                    if (isset($payment['official_payments'])) {
                                                        foreach ($payment['official_payments'] as $key => $official_payment) {
                                                            if (empty($official_payment['deleted_at'])) {
                                                                $rebate  = money_php($official_payment['rebate_amount']);

                                                                if ($official_payment['payment_application'] == 1) {
                                                                    //Advance Payment
                                                                    $remarks = "Advance Payment";
                                                                    $receipt = $official_payment['or_number']."-".$receipt_count;
                                                                    $payment_date = view_date_custom($official_payment['payment_date']);

                                                                    $days = date_diffs($official_payment['payment_date'], $payment['due_date']);
                                                                    $penalty = 0; 

                                                                    if ($official_payment['penalty_amount'] != "0.00") {
                                                                        $penalty = $this->loan_library->get_penalty_normal($days, $official_payment['period_id'], $og_total_amount);
                                                                    }

                                                                    if ($e) {

                                                                        $principal = $payment['principal_amount'];
                                                                        $interest  = $payment['interest_amount'];

                                                                        $paid_principal =  money_php($payment['principal_amount']);
                                                                        $paid_interest =  money_php($payment['interest_amount']);

                                                                        $amount_due = money_php($a = $og_total_amount + $penalty);
                                                                        $penalty = money_php($penalty);
                                                                        $amount_paid = $amount_due;

                                                                        $t_amt_due += $a;

                                                                    } else {

                                                                        if ($payment['is_complete']) {

                                                                            $penalty = money_php($penalty);
                                                                            $amount_due = money_php($principal + $interest);
                                                                            $amount_paid = $amount_due;

                                                                            $paid_principal = money_php($payment['principal_amount']);

                                                                            $paid_interest =  money_php($payment['interest_amount']);

                                                                            if( count($payment['official_payments']) >= 2 ){

                                                                                $payment_to_complete = 1;
                                                                                
                                                                                //advance with partial, partial already complete
                                                                                $remaining = $official_payment['amount_paid'] - $t_amt_due;

                                                                                $amount_due = money_php($remaining);

                                                                                $amount_paid = $amount_due;

                                                                                if ($remaining >= $payment['interest_amount']) {
                                                                                    # code...
                                                                                    $paid_interest =  money_php($payment['interest_amount']);
                                                                                    $remaining = $remaining - $payment['interest_amount'];
                                                                                }

                                                                                $paid_principal = money_php($remaining);
                                                                                
                                                                            }
                                                                            

                                                                        } else {

                                                                            //advance with partial is not complete yet
                                                                            $penalty = money_php($penalty);

                                                                            $remaining = $official_payment['amount_paid'] - $t_amt_due;

                                                                            $amount_due = money_php($remaining);

                                                                            $amount_paid = $amount_due;

                                                                            
                                                                            if ($remaining >= $payment['interest_amount']) {
                                                                                # code...
                                                                                $paid_interest =  money_php($payment['interest_amount']);
                                                                                $remaining = $remaining - $payment['interest_amount'];
                                                                            }

                                                                            $paid_principal = money_php($remaining);
                                                                        }

                                                                        $end_receipt = 1;
                                                                    }
                                                                    
                                                                    $receipt_count++;
                                                                
                                                                } else if ($official_payment['payment_application'] == 2) {
                                                                    //Balloon Payment
                                                                    $remarks = "Balloon Payment";

                                                                    $paid_principal =  money_php($payment['principal_amount']);
                                                                    $paid_interest =  money_php($payment['interest_amount']);

                                                                    $penalty = money_php($official_payment['penalty_amount']);
                                                                    $amount_due = money_php($official_payment['amount_paid']);
                                                                    $payment_date = view_date_custom($official_payment['payment_date']);
                                                                    $amount_paid = money_php($official_payment['amount_paid']);
                                                                    $receipt = $official_payment['or_number'];

                                                                } else if ($official_payment['payment_application'] == 3) {
                                                                    //Continuous Payment
                                                                    $remarks = "Continuous Payment";

                                                                    $interest = 0;

                                                                    $paid_principal =  money_php($payment['principal_amount']);
                                                                    $paid_interest = 0;

                                                                    $penalty = money_php($official_payment['penalty_amount']);
                                                                    $amount_due = money_php($official_payment['amount_paid']);
                                                                    $payment_date = view_date_custom($official_payment['payment_date']);
                                                                    $amount_paid = money_php($official_payment['amount_paid']);
                                                                    $receipt = $official_payment['or_number'];

                                                                } else if ($official_payment['payment_application'] == 4 || $official_payment['payment_application'] == 9) {
                                                                    //Partial Payment

                                                                    if ($payment_to_complete ) {
                                                                        # code...

                                                                        //Advance Partial Payment Completion
                                                                        $remarks .= "<br>"."Partial Payment";

                                                                        $payment_date .= "<br>".view_date_custom($official_payment['payment_date']);
                                                                        $principal = $payment['principal_amount'];
                                                                        $interest  = $payment['interest_amount'];
                                                                        $penalty .= "<br>".money_php($official_payment['penalty_amount']);
                                                                        $amount_paid .= "<br>".money_php($official_payment['amount_paid']);

                                                                        $amount_due .= "<br>".money_php($official_payment['amount_paid']);
                                                                        $receipt .= "<br>".$official_payment['or_number'];

                                                                        $paid_principal .= "<br>".money_php($official_payment['principal_amount']);
                                                                        $paid_interest .= "<br>".money_php($official_payment['interest_amount']);

                                                                        $payment_to_complete = 0;

                                                                    } else {
                                                                        //Partial Payment
                                                                        $remarks .= $official_payment['payment_application'] == 9 ? "Penalty Only<br>" : "Partial Payment"."<br>";

                                                                        $payment_date .= view_date_custom($official_payment['payment_date'])."<br>";
                                                                        $penalty .= money_php($official_payment['penalty_amount'])."<br>";
                                                                        $amount_paid .= money_php($official_payment['amount_paid'])."<br>";

                                                                        $paid_principal .= money_php($official_payment['principal_amount'])."<br>";
                                                                        $paid_interest .= money_php($official_payment['interest_amount'])."<br>";

                                                                        $amount_due .= money_php($official_payment['amount_paid'])."<br>";
                                                                        $receipt .= $official_payment['or_number']."<br>";
                                                                    }

                                                                } else {
                                                                    //regular payment

                                                                    $paid_principal =  money_php($payment['principal_amount']);
                                                                    $paid_interest =  money_php($payment['interest_amount']);

                                                                    $penalty = money_php($official_payment['penalty_amount']);
                                                                    $amount_due = money_php($official_payment['amount_paid']);
                                                                    $payment_date = view_date_custom($official_payment['payment_date']);
                                                                    $amount_paid = money_php($official_payment['amount_paid']);
                                                                    $receipt = $official_payment['or_number'];
                                                                    $remarks = "Regular Payment";
                                                                }

                                                                if(!$e){
                                                                    $previous_official_id = $official_payment['id']; $t_amt_due = 0;
                                                                }
                                                            }
                                                        }
                                                    }
                                        ?>
                                                <tr class="<?php echo $status; ?>">

                                                    <td><?php echo view_date_custom($payment['due_date']); ?></td>

                                                   

                                                    <td><?php echo money_php($principal);  ?></td>

                                                    <td><?php echo money_php($interest); ?></td>

                                                    <td><?php echo money_php($og_total_amount); ?> </td>

                                                    <td><?php echo money_php($beginning_balance); ?></td>

                                                    <td><?php echo money_php($ending_balance); ?> </td>

                                                    <td><?php echo ($paid_principal); ?> </td>

                                                    <td><?php echo ($paid_interest); ?> </td>

                                                    <!-- penalty -->
                                                    <td><?php echo ($penalty); ?> </td>

                                                    <!-- rebate -->
                                                    <!-- <td><?php echo ($rebate); ?> </td> -->

                                                    <!-- amount due -->
                                                    <td><?php echo ($amount_due); ?> </td>

                                                    <!-- payment date -->
                                                    <td><?php echo ($payment_date); ?> </td>

                                                    <!-- amount paid -->
                                                    <td><?php echo ($amount_paid); ?> </td>

                                                    <!-- receipt # -->
                                                    <td> <?php echo $receipt; ?> </td>

                                                    <td><?php echo $payment['particulars']; ?></td>

                                                    <!-- remarks # -->
                                                    <?php if(!$this->ion_auth->is_buyer()): ?>
                                                    <td> <?php echo $remarks; ?> </td>
                                                    <?php endif ?>
                                                    <td>

                                                     <a data-amount="100" data-fee="0" data-expiry="6" data-description="Payment for services rendered" data-href="https://getpaid.gcash.com/paynow" data-public-key="pk_4b2ab493d7792c176fb8c4b1494ff8d2" onclick="this.href = this.getAttribute('data-href')+'?public_key='+this.getAttribute('data-public-key')+'&amp;amount='+this.getAttribute('data-amount')+'&amp;fee='+this.getAttribute('data-fee')+'&amp;expiry='+this.getAttribute('data-expiry')+'&amp;description='+this.getAttribute('data-description');" href="https://getpaid.gcash.com/paynow?public_key=pk_4b2ab493d7792c176fb8c4b1494ff8d2&amp;amount=100&amp;fee=0&amp;expiry=6&amp;description=Payment for services rendered" target="_blank" class="hide x-getpaid-button <?=$gcash;?>"><img src="https://getpaid.gcash.com/assets/img/paynow.png"></a> </td>
                                                </tr>
                                            <?php $prev_period_id = $payment['period_id'];  if($end_receipt){ $receipt_count = 0; }
                                            } ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<style>
    .table#m_schedule td {
        padding: 0.75rem 0.25rem;
        vertical-align: top;
        border-top: 1px solid #ebedf2;
        font-size: 12px;
    }
</style>

</div>
<!-- begin:: Footer -->





