<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        .container {
            max-width: 600px;
            margin: 0 auto;
            background: #f4f4f4;
        }
        .header {
            background: #2c3e50;
            color: #ecf0f1;
            text-align: center;
            padding: 20px;
        }
        .content {
            background: #ffffff;
            padding: 20px;
        }
        .field {
            margin-bottom: 10px;
        }
        ul {
            list-style: none;
            padding: 0;
        }
        ul li {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            <h1>Loan Application from Alon Capital</h1>
        </div>
        <div class="content">
            <p>Dear vOffice,</p>
            <p>We hope this message finds you well. We received the following details from our application form throught the vOffice website:</p>

            <div class="field">
                <strong>Personal Information:</strong>
                <ul>
                    <li><strong>Loan Type:</strong> <?= Dropdown::get_static('applicant_type',$applicant['type_id'],'view');?></li>
                    <li><strong>Email:</strong> <?=$applicant['email'];?></li>
                    <li><strong>First Name:</strong> <?=$applicant['first_name'];?></li>
                    <li><strong>Middle Name:</strong> <?=$applicant['middle_name'];?></li>
                    <li><strong>Last Name:</strong> <?=$applicant['last_name'];?></li>
                    <li><strong>Birthday:</strong> <?=$applicant['birth_date'];?></li>
                    <li><strong>Gender:</strong> <?=Dropdown::get_static('sex',$applicant['gender'],'view');?></li>
                    <li><strong>Civil Status:</strong> <?=Dropdown::get_static('civil_status',$applicant['civil_status_id'],'view');?></li>
                    <li><strong>Nationality:</strong> <?=Dropdown::get_static('nationality',$applicant['nationality'],'view');?></li>
                    <li><strong>Mobile Number:</strong> <?=$applicant['mobile_no'];?></li>
                    <li><strong>Present Address:</strong> <?=$applicant['present_address'];?></li>
                    <li><strong>Referral Source:</strong> vOffice Website </li>
                </ul>
            </div>

            <div class="field">
                <strong>Employment Information:</strong>
                <ul>
                    <li><strong>Company Name:</strong>  <?=$applicant['company_name'];?></li>
                    <li><strong>TIN:</strong>  <?=$applicant['tin'];?></li>
                    
                    <li><strong>Location:</strong><?=Dropdown::get_static('occupation_location',$applicant['employment']['location_id'],'view');?></li>
                    <li><strong>Occupation Type:</strong> <?=Dropdown::get_static('occupation_type',$applicant['employment']['occupation_type_id'],'view');?><?=$applicant['employment']['occupation_type_id'];?></li>
                    <li><strong>Industry:</strong><?=Dropdown::get_static('industry',$applicant['employment']['industry_id'],'view');?></li>
                    <li><strong>Job/Occupation:</strong><?=Dropdown::get_static('occupation',$applicant['employment']['occupation_id'],'view');?></li>


                    <li><strong>Designation/Title:</strong>  <?=$applicant['employment']['designation'];?></li>
                    <li><strong>Business/Employer Name:</strong>  <?=$applicant['employment']['employer'];?></li>
                    <li><strong>Gross Salary:</strong>  <?=$applicant['employment']['gross_salary'];?></li>
                    <li><strong>Full Address:</strong>  <?=$applicant['employment']['address'];?></li>
                </ul>
            </div>

            <div class="field">
                <strong>Loan Details:</strong>
                <ul>
                    <li><strong>Loan Amount:</strong> <?=$loan['loan_amount'];?></li>
                    <li><strong>Loan Duration:</strong> <?=$loan['loan_period'];?></li>
                    <li><strong>Loan Purpose:</strong> <?=$loan['remarks'];?></li>
                </ul>
            </div>

            <!-- coe_bir_certificate
            latest_financial
            latest_payslip_itr_company
            latest_annual_proprietor_borrower
            latest_bank_statement
            government_id
            proof_of_billing
            checking_account_details -->

            <div class="field">
                <strong>Identifications:</strong>
                <ul>
                    <?php if ($applicant['identifications']): ?>
                        <?php foreach ($applicant['identifications'] as $key => $identification): ?>
                            <li> 
                                <strong><?=Dropdown::get_static('ids',$identification['type_of_id'],'view');?> : <?=$identification['id_number'];?></strong><br>
                                <strong>Date Issued : </strong> <?=$identification['date_issued'];?> <br>
                                <strong>Date Expiration : </strong> <?=$identification['date_expiration'];?> <br>
                                <strong>Place Issued : </strong> <?=$identification['place_issued'];?> <br>
                            </li>
                        <?php endforeach ?>
                    <?php endif ?>
                </ul>
            </div>

             <div class="field">
                <strong>Loan Documents:</strong>
                <ul><?php 
                    $type = $applicant['type_id']; 
                    $file_path = "https://aloncapitalcorp.com/lms/assets/uploads/applicant/files/";
                    $ygg_file_path = "https://aloncapital.yggdrasil.ph/assets/uploads/applicant/files/";

                    ?>
                    <?php if ($type == 1){ ?>

                    <li>
                        Company Registration Documents (DTI/GIS/Articles of Incorporation)
                    :
                        <?php if ($loan['coe_bir_certificate']): ?>
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['coe_bir_certificate'])) ? $file_path.$loan['coe_bir_certificate']  : $ygg_file_path.$loan['coe_bir_certificate'];?>">File</a>
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>
                    <li>
                        Latest Audited Financial Statements
                    :
                        <?php if ($loan['latest_financial']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_financial'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_financial'])) ? $file_path.$loan['latest_financial']  : $ygg_file_path.$loan['latest_financial'];?>">File</a>
                            
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>
                    <li>
                        Latest Income Tax Returns
                    :
                        <?php if ($loan['latest_payslip_itr_company']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_payslip_itr_company'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_payslip_itr_company'])) ? $file_path.$loan['latest_payslip_itr_company']  : $ygg_file_path.$loan['latest_payslip_itr_company'];?>">File</a>
                            
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>
                    <li>
                       Business Permits
                    :
                        <?php if ($loan['latest_annual_proprietor_borrower']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_annual_proprietor_borrower'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_annual_proprietor_borrower'])) ? $file_path.$loan['latest_annual_proprietor_borrower']  : $ygg_file_path.$loan['latest_annual_proprietor_borrower'];?>">File</a>
                            
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>
                    <li>
                        Bank Statement for the Past 3 Months for both Proprietor and Company
                    :
                        <?php if ($loan['latest_bank_statement']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_bank_statement'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_bank_statement'])) ? $file_path.$loan['latest_bank_statement']  : $ygg_file_path.$loan['latest_bank_statement'];?>">File</a>
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>

                    <li>
                       Government IDs
                    :
                        <?php if ($loan['government_id']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['government_id'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['government_id'])) ? $file_path.$loan['government_id']  : $ygg_file_path.$loan['government_id'];?>">File</a>
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>

                    <li>
                       Proof of Billing
                    :
                        <?php if ($loan['proof_of_billing']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['proof_of_billing'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['proof_of_billing'])) ? $file_path.$loan['proof_of_billing']  : $ygg_file_path.$loan['proof_of_billing'];?>">File</a>
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>

                    <li>
                       Checking Account Details (for post-dated checks)
                    :
                        <?php if ($loan['checking_account_details']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['checking_account_details'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['checking_account_details'])) ? $file_path.$loan['checking_account_details']  : $ygg_file_path.$loan['checking_account_details'];?>">File</a>
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>

                    <?php } else if($type == 2) { ?>

                    <li>
                        Latest Annual Income Tax Return
                    :
                        <?php if ($loan['latest_annual_proprietor_borrower']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_annual_proprietor_borrower'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_annual_proprietor_borrower'])) ? $file_path.$loan['latest_annual_proprietor_borrower']  : $ygg_file_path.$loan['latest_annual_proprietor_borrower'];?>">File</a>
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>
                    <li>
                        Bank Statement for the Past 3 Months
                    :
                        <?php if ($loan['latest_bank_statement']): ?>
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_bank_statement'])) ? $file_path.$loan['latest_bank_statement']  : $ygg_file_path.$loan['latest_bank_statement'];?>">File</a>
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>
                    <li>
                        Certificate of Employment
                    :
                        <?php if ($loan['coe_bir_certificate']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['coe_bir_certificate'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['coe_bir_certificate'])) ? $file_path.$loan['coe_bir_certificate']  : $ygg_file_path.$loan['coe_bir_certificate'];?>">File</a>
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>
                    <li>
                        Latest Payslip
                    :
                        <?php if ($loan['latest_payslip_itr_company']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_payslip_itr_company'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_payslip_itr_company'])) ? $file_path.$loan['latest_payslip_itr_company']  : $ygg_file_path.$loan['latest_payslip_itr_company'];?>">File</a>
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>

                    <li>
                       Government IDs
                    :
                        <?php if ($loan['government_id']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['government_id'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['government_id'])) ? $file_path.$loan['government_id']  : $ygg_file_path.$loan['government_id'];?>">File</a>
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>

                    <li>
                       Proof of Billing
                    :
                        <?php if ($loan['proof_of_billing']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['proof_of_billing'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['proof_of_billing'])) ? $file_path.$loan['proof_of_billing']  : $ygg_file_path.$loan['proof_of_billing'];?>">File</a>
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>

                    <li>
                       Checking Account Details (for post-dated checks)
                    :
                        <?php if ($loan['checking_account_details']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['checking_account_details'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['checking_account_details'])) ? $file_path.$loan['checking_account_details']  : $ygg_file_path.$loan['checking_account_details'];?>">File</a>
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>

                    <?php } else { ?>
                    <li>
                        Latest Payslip
                    :
                            <?php if ($loan['latest_payslip_itr_company']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_payslip_itr_company'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_payslip_itr_company'])) ? $file_path.$loan['latest_payslip_itr_company']  : $ygg_file_path.$loan['latest_payslip_itr_company'];?>">File</a>
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>
                    <li>
                        Latest 1 Month Bank Statement
                    :
                            <?php if ($loan['latest_bank_statement']): ?>
                            <!-- <a target="_BLANK" class="btn btn-primary" href="<?=$file_path.$loan['latest_bank_statement'];?>">File</a> -->
                            <a target="_BLANK" class="btn btn-primary" href="<?=(file_exists2($file_path.$loan['latest_bank_statement'])) ? $file_path.$loan['latest_bank_statement']  : $ygg_file_path.$loan['latest_bank_statement'];?>">File</a>
                        <?php else: ?>
                            N/A
                        <?php endif ?>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            
            <p>If you have any questions or need further assistance, please don't hesitate to contact our team. We will be in touch with you soon regarding the status of your loan application.</p>
            
            <p>Thank you for choosing Alon Capital!</p>
        </div>
    </div>
</body>
</html>
