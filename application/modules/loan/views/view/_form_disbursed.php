<?php
    $loan_id = isset($loan['id']) && $loan['id'] ? $loan['id'] : '';
    $loan_amount = isset($loan['loan_amount']) && $loan['loan_amount'] ? $loan['loan_amount'] : '';
    $disbursed_at = isset($loan['disbursed_at']) && $loan['disbursed_at'] ? $loan['disbursed_at'] : date('Y-m-d');


?>
<div class="row">

    <div class="col-sm-6">

        <div class="form-group">
            <label>Loan Amount <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input readonly type="number" class="form-control" id="loan_amount" placeholder="Amount"
                           name="loan_amount"
                           value="<?php echo set_value('loan_amount', @$loan_amount); ?>">
                </div>
            </div>
        </div>

        <div class="form-group">
            <label>Transaction Fee <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">%</span></div>
                    <input type="number" class="form-control" id="transaction_fee" placeholder="0.00"
                           name="transaction_fee"
                           value="<?php echo set_value('transaction_fee', @$transaction_fee); ?>">
                </div>
            </div>
        </div>


        <div class="form-group">
            <label>Transaction Fee Amount<span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="number" class="form-control" id="transaction_fee_amount" placeholder="0.00"
                           name="transaction_fee_amount"
                           value="<?php echo set_value('transaction_fee_amount', @$transaction_fee_amount); ?>" readonly>
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <label>Net Disbursement Amount <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="number" class="form-control" id="net_disbursement_amount" placeholder="0.00"
                           name="net_disbursement_amount"
                           value="<?php echo set_value('net_disbursement_amount', @$loan_amount); ?>" readonly>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label>Disbursement Type <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('disbursement_type', Dropdown::get_static('disbursement_type'), set_value('disbursement_type', @$disbursement_type), 'class="form-control"'); ?>
            </div>
            <?php echo form_error('disbursement_type'); ?>
            <span class="form-text text-muted"></span>
        </div>

        <div class="form-group">
            <label>Disbursement Date <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control datePicker " readonly name="disbursed_at" value="<?php echo set_value('disbursed_at', $disbursed_at); ?>" placeholder="" autocomplete="off">
            </div>
            <?php echo form_error('disbursed_at'); ?>
            <span class="form-text text-muted"></span>
        </div>

        <div class="form-group">
            <div class="form-group">
                <label for="message">Remarks</label>
                <textarea type="text" class="form-control" id="remarks" name="remarks" placeholder="Remarks" rows="7"><?php echo set_value('remarks', @$remarks); ?></textarea>
            </div>
        </div>
    </div>

</div>
<!-- ==================== end: Add form model fields ==================== -->

