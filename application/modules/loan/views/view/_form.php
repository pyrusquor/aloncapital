<?php
// ==================== begin: Add model fields ====================
$applicant_id = isset($loan['applicant_id']) && $loan['applicant_id'] ? $loan['applicant_id'] : '';
$application_at = isset($loan['application_at']) && $loan['application_at'] ? $loan['application_at'] : date('Y-m-d H:i:s');
$loan_amount = isset($loan['loan_amount']) && $loan['loan_amount'] ? $loan['loan_amount'] : '';
$loan_period = isset($loan['loan_period']) && $loan['loan_period'] ? $loan['loan_period'] : '';
$terms = isset($loan['terms']) && $loan['terms'] ? $loan['terms'] : '';
$remarks = isset($loan['remarks']) && $loan['remarks'] ? $loan['remarks'] : '';
$status = isset($loan['status']) && $loan['status'] ? $loan['status'] : '';
$applicant_name = isset($loan['applicant']['first_name']) && $loan['applicant']['first_name'] ? $loan['applicant']['first_name']." ".$loan['applicant']['last_name'] : '';
$logged = 0;
$to_hide_file_1 = "hide";
$to_hide_file_2 = "hide";
$to_hide_file_3 = "hide";

if ($this->ion_auth->is_buyer()) {
    $login_id = get_value_field($this->session->userdata['user_id'],'applicants','id','user_id');
}

if ($login_id) {
    $logged = "hide";
    $type = get_value_field($login_id,'applicants','type_id');
    if ($type == 1) {
        $to_hide_file_1 = "";
    } else if ($type == 2){
        $to_hide_file_2 = "";
    } else {
        $to_hide_file_3 = "";
    }
    $applicant_id = $login_id;
    $applicant_name = $login_id;
    $status = 1;
}

// ==================== end: Add model fields ====================

?>
<!-- ==================== begin: Add form model fields ==================== -->
<div class="row">

    <div class="col-sm-6">
        <div class="form-group <?=$logged;?>">
            <label>Applicant <?=$login_id;?> <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="applicants" data-type="person"  id="applicant_id" name="applicant_id">
                <option value="">Select Applicant</option>
                <?php if ($applicant_name): ?>
                    <option value="<?php echo $applicant_id; ?>" selected><?php echo $applicant_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>

        <div class="form-group <?=$logged;?>">
            <label>Application Date <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control datePicker " readonly name="application_at" value="<?php echo set_value('application_at', $application_at); ?>" placeholder="" autocomplete="off">
            </div>
            <?php echo form_error('application_at'); ?>
            <span class="form-text text-muted"></span>
        </div>

        <div class="form-group">
            <label>Loan Amount <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="number" class="form-control" id="loan_amount" placeholder="Amount"
                           name="loan_amount"
                           value="<?php echo set_value('loan_amount', @$loan_amount); ?>">
                </div>
            </div>
        </div>

        <div class="form-group">
            <label>Loan Duration (mos) <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="loan_period" value="<?php echo set_value('loan_period', $loan_period); ?>" placeholder="(MOS)" autocomplete="off">
            </div>
            <?php echo form_error('loan_period'); ?>
            <span class="form-text text-muted"></span>
        </div>

        <div class="form-group hide">
            <label>Loan Repayment <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('terms', Dropdown::get_static('loan_terms'), set_value('terms', 2), 'class="form-control"'); ?>
            </div>
            <span class="form-text text-muted"></span>
        </div>

        <div class="form-group <?=$logged;?>">
            <label>Loan Status <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('status', Dropdown::get_static('loan_status'), set_value('status', @$status), 'class="form-control"'); ?>
                
            </div>
            <?php echo form_error('status'); ?>
            <span class="form-text text-muted"></span>
        </div>

        <div class="form-group">
            <div class="form-group">
                <label for="message">Loan Purpose</label>
                <textarea type="text" class="form-control" id="remarks" name="remarks" placeholder="Remarks" rows="7"><?php echo set_value('remarks', @$remarks); ?></textarea>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <?php if ($type == 1) { ?>
            <div class="form_sme_files">

                <div class="form-group">
                    <label>Company Registration Documents (DTI/GIS/Articles of Incorporation)</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="coe_bir_certificate" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

                <div class="form-group">
                    <label>Latest Audited Financial Statements</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="latest_financial" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>


                <div class="form-group">
                    <label>Latest Income Tax Returns</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="latest_payslip_itr_company" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

                <div class="form-group">
                    <label>Business Permits</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="latest_annual_proprietor_borrower" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

                <div class="form-group">
                    <label>Bank Statement for the Past 3 Months for both Proprietor and Company</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="latest_bank_statement" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

                <div class="form-group">
                    <label>Government IDs</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="government_id" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

                <div class="form-group">
                    <label>Proof of Billing</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="proof_of_billing" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

                <div class="form-group">
                    <label>Checking Account Details (for post-dated checks)</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="checking_account_details" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

            </div>
        <?php } else if($type == 2) { ?>
            <div class="form_personal_files">

                <div class="form-group">
                    <label>Latest Annual Income Tax Return</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="latest_annual_proprietor_borrower" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

                <div class="form-group">
                    <label>Bank Statement for the Past 3 Months</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="latest_bank_statement" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

                <div class="form-group">
                    <label>Certificate of Employment</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="coe_bir_certificate" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>


                <div class="form-group">
                    <label>Latest Payslip</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="latest_payslip_itr_company" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

                <div class="form-group">
                    <label>Government ID</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="government_id" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

                <div class="form-group">
                    <label>Proof of Billing</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="proof_of_billing" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

                <div class="form-group">
                    <label>Checking Account Details (for post-dated checks)</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="checking_account_details" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

            </div>
        <?php } else { ?>
            <div class="form_salary_files">
            
                <div class="form-group">
                    <label>Latest Payslip</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="latest_payslip_itr_company" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

                <div class="form-group">
                    <label>Latest 1 Month Bank Statement</label>
                    <div>
                        <span class="btn btn-sm">
                            <input type="file" name="latest_bank_statement" class="" aria-invalid="false">
                        </span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>

            </div>
        <?php } ?>
        

    </div>
</div>
<!-- ==================== end: Add form model fields ==================== -->

