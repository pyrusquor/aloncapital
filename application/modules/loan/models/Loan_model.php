<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Loan_model extends MY_Model {
    public $table = 'loans'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'reference',
        'applicant_id',
        'application_at',
        'approval_at',
        'loan_amount',
        'loan_period',
        'loan_interest',
        'loan_effectivity_date',
        'scheme',
        'status',
        'terms',
        'remarks',
        'coe_bir_certificate',
        'latest_financial',
        'latest_payslip_itr_company',
        'latest_annual_proprietor_borrower',
        'latest_bank_statement',
        'government_id',
        'proof_of_billing',
        'checking_account_details',
        'status_remarks',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by'
        ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        /* ==================== begin: Add model fields ==================== */
        'bank_id' => array(
            'field' => 'bank_id',
            'label' => 'Bank ID',
            'rules' => 'trim|required'
        ),
        'branch' => array(
            'field' => 'branch',
            'label' => 'Branch',
            'rules' => 'trim'
        ),
        'amount' => array(
            'field' => 'amount',
            'label' => 'Amount',
            'rules' => 'trim|required'
        ),
        'unique_number' => array(
            'field' => 'unique_number',
            'label' => 'Unique Number',
            'rules' => 'trim'
        ),
        'due_date' => array(
            'field' => 'due_date',
            'label' => 'Due Date',
            'rules' => 'trim'
        ),
        'particulars' => array(
            'field' => 'particulars',
            'label' => 'Particulars',
            'rules' => 'trim'
        ),
        'status' => array(
            'field' => 'status',
            'label' => 'Status',
            'rules' => 'trim'
        ),
        'is_active' => array(
            'field' => 'is_active',
            'label' => 'Is Active',
            'rules' => 'trim'
        ),
        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['applicant'] = array('foreign_model' => 'applicant/applicant_model', 'foreign_table' => 'applicants', 'foreign_key' => 'id', 'local_key' => 'applicant_id');

        $this->has_many['payments'] = array('foreign_model' => 'loan/Loan_payment_model', 'foreign_table' => 'loan_payments', 'foreign_key' => 'loan_id', 'local_key' => 'id');
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}