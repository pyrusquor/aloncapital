<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Loan_payment_model extends MY_Model {

	public $table = 'loan_payments'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'loan_id',
		'total_amount',
		'principal_amount',
		'interest_amount',
		'beginning_balance',
		'ending_balance',
		'period_id',
		'particulars',
		'due_date',
		'next_payment_date',
		'is_paid',
		'is_complete',
		'process',
		'deleted_reason',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
		'deleted_at',
		'deleted_by',
	]; // If you want, you can set an array with the fields that can be filled by insert/update


	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = TRUE;
		$this->return_as = 'array';
		
		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;


		$this->has_one['loan'] = array('foreign_model' => 'loan/Loan_model','foreign_table' => 'loans', 'foreign_key' => 'id','local_key'=>'loan_id');
		
		$this->has_many['official_payments']  = array('foreign_model'=>'loan_payments/loan_official_payment_model','foreign_table'=>'loan_official_payments','foreign_key'=>'loan_payment_id','local_key'=>'id');

		$this->has_many['entry_items'] = array('foreign_model' => 'accounting_entry_items/accounting_entry_items_model', 'foreign_table' => 'accounting_entry_items', 'foreign_key' => 'accounting_entry_id', 'local_key' => 'accounting_entry_id');

	}

	

}
