<?php
    $name            =    isset($form['name']) && $form['name'] ? $form['name'] : '';
    $type            =    isset($form['type']) && $form['type'] ? $form['type'] : '';
    $description     =    isset($form['description']) && $form['description'] ? $form['description'] : '';
    $content         =    isset($form['content']) && $form['content'] ? $form['content'] : '';
    $email_template  =    isset($email_template) && $email_template ? $email_template : '';
    // vdebug($email_template );
?>

<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <label>Please Choose your Table</label>
            <select class="form-control kt-select2" id="table_list">
                <option value="">Select Table</option>
                <?php if (!empty($table_list)) : ?>
                    <?php foreach ($table_list as $key => $table) : ?>
                        <option value="<?php echo $key; ?>">
                            <?php $key = str_replace("_", " ", $key);echo ucwords($key); ?>
                        </option>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>

            <div class="kt-margin-t-15" id="tblFields">
                <table class="table table-striped">
                    <thead class="thead-light">
                        <tr>
                            <th>Fields</th>
                            <th class="kt-align-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tbody">
           
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <div class="col-lg-8">
        <ul class="nav nav-pills nav-fill" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#information">Information</a>
            </li>
           
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#content">Content</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="information" role="tabpanel">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="">Fill Up Template Name <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon kt-input-icon--left">
                                <input type="text" class="form-control" placeholder="Fill Up Template Name" value="<?php echo set_value('name', $name); ?>" name="name">
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-pencil-square-o"></i></span>
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>

                        <div class="form-group">
                            <label class="">Subject (this will use for subject in emails) <span class="kt-font-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="Fill Up Subject Name" value="<?php echo set_value('description', $description); ?>" name="description">

                            <span class="form-text text-muted"></span>
                        </div>

                          <div class="form-group">
                            <label class="">Template Category <span class="kt-font-danger">*</span></label>
                            <select name="type" id="type" class="form-control">
                                <option <?=($type=="sms") ? "selected" : "" ;?>value="sms">SMS</option>
                                <option <?=($type=="email") ? "selected" : "" ;?>value="email">Email</option>
                            </select>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="tab-pane" id="content" role="tabpanel">
                <textarea id="emailTemplate" class="d-none"><?php echo set_value('emailTemplate', $email_template)?></textarea>
                <textarea name="content" id="contentEditor"><?php echo set_value('content', $content); ?></textarea>
            </div>
        </div>
    </div>
</div>