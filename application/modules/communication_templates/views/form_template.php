<html>
    <head>
        <style>
            /** 
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
            @page {
                margin: 0cm 0cm;
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 3.2cm;
                margin-left: 1cm;
                margin-right: 1cm;
                margin-bottom: 2cm;
            }

            /** Define the header rules **/
            header {
                position: fixed;
                top: 0cm;
                left: 0cm;
                right: 0cm;
                height: 3cm;
            }

            /** Define the footer rules **/
            footer {
                position: fixed; 
                bottom: 0cm; 
                left: 0cm; 
                right: 0cm;
                height: 2cm;
            }

            p {
                padding: 0px;margin:0px;
            }
        </style>
        <title>
            <?=$data['name']?>
        </title>
    </head>
    <body>
        <!-- Define header and footer blocks before your content -->
        <header>
            <?php echo get_document_img('header', $data['header'], $data['name']); ?>
        </header>

        <footer>
            <?php echo get_document_img('footer', $data['footer']); ?>
        </footer>

        <!-- Wrap the content of your PDF inside a main tag -->
        <main>
            <?php //echo str_replace('{{', '', str_replace('}}', ' ',$data['content'])) ?>
            <?php echo $data['content']; ?>
        </main>
    </body>
</html>
