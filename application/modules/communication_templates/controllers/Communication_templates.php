<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Communication_templates extends MY_Controller
{

    public $fields = [
		'name' => array(
			'field'=>'name',
			'label'=>'Template Name',
			'rules'=>'trim|required'
		),
		'description' => array(
			'field'=>'description',
			'label'=>'Template Description',
			'rules'=>'trim|required'
		),
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Communication_template_model', 'M_comm_temp');
		$this->load->library('parser');

        $this->load_fields = $this->communication_library->load_fields();

	}

	public function get_all()
    {
        $data = $this->M_comm_temp->as_array()->get_all();
        $output = array(
            'data' => $data,
        );
        echo json_encode($output);
    }

    public function index()
    {
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index');
    }

     public function view($id = FALSE, $type = '')
    {
        if ($id) {

            $this->view_data['row'] = $this->M_comm_temp->get($id);

            if ($this->view_data['row']) {

                if ($type == "json") {
                	$this->view_data['row']['content'] = html_entity_decode(htmlentities($this->view_data['row']['content']));
                	echo json_encode( $this->view_data['row']);
                	
                } else {
               		 $this->template->build('view', $this->view_data);
                }

            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }


    public function create()
    {
		$this->js_loader->queue(['tinymce/tinymce.min.js']);

        $this->view_data['table_list'] = $this->load_fields;


        if($this->input->post()){

            $this->form_validation->set_rules($this->fields);

			if ($this->form_validation->run() === TRUE) {

                $post = $this->input->post();

				$insert_id = $this->M_comm_temp->insert($post);

				if($insert_id !== FALSE){

					redirect('communication_templates/view/'.$insert_id, 'refresh');
				}

            } else {

                // Error
				$this->notify->error('Oops something went wrong.');

            }
		}
		$data = array(
			'body' => 'Sample Body'
		);
		$this->view_data['email_template'] = $this->parser->parse('templates/header', $data);
        $this->template->build('create', $this->view_data);
	}
	
	public function update($id = FALSE)
	{
		if ($id) {

			$this->js_loader->queue(['tinymce/tinymce.min.js']);
			
			$this->view_data['table_list'] = $this->communication_library->load_fields();

			$this->view_data['form'] = $data =  $this->M_comm_temp->get($id);

			if ($data) {

				 if($this->input->post()){

					$this->form_validation->set_rules($this->fields);

					if ($this->form_validation->run() === TRUE) {

						$post = $this->input->post();

						$result = $this->M_comm_temp->update($post, $data['id']);

						if($result !== FALSE){

							redirect('communication_templates/view/'.$data['id'], 'refresh');
						}

					} else {
						
						// Error
						$this->notify->error('Oops something went wrong.');

					}
				}

				$this->template->build('update', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function delete()
	{	
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		$id	= $this->input->post('id');
		if ($id) {

			$list = $this->M_comm_temp->get($id);
			if ($list) {

				$deleted = $this->M_comm_temp->delete($list['id']);
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Template successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}


    public function get_table_fields($TblName = FALSE)   {
        
        $tables = $this->load_fields;

        if($this->input->is_ajax_request()){

            $fields = $tables[$TblName];

            echo json_encode($fields);

            exit(0);
        } else {
            show_404();
        }
    }

    public function showData()
	{
		$output = ['data' => ''];
		$columnsDefault = [
			'id'     => true,
			'name'      => true,
			'description'      => true,
			'type'      => true,
			'created_at'     => true,
			'Actions'      => true,
		];

		if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
			$columnsDefault = [];
			foreach ($_REQUEST['columnsDef'] as $field) {
				$columnsDefault[$field] = true;
			}
		}

		// get all raw data
		$alldata = $this->M_comm_temp->as_array()->get_all();

		$data = [];
		// internal use; filter selected columns only from raw data
		if($alldata) {
			foreach ($alldata as $d) {
				$data[] = $this->filterArray($d, $columnsDefault);
			}

			// count data
			$totalRecords = $totalDisplay = count($data);

			// filter by general search keyword
			if (isset($_REQUEST['search'])) {
				$data         = $this->filterKeyword($data, $_REQUEST['search']);
				$totalDisplay = count($data);
			}

			if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
				foreach ($_REQUEST['columns'] as $column) {
					if (isset($column['search'])) {
						$data         = $this->filterKeyword($data, $column['search'], $column['data']);
						$totalDisplay = count($data);
					}
				}
			}

			// sort
			if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
				$column = $_REQUEST['order'][0]['column'];
				$dir    = $_REQUEST['order'][0]['dir'];
				usort($data, function ($a, $b) use ($column, $dir) {
					$a = array_slice($a, $column, 1);
					$b = array_slice($b, $column, 1);
					$a = array_pop($a);
					$b = array_pop($b);

					if ($dir === 'asc') {
						return $a > $b ? true : false;
					}

					return $a < $b ? true : false;
				});
			}

			// pagination length
			if (isset($_REQUEST['length'])) {
				$data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
			}

			// return array values only without the keys
			if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
				$tmp  = $data;
				$data = [];
				foreach ($tmp as $d) {
					$data[] = array_values($d);
				}
			}
			$secho = 0;
			if (isset($_REQUEST['sEcho'])) {
				$secho = intval($_REQUEST['sEcho']);
			}

			$output = array(
				'sEcho' => $secho,
				'sColumns' => '',
				'iTotalRecords' => $totalRecords,
				'iTotalDisplayRecords' => $totalDisplay,
				'data' => $data
			);

		}		
		echo json_encode($output);
		exit();
	}

	public function clean_key($key = ''){

		if ($key == "buyer_identifications") {
			$key = 'identifications';
		}

		return $key;
	}

	public function process_array($clean_key = '', $results = array(), $clean_val = ''){
		$rows = array_key_exists_r($clean_key, $results, $clean_val);

		if (isset($rows[$clean_key][$clean_val])) {
			$value = $rows[$clean_key][$clean_val];
		} else {
			$value = @$rows[$clean_key][0][$clean_val];
		}

		return $value;
	}
}
