<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Seller_teams_model extends MY_Model
{
    public $table = 'seller_teams'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'name',
        'seller_group_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required',
        ),
        'seller_group_id' => array(
            'field' => 'seller_group_id',
            'label' => 'Seller Group',
            'rules' => 'trim|required',
        ),
        'seller_group_name' => array(
            'field' => 'seller_group_name',
            'label' => 'Seller Group Name',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['seller_group'] = array('foreign_model' => 'seller_group/Seller_group_model', 'foreign_table' => 'seller_group', 'foreign_key' => 'id', 'local_key' => 'seller_group_id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'name' => $faker->word,
                'seller_group_id' => $faker->numberBetween(1, 3),
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}
