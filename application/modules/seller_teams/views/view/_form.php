<?php
$id = isset($seller_teams['id']) && $seller_teams['id'] ? $seller_teams['id'] : '';
$name = isset($seller_teams['name']) && $seller_teams['name'] ? $seller_teams['name'] : '';
$seller_group_id = isset($seller_teams['seller_group_id']) && $seller_teams['seller_group_id'] ? $seller_teams['seller_group_id'] : '';

$seller_group = get_person($seller_group_id, 'seller_group');
$seller_group_name = get_name($seller_group);

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Seller Group <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="seller_group"  id="seller_group_id" name="seller_group_id">
                <option value="0">Select Group</option>
                <?php if ($seller_group_name): ?>
                    <option value="<?php echo $seller_group_id; ?>" selected><?php echo $seller_group_name; ?></option>
                <?php endif?>
            </select>
            <?php echo form_error('seller_group_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>