<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Petty_cash_replenishments extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $petty_cash_replenishment_model = [
            'petty_cash_replenishments/Petty_cash_replenishment_model' => 'M_petty_cash_replenishment',
            'accounting_entries/Accounting_entries_model' => 'M_Accounting_entries',
            'accounting_entry_items/Accounting_entry_items_model' => 'M_Accounting_entry_items'
        ];

        // Load models
        $this->load->model($petty_cash_replenishment_model);

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_petty_cash_replenishment->fillable;
        $this->_table_columns = $this->M_petty_cash_replenishment->__get_columns();

        $this->u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $this->additional = [
            'is_active' => 1,
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];

        $this->load->helper('format');
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_petty_cash_replenishment->get_columns();

        if ($db_columns) {

            $column = [];

            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_petty_cash_replenishment->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showPettyCashReplenishments()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'reference' => true,
            'amount' => true,
            'payee_type_id' => true,
            'payee_id' => true,
            'remarks' => true,
            'replenished_at' => true,
            'department_id' => true,
            'journal_type_id' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        $petty_cash_replenishment = $this->M_petty_cash_replenishment->get_all();

        $data = [];

        if ($petty_cash_replenishment) {

            foreach ($petty_cash_replenishment as $key => $value) {

                $petty_cash_replenishment[$key]['payee_type_id'] = Dropdown::get_static('accounting_entries_payee_type', $value['payee_type_id']);

                if ($value['payee_type_id'] == 1 || $value['payee_type_id'] == 4) {

                    $payee_name = get_value_field($value['payee_id'], strtolower(Dropdown::get_static('accounting_entries_payee_type', $value['payee_type_id'])), 'name');
                } else {

                    $payee_first_name = get_value_field($value['payee_id'], strtolower(Dropdown::get_static('accounting_entries_payee_type', $value['payee_type_id'])), 'first_name');

                    $payee_last_name = get_value_field($value['payee_id'], strtolower(Dropdown::get_static('accounting_entries_payee_type', $value['payee_type_id'])), 'last_name');

                    $payee_name = $payee_first_name . ' ' . $payee_last_name;
                }

                $petty_cash_replenishment[$key]['payee_id'] = isset($payee_name) && $payee_name ? $payee_name : '';

                $petty_cash_replenishment[$key]['department_id'] = get_value_field($value['department_id'], 'departments', 'name');

                $petty_cash_replenishment[$key]['journal_type_id'] = Dropdown::get_static('accounting_entries_journal_type', $value['journal_type_id']);

                $petty_cash_replenishment[$key]['replenished_at'] = isset($value['replenished_at']) && $value['replenished_at'] ? date("Y-m-d", strtotime($value['replenished_at'])) : '';

                $petty_cash_replenishment[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                $petty_cash_replenishment[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
            }

            foreach ($petty_cash_replenishment as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                $_column = $_REQUEST['order'][0]['column'] - 1;
                $_dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($x, $y) use ($_column, $_dir) {

                    $x = array_slice($x, $_column, 1);

                    $x = array_pop($x);

                    $y = array_slice($y, $_column, 1);
                    $y = array_pop($y);

                    if ($_dir === 'asc') {

                        return $x > $y ? true : false;
                    } else {

                        return $x < $y ? true : false;
                    }
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    public function form($id = false)
    {

        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $info = $this->input->post();

            $request['amount'] = $info['amount'];
            $request['journal_type_id'] = $info['journal_type_id'];
            $request['payee_type_id'] = $info['payee_type_id'];
            $request['payee_id'] = $info['payee_id'];
            $request['department_id'] = $info['department_id'];
            $request['replenished_at'] = $info['replenished_at'];
            $request['remarks'] = $info['remarks'];

            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

            if ($id) {

                $additional = [
                    'updated_by' => $this->user->id,
                    'updated_at' => NOW,
                ];

                $result = $this->M_petty_cash_replenishment->update($request + $additional, $id);
            } else {

                $additional = [
                    'created_by' => $this->user->id,
                    'created_at' => NOW,
                ];

                // begin:Save accounting entries
                $accounting_entry['or_number'] = 0;
                $accounting_entry['invoice_number'] = uniqidPCR();
                $accounting_entry['journal_type'] = $info['journal_type_id'];
                $accounting_entry['payee_type'] = strtolower(Dropdown::get_static('accounting_entries_payee_type', $info['payee_type_id']));
                $accounting_entry['payee_type_id'] = $info['payee_id'];
                $accounting_entry['remarks'] = $info['remarks'];
                $accounting_entry['project_id'] = 0;
                $accounting_entry['property_id'] = 0;
                $accounting_entry['company_id'] = $info['accounting_entry']['company_id'];
                $accounting_entry['dr_total'] = $info['accounting_entry']['dr_total'];
                $accounting_entry['cr_total'] = $info['accounting_entry']['cr_total'];

                $accounting_entry_id = $this->M_Accounting_entries->insert($accounting_entry + $additional);
                // end:Save accounting entries

                $entry_items = $info['entry_item'];

                foreach ($entry_items as $key => $entry) {
                    $entry_item['accounting_entry_id'] = $accounting_entry_id;
                    $entry_item['ledger_id'] = $entry['ledger_id'];
                    $entry_item['amount'] = $entry['amount'];
                    $entry_item['dc'] = $entry['dc'];
                    $entry_item['payee_type'] = $accounting_entry['payee_type'];
                    $entry_item['payee_type_id'] = $accounting_entry['payee_type_id'];
                    $entry_item['is_reconciled'] = 0;
                    $entry_item['description'] = $entry['description'];

                    $this->M_Accounting_entry_items->insert($entry_item + $additional);
                }

                $request['reference'] = $accounting_entry['invoice_number'];
                $request['accounting_entry_id'] = $accounting_entry_id;

                $result = $this->M_petty_cash_replenishment->insert($request);
            }

            $this->db->trans_complete(); # Completing payment_request

            /*Optional*/
            if ($this->db->trans_status() === false) {
                # Something went wrong.
                $this->db->trans_rollback();
                $response['status'] = 0;
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();
                $response['status'] = 1;
                $response['message'] = 'Petty Cash Replenishment Successfully ' . $method . 'd!';
            }

            echo json_encode($response);
            exit();
        }

        if ($id) {

            $this->view_data['info'] = $info = $this->M_petty_cash_replenishment->get($id);
        }

        $this->view_data['method'] = $method;

        $this->template->build('form', $this->view_data);
    }

    public function view($id = false)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['info'] = $this->M_petty_cash_replenishment->with_accounting_entry()->get($id);

            if ($this->view_data['info']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_petty_cash_replenishment->get($id);
            if ($list) {

                $deleted = $this->M_petty_cash_replenishment->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Vehicle Activities Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_petty_cash_replenishment->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Vehicle Activities/s Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }
}
