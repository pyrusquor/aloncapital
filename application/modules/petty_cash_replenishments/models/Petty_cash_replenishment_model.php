<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Petty_cash_replenishment_model extends MY_Model
{
    public $table = 'petty_cash_replenishments'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        // Add db columns here
        'reference',
        'accounting_entry_id',
        'amount',
        'payee_type_id',
        'payee_id',
        'remarks',
        'replenished_at',
        'department_id',
        'journal_type_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['accounting_entry'] = array('foreign_model' => 'accounting_entries/Accounting_entries_model', 'foreign_table' => 'accounting_entries', 'foreign_key' => 'id', 'local_key' => 'accounting_entry_id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }
}
