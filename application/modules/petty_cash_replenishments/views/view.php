<?php

$id = isset($info['id']) && $info['id'] ? $info['id'] : '';

?>

<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Petty Cash Replenishment</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('petty_cash_replenishments/form/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('petty_cash_replenishments'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#general_information" role="tab" aria-selected="true">
                            <i class="flaticon2-information"></i> General Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#cash_disbursements" role="tab" aria-selected="false">
                            <i class="flaticon-price-tag"></i> Cash Disbursements
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#accounting_entries" role="tab" aria-selected="false">
                            <i class="flaticon2-list"></i> Accounting Entries
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="kt-portlet__body">
            <div class="tab-content  kt-margin-t-20">
                <!-- General Information -->
                <div class="tab-pane active" id="general_information" role="tabpanel-1">
                    <?php $this->load->view('view/general_information'); ?>
                </div>

                <!-- Cash Disbursements -->
                <div class="tab-pane" id="cash_disbursements" role="tabpanel-2">
                    <?php $this->load->view('view/cash_disbursements_information'); ?>
                </div>

                <!-- Accounting Entries Information -->
                <div class="tab-pane" id="accounting_entries" role="tabpanel-3">
                    <?php $this->load->view('view/accounting_entries_information'); ?>
                </div>
            </div>
        </div>
    </div>
</div>