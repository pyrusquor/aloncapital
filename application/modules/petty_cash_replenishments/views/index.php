<!-- CONTENT HEADER -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Petty Cash Replenishments</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc" id="total"></span>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input type="text" class="form-control" placeholder="Search Petty Cash Replenishments..." id="generalSearch">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <!-- <button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm"
                        data-toggle="collapse" data-target="#_advance_search" aria-expanded="true"
                        aria-controls="_advance_search">
                    <i class="fa fa-filter"></i> Filter
                </button> -->
                <!-- <button type="button" id="_batch_upload_btn" class="btn btn-label-primary btn-elevate btn-sm"
                        data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true"
                        aria-controls="_batch_upload">
                    <i class="fa fa-upload"></i> Import
                </button>
                <button type="button" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal"
                        data-target="#_export_option">
                    <i class="fa fa-download"></i> Export
                </button> -->
            </div>
        </div>
    </div>
</div>

<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">
        <div class="module__create">
            <a href="<?php echo site_url('petty_cash_replenishments/form'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                <i class="fa fa-plus"></i> Add Petty Cash Replenishment
            </a>
        </div>

        <!-- <div class="module__filter">
            <button class="btn btn-label-primary btn-elevate btn-sm btn-filter" id="_advance_search_btn" data-toggle="collapse" data-target="#_advance_search">
                <i class="fa fa-filter"></i> Filter
            </button>
        </div> -->
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="petty_cash_replenishments_table">
            <thead>
                <tr>
                    <th width="1%">
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="all" class="m-checkable" id="select-all">
                            <span></span>
                        </label>
                    </th>
                    <th>ID</th>
                    <th>Reference</th>
                    <th>Amount</th>
                    <th>Payee Type</th>
                    <th>Payee</th>
                    <th>Remarks</th>
                    <th>Replenished At</th>
                    <th>Department</th>
                    <th>Journal Type</th>
                    <th>Created By</th>
                    <th>Last Update By</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable -->
    </div>
</div>