<div class="kt-form__body">
    <div class="kt-section">
        <div class="kt-section__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="cash_disbursements_table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Reference</th>
                        <th>Petty Cash Replenishment</th>
                        <th>Amount</th>
                        <th>Received By Payee Type ID</th>
                        <th>Received By Payee ID</th>
                        <th>Disbursed At</th>
                        <th>Disbursed By ID</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>