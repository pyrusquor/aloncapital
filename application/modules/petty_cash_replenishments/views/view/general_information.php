<?php

$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$reference = isset($info['reference']) && $info['reference'] ? $info['reference'] : '';
$amount = isset($info['amount']) && $info['amount'] ? $info['amount'] : '';
$payee_type_id = isset($info['payee_type_id']) && $info['payee_type_id'] ? $info['payee_type_id'] : '';
$payee_id = isset($info['payee_id']) && $info['payee_id'] ? $info['payee_id'] : '';
$remarks = isset($info['remarks']) && $info['remarks'] ? $info['remarks'] : '';
$replenished_at = isset($info['replenished_at']) && $info['replenished_at'] ? date("Y-m-d", strtotime($info['replenished_at'])) : '';
$department_id = isset($info['department_id']) && $info['department_id'] ? $info['department_id'] : '';
$journal_type_id = isset($info['journal_type_id']) && $info['journal_type_id'] ? $info['journal_type_id'] : '';

if ($payee_type_id == 1 || $payee_type_id == 4) {

    $payee_name = get_value_field($payee_id, strtolower(Dropdown::get_static('accounting_entries_payee_type', $payee_type_id)), 'name');
    $payee_data_type = 'payee';
} else {

    $payee_first_name = get_value_field($payee_id, strtolower(Dropdown::get_static('accounting_entries_payee_type', $payee_type_id)), 'first_name');
    $payee_last_name = get_value_field($payee_id, strtolower(Dropdown::get_static('accounting_entries_payee_type', $payee_type_id)), 'last_name');
    $payee_name = $payee_last_name . ' ' . $payee_first_name;
}

$payee_name = isset($payee_name) && $payee_name ? $payee_name : '';
$payee_type = isset($payee_type_id) && $payee_type_id ? Dropdown::get_static('accounting_entries_payee_type', $payee_type_id) : '';
$department_name = $department_id ? get_value_field($department_id, 'departments', 'name') : '';
$journal_name = $journal_type_id ? Dropdown::get_static('accounting_entries_journal_type', $journal_type_id) : '';
$accounting_entry_id = isset($info['accounting_entry_id']) && $info['accounting_entry_id'] ? $info['accounting_entry_id'] : '';

?>

<script type="text/javascript">
    window.petty_cash_replenishment_id = "<?= $id ?>";
    window.accounting_entry_id = "<?= $accounting_entry_id ?>";
</script>

<div class="kt-form__body">
    <div class="kt-section">
        <div class="kt-section__body">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Reference
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $reference ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Amount
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $amount ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Payee Type
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $payee_type ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Payee
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $payee_name ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Remarks
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $remarks ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Replenished At
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $replenished_at ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Department
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $department_name ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Journal Type
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $journal_name ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>