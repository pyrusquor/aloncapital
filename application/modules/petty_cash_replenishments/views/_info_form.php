<?php

$amount = isset($info['amount']) && $info['amount'] ? $info['amount'] : '';
$journal_type_id = isset($info['journal_type_id']) && $info['journal_type_id'] ? $info['journal_type_id'] : '';
$payee_type_id = isset($info['payee_type_id']) && $info['payee_type_id'] ? $info['payee_type_id'] : '5';
$payee_id = isset($info['payee_id']) && $info['payee_id'] ? $info['payee_id'] : '';
$remarks = isset($info['remarks']) && $info['remarks'] ? $info['remarks'] : '';
$replenished_at = isset($info['replenished_at']) && $info['replenished_at'] ? date("Y-m-d", strtotime($info['replenished_at'])) : '';
$department_id = isset($info['department_id']) && $info['department_id'] ? $info['department_id'] : '';

$payee_type = isset($payee_type_id) && $payee_type_id ? strtolower(Dropdown::get_static('accounting_entries_payee_type', $payee_type_id)) : '';

if ($payee_type_id == 1 || $payee_type_id == 4) {

    $payee_name = get_value_field($payee_id, strtolower(Dropdown::get_static('accounting_entries_payee_type', $payee_type_id)), 'name');

    $payee_data_type = 'payee';
} else {

    $payee_first_name = get_value_field($payee_id, strtolower(Dropdown::get_static('accounting_entries_payee_type', $payee_type_id)), 'first_name');

    $payee_last_name = get_value_field($payee_id, strtolower(Dropdown::get_static('accounting_entries_payee_type', $payee_type_id)), 'last_name');

    $payee_name = $payee_last_name . ' ' . $payee_first_name;

    $payee_data_type = 'person';
}

$payee_name = isset($payee_name) && $payee_name ? $payee_name : '';
$department_name = $department_id ? get_value_field($department_id, 'departments', 'name') : '';

?>

<script type="text/javascript">
    window.payee_types = <?= json_encode(Dropdown::get_static('accounting_entries_payee_type')) ?>;
</script>

<div class="row">

    <div class="col-sm-6">
        <div class="form-group">
            <label>Amount</label>
            <div class="kt-input-icon">
                <input type="number" class="form-control" name="amount" value="<?= set_value('amount', $amount); ?>" placeholder="Amount" autocomplete="off" id="amount">
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Journal Type</label>
            <div class="kt-input-icon">
                <?= form_dropdown('journal_type_id', Dropdown::get_static('accounting_entries_journal_type'), set_value('journal_type_id', @$journal_type_id), 'class="form-control"'); ?>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Payee Type</label>
            <?= form_dropdown('payee_type_id', Dropdown::get_static('accounting_entries_payee_type'), set_value('payee_type_id', @$payee_type_id), 'class="form-control" id="payee_type_id"'); ?>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Payee</label>
            <div class="row">
                <div class="col-sm-12">
                    <select class="form-control suggests" data-type="<?= $payee_data_type ?>" data-module="<?= $payee_type ? $payee_type : "buyers" ?>" id="payee_id" name="payee_id">
                        <option value="">Select Payee</option>
                        <?php if ($payee_name) : ?>
                            <option value="<?= $payee_id; ?>" selected><?= $payee_name; ?></option>
                        <?php endif ?>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Department</label>
            <div class="row">
                <div class="col-sm-12">
                    <select class="form-control suggests" data-module="departments" id="department_id" name="department_id">
                        <option value="">Select Department</option>
                        <?php if ($department_id) : ?>
                            <option value="<?= $department_id; ?>" selected><?= $department_name; ?></option>
                        <?php endif ?>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Replenished At</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control kt_datepicker" name="replenished_at" value="<?= set_value('replenished_at', @$replenished_at); ?>" placeholder="Replenished At" autocomplete="off" readonly>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Remarks</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="remarks" value="<?= set_value('remarks', $remarks); ?>" placeholder="Remarks" autocomplete="off">
            </div>
        </div>
    </div>
</div>