<?php

$id = isset($accounting_entry['id']) && $accounting_entry['id'] ? $accounting_entry['id'] : '';
$company_id = isset($accounting_entry['company_id']) && $accounting_entry['company_id'] ? $accounting_entry['company_id'] : '';
$company_name = isset($accounting_entry['company_id']) && $accounting_entry['company_id'] ? get_value_field($accounting_entry['company_id'], 'companies', 'name') : '';
$cr_total = isset($accounting_entry['cr_total']) && $accounting_entry['cr_total'] ? $accounting_entry['cr_total'] : '';
$dr_total = isset($accounting_entry['dr_total']) && $accounting_entry['dr_total'] ? $accounting_entry['dr_total'] : '';
$accounting_entry_items = isset($accounting_entry['accounting_entry_items']) && $accounting_entry['accounting_entry_items'] ? $accounting_entry['accounting_entry_items'] : [];

?>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="">Companies <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <select name="accounting_entry[company_id]" id="company_id" class="suggests form-control" data-module="companies">
                    <option value="0">Select Company</option>
                    <?php if ($company_name) : ?>
                        <option value="<?= $company_id; ?>" selected><?= $company_name; ?></option>
                    <?php endif ?>
                </select>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<!-- Accounting Entry Items -->
<?php if (!empty($accounting_entry_items)) : ?>
    <?php foreach ($accounting_entry_items as $key => $entry_item) : ?>
        <?php

        $item_id = isset($entry_item['id']) && $entry_item['id'] ? $entry_item['id'] : '';
        $ledger_id = isset($entry_item['ledger_id']) && $entry_item['ledger_id'] ? $entry_item['ledger_id'] : '';
        $dc = isset($entry_item['dc']) && $entry_item['dc'] ? $entry_item['dc'] : '';
        $description = isset($entry_item['description']) && $entry_item['description'] ? $entry_item['description'] : '';
        $amount = isset($entry_item['amount']) && $entry_item['amount'] ? $entry_item['amount'] : '';
        $payee_type = isset($entry_item['payee_type']) && $entry_item['payee_type'] ? $entry_item['payee_type'] : '';
        $payee_type_id = isset($entry_item['payee_type_id']) && $entry_item['payee_type_id'] ? $entry_item['payee_type_id'] : '';

        ?>

        <div id="entry_item_form_repeater">
            <div data-repeater-list="entry_item[<?php echo $item_id; ?>]">
                <div data-repeater-item="entry_item[<?php echo $item_id; ?>]" class="row">
                    <div class="col-sm-2 d-none">
                        <div class="form-group">
                            <label>ID <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon kt-input-icon--left">
                                <input class="form-control" name="entry_item[<?php echo $item_id ?>][id]" value="<?php echo set_value('item_id', $item_id); ?>" placeholder="Type" autocomplete="off" />

                            </div>
                            <?php echo form_error('dc'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Type <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <select class="form-control" name="entry_item[<?php echo $item_id ?>][dc]" value="<?php echo set_value('dc', $dc); ?>" placeholder="Type" autocomplete="off" id="entry_item_dc">
                                    <option value="">Select Option</option>
                                    <option value="d">D</option>
                                    <option value="c">C</option>
                                </select>

                            </div>
                            <?php echo form_error('dc'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Ledger <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <select class="form-control suggests" name="entry_item[<?php echo $item_id ?>][ledger_id]" value="<?php echo set_value('ledger_id', $ledger_id); ?>" id="entry_item_ledger" data-ledger="<?php echo $ledger_id; ?>" data-param="company_id" data-module="accounting_ledgers">
                                    <option>Select Option</option>
                                </select>

                            </div>
                            <?php echo form_error('ledger_id'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Dr Amount <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <input type="number" class="form-control" name="entry_item[<?php echo $item_id ?>][amount]" value="<?php echo set_value('amount', $amount); ?>" placeholder="DR Amount" autocomplete="off" id="dr_amount_input">

                            </div>
                            <?php echo form_error('amount'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Cr Amount <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <input type="number" class="form-control" name="entry_item[<?php echo $item_id ?>][amount]" value="<?php echo set_value('amount', $amount); ?>" placeholder="CR Amount" autocomplete="off" id="cr_amount_input">

                            </div>
                            <?php echo form_error('amount'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Description</label>
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control" name="entry_item[<?php echo $item_id ?>][description]" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">

                                    </div>
                                    <?php echo form_error('description'); ?>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="col-sm-3">

                                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                        <i class="la la-trash-o"></i>
                                    </a>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        <?php endforeach; ?>
    <?php else : ?>
        <div id="entry_item_form_repeater">
            <div data-repeater-list="entry_item">
                <div data-repeater-item="entry_item" class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Type <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <select class="form-control" name="entry_item[dc]" value="<?php echo set_value('dc', $dc); ?>" placeholder="Type" autocomplete="off" id="entry_item_dc">
                                    <option value="">Select option</option>
                                    <option value="d">D</option>
                                    <option value="c">C</option>
                                </select>

                            </div>
                            <?php echo form_error('dc'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Ledger <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <select class="form-control suggests" name="entry_item[ledger_id]" value="<?php echo set_value('ledger_id', $ledger_id); ?>" placeholder="Type" autocomplete="off" id="entry_item_ledger" data-param="company_id" data-module="accounting_ledgers">
                                    <option>Select option</option>
                                </select>
                            </div>
                            <?php echo form_error('ledger_id'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Dr Amount <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <input type="number" class="form-control" name="entry_item[amount]" value="<?php echo set_value('amount', $amount); ?>" placeholder="DR Amount" autocomplete="off" id="dr_amount_input">

                            </div>
                            <?php echo form_error('amount'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Cr Amount <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <input type="number" class="form-control" name="entry_item[amount]" value="<?php echo set_value('amount', $amount); ?>" placeholder="CR Amount" autocomplete="off" id="cr_amount_input">

                            </div>
                            <?php echo form_error('amount'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Description</label>
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control" name="entry_item[description]" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">

                                    </div>
                                    <?php echo form_error('description'); ?>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="col-sm-3">

                                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                        <i class="la la-trash-o"></i>
                                    </a>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        <?php endif; ?>

        <?php if (empty($accounting_entry_items)) : ?>
            <div class="form-group form-group-last row">
                <div class="offset-10"></div>
                <div class="col-lg-2">
                    <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                        <i class="la la-plus"></i> Add Entry Item
                    </a>
                </div>
            </div>
        </div>
    <?php endif ?>

    <div class="row">
        <div class="offset-3"></div>
        <div class="col-sm-3">
            <div class="form-group row">
                <label for="dTotal" class="col-sm-4 col-form-label">Dr Total</label>
                <div class="col-sm-8">
                    <input class="form-control" name="accounting_entry[dr_total]" type="text" placeholder="D Total" id="dTotal" value="<?php echo set_value('dr_total', $dr_total); ?>" readonly>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group row">
                <label for="cTotal" class="col-sm-4 col-form-label">Cr Total</label>
                <div class="col-sm-8">
                    <input class="form-control" name="accounting_entry[cr_total]" type="text" placeholder="C Total" id="cTotal" value="<?php echo set_value('cr_total', $cr_total); ?>" readonly>
                </div>
            </div>
        </div>

        <div class="offset-3"></div>
    </div>
    <!-- /Accounting Entry Items -->