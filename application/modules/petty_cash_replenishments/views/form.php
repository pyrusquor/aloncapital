<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title"><?= $method ?> Petty Cash Replenishment</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <!-- <button type="submit" class="btn btn-label-success btn-elevate btn-sm" form="petty_cash_replenishment_form">
                    <i class="fa fa-plus-circle"></i> Submit
                </button> -->
                <a href="<?php echo site_url('petty_cash_replenishments'); ?>" class="btn btn-label-instagram btn-elevate btn-sm">
                    <i class="fa fa-reply"></i> Cancel
                </a>
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
                <div class="kt-grid__item">

                    <!--begin: Form Wizard Nav -->
                    <div class="kt-wizard-v3__nav">
                        <div class="kt-wizard-v3__nav-items">
                            <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
                                <div class="kt-wizard-v3__nav-body">
                                    <div class="kt-wizard-v3__nav-label">
                                        <span>1</span> General Information
                                    </div>
                                    <div class="kt-wizard-v3__nav-bar"></div>
                                </div>
                            </a>
                            <?php if (!isset($info)) : ?>
                                <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <span>4</span> Entry Items
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar"></div>
                                    </div>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!--end: Form Wizard Nav -->
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                    <!--begin: Form Wizard Form-->
                    <form class="kt-form" method="POST" action="<?php form_open('petty_cash_replenishments/form/' . @$info['id']); ?>" id="petty_cash_replenishment_form" enctype="multipart/form-data">
                        <?php $this->load->view('_form'); ?>
                    </form>

                    <!--end: Form Wizard Form-->
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    <?php if (isset($info)) : ?>
        window.is_update = true
    <?php else : ?>
        window.is_update = false
    <?php endif; ?>
</script>