<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Account_manager extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('transaction_personnel_logs/Transaction_personnel_logs_model', 'M_Account_manager');
        $this->load->model('transaction/Transaction_model', 'M_transaction');
        $this->load->model('transaction_payment/Transaction_official_payment_model', 'M_Transaction_official_payment');
        $this->load->model('property/Property_model', 'M_property');
        $this->load->model('staff/Staff_model', 'M_staff');
        $this->load->model('project/Project_model', 'M_project');
        $this->load->model('transaction_post_dated_check/Transaction_post_dated_check_model', 'M_pdc');

        // Load pagination library 
		$this->load->library('ajax_pagination');
        // Per page limit 
        $this->perPage = 6;

        $this->_table_fillables = $this->M_Account_manager->fillable;
        $this->_table_columns = $this->M_Account_manager->__get_columns();
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $total_count = $this->M_Account_manager->limit(500)->total_overdue_collections();

        $this->view_data['total'] = $totalRec = count($total_count);

        // Pagination configuration
        $config['target']      = '#accountManagerContent';
        $config['base_url']    = base_url('accountManager/paginationData');
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $config['link_func']   = 'AccountManagerPagination';
        
        // Initialize pagination library
        $this->ajax_pagination->initialize($config);

        $this->view_data['records'] = $this->M_Account_manager->limit(500)->total_overdue_collections($this->perPage, 0);

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function view($id = FALSE)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['data'] = $this->M_Account_manager->with_tax()->get($id);

            if ($this->view_data['data']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function paginationData()
    {
        if ($this->input->is_ajax_request()) {

            // Input from General Search
            $keyword = $this->input->post('keyword');
            $project_id = $this->input->post('project_id');
            $personnel_id = $this->input->post('personnel_id');
            $collection_date = $this->input->post('collection_date');

            $page = $this->input->post('page');

			
            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $data = array();

            if($collection_date == "due_today") {
                
                $data = $this->M_Account_manager->limit(500)->total_due_today();

            } else if($collection_date == "overdue_collections") {

                $data = $this->M_Account_manager->limit(500)->total_overdue_collections();

            } else if($collection_date == "upcoming_collections") {

                $data = $this->M_Account_manager->limit(500)->total_upcoming_collections();

            } else {
                $data = $this->M_Account_manager->limit(500)->total_overdue_collections();
            }
            

            $totalRec = count($data);
            $where = array();

            // Pagination configuration
            $config['target']      = '#accountManagerContent';
            $config['base_url']    = base_url('accountManager/paginationData');
            $config['total_rows']  = $totalRec;
            $config['per_page']    = $this->perPage;
            $config['link_func']   = 'AccountManagerPagination';
			
            if (!empty($project_id) && !empty($project_id)):
				$where['project_id'] = $project_id;
            endif;
            
			if (!empty($personnel_id) && !empty($personnel_id)):
				$where['personnel_id'] = $personnel_id;
            endif;

			if (!empty($collection_date) && !empty($collection_date)):
				$where['collection_date'] = $collection_date;
            endif;

            $this->view_data['total'] = $totalRec = count($data);

			// Pagination configuration
			$config['total_rows'] = $totalRec;

			// Initialize pagination library
			$this->ajax_pagination->initialize($config);

			if (!empty($project_id) && !empty($project_id)):
				$where['project_id'] = $project_id;
            endif;
            
			if (!empty($personnel_id) && !empty($personnel_id)):
				$where['personnel_id'] = $personnel_id;
            endif;

            if (!empty($collection_date) && !empty($collection_date)):
				$where['collection_date'] = $collection_date;
            endif;
            

            if($collection_date == "due_today") {
                
                $this->view_data['records'] = $records =  $this->M_Account_manager->total_due_today($this->perPage, $offset, $where);

            } else if($collection_date == "overdue_collections") {

                $this->view_data['records'] = $records = $this->M_Account_manager->total_overdue_collections($this->perPage, $offset, $where);

            } else if($collection_date == "upcoming_collections") {
                
                $this->view_data['records'] = $records = $this->M_Account_manager->total_upcoming_collections($this->perPage, $offset, $where);

            } else {

                $this->view_data['records'] = $records = $this->M_Account_manager->total_overdue_collections($this->perPage, $offset, $where);

            }
            
            $this->view_data['table'] = "overdue";

			$this->load->view('account_manager/_filter', $this->view_data, false);
        }
    }

    public function paginationDataPDC()
    {
        if ($this->input->is_ajax_request()) {

            // Input from General Search
            $keyword = $this->input->post('keyword');
            $project_id = $this->input->post('project_id');
            $personnel_id = $this->input->post('personnel_id');
            $collection_date = $this->input->post('collection_date');

            $page = $this->input->post('page');

			
            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $data = array();

            if($collection_date == "for_clearing_today") {
                
                $data = $this->M_pdc->limit(500)->for_clearing_today();

            } else if($collection_date == "overdue_checks") {

                $data = $this->M_pdc->limit(500)->overdue_checks();

            } else if($collection_date == "upcoming_clearing") {

                $data = $this->M_pdc->limit(500)->upcoming_clearing();

            } else {
                $data = $this->M_pdc->limit(500)->for_clearing_today();
            }
            

            $totalRec = count($data);
            $where = array();

            // Pagination configuration
            $config['target']      = '#accountManagerContent';
            $config['base_url']    = base_url('accountManager/paginationDataPDC');
            $config['total_rows']  = $totalRec;
            $config['per_page']    = $this->perPage;
            $config['link_func']   = 'PDCPagination';
			
            if (!empty($project_id) && !empty($project_id)):
				$where['project_id'] = $project_id;
            endif;
            
			if (!empty($personnel_id) && !empty($personnel_id)):
				$where['personnel_id'] = $personnel_id;
            endif;

			if (!empty($collection_date) && !empty($collection_date)):
				$where['collection_date'] = $collection_date;
            endif;

            $this->view_data['total'] = $totalRec = count($data);

			// Pagination configuration
			$config['total_rows'] = $totalRec;

			// Initialize pagination library
			$this->ajax_pagination->initialize($config);

			if (!empty($project_id) && !empty($project_id)):
				$where['project_id'] = $project_id;
            endif;
            
			if (!empty($personnel_id) && !empty($personnel_id)):
				$where['personnel_id'] = $personnel_id;
            endif;

            if (!empty($collection_date) && !empty($collection_date)):
				$where['collection_date'] = $collection_date;
            endif;
            

            if($collection_date == "for_clearing_today") {
                
                $this->view_data['records'] = $records =  $this->M_pdc->for_clearing_today($this->perPage, $offset, $where);

            } else if($collection_date == "overdue_checks") {

                $this->view_data['records'] = $records = $this->M_pdc->overdue_checks($this->perPage, $offset, $where);

            } else if($collection_date == "upcoming_clearing") {
                
                $this->view_data['records'] = $records = $this->M_pdc->upcoming_clearing($this->perPage, $offset, $where);

            } else {

                $this->view_data['records'] = $records = $this->M_pdc->for_clearing_today($this->perPage, $offset, $where);

            }
            
            $this->view_data['table'] = "pdc";

			$this->load->view('account_manager/_filter', $this->view_data, false);
        }
    }

    public function get_all_am()
    {
        $data = $this->M_staff->as_array()->get_all();
        $output = array(
            'data' => $data,
        );
        echo json_encode($output);
    }

    public function get_all_projects()
    {
        $data = $this->M_project->as_array()->get_all();
        $output = array(
            'data' => $data,
        );
        echo json_encode($output);
    }

   

}
