<div class="row">
    <div class="col-lg-12">
        <h4 id="collection_title">Overdue Collections</h4> 
    </div>
    <div class="col-lg-12">
        <div class="card-body">
            <div class="form-group form-group-xs row">
                <table
                    class="table table-striped- table-bordered table-hover"
                >
                    <thead>
                        <tr>
                            <th>Client</th>
                            <th>Type</th>
                            <th>Amount</th>
                            <th>Penalty</th>
                            <th>Overdue By</th>
                            <th>Action;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records as $key => $r): ?>
                            <?php
                                $buyer = get_person($r['buyer_id'], 'buyers');
                                $buyer_name = get_fname($buyer);

                                $property = get_person($r['property_id'], 'properties');
                                $property_name = get_name($property);

                                $days = date_diffs(NOW, $r['tp_due_date']);

                                $payment_status = payment_stats($days);

                                $t = $r['total_amount'];

                                $this->transaction_library->initiate($r['id']);

                                $penalty_amount = $this->transaction_library->get_penalty($r['tp_due_date'], 0);

                            ?>
                            <tr>
                                <td>
                                    <?=$buyer_name?>
                                    <br>
                                    <?=$property_name;?>
                                    <br>
                                    <a href="<?php echo base_url("transaction/view/" . $r['id'])?>" target="_blank" rel="noopener noreferrer">
                                        <?=$r['reference'];?>
                                    </a>
                                </td>
                                <td><?=$r['particulars']?></td>
                                <td><?=money_php($r['total_amount'])?></td>
                                <td><?=money_php($penalty_amount)?></td>
                                <td>
                                    <?=view_date($r['tp_due_date'])?>
                                    <br>
                                    <?=$payment_status?>
                                </td>
                                <td>
                                    <a href="<?php echo base_url("transaction_payment/form/" . $r['id'])?>" target="_blank" rel="noopener noreferrer" class="btn btn-label-primary btn-elevate btn-sm">
                                        Collect
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>               
    </div>
</div>

<div class="row">
    <div class="col-xl-12">

        <!--begin:: Components/Pagination/Default-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">

                <!--begin: Pagination-->
                <div class="kt-pagination kt-pagination--brand">
                    <?php echo $this->ajax_pagination->create_links(); ?>

                    <div class="kt-pagination__toolbar">
                        <span class="pagination__desc">
                            <?php echo $this->ajax_pagination->show_count(); ?>
                        </span>
                    </div>
                </div>

                <!--end: Pagination-->
            </div>
        </div>

        <!--end:: Components/Pagination/Default-->
    </div>
</div>