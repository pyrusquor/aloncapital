<div class="row">
    <div class="col-lg-12">
        <h4 id="collection_title">Post Dated Checks</h4> 
    </div>
    <div class="col-lg-12">
        <div class="card-body">
            <div class="form-group form-group-xs row">
                <table
                    class="table table-striped- table-bordered table-hover"
                >
                    <thead>
                        <tr>
                            <th>Client</th>
                            <th>Period</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Due Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records as $key => $value): ?>
                            <?php
                                $buyer = get_person($value['buyer_id'], 'buyers');
                                $buyer_name = get_fname($buyer);

                                $transaction_reference = $value['transaction_reference'];

                                $property_name = $value['property_name'];

                                $period = Dropdown::get_static('period_names', $value['transaction_period_id']);

                                $amount = money_php($value['pdc_amount']);

                                $status = Dropdown::get_static('pdc_status', $value['pdc_status']);

                                $due_date = view_date($value['pdc_due_date']);

                                $days = date_diffs(NOW, $value['pdc_due_date']);

                                $payment_status = payment_stats($days);
                            ?>
                            <tr>
                                <td>
                                    <?= $buyer_name ?>
                                    <br>
                                    <?= $property_name ?>
                                    <br>
                                    <a href="<?php echo base_url("transaction/view/" . $value['transaction_id'])?>" target="_blank" rel="noopener noreferrer">
                                        <?= $transaction_reference ?>
                                    </a>
                                </td>
                                <td><?= $period ?></td>
                                <td><?= $amount ?></td>
                                <td><?= $status ?></td>
                                <td>
                                    <?= $due_date ?>
                                    <br>
                                    <?=$payment_status?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>               
    </div>
</div>

<div class="row">
    <div class="col-xl-12">

        <!--begin:: Components/Pagination/Default-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">

                <!--begin: Pagination-->
                <div class="kt-pagination kt-pagination--brand">
                    <?php echo $this->ajax_pagination->create_links(); ?>

                    <div class="kt-pagination__toolbar">
                        <span class="pagination__desc">
                            <?php echo $this->ajax_pagination->show_count(); ?>
                        </span>
                    </div>
                </div>

                <!--end: Pagination-->
            </div>
        </div>

        <!--end:: Components/Pagination/Default-->
    </div>
</div>