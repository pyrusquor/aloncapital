<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Account manager
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <div class="row">
                    <div class="col-md-6">
                        <div class="kt-input-icon kt-input-icon--left">
                            <select
                                class="form-control"
                                id="project_id"
                                name="project_id"
                            >
                                <option value="">Select Projects</option>
                            </select>
                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                <span><i class="flaticon2-search-1"></i></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="kt-input-icon kt-input-icon--left">
                            <select
                                class="form-control"
                                id="personnel_id"
                                name="personnel_id"
                            >
                                <option value="">Select AM</option>
                            </select>
                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                <span><i class="flaticon2-search-1"></i></span>
                            </span>
                        </div>
                    </div>
                    <input type="text" id="collection_date" class="d-none">
                </div>
                
            </div>
        </div>
    </div>
</div>
<!-- Batch Upload -->
<div id="_batch_upload" class="collapse kt-margin-b-35 kt-margin-t-10">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <form class="kt-form" id="_export_csv" action="<?php echo site_url('account_manager/export_csv'); ?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">File Type</label>
                            <div class="kt-input-icon  kt-input-icon--left">
                                <select class="form-control form-control-sm" name="update_existing_data">
                                    <option value=""> -- Update Existing Data -- </option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-cloud-upload"></i></span></span>
                            </div>
                            <?php echo form_error('update_existing_data'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <form class="kt-form" id="_upload_form" action="<?php echo site_url('account_manager/import'); ?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Upload CSV file:</label>
                            <label class="form-control-label text-muted">Note: Maximum of 1,000 items only per file.</label>
                            <input type="file" name="csv_file" class="" size="1000" accept="*.csv">
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group form-group-last row custom_import_style">
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-sm" form="_upload_form">
                                <i class="fa fa-upload"></i> Upload
                            </button>
                        </div>
                        <div class="col-lg-6 kt-align-right">
                            <!-- <a href="<?php echo site_url('document/export') ?>" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm">
								<i class="fa fa-file-csv"></i>
							</a> -->
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" form="_export_csv">
                                <i class="fa fa-file-csv"></i>
                            </button>
                            <button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" data-toggle="modal" data-target="#upload_guide">
                                <i class="fa fa-info-circle"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--  Batch Upload Guide -->
<div class="modal fade" id="upload_guide" tabindex="-1" role="dialog" aria-labelledby="upload_guide_label" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="upload_guide_label">Transaction Payment Upload Guide</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-head-bg-brand table-striped table-hover table-bordered table-condensed table-checkable" id="_upload_guide_modal">
                    <thead>
                        <tr class="text-center">
                            <th width="1%" scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Type</th>
                            <th scope="col">Format</th>
                            <th scope="col">Option</th>
                            <th scope="col">Required</th>
                        </tr> 
                    </thead>
                    <?php if (isset($_fillables) && $_fillables) : ?>

                        <tbody>
                            <?php foreach ($_fillables as $_fkey => $_fill) : ?>

                                <?php
                                $_no                =    isset($_fill['no']) && $_fill['no'] ? $_fill['no'] : '';
                                $_name            =    isset($_fill['name']) && $_fill['name'] ? $_fill['name'] : '';
                                $_type            =    isset($_fill['type']) && $_fill['type'] ? $_fill['type'] : '';
                                $_required    =    isset($_fill['required']) && $_fill['required'] ? $_fill['required'] : '';
                                $_dropdown    =    isset($_fill['dropdown']) && $_fill['dropdown'] && !empty($_fill['dropdown']) ? $_fill['dropdown'] : '';
                                ?>

                                <tr>
                                    <td><?php echo @$_no; ?></td>
                                    <td class="_ug_name"><?php echo @$_name; ?></td>
                                    <td><?php echo @$_type; ?></td>
                                    <td>
                                        <?php if ($_name === 'birth_date') : ?>
                                            yyyy-mm-dd (e.g. 2020-01-28)
                                        <?php endif; ?>
                                    </td>
                                    <td class="_ug_option">
                                        <?php if ($_dropdown) : ?>

                                            <ul class="_ul">
                                                <?php foreach ($_dropdown as $_dkey => $_drop) : ?>

                                                    <li><?php echo @$_drop; ?></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
                                        <!-- <ul class="_ul">
									    <li>1 = Legal Staff</li>
											<li>2 = Legal Head</li>
											<li>3 = Agent</li>
									   </ul> -->
                                    </td>
                                    <td>
                                        <?php if ($_required === 'Yes') : ?>

                                            <span class="kt-font-danger">Yes</span>
                                        <?php elseif ($_required === 'No') : ?>

                                            No
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php endif; ?>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-elevate btn-outline-hover-brand btn-sm btn-icon-sm" data-dismiss="modal">
                    <i class="la la-close"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid kt-grid__item kt-grid__item--fluid" id="accountManagerContent">
    <?php $this->load->view('_filter'); ?>
</div>

<!-- EXPORT -->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_export_option_label">Export Options</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" id="_export_form" target="_blank" action="<?php echo site_url('account_manager/export'); ?>" method="POST">
                    <div class="row">
                        <?php if (isset($_columns) && $_columns) : ?>

                            <div class="col-lg-12">
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox kt-checkbox--bold">
                                        <input type="checkbox" id="_export_select_all"> Field
                                        <span></span>
                                    </label>
                                    <label class="kt-checkbox kt-checkbox--bold"></label>
                                </div>
                            </div>

                            <?php foreach ($_columns as $key => $_column) : ?>

                                <?php if ($_column) : ?>

                                    <?php
                                    $_offset    =    '';
                                    if ($_column === reset($_columns)) {

                                        $_offset    =    'offset-lg-1';
                                    }
                                    ?>

                                    <div class="col-lg-6">
                                        <div class="kt-checkbox-list">
                                            <?php foreach ($_column as $_ckey => $_clm) : ?>

                                                <?php
                                                $_label    =    isset($_clm['label']) && $_clm['label'] ? $_clm['label'] : '';
                                                $_value    =    isset($_clm['value']) && $_clm['value'] ? $_clm['value'] : '';
                                                ?>

                                                <label class="kt-checkbox kt-checkbox--bold">
                                                    <input type="checkbox" name="_export_column[]" class="_export_column" value="<?php echo @$_value; ?>"> <?php echo @$_label; ?>
                                                    <span></span>
                                                </label>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        <?php else : ?>

                            <div class="col-lg-10 offset-lg-1">
                                <div class="form-group form-group-last">
                                    <div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            Something went wrong. Please contact your system administrator.
                                        </div>
                                        <div class="alert-close">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true"><i class="la la-close"></i></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <button type="submit" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm btn-font-sm pull-right" form="_export_form">
                        <i class="fa fa-file-export"></i> Export
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- FILTER MODAL -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="advanceSearch">
                    
                    <div class="row">
                        <div class="form-group col-md-6">
                             <label class="form-control-label">Project Name:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="projects" id="project_id" name="project_id">
                                <option value="">Select Project</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Paid Date:</label>
                            <input type="text" class="form-control" readonly name="daterange" id="kt_collection_daterangepicker">
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Payment Type:</label>
                            <?php echo form_dropdown('payment_type_id', Dropdown::get_static('payment_types'), set_value('payment_type_ud', ''), 'class="form-control" id="payment_type_id"'); ?>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Period Type:</label>
                            <?php echo form_dropdown('period_type_id', Dropdown::get_static('period_names'), set_value('period_type_id', ''), 'class="form-control" id="period_type_id"'); ?>
                        </div>
                    </div>

                     <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Reference:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="transactions" id="reference" name="reference" data-select="reference">
                                <option value="">Select Reference</option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Buyer:</label>
                            <select class="form-control suggests_modal" data-type="person" style="width: 100%" data-module="buyers" id="buyer_id" name="buyer_id">
                                <option value="">Select Buyer</option>
                            </select>
                        </div>
                     </div>

                     <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Property:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="properties" id="property_id" name="property_id">
                                <option value="">Select Property</option>
                            </select>
                        </div>
                     </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="apply_filter" form="advanceSearch">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>