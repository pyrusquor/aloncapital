<?php

// vdebug($records);
$total_due_today = 0;
$total_overdue_collections = 0;
$total_upcoming_collections = 0;

$due_today = $this->M_Account_manager->total_due_today();
$overdue_collections = $this->M_Account_manager->total_overdue_collections();
$upcoming_collections = $this->M_Account_manager->total_upcoming_collections();

$pdc_for_clearing_today = $this->M_pdc->total_for_clearing_today();
$pdc_overdue_checks = $this->M_pdc->total_overdue_checks();
$pdc_upcoming_clearing = $this->M_pdc->total_upcoming_clearing();
?>

<div class="row" id="kt-content">
    <div class="kt-portlet col-sm-12">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12 mb-4">
                            <div class="card card-custom gutter-b">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h5 class="card-label">Collection Tasks</h5>        
                                    </div>
                                </div>
                                <div class="card-body">
                                    <p>
                                        <a
                                            href="javascript:void(0)"
                                            class="grid"
                                            id="due_today"
                                        >
                                            <span>Due Today</span>
                                            <span>(<?=count($due_today);?>)</span>
                                            <span>
                                                <?php foreach ($due_today as $key => $r): ?>
                                                <?php $total_due_today += $r['total_amount']?>
                                                <?php endforeach; ?>
                                                <?=money_php($total_due_today)?>
                                            </span>
                                            <span>
                                                <i class="flaticon2-right-arrow"></i>
                                            </span>
                                        </a>
                                    </p>
                                    <p>
                                        <a
                                            href="javascript:void(0)"
                                            class="grid"
                                            id="overdue_collections"
                                        >
                                            <span>Overdue Collections</span>
                                            <span>(<?=count($overdue_collections);?>)</span>
                                            <span>
                                                <?php foreach ($overdue_collections as $key => $r): ?>
                                                <?php $total_overdue_collections += $r['total_amount']?>
                                                <?php endforeach; ?>
                                                <?=money_php($total_overdue_collections)?>
                                            </span>
                                            <span>
                                                <i class="flaticon2-right-arrow"></i>
                                            </span>
                                        </a>
                                    </p>
                                    <p>
                                        <a
                                            href="javascript:void(0)"
                                            class="grid"
                                            id="upcoming_collections"
                                        >
                                            <span>Upcoming Collections</span>
                                            <span>(<?=count($upcoming_collections);?>)</span>
                                            <span>
                                                <?php foreach ($upcoming_collections as $key => $r): ?>
                                                <?php $total_upcoming_collections += $r['total_amount']?>
                                                <?php endforeach; ?>
                                                <?=money_php($total_upcoming_collections)?>
                                            </span>
                                            <span>
                                                <i class="flaticon2-right-arrow"></i>
                                            </span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card card-custom gutter-b">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h5 class="card-label">Post Dated Checks Tasks</h5>        
                                    </div>
                                </div>
                                <div class="card-body">
                                    <p>
                                        <a
                                            href="javascript:void(0)"
                                            class="grid"
                                            id="for_clearing_today"
                                        >
                                            <span>For Clearing Today</span>
                                            <span>(<?= $pdc_for_clearing_today['count'] ?>)</span>
                                            <span>
                                                <?= money_php($pdc_for_clearing_today['amount']) ?>
                                            </span>
                                            <span>
                                                <i class="flaticon2-right-arrow"></i>
                                            </span>
                                        </a>
                                    </p>
                                    <p>
                                        <a
                                            href="javascript:void(0)"
                                            class="grid"
                                            id="overdue_checks"
                                        >
                                            <span>Overdue Checks</span>
                                            <span>(<?= $pdc_overdue_checks['count'] ?>)</span>
                                            <span>
                                                <?= money_php($pdc_overdue_checks['amount']) ?>
                                            </span>
                                            <span>
                                                <i class="flaticon2-right-arrow"></i>
                                            </span>
                                        </a>
                                    </p>
                                    <p>
                                        <a
                                            href="javascript:void(0)"
                                            class="grid"
                                            id="upcoming_clearing"
                                        >
                                            <span>Upcoming Clearing</span>
                                            <span>(<?= $pdc_upcoming_clearing['count'] ?>)</span>
                                            <span>
                                                <?= money_php($pdc_upcoming_clearing['amount']) ?>
                                            </span>
                                            <span>
                                                <i class="flaticon2-right-arrow"></i>
                                            </span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <?php if ($table == "pdc"): ?>
                        <?php $this->load->view('_pdc_table'); ?>
                    <?php else: ?>
                        <?php $this->load->view('_overdue_table'); ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
