<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Item_model extends MY_Model {
    public $table = 'transaction_personnel_logs'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
        'transaction_id', 
        'personnel_id', 
        'assigned_at', 
        'is_active', 
        'created_by',
		'created_at',
		'updated_by',
		'updated_at'
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;


		// $this->has_one['transaction'] = array('foreign_model'=>'transaction/Transaction_model','foreign_table'=>'transactions','foreign_key'=>'id','local_key'=>'transaction_id');


		// $this->has_one['bank'] = array('foreign_model'=>'banks/Banks_model','foreign_table'=>'banks','foreign_key'=>'id','local_key'=>'bank_id');

	}

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

   

}