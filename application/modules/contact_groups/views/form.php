<?php
// ==================== begin: Add model fields ====================
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';

$name = isset($info['name']) && $info['name'] ? $info['name'] : '';

$code = isset($info['code']) && $info['code'] ? $info['code'] : '';

$is_active = isset($info['is_active']) && $info['is_active'] ? $info['is_active'] : '2';
// ==================== end: Add model fields ====================
?>
<!--begin::Form-->
<form method="POST" class="kt-form kt-form--label-right" id="form_contact_groups" enctype="multipart/form-data" action="<?php form_open('contact_groups/form/' . @$info['id']); ?>">
    <!-- CONTENT HEADER -->
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title"><?= $method ?> Contact Groups</h3>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <button type="submit" class="btn btn-label-success btn-elevate btn-sm" form="form_contacts" data-ktwizard-type="action-submit">
                        <i class="fa fa-plus-circle"></i> Submit
                    </button>
                    <a href="<?php echo site_url('contact_groups'); ?>" class="btn btn-label-instagram btn-elevate btn-sm">
                        <i class="fa fa-reply"></i> Cancel
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- CONTENT -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class=" kt-portlet__body">
                    <div class="row">
                        <!-- ==================== begin: Add form model fields ==================== -->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="">Name <span class="kt-font-danger">*</span></label>
                                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off" id="name" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="">Code <span class="kt-font-danger"></span></label>
                                <input type="text" class="form-control" name="code" value="<?php echo set_value('code', $code); ?>" placeholder="Code" autocomplete="off" id="code">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="">Status <span class="kt-font-danger">*</span></label>
                                <!-- <input type="text" class="form-control" name="is_active" value="<?php echo set_value('is_active', $is_active); ?>" placeholder="Status" autocomplete="off" id="is_active" required> -->
                                <?php echo form_dropdown('is_active', Dropdown::get_static("contact_group_status"), $is_active, 'class="form-control" required') ?>
                            </div>
                        </div>
                        <!-- ==================== end: Add form model fields ==================== -->
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- begin:: Footer -->
</form>
<!--end::Form-->