<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Contact_group_model extends MY_Model
{
    public $table = 'contact_groups'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'name',
        'code',
        'is_active',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by',
        'deleted_by',
        'deleted_at',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];
    public $fields = [
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'code',
            'label' => 'Code',
            'rules' => 'trim',
        ),
        array(
            'field' => 'is_active',
            'label' => 'Is Active',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = TRUE;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert']    =    $this->fields;
        $this->rules['update']    =    $this->fields;

        $this->has_many_pivot['contacts'] = array(
            'foreign_model' => 'contacts/Contact_model',
            'pivot_table' => 'contact_groups_contacts',
            'local_key' => 'id',
            'pivot_local_key' => 'contact_group_id',
            'pivot_foreign_key' => 'contact_id',
            'foreign_key' => 'id',
            'get_relate' => FALSE
        );
    }

    function get_columns()
    {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }
}
