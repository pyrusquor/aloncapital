<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Alerts extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $transaction_models = array('transaction/Transaction_billing_model' => 'M_transaction_billing', 'transaction/Transaction_client_model' => 'M_transaction_client', 'transaction/Transaction_discount_model' => 'M_transaction_discount', 'transaction/Transaction_fee_model' => 'M_transaction_fee', 'transaction/Transaction_payment_model' => 'M_transaction_payment', 'transaction/Transaction_property_model' => 'M_transaction_property', 'transaction/Transaction_seller_model' => 'M_transaction_seller', 'transaction_commission/Transaction_commission_model' => 'M_Transaction_commission', 'ticketing/Ticketing_model' => 'M_Transaction_ticketing', 'transaction_post_dated_check/Transaction_post_dated_check_model' => 'M_transaction_post_dated_check', 'transaction_payment/Transaction_official_payment_model' => 'M_Transaction_official_payment', 'seller/Seller_model' => 'M_seller', 'buyer/Buyer_model' => 'M_buyer', 'transaction/Transaction_billing_logs_model' => 'M_Tbilling_logs', 'transaction_payment/Transaction_penalty_logs_model' => 'M_tpenalty_logs', 'transaction/Transaction_refund_logs_model' => 'M_refund_logs');

        // Load models
        $this->load->model('transaction/Transaction_model', 'M_transaction');
        $this->load->model('property/Property_model', 'M_property');
        $this->load->model('form_generator/Form_generator_model', 'M_printable');
        $this->load->model('project/Project_model', 'M_project');
        $this->load->model('fees/Fee_model', 'M_fees');
        $this->load->model('promos/Promo_model', 'M_promo');
        $this->load->model('financing_scheme/Financing_scheme_model', 'M_scheme');
        $this->load->model('financing_scheme/Financing_scheme_commission_values_model', 'M_scheme_commission');
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model('transaction/Transaction_model', 'M_transactions');
        $this->load->model('transaction_post_dated_check/Transaction_post_dated_check_model', 'M_transaction_post_dated_check');
        $this->load->model($transaction_models);

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper

        // Per page limit
        $this->perPage = 20;
    }

    public function payments_1()
    {
        $this->view_data['totalRec'] = $totalRec = $this->M_transaction_payment->count_rows();

        // Pagination configuration
        $config['target'] = '#propertyContent';
        $config['base_url'] = base_url('alerts/paginationData');
        $config['total_rows'] = $totalRec;
        $config['per_page'] = $this->perPage;
        $config['link_func'] = 'Pagination';

        // Initialize pagination library
        $this->ajax_pagination->initialize($config);

        $where['is_paid'] = 1;
        $where['due_date <'] = date('Y-m-d');
        $this->db->where($where);
        $this->view_data['records'] = $this->M_transaction_payment->with_transaction()->limit($this->perPage, 0)->get_all();

        $this->template->build('payments/index', $this->view_data);
    }


    public function accounts()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        // $db_columns = $this->M_Transaction_official_payment->get_columns();
        // if ($db_columns) {
        //     $column = [];
        //     foreach ($db_columns as $key => $dbclm) {
        //         $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

        //         if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
        //             if (strpos($name, '_id') !== false) {
        //                 $column = $name;
        //                 $column[$key]['value'] = $column;
        //                 $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             } elseif (strpos($name, 'is_') !== false) {
        //                 $column = $name;
        //                 $column[$key]['value'] = $column;
        //                 $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             } else {
        //                 $column[$key]['value'] = $name;
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             }
        //         } else {
        //             continue;
        //         }
        //     }

        //     $column_count = count($column);
        //     $cceil = ceil(($column_count / 2));

        //     $this->view_data['columns'] = array_chunk($column, $cceil);
        //     $column_group = $this->M_Transaction_official_payment->count_rows();
        //     if ($column_group) {
        //         $this->view_data['total'] = $column_group;
        //     }
        // }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('accounts/index', $this->view_data);
    }

    public function showDueAccounts()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'transaction' => true,
            'transaction_id' => true,
            'buyer_id' => true,
            'project_id' => true,
            'property_id' => true,
            'total_amount_due' => true,
            'days_delayed' => true,
            'no_of_mos' => true,
            'last_date' => true,
            'period' => true,
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        // $transaction_payment = $this->M_Transaction_official_payment->with_transaction()->as_array()->get_all();

        // $where['is_paid'] = 0;
        // $where['due_date <'] = date('Y-m-d 23:59:59');
        // $this->db->where($where);
        // $transaction_payment = $this->M_transaction_payment->with_transaction()->limit($this->perPage, 0)->as_array()->get_all();

        $where['collection_status'] = 3;
        $this->db->where($where);
        $transactions = $this->M_transactions->as_array()->get_all();

        $data = [];

        if ($transactions) {

            foreach ($transactions as $key => $value) {

                // if (empty($value['transaction'])) {
                //     continue;
                // }
                $reference = $value['reference'];
                $transaction_id = $value['id'];
                $property_id = $value['property_id'];
                $project_id = $value['project_id'];

                $this->transaction_library->initiate($transaction_id);
                $payment_status = $this->transaction_library->payment_status(1);
                $payment_status = $payment_status['days'];
                $due_date = $this->transaction_library->due_date();
                $amount_due = $this->transaction_library->total_amount_due();
                $no_of_mos = $this->transaction_library->total_amount_due(4);

                $transactions[$key]['total_amount_due'] = money_php($amount_due);
                $transactions[$key]['days_delayed'] = $payment_status . ' day(s) overdue';
                $transactions[$key]['no_of_mos'] = $no_of_mos;
                $transactions[$key]['last_date'] = view_date($due_date);
                $transactions[$key]['period'] = Dropdown::get_static('period_names', $value['period_id'], 'view');

                // Buyer
                $buyer_id = $value['buyer_id'];
                $buyer = get_person(@$value['buyer_id'], "buyers");
                $transactions[$key]['buyer_id'] = $buyer_name = get_fname($buyer);


                // Project Name
                $project_name = isset($value['project_id']) && $value['project_id'] ? get_value_field($value['project_id'], 'projects', 'name') : '';
                $transactions[$key]['project_id'] = $project_name;

                // Property Number
                $property_name = isset($value['property_id']) && $value['property_id'] ? get_value_field($value['property_id'], 'properties', 'name') : '';
                $transactions[$key]['property_id'] = $property_name;

                $transactions[$key]['transaction'] = "<a target='_BLANK' href='" . base_url() . "transaction/view/" . $transaction_id . "'>" . $reference . "</a>";
                $transactions[$key]['property_id'] = "<a target='_BLANK' href='" . base_url() . "property/view/" . $property_id . "'>" . $property_name . "</a>";
                $transactions[$key]['project_id'] = "<a target='_BLANK' href='" . base_url() . "project/view/" . $project_id . "'>" . $project_name . "</a>";
                $transactions[$key]['buyer_id'] = "<a target='_BLANK' href='" . base_url() . "buyer/view/" . $buyer_id . "'>" . $buyer_name . "</a>";
            }


            foreach ($transactions as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                $_column = $_REQUEST['order'][0]['column'] - 1;
                $_dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($x, $y) use ($_column, $_dir) {

                    // echo "<pre>";
                    // print_r($x) ;echo "<br>";
                    // echo $_column;echo "<br>";

                    $x = array_slice($x, $_column, 1);

                    // vdebug($x);

                    $x = array_pop($x);

                    $y = array_slice($y, $_column, 1);
                    $y = array_pop($y);

                    if ($_dir === 'asc') {

                        return $x > $y ? true : false;
                    } else {

                        return $x < $y ? true : false;
                    }
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    public function showDueAccountItems()
    {
        $columnsDefault = [
            'id' => true,
            'reference' => true,
            'transaction_id' => true,
            'buyer_id' => true,
            'project_id' => true,
            'property_id' => true,
            'total_amount_due' => true,
            'days_delayed' => true,
            'no_of_mos' => true,
            'last_date' => true,
            'period' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_transactions
                ->with_project('fields:name')
                ->with_property('fields:name')
                ->with_buyer('fields:first_name,middle_name,last_name')
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->like('id', $searchValue, 'both');
                $query->or_like('reference', $searchValue, 'both');
            }

            $advanceSearchValues = [
                'reference' => [
                    'data' => $filters['reference'] ?? null,
                    'operator' => 'like',
                ],
                'project_id' => [
                    'data' => $filters['project_id'] ?? null,
                    'operator' => '=',
                ],
                'property_id' => [
                    'data' => $filters['property_id'] ?? null,
                    'operator' => '=',
                ],
                'buyer_id' => [
                    'data' => $filters['buyer_id'] ?? null,
                    'operator' => '=',
                ],
                'period_id' => [
                    'data' => $filters['period_id'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $this->transaction_library->initiate($value['id']);
                        $payment_status = $this->transaction_library->payment_status(1);
                        $payment_status = $payment_status['days'];
                        $due_date = $this->transaction_library->due_date();
                        $amount_due = $this->transaction_library->total_amount_due();
                        $no_of_mos = $this->transaction_library->total_amount_due(4);

                        $items[$key]['total_amount_due'] = money_php($amount_due);
                        $items[$key]['days_delayed'] = $payment_status . ' day(s) overdue';
                        $items[$key]['no_of_mos'] = $no_of_mos;
                        $items[$key]['last_date'] = view_date($due_date);
                        $items[$key]['period'] = Dropdown::get_static('period_names', $value['period_id'], 'view');

                        $items[$key]['reference'] = isset($value['reference']) ? "<a target='_BLANK' href='" . base_url() . "transaction/view/" . $value['id'] . "'>" . $value['reference'] . "</a>" : '';

                        $items[$key]['project_id'] = isset($value['project']['name']) ? "<a target='_BLANK' href='" . base_url() . "project/view/" . $value['project']['id'] . "'>" . $value['project']['name'] . "</a>" : '';

                        $items[$key]['property_id'] = isset($value['property']['name']) ? "<a target='_BLANK' href='" . base_url() . "property/view/" . $value['property']['id'] . "'>" . $value['property']['name'] . "</a>" : '';

                        $items[$key]['buyer_id'] = isset($value['buyer']) ? "<a target='_BLANK' href='" . base_url() . "buyer/view/" . $value['buyer']['id'] . "'>" . get_fname($value['buyer']) . "</a>" : '';

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_transactions->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function payments()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Transaction_official_payment->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Transaction_official_payment->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        // Tiles data
        $this->db->select_sum('total_amount')
            ->select('COUNT(id) as count')
            ->from('transaction_payments')
            ->where('is_paid', 1)
            ->where('due_date', date('Y-m-d 00:00:00'))
            ->where('deleted_at', NULL);

        $query = $this->db->get()->row_array();

        $this->view_data['paid'] = $query;

        $this->db->select_sum('total_amount')
            ->select('COUNT(id) as count')
            ->from('transaction_payments')
            ->where('is_paid', 0)
            ->where('due_date', date('Y-m-d 00:00:00'))
            ->where('deleted_at', NULL);

        $query = $this->db->get()->row_array();

        $this->view_data['unpaid'] = $query;

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('payments/index', $this->view_data);
    }

    public function showDuePayments()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'transaction' => true,
            'transaction_id' => true,
            'buyer_id' => true,
            'project_id' => true,
            'property_id' => true,
            'period' => true,
            'amount' => true,
            'principal' => true,
            'interest' => true,
            'next_payment_date' => true,
            'particulars' => true,
            'status' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }
        $project_id = $this->input->post('project_id');

        // get all raw data
        // $where['due_date'] = date('Y-m-d 00:00:00');
        // $this->db->where($where);
        // $transaction_payments = $this->M_transaction_payment->with_transaction()->as_array()->get_all();
        $transaction_payments = $this->M_transaction_payment->get_due_payments_today($project_id);

        $data = [];

        if ($transaction_payments) {

            foreach ($transaction_payments as $key => $value) {

                // if (empty($value['transaction'])) {
                //     continue;
                // }
                $reference = $value['reference'];
                $transaction_id = $value['transaction_id'];
                $property_id = $value['property_id'];
                $project_id = $value['project_id'];
                $buyer_id = $value['buyer_id'];



                // <th>Amount</th>
                // <th>Days Delayed</th>
                // <th>Due Date</th>
                // <th>Period Type</th>
                // <th>Particulars</th>

                $transaction_payments[$key]['period'] =  Dropdown::get_static('period_names', $value['period_id'], 'view');
                $transaction_payments[$key]['amount'] = money_php($value['total_amount']);
                $transaction_payments[$key]['principal'] = money_php($value['principal_amount']);
                $transaction_payments[$key]['interest'] = money_php($value['interest_amount']);
                $transaction_payments[$key]['next_payment_date'] = view_date($value['next_payment_date']);
                $transaction_payments[$key]['particulars'] = ($value['particulars']);
                $transaction_payments[$key]['status'] = ($value['is_paid']) ? "Paid" : "Unpaid";

                // Buyer
                $buyer = get_person(@$buyer_id, "buyers");
                $transaction_payments[$key]['buyer_id'] = $buyer_name = get_fname($buyer);

                // Project Name
                $project_name = isset($project_id) && $project_id ? get_value_field($project_id, 'projects', 'name') : '';
                $transaction_payments[$key]['project_id'] = $project_name;

                // Property Number
                $property_name = isset($property_id) && $property_id ? get_value_field($property_id, 'properties', 'name') : '';
                $transaction_payments[$key]['property_id'] = $property_name;

                $transaction_payments[$key]['transaction'] = "<a target='_BLANK' href='" . base_url() . "transaction/view/" . $transaction_id . "'>" . $reference . "</a>";
                $transaction_payments[$key]['property_id'] = "<a target='_BLANK' href='" . base_url() . "property/view/" . $property_id . "'>" . $property_name . "</a>";
                $transaction_payments[$key]['project_id'] = "<a target='_BLANK' href='" . base_url() . "project/view/" . $project_id . "'>" . $project_name . "</a>";
                // $transaction_payments[$key]['project_id'] =$project_name;
                $transaction_payments[$key]['buyer_id'] = "<a target='_BLANK' href='" . base_url() . "buyer/view/" . $buyer_id . "'>" . $buyer_name . "</a>";

                $transaction_payments[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                $transaction_payments[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
            }

            foreach ($transaction_payments as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                $_column = $_REQUEST['order'][0]['column'] - 1;
                $_dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($x, $y) use ($_column, $_dir) {

                    // echo "<pre>";
                    // print_r($x) ;echo "<br>";
                    // echo $_column;echo "<br>";

                    $x = array_slice($x, $_column, 1);

                    // vdebug($x);

                    $x = array_pop($x);

                    $y = array_slice($y, $_column, 1);
                    $y = array_pop($y);

                    if ($_dir === 'asc') {

                        return $x > $y ? true : false;
                    } else {

                        return $x < $y ? true : false;
                    }
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    public function post_dated_checks()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Transaction_official_payment->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Transaction_official_payment->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('post_dated_checks/index', $this->view_data);
    }

    public function showPostDatedChecks()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'transaction' => true,
            'transaction_id' => true,
            'buyer_id' => true,
            'project_id' => true,
            'property_id' => true,
            'bank' => true,
            'number' => true,
            'amount' => true,
            'status' => true,
            'period' => true,
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        $project_id = $this->input->post('project_id');

        // get all raw data
        // $where['due_date'] = date('Y-m-d 00:00:00');
        // $this->db->where($where);
        // $post_dated_checks = $this->M_transaction_post_dated_check->with_transaction()->as_array()->get_all();
        $post_dated_checks = $this->M_transaction_post_dated_check->get_due_payments_today($project_id);

        $data = [];

        if ($post_dated_checks) {

            foreach ($post_dated_checks as $key => $value) {

                // if (empty($value['transaction'])) {
                //     continue;
                // }

                $reference = $value['reference'];
                $transaction_id = $value['transaction_id'];
                $property_id = $value['property_id'];
                $project_id = $value['project_id'];
                $bank_id = $value['bank_id'];

                // <th>Bank</th>
                // <th>Cheque Number</th>
                // <th>Cheque Amount</th>
                // <th>Status</th>
                // <th>Period</th>
                $transaction_payments[$key]['bank'] = isset($project_id) && $project_id ? get_value_field($project_id, 'projects', 'name') : '';
                $transaction_payments[$key]['number'] =  $value['number'];
                $transaction_payments[$key]['amount'] =  money_php($value['amount']);
                $transaction_payments[$key]['status'] =  Dropdown::get_static('pdc_status', $value['pdc_status'], 'view');
                $transaction_payments[$key]['period'] =  Dropdown::get_static('period_names', $transaction['period_id'], 'view');


                // Buyer
                $buyer_id = $transaction['buyer_id'];
                $buyer = get_person(@$buyer_id, "buyers");
                $transaction_payments[$key]['buyer_id'] = $buyer_name = get_fname($buyer);

                // Project Name
                $project_name = isset($project_id) && $project_id ? get_value_field($project_id, 'projects', 'name') : '';
                $transaction_payments[$key]['project_id'] = $project_name;

                // Property Number
                $property_name = isset($property_id) && $property_id ? get_value_field($property_id, 'properties', 'name') : '';
                $transaction_payments[$key]['property_id'] = $property_name;

                $transaction_payments[$key]['transaction'] = "<a target='_BLANK' href='" . base_url() . "transaction/view/" . $transaction_id . "'>" . $reference . "</a>";
                $transaction_payments[$key]['property_id'] = "<a target='_BLANK' href='" . base_url() . "property/view/" . $property_id . "'>" . $property_name . "</a>";
                $transaction_payments[$key]['project_id'] = "<a target='_BLANK' href='" . base_url() . "project/view/" . $project_id . "'>" . $project_name . "</a>";
                $transaction_payments[$key]['buyer_id'] = "<a target='_BLANK' href='" . base_url() . "buyer/view/" . $buyer_id . "'>" . $buyer_name . "</a>";
            }

            foreach ($transaction_payments as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                $_column = $_REQUEST['order'][0]['column'] - 1;
                $_dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($x, $y) use ($_column, $_dir) {

                    // echo "<pre>";
                    // print_r($x) ;echo "<br>";
                    // echo $_column;echo "<br>";

                    $x = array_slice($x, $_column, 1);

                    // vdebug($x);

                    $x = array_pop($x);

                    $y = array_slice($y, $_column, 1);
                    $y = array_pop($y);

                    if ($_dir === 'asc') {

                        return $x > $y ? true : false;
                    } else {

                        return $x < $y ? true : false;
                    }
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    public function showPostDatedCheckItems()
    {
        $columnsDefault = [
            'id' => true,
            'reference' => true,
            'project_id' => true,
            'property_id' => true,
            'file_name' => true,
            'number_of_days' => true,
            'period_id' => true,
            'due_date' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_transactions
                ->with_project('fields:name')
                ->with_property('fields:name')

                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->like('id', $searchValue, 'both');
                $query->or_like('reference', $searchValue, 'both');
            }

            $advanceSearchValues = [
                'reference' => [
                    'data' => $filters['reference'] ?? null,
                    'operator' => 'like',
                ],
                'project_id' => [
                    'data' => $filters['project_id'] ?? null,
                    'operator' => '=',
                ],
                'property_id' => [
                    'data' => $filters['property_id'] ?? null,
                    'operator' => '=',
                ],
                'period_id' => [
                    'data' => $filters['period_id'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $items[$key]['reference'] = isset($value['reference']) ? "<a target='_BLANK' href='" . base_url() . "transaction/view/" . $value['id'] . "'>" . $value['reference'] . "</a>" : '';

                        $items[$key]['project_id'] = isset($value['project']['name']) ? "<a target='_BLANK' href='" . base_url() . "project/view/" . $value['project']['id'] . "'>" . $value['project']['name'] . "</a>" : '';

                        $items[$key]['property_id'] = isset($value['property']['name']) ? "<a target='_BLANK' href='" . base_url() . "property/view/" . $value['property']['id'] . "'>" . $value['property']['name'] . "</a>" : '';

                        $items[$key]['period_id'] = Dropdown::get_static('period_names', $value['period_id'], 'view');
                        $items[$key]['due_date'] = view_date($value['due_date']);

                        $items[$key]['file_name'] = '';
                        $items[$key]['number_of_days'] = '';

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_transactions->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function documents()
    {
        // $this->view_data['records'] = $this->M_transaction->with_project()->with_property()->get_all();
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('documents/index', $this->view_data);
    }

    public function showDocumentItems()
    {
        $columnsDefault = [
            'id' => true,
            'reference' => true,
            'project_id' => true,
            'property_id' => true,
            'file_name' => true,
            'number_of_days' => true,
            'period_id' => true,
            'due_date' => true,
            'created_at' => true,
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_transactions
                ->with_project('fields:name')
                ->with_property('fields:name')

                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->like('id', $searchValue, 'both');
                $query->or_like('reference', $searchValue, 'both');
            }

            $advanceSearchValues = [
                'reference' => [
                    'data' => $filters['reference'] ?? null,
                    'operator' => 'like',
                ],
                'project_id' => [
                    'data' => $filters['project_id'] ?? null,
                    'operator' => '=',
                ],
                'property_id' => [
                    'data' => $filters['property_id'] ?? null,
                    'operator' => '=',
                ],
                'period_id' => [
                    'data' => $filters['period_id'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $items[$key]['reference'] = isset($value['reference']) ? "<a target='_BLANK' href='" . base_url() . "transaction/view/" . $value['id'] . "'>" . $value['reference'] . "</a>" : '';

                        $items[$key]['project_id'] = isset($value['project']['name']) ? "<a target='_BLANK' href='" . base_url() . "project/view/" . $value['project']['id'] . "'>" . $value['project']['name'] . "</a>" : '';

                        $items[$key]['property_id'] = isset($value['property']['name']) ? "<a target='_BLANK' href='" . base_url() . "property/view/" . $value['property']['id'] . "'>" . $value['property']['name'] . "</a>" : '';

                        $items[$key]['period_id'] = Dropdown::get_static('period_names', $value['period_id'], 'view');
                        $items[$key]['due_date'] = view_date($value['due_date']);
                        $items[$key]['created_at'] = view_date($value['created_at']);

                        $items[$key]['file_name'] = '';
                        $items[$key]['number_of_days'] = '';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_transactions->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function post_dated_checks_()
    {
        $this->view_data['totalRec'] = $totalRec = $this->M_transaction_post_dated_check->count_rows();

        // Pagination configuration
        // $config['target'] = '#propertyContent';
        // $config['base_url'] = base_url('alerts/paginationData');
        // $config['total_rows'] = $totalRec;
        // $config['per_page'] = $this->perPage;
        // $config['link_func'] = 'PropertyPagination';

        // Initialize pagination library
        // $this->ajax_pagination->initialize($config);

        $today = date("Y-m-d");

        $this->view_data['records'] = $records = $this->M_transaction_post_dated_check->with_transaction()->get_all();

        // Due Post Dated Checks
        $due_records = [];
        if (!empty($records)) {
            foreach ($records as $r) {
                if ($today >= $r['due_date']) {
                    array_push($due_records, $r);
                }
            }
        }

        // Bounced Post Dated Checks
        $bounced_records = [];
        if (!empty($records)) {
            foreach ($records as $r) {
                if ($r['pdc_status'] == 4) {
                    array_push($bounced_records, $r);
                }
            }
        }

        // Cleared Post Dated Checks
        $cleared_records = [];
        if (!empty($records)) {
            foreach ($records as $r) {
                if ($r['pdc_status'] == 3) {
                    array_push($cleared_records, $r);
                }
            }
        }

        // vdebug($due_records);

        $this->view_data['due_records'] = $due_records;
        $this->view_data['bounced_records'] = $bounced_records;
        $this->view_data['cleared_records'] = $cleared_records;

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('post_dated_checks/index', $this->view_data);
    }

    public function paginationData()
    {
        if ($this->input->is_ajax_request()) {

            // Input from General Search
            $keyword = $this->input->post('keyword');

            // Input from Advanced Filter
            $project_id = $this->input->post('project_id');

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $totalRec = $this->M_property->count_rows();
            $where = array();

            // Pagination configuration
            $config['target'] = '#propertyContent';
            $config['base_url'] = base_url('alerts/paginationData');
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $config['link_func'] = 'Pagination';

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('reference', $keyword, 'both');
                $this->db->group_end();
            endif;

            // if (!empty($project_id) && !empty($project_id)):
            //     $this->db->where('project_id', $project_id);
            // endif;

            $totalRec = $this->M_property->count_rows();

            // Pagination configuration
            $config['total_rows'] = $totalRec;

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('name', $keyword, 'both');
                $this->db->or_like('block', $keyword, 'both');
                $this->db->or_like('lot', $keyword, 'both');
                $this->db->group_end();
            endif;

            // if (!empty($project_id) && !empty($project_id)):
            //     $this->db->where('project_id', $project_id);
            // endif;

            $this->view_data['records'] = $records = $this->M_transaction_payment->with_transaction()->limit($this->perPage, $offset)->get_all();

            $this->load->view('alerts/payments/_filter_table', $this->view_data, false);
        }
    }
}
