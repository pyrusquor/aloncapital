<?php if (!empty($due_records)): ?>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <div class="row">
            <h3 class="mb-3">Due Post Dated Checks</h3>
            <?php $this->load->view('_due_post_dated_table');?>
        </div>
    </div>
</div>


<?php else: ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Due Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found.
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>

<?php if (!empty($bounced_records)): ?>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <div class="row">
            <h3 class="mb-3">Bounced Post Dated Checks</h3>
            <?php $this->load->view('_bounced_post_dated_table');?>
        </div>
    </div>
</div>
<?php else: ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Bounced Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found.
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>

<?php if (!empty($cleared_records)): ?>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <div class="row">
            <h3 class="mb-3">Cleared Post Dated Checks</h3>
            <?php $this->load->view('_cleared_post_dated_table');?>
        </div>
    </div>
</div>
<?php else: ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Cleared Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found.
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>