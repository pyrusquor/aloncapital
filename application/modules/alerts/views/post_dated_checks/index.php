<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Alerts (Due Post Dated Checks for the day)
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc"><?php echo (!empty($totalRec)) ? $totalRec : 0; ?> TOTAL</span>
                <form class="kt-margin-l-20" id="kt_subheader_search_form">
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                        <input type="text" class="form-control" placeholder="Search Payment..." id="generalSearch">
                        <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                                        <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" id="Path-2" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                        <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" id="Path" fill="#000000" fill-rule="nonzero"></path>
                                    </g>
                                </svg>

                                <!--<i class="flaticon2-search-1"></i>-->
                            </span>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="module__filter">
                <button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm btn-filter" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
                    <i class="fa fa-filter"></i> Filter
                </button>
            </div>
        </div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">

        <!--begin: Advance Search -->
        <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form">
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label class="form-control-label">Transaction ID:</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="text" name="company_address" class="form-control form-control-sm _filter" placeholder="Or Number" id="_column_3" data-column="3">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-marker"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Project:</label>
                                <select class="form-control suggests" data-module="projects" id="alerts_project_id">
                                    <option value="">ALL PROJECTS</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Buyer:</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="text" name="company_contact_info" class="form-control form-control-sm _filter" placeholder="Buyer" id="_column_4" data-column="4">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-phone"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Receipt Number:</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="text" name="company_address" class="form-control form-control-sm _filter" placeholder="Or Number" id="_column_8" data-column="8">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-marker"></i></span></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form">
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label class="form-control-label">Receipt Date:</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="text" name="company_contact_info" class="form-control form-control-sm _filter" placeholder="OR Date" id="_column_9" data-column="9">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-phone"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Period:</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <?php echo form_dropdown('period_names', Dropdown::get_static('period_names'), '', 'class="form-control form-control-sm _filter" id="_column_10" data-column="10"'); ?>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-building-o"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Payment Date:</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="text" name="company_address" class="form-control form-control-sm _filter" placeholder="Payee" id="_column_11" data-column="11">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-marker"></i></span></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form">
                        <div class="form-group row">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end: Advance Search -->

        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="transaction_payment_table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Transaction</th>
                    <th>Project</th>
                    <th>Property</th>
                    <th>Buyer</th>
                    <th>Bank</th>
                    <th>Cheque Number</th>
                    <th>Cheque Amount</th>
                    <th>Status</th>
                    <th>Period</th>
                    <th>Created By</th>
                    <th>Last Update By</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable -->

    </div>
</div>



<!-- FILTER MODAL -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="advanceSearch">

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Project Name:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="projects" id="project_id" name="project_id">
                                <option value="">Select Project</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Paid Date:</label>
                            <input type="text" class="form-control" readonly name="daterange" id="kt_collection_daterangepicker">
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Payment Type:</label>
                            <?php echo form_dropdown('payment_type_id', Dropdown::get_static('payment_types'), set_value('payment_type_ud', ''), 'class="form-control" id="payment_type_id"'); ?>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Period Type:</label>
                            <?php echo form_dropdown('period_type_id', Dropdown::get_static('period_names'), set_value('period_type_id', ''), 'class="form-control" id="period_type_id"'); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Reference:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="transactions" id="reference" name="reference" data-select="reference">
                                <option value="">Select Reference</option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Buyer:</label>
                            <select class="form-control suggests_modal" data-type="person" style="width: 100%" data-module="buyers" id="buyer_id" name="buyer_id">
                                <option value="">Select Buyer</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Property:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="properties" id="property_id" name="property_id">
                                <option value="">Select Property</option>
                            </select>
                        </div>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="apply_filter" form="advanceSearch">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>