<div class="col-md-12 col-sm-12">
    <div class="row" id="propertyList">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead class="thead-dark">
                    <tr>
                    <th>Reference</th>
                        <th>Property</th>
                        <th>Buyer</th>
                        <th>Bank</th>
                        <th>Cheque Number</th>
                        <th>Cheque Amount</th>
                        <th>Due Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($bounced_records)):
?>
                    <?php foreach ($bounced_records as $r):

    $reference = isset($r['transaction']['reference']) && $r['transaction']['reference'] ? $r['transaction']['reference'] : '';

    $property_name = isset($r['transaction']['property_id']) && $r['transaction']['property_id'] ? get_value_field($r['transaction']['property_id'], 'properties', 'name') : '';

    $buyer_name = isset($r['transaction']['client_id']) && $r['transaction']['client_id'] ? get_person_name(get_person($r['transaction']['client_id'], 'buyers')) : '';

    $due_date = isset($r['due_date']) && $r['due_date'] ? $r['due_date'] : '';
    $amount = isset($r['amount']) && $r['amount'] ? $r['amount'] : '';
    $unique_number = isset($r['unique_number']) && $r['unique_number'] ? $r['unique_number'] : '';
    $bank_name = isset($r['bank_id']) && $r['bank_id'] ? get_value_field($r['bank_id'], 'banks', 'name') : '';
    ?>

	                    <tr>
	                        <td><?=$reference;?></td>
	                        <td><?=$property_name;?></td>
	                        <td><?=$buyer_name;?></td>
	                        <td><?=$bank_name;?></td>
	                        <td><?=$unique_number;?></td>
	                        <td><?=money_php($amount);?></td>
	                        <td><?=view_date($due_date);?></td>
	                    </tr>
	                    <?php endforeach;?>
                    <?php endif?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- <div class="row"> -->
    <!-- <div class="col-xl-12"> -->
    <!--begin:: Components/Pagination/Default-->
    <!-- <div class="kt-portlet"> -->
    <!-- <div class="kt-portlet__body"> -->
    <!--begin: Pagination-->
    <!-- <div class="kt-pagination kt-pagination--brand">
                    <?php echo $this->ajax_pagination->create_links(); ?>

<div class="kt-pagination__toolbar">
    <span class="pagination__desc">
        <?php echo $this->ajax_pagination->show_count();
?>
    </span> -->
    <!-- </div> -->
    <!-- </div> -->

    <!--end: Pagination-->
    <!-- </div> -->
    <!-- </div> -->

    <!--end:: Components/Pagination/Default-->
    <!-- </div> -->
    <!-- </div> -->
</div>
