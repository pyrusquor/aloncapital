<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="row" id="propertyList">
                    <div class="col-md-12">

                        <!--begin: Advance Search -->
                        <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form class="kt-form" id="advance_search">
                                        <div class="form-group row">
                                            <div class="col-sm-4">
                                                <label class="form-control-label">Reference:</label>
                                                <input type="text" name="reference" class="form-control _filter" placeholder="Reference">
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="form-control-label">Project:</label>
                                                <select name="project_id" class="form-control _filter suggests" placeholder="Project" data-module="projects">
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="form-control-label">Property:</label>
                                                <select name="property_id" class="form-control _filter suggests" placeholder="Property" data-module="properties">
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="form-control-label">Period:</label>
                                                <?php echo form_dropdown('period_id', Dropdown::get_static('period_names'), '', 'class="form-control _filter"'); ?>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- end: Advance Search -->

                        <table class="table table-striped- table-bordered table-hover table-checkable" id="alert_documents_table">
                            <thead>
                                <tr>
                                    <td>ID</td>
                                    <td>Reference</td>
                                    <td>Project</td>
                                    <td>Property</td>
                                    <td>File Name</td>
                                    <td># of Days</td>
                                    <td>Period Stage</td>
                                    <td>Due Date</td>
                                    <td>Date Created</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-12">

                        <!--begin:: Components/Pagination/Default-->
                        <div class="kt-portlet">
                            <div class="kt-portlet__body">

                                <!--begin: Pagination-->
                                <div class="kt-pagination kt-pagination--brand">
                                    <?php echo $this->ajax_pagination->create_links(); ?>

                                    <div class="kt-pagination__toolbar">
                                        <span class="pagination__desc">
                                            <?php echo $this->ajax_pagination->show_count(); ?>
                                        </span>
                                    </div>
                                </div>

                                <!--end: Pagination-->
                            </div>
                        </div>

                        <!--end:: Components/Pagination/Default-->
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- <div class="row">
    <div class="col-lg-12">
        <div class="kt-portlet kt-callout">
            <div class="kt-portlet__body">
                <div class="kt-callout__body">
                    <div class="kt-callout__content">
                        <h3 class="kt-callout__title">No Records Found</h3>
                        <p class="kt-callout__desc">
                            Sorry no record were found.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->