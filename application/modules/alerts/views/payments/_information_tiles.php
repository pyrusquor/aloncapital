<div class="row">
    <div class="col-sm col-12">
        <div class="kt-portlet kt-portlet--fluid kt-portlet--solid-success">
            <div class="kt-portlet__head text-white">
                <div class="kt-portlet__head-label">
                    <div class="kt-portlet__head-titles d-flex" style="align-items: center;">
                        <h4 class="mb-0">
                            PAID
                        </h4>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body text-white">
                <span class="d-block font-weight-bold mb-7 text-dark-50">
                    <h1><?= money_php($paid['total_amount']) ?></h1>
                </span>
                <span class="kt-widget20__desc">
                    <h5 class="mb-0"><?= number_format($paid['count']) ?> Transaction<?= $paid['count'] > 1 ? 's' : '' ?></h5>
                </span>
            </div>
        </div>
    </div>
    <div class="col-sm col-12">
        <div class="kt-portlet kt-portlet--fluid kt-portlet--solid-danger">
            <div class="kt-portlet__head text-white">
                <div class="kt-portlet__head-label">
                    <div class="kt-portlet__head-titles d-flex" style="align-items: center;">
                        <h4 class="mb-0">
                            UNPAID
                        </h4>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body text-white">
                <span class="d-block font-weight-bold mb-7 text-dark-50">
                    <h1><?= money_php($unpaid['total_amount']) ?></h1>
                </span>
                <span class="kt-widget20__desc">
                    <h5 class="mb-0"><?= number_format($unpaid['count']) ?> Transaction<?= $unpaid['count'] > 1 ? 's' : '' ?></h5>
                </span>
            </div>
        </div>
    </div>
</div>