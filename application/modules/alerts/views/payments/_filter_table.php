<?php if (!empty($records)) : ?>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="row" id="propertyList">
                        <div class="col-md-12">
                            <table class="table table-striped table-sm">
                                    <thead class="thead-dark ">
                                        <tr>
                                            <th>ID</th>
                                            <th>Reference</th>
                                            <th>Project</th>
                                            <th>Property</th>
                                            <th>Client</th>
                                            <th>Amount</th>
                                            <th>Days Delayed</th>
                                            <th>Due Date</th>
                                            <th>Period Type</th>
                                            <th>Particulars</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($records as $r) :
                                            // vdebug($records);
                                            $id = isset($r['id']) && $r['id'] ? $r['id'] : '';

                                            $reference = isset($r['transaction']['reference']) && $r['transaction']['reference'] ? $r['transaction']['reference'] : '';

                                            $project_name = isset($r['transaction']['project_id']) && $r['transaction']['project_id'] ? get_value_field($r['transaction']['project_id'],'projects','name') : '';

                                            $property_name = isset($r['transaction']['property_id']) && $r['transaction']['property_id'] ? get_value_field($r['transaction']['property_id'],'properties','name') : '';

                                            // $client_name = isset($r['transaction']['buyer_id']) && $r['transaction']['buyer_id'] ? get_person_name(get_person($r['transaction']['buyer_id'],'buyers')) : '';

                                            $client = get_person($r['transaction']['buyer_id'],'buyers');
                                            $client_name = get_fname($client);


                                            $due_date = isset($r['due_date']) && $r['due_date'] ? $r['due_date'] : '';
                                            $period_type = isset($r['period_id']) && $r['period_id'] ? $r['period_id'] : '';

                                            $days = isset($r['transaction']['property_id']) && $r['transaction']['property_id'] ? $r['transaction']['property_id'] : '';
                                            $amount = isset($r['total_amount']) && $r['total_amount'] ? $r['total_amount'] : '';

                                            $particulars = isset($r['particulars']) && $r['particulars'] ? $r['particulars'] : '';
                                        ?>
                                        <tr>
                                            <td><?=$id; ?></td>
                                            <td>
                                                <a href="<?php echo base_url("transaction/view/" . $r['transaction']['id']) ?>"><?=$reference; ?></a>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url("project/view/" . $r['transaction']['project_id'])?>"><?=$project_name; ?></a>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url("property/view/" . $r['transaction']['property_id'])?>"><?=$property_name; ?></a>
                                            </td>
                                            <td>
                                            <a href="<?php echo base_url("buyer/view/" . $r['transaction']['buyer_id'])?>"><?=$client_name; ?></a>
                                            </td>
                                            <td><?=money_php($amount); ?></td>
                                            <td><?=date_diffs(NOW,$due_date); ?></td>
                                            <td><?=view_date($due_date); ?></td>
                                            <td><?=Dropdown::get_static('period_names',$period_type,'view'); ?></td>
                                            <td><?=$particulars; ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-12">

                            <!--begin:: Components/Pagination/Default-->
                            <div class="kt-portlet">
                                <div class="kt-portlet__body">

                                    <!--begin: Pagination-->
                                    <div class="kt-pagination kt-pagination--brand">
                                        <?php echo $this->ajax_pagination->create_links(); ?>

                                        <div class="kt-pagination__toolbar">
                                            <span class="pagination__desc">
                                                <?php echo $this->ajax_pagination->show_count(); ?>
                                            </span>
                                        </div>
                                    </div>

                                    <!--end: Pagination-->
                                </div>
                            </div>

                            <!--end:: Components/Pagination/Default-->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
<?php else : ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found.
                            </p>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>