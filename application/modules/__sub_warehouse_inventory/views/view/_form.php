<?php
$id = isset($sub_warehouse_inventory['id']) && $sub_warehouse_inventory['id'] ? $sub_warehouse_inventory['id'] : '';
$company = isset($sub_warehouse_inventory['company']) && $sub_warehouse_inventory['company'] ? $sub_warehouse_inventory['company'] : '';
$sbu = isset($sub_warehouse_inventory['sbu']) && $sub_warehouse_inventory['sbu'] ? $sub_warehouse_inventory['sbu'] : '';
$warehouse_id = isset($sub_warehouse_inventory['warehouse_id']) && $sub_warehouse_inventory['warehouse_id'] ? $sub_warehouse_inventory['warehouse_id'] : '';
$sub_warehouse_id = isset($sub_warehouse_inventory['sub_warehouse_id']) && $sub_warehouse_inventory['sub_warehouse_id'] ? $sub_warehouse_inventory['sub_warehouse_id'] : '';
$item_code = isset($sub_warehouse_inventory['item_code']) && $sub_warehouse_inventory['item_code'] ? $sub_warehouse_inventory['item_code'] : '';
$item_name = isset($sub_warehouse_inventory['item_name']) && $sub_warehouse_inventory['item_name'] ? $sub_warehouse_inventory['item_name'] : '';
$current_physical_count = isset($sub_warehouse_inventory['current_physical_count']) && $sub_warehouse_inventory['current_physical_count'] ? $sub_warehouse_inventory['current_physical_count'] : '';
$is_active = isset($sub_warehouse_inventory['is_active']) && $sub_warehouse_inventory['is_active'] ? $sub_warehouse_inventory['is_active'] : '';
$warehouse_name = isset($sub_warehouse_inventory['warehouse']['name']) && $sub_warehouse_inventory['warehouse']['name'] ? $sub_warehouse_inventory['warehouse']['name'] : 'N/A';
$sub_warehouse_name = isset($sub_warehouse_inventory['sub_warehouse']['name']) && $sub_warehouse_inventory['sub_warehouse']['name'] ? $sub_warehouse_inventory['sub_warehouse']['name'] : 'N/A';
?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Company <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="company" value="<?php echo set_value('company', $company); ?>" placeholder="Company" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-building"></i></span>
            </div>
            <?php echo form_error('company'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>SBU <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="sbu" value="<?php echo set_value('sbu', $sbu); ?>" placeholder="SBU" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-barcode"></i></span>
            </div>
            <?php echo form_error('sbu'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Select Warehouse<span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="warehouse"  id="warehouse_id" name="warehouse_id">
                <option value="0">Select Group</option>
                <?php if ($warehouse_name): ?>
                    <option value="<?php echo $warehouse_id; ?>" selected><?php echo $warehouse_name; ?></option>
                <?php endif?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Select Sub-warehouse<span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="sub_warehouse"  id="sub_warehouse_id" name="sub_warehouse_id">
                <option value="0">Select Group</option>
                <?php if ($sub_warehouse_name): ?>
                    <option value="<?php echo $sub_warehouse_id; ?>" selected><?php echo $sub_warehouse_name; ?></option>
                <?php endif?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Item Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="item_code" value="<?php echo set_value('item_code', $item_code); ?>" placeholder="Item Code" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-barcode"></i></span>
            </div>
            <?php echo form_error('item_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Item Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="item_name" value="<?php echo set_value('item_name', $item_name); ?>" placeholder="Item Name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-link"></i></span>
            </div>
            <?php echo form_error('item_name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Current Physical Count <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="current_physical_count" value="<?php echo set_value('current_physical_count', $current_physical_count); ?>" placeholder="Current Physical Count" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-italic"></i></span>
            </div>
            <?php echo form_error('current_physical_count'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Is Active <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
            <?php echo form_dropdown('is_active', Dropdown::get_static('warehouse_status'), set_value('is_active', @$is_active), 'class="form-control"'); ?>
            </div>
            <?php echo form_error('is_active'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== begin: Add form model fields ==================== -->

    <!-- ==================== end: Add form model fields ==================== -->
</div>