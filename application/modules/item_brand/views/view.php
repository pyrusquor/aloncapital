<?php
$id = isset($item_brand['id']) && $item_brand['id'] ? $item_brand['id'] : '';
$name = isset($item_brand['name']) && $item_brand['name'] ? $item_brand['name'] : '';
$brand_code = isset($item_brand['brand_code']) && $item_brand['brand_code'] ? $item_brand['brand_code'] : '';
$description = isset($item_brand['description']) && $item_brand['description'] ? $item_brand['description'] : '';
$is_active = isset($item_brand['is_active']) && $item_brand['is_active'] ? $item_brand['is_active'] : '0';
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Company</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('item_brand/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('item_brand');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Brand Name
							</span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
								<?php echo $name; ?>
							</span>
                                </div>
                                <div class="kt-widget13__item">
							<span class="kt-widget13__desc kt-align-right">
								Brand Code:
							</span>
                                    <span class="kt-widget13__text">
								<?php echo $brand_code; ?>
							</span>
                                </div>
                                <div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Description:
							</span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
								<?php echo $description; ?>
							</span>
                                </div>
                                <div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Status:
							</span>
                                    <span class="kt-widget13__text">
								<span class="kt-widget__data" style="font-weight: 500"><?php echo Dropdown::get_static('inventory_status', $is_active, 'view'); ?></span>

                                </div>
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Items
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">


                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- begin:: Footer -->
