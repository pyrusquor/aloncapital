<?php
$name = isset($item_brand['name']) && $item_brand['name'] ? $item_brand['name'] : '';
$brand_code = isset($item_brand['brand_code']) && $item_brand['brand_code'] ? $item_brand['brand_code'] : '';
$description = isset($item_brand['description']) && $item_brand['description'] ? $item_brand['description'] : '';
$status = isset($item_brand['is_active']) && $item_brand['is_active'] ? $item_brand['is_active'] : '';

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Brand Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="brand_code" value="<?php echo set_value('brand_code', $brand_code); ?>" placeholder="Type Code" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
            </div>
            <?php echo form_error('brand_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Description <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="description" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-align-left"></i></span>
            </div>
            <?php echo form_error('description'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Status</label>
            <div class="kt-input-icon kt-input-icon--left">

                <?php echo form_dropdown('is_active', Dropdown::get_static('inventory_status'), set_value('is_active', @$status), 'class="form-control"'); ?>
                <?php echo form_error('is_active'); ?>

       
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-check-square"></i></span>
            </div>

            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>