<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Item_brand_model extends MY_Model {
    public $table = 'item_brand'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = ['name', 'brand_code', 'description', 'is_active', 'created_by', 'updated_by', 'deleted_by'];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
        ),
        'brand_code' => array(
            'field' => 'brand_code',
            'label' => 'Brand Code',
            'rules' => 'trim'
        ),
        'description' => array(
            'name' => 'description',
            'label' => 'Description',
            'rules' => 'trim'
        ),
        'is_active' => array(
            'name' => 'is_active',
            'label' => 'Status',
            'rules' => 'trim'
        )
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
                'brand_code' => $faker->word,
                'description' => $faker->paragraph,
                'is_active' => $faker->numberBetween(0, 1)
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}