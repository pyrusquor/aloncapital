<?php
// ==================== begin: Add model fields ====================
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';

$first_name = isset($info['first_name']) && $info['first_name'] ? $info['first_name'] : '';

$middle_name = isset($info['middle_name']) && $info['middle_name'] ? $info['middle_name'] : '';

$last_name = isset($info['last_name']) && $info['last_name'] ? $info['last_name'] : '';

$present_address = isset($info['present_address']) && $info['present_address'] ? $info['present_address'] : '';

$mobile_no = isset($info['mobile_no']) && $info['mobile_no'] ? $info['mobile_no'] : '';

$email = isset($info['email']) && $info['email'] ? $info['email'] : '';

$contact_group_ids = isset($info['contact_groups']) && $info['contact_groups'] ? array_column($info['contact_groups'], 'id') : [];

// ==================== end: Add model fields ====================

?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">First Name <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="first_name" value="<?php echo set_value('first_name', $first_name); ?>" placeholder="First Name" autocomplete="off" id="first_name">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Middle Name <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="middle_name" value="<?php echo set_value('middle_name', $middle_name); ?>" placeholder="Middle Name" autocomplete="off" id="middle_name">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Last Name <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="last_name" value="<?php echo set_value('last_name', $last_name); ?>" placeholder="Last Name" autocomplete="off" id="last_name">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Present Address <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="present_address" value="<?php echo set_value('present_address', $present_address); ?>" placeholder="Present Address" autocomplete="off" id="present_address">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Mobile No. <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="mobile_no" value="<?php echo set_value('mobile_no', $mobile_no); ?>" placeholder="Mobile No." autocomplete="off" id="mobile_no">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Email <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="email" value="<?php echo set_value('email', $email); ?>" placeholder="Email" autocomplete="off" id="email">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Contact Groups <span class="kt-font-danger"></span></label>
            <?php echo form_dropdown('contact_group_ids[]', $contact_groups, set_value('contact_group_ids', $contact_group_ids), 'class="form-control kt-select2" id="contact_group_ids" name="param" multiple="multiple"'); ?>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>