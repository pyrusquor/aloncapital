<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$first_name = isset($data['first_name']) && $data['first_name'] ? $data['first_name'] : '';
$middle_name = isset($data['middle_name']) && $data['middle_name'] ? $data['middle_name'] : '';
$last_name = isset($data['last_name']) && $data['last_name'] ? $data['last_name'] : '';
$present_address = isset($data['present_address']) && $data['present_address'] ? $data['present_address'] : 'N/A';
$mobile_no = isset($data['mobile_no']) && $data['mobile_no'] ? $data['mobile_no'] : 'N/A';
$email = isset($data['email']) && $data['email'] ? $data['email'] : 'N/A';
$name = $first_name . " " . $last_name; 
// ==================== begin: Add model fields ====================

// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
  <div class="kt-container kt-container--fluid">
    <div class="kt-subheader__main">
      <h3 class="kt-subheader__title">Contact Information</h3>
    </div>
    <div class="kt-subheader__toolbar">
      <div class="kt-subheader__wrapper">
        <a href="<?php echo site_url('contacts/form/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
          <i class="fa fa-edit"></i> Edit Info
        </a>   
        <a href="<?php echo site_url('contacts');?>" class="btn btn-label-instagram btn-sm btn-elevate">
          <i class="fa fa-reply"></i> Back
        </a>
      </div>
    </div>
  </div>
</div>

<!-- begin:: Content -->
<div
  class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid"
>
  <div class="row">
    <div class="col-md-6">
      <!--begin::Portlet-->
      <div class="kt-portlet">
        <div class="kt-portlet__body">
          <!--begin::Portlet-->
          <div class="kt-portlet">
            <div class="kt-portlet__head">
              <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                  General Information
                </h3>
              </div>
            </div>
            <div class="kt-portlet__body">
              <!--begin::Form-->
              <div class="kt-widget13">
                <div class="kt-widget13__item">
                  <span class="kt-widget13__desc">
                    Name
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$name?></span>
                </div>
                <div class="kt-widget13__item">
                  <span class="kt-widget13__desc">
                    Address
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$present_address?></span>
                </div>
                <div class="kt-widget13__item">
                  <span class="kt-widget13__desc">
                    Mobile No.
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$mobile_no?></span>
                </div>
                <div class="kt-widget13__item">
                  <span class="kt-widget13__desc">
                    Email
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$email?></span>
                </div>
              </div>
              <!--end::Form-->
            </div>
          </div>
          <!--end::Portlet-->
        </div>
      </div>
      <!--end::Portlet-->
    </div>

    <div class="col-md-6"></div>
  </div>
</div>
<!-- begin:: Footer -->
