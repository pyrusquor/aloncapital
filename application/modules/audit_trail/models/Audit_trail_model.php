<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Audit_trail_model extends MY_Model {
    public $table = 'audit_trail'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'affected_id',
        'affected_table',
        'action',
        'previous_record',
        'new_record',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        /* ==================== begin: Add model fields ==================== */
        'affected_id' => array(
            'field' => 'affected_id',
            'label' => 'Affected ID',
            'rules' => 'trim|required'
        ),
        'affected_table' => array(
            'field' => 'affected_table',
            'label' => 'Affected Table',
            'rules' => 'trim|required'
        ),
        'action' => array(
            'field' => 'action',
            'label' => 'Action',
            'rules' => 'trim|required'
        ),
        'previous_record' => array(
            'field' => 'previous_record',
            'label' => 'Previous Record',
            'rules' => 'trim'
        ),
        'new_record' => array(
            'field' => 'new_record',
            'label' => 'New Record',
            'rules' => 'trim|required'
        ),
        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        // $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['user'] = array('foreign_model' => 'User_model', 'foreign_table' => 'users', 'foreign_key' => 'id', 'local_key' => 'created_by');
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}