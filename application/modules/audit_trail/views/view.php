<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$affected_id = isset($data['affected_id']) && $data['affected_id'] ? $data['affected_id'] : '';
$affected_table = isset($data['affected_table']) && $data['affected_table'] ? $data['affected_table'] : '';
$action = isset($data['action']) && $data['action'] ? $data['action'] : '';
$previous_record = isset($data['previous_record']) && $data['previous_record'] ? $data['previous_record'] : '';
$new_record = isset($data['new_record']) && $data['new_record'] ? $data['new_record'] : '';
$created_by = isset($data['created_by']) && $data['created_by'] ? $data['created_by'] : '';
$user = isset($data['user']['last_name']) && $data['user']['last_name'] ? $data['user']['last_name'] : '';
// ==================== begin: Add model fields ====================

// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Audit Trail</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('audit_trail/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('audit_trail');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <!-- ==================== begin: Add fields details  ==================== -->
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Action
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $action; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Affected ID
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $affected_id; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Previous Record
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $previous_record; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        New Record
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $new_record; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Created By
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $user; ?></span>
                                </div>
                                <!-- ==================== end: Add model details ==================== -->
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
