<?php if ($audit_trail):
    foreach ($audit_trail as $key => $log): $fullname = @$log['user']['first_name'].' '.@$log['user']['last_name']; ?>
        <a href="javascript: void(0)" class="kt-notification-v2__item">
            <div class="kt-notification-v2__item-icon">
                <i class="<?=$log['icon']?> kt-font-<?=$log['extra_tag']?>"></i>
            </div>
            <div class="kt-notification-v2__itek-wrapper">
                <div class="kt-notification-v2__item-title">
                    <?=$fullname;?>
                </div>
                <div class="kt-notification-v2__item-text">
                    <?=$log['remarks'];?>
                </div>
                <div class="kt-notification-v2__item-desc">
                    <?=view_hrdate($log['created_at']);?>
                </div>
            </div>
        </a>
    <?php endforeach ?>
<?php endif ?>