<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Commissions extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('commissions/Commissions_model', 'M_Commissions');
        $this->load->model('commission_setup_values/Commission_setup_values_model', 'M_Commission_setup_values');
        $this->load->model('warehouse/Warehouse_model', 'M_Warehouse');
        $this->_table_fillables = $this->M_Commissions->fillable;
        $this->_table_columns = $this->M_Commissions->__get_columns();
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Commissions->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Commissions->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }

        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showCommissionss()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'name' => true,
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        $commissions = $this->M_Commissions->with_commission_setup_values()->as_array()->get_all();

        $data = [];


        if ($commissions) {

            foreach ($commissions as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                $_column = $_REQUEST['order'][0]['column'] - 1;
                $_dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($x, $y) use ($_column, $_dir) {

                    // echo "<pre>";
                    // print_r($x) ;echo "<br>";
                    // echo $_column;echo "<br>";

                    $x = array_slice($x, $_column, 1);

                    // vdebug($x);

                    $x = array_pop($x);

                    $y = array_slice($y, $_column, 1);
                    $y = array_pop($y);

                    if ($_dir === 'asc') {

                        return $x > $y ? true : false;
                    } else {

                        return $x < $y ? true : false;
                    }
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );

        }

        echo json_encode($output);
        exit();
    }

    public function create()
    {
        if ($this->input->post()) {
            $_input = $this->input->post();

            $additional = [
                'is_active' => 1,
                'created_by' => $this->user->id,
                'created_at' => NOW,
            ];

            $info = $_input;
            $commissions['name'] =  $info['name'];
            $commissions['wht_percentage'] = $info['wht_percentage'];
            $commission_values = $_input['commission_values'];

            // $result = $this->M_Commissions->from_form()->insert($_input);
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
            $commission_id = $this->M_Commissions->from_form()->insert($commissions); #lqq(); ud($_insert);

            $result = $this->M_Commissions->get($commission_id);
            
            if ($commission_values) {

                foreach ($commission_values['period_id'] as $key => $period_id) {
                    $c_values['commission_id'] = $result['id'];
                    $c_values['period_id'] = $period_id;
                    $c_values['percentage_to_finish'] = cln($commission_values['percentage_to_finish'][$period_id]);
                    $c_values['commission_rate_amount'] = cln($commission_values['commission_rate_amount'][$period_id]);
                    $c_values['commission_rate_percentage'] = cln($commission_values['commission_rate_percentage'][$period_id]);
                    $c_values['commission_term'] = cln($commission_values['commission_term'][$period_id]);
                    $c_values['commission_vat'] = cln($info['wht_percentage']);

                    $this->M_Commission_setup_values->insert($c_values + $additional);

                }

            }

            $this->db->trans_complete(); # Completing transaction

            /*Optional*/
            if ($this->db->trans_status() === false) {
                # Something went wrong.
                $this->db->trans_rollback();
                $this->notify->error('Oh snap! Please refresh the page and try again.');
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();
                $this->notify->success('Commission successfully created.', 'commissions');
            }
        }

        $this->template->build('create');
    }

    public function update($_id = false, $type = "")
    {

        if ($_id) {

            $_Commissions = $this->M_Commissions->with_commission_setup_values()->get($_id);

            $_input = $this->input->post();

            if ($type == "debug") {

                vdebug($_Commissions);

            }

            if ($_Commissions) {

                $this->view_data['_Commissions'] = $_Commissions;

                if ($this->input->post()) {

                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];
                    $info['name'] = $_input['name'];
                    $info['wht_percentage'] = $_input['wht_percentage'];

                    $commission_values = $_input['commission_values'];
                    // vdebug($_input);

                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

                    // $_updated    =    $this->M_Commissions->update($_update, $_id);

                    $this->M_Commissions->update($info + $additional, $_id); #lqq(); ud($_insert);


                    if ($commission_values) {

                        foreach ($commission_values['period_id'] as $key => $period_id) {

                            $c_id = cln($commission_values[$period_id]);

                            $c_values['commission_id'] = $_id;
                            $c_values['period_id'] = $period_id;
                            $c_values['percentage_to_finish'] = cln($commission_values['percentage_to_finish'][$period_id]);
                            $c_values['commission_rate_amount'] = cln($commission_values['commission_rate_amount'][$period_id]);
                            $c_values['commission_rate_percentage'] = cln($commission_values['commission_rate_percentage'][$period_id]);
                            $c_values['commission_term'] = cln($commission_values['commission_term'][$period_id]);
                            $c_values['commission_vat'] = cln($info['wht_percentage']);
                            $this->M_Commission_setup_values->update($c_values, $c_id); #lqq(); ud($_insert);

                        }

                    }

                    $this->db->trans_complete(); # Completing transaction

                    if ($_updated !== false) {

                        $this->notify->success('Commission successfully updated.', 'commissions');
                    } else {

                        $this->notify->error('Oh snap! Please refresh the page and try again.');
                    }
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Commissions->get($id);
            if ($list) {

                $deleted = $this->M_Commissions->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Class Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Commissions->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Type Sucessfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = false)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['commissions'] = $this->M_Commissions->with_commission_setup_values()->get($id);

            if ($this->view_data['commissions']) {
                

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;

        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        'commissions_name' => $upload_data[1],
                        /* ==================== begin: Add model fields ==================== */
                        'commissions_code' => $upload_data[2],
                        'warehouse_id' => $upload_data[3],
                        'address' => $upload_data[4],
                        'telephone_number' => $upload_data[5],
                        'fax_number' => $upload_data[6],
                        'mobile_number' => $upload_data[7],
                        'email_address' => $upload_data[8],
                        'contact_person' => $upload_data[9],
                        'is_active' => $upload_data[10],
                        /* ==================== end: Add model fields ==================== */
                    );

                    $commissions_id = $upload_data[0];
                    $commissions = $this->M_Commissions->get($commissions_id);

                    if ($commissions) {
                        $result = $this->M_Commissions->update($fields, $commissions_id);
                    }

                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            'commissions_name' => $upload_data[0],
                            /* ==================== begin: Add model fields ==================== */
                            'commissions_code' => $upload_data[1],
                            'warehouse_id' => $upload_data[2],
                            'address' => $upload_data[3],
                            'telephone_number' => $upload_data[4],
                            'fax_number' => $upload_data[5],
                            'mobile_number' => $upload_data[6],
                            'email_address' => $upload_data[7],
                            'contact_person' => $upload_data[8],
                            'is_active' => $upload_data[9],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Commissions->insert($fields);
                    } else {
                        $fields = array(
                            'commissions_name' => $upload_data[1],
                            /* ==================== begin: Add model fields ==================== */
                            'commissions_code' => $upload_data[2],
                            'warehouse_id' => $upload_data[3],
                            'address' => $upload_data[4],
                            'telephone_number' => $upload_data[5],
                            'fax_number' => $upload_data[6],
                            'mobile_number' => $upload_data[7],
                            'email_address' => $upload_data[8],
                            'contact_person' => $upload_data[9],
                            'is_active' => $upload_data[10],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Commissions->insert($fields);
                    }

                }
                if ($result === false) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'commissions');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'commissions');
        die();
    }

    public function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');

            $_is_update = $_ued === '1' ? true : false;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Sub Warehouse CSV Template.csv';

            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'commissions');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === false) && (strpos($_fill, 'updated_') === false) && (strpos($_fill, 'deleted_') === false)) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group = $this->M_Commissions->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title] = isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Sub Warehouse');
            $_objSheet->setCellValue('A1', 'SUB WAREHOUSE');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
            }

            $_style = array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Verdana',
                ),
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    public function export()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $commissionss = $this->M_Commissions->as_array()->get_all();
        if ($commissionss) {

            foreach ($commissionss as $_lkey => $commissions) {

                $_datas[$commissions['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_commissionss_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'commissions');
                }
            } else {

                $_filename = 'list_of_commissionss_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns    =    $this->M_land_inventory->fillable;
                $_db_columns = $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'commissions');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === false) && (strpos($_name, 'updated_') === false) && (strpos($_name, 'deleted_') === false) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== false)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                        } elseif ((strpos($_name, 'is_') !== false)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($commissionss as $_lkey => $commissions) {

                                $_datas[$commissions['id']][$_title] = isset($commissions[$_column]) && ($commissions[$_column] !== '') ? Dropdown::get_static('bool', $commissions[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($commissionss as $_lkey => $commissions) {

                                if ($_name === 'status') {

                                    $_datas[$commissions['id']][$_title] = isset($commissions[$_name]) && $commissions[$_name] ? Dropdown::get_static('inventory_status', $commissions[$_name], 'view') : '';
                                } else {

                                    $_datas[$commissions['id']][$_title] = isset($commissions[$_name]) && $commissions[$_name] ? $commissions[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Sub Warehouse');
                $_objSheet->setCellValue('A1', 'LIST OF SUB WAREHOUSE');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
                }

                $_style = array(
                    'font' => array(
                        'bold' => true,
                        'size' => 10,
                        'name' => 'Verdana',
                    ),
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'commissions');
            }
        } else {

            $this->notify->error('No Record Found', 'commissions');
        }
    }

}
