<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Commissions_model extends MY_Model
{
    public $table = 'commission_setup'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'name',
        'wht_percentage',
        'is_active',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at'
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_many['commission_setup_values'] = array('foreign_model' => 'commission_setup_values/Commission_setup_values_model', 'foreign_table' => 'commission_setup_values', 'foreign_key' => 'commission_id', 'local_key' => 'id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    // public function insert_dummy()
    // {
    //     require APPPATH . '/third_party/faker/autoload.php';
    //     $faker = Faker\Factory::create();

    //     $data = [];

    //     for ($x = 0; $x < 10; $x++) {
    //         array_push($data, array(
    //             'sub_warehouse_name' => $faker->word,
    //             'sub_warehouse_code' => $faker->word,
    //             'warehouse_id' => $faker->numberBetween(1, 10),
    //             'address' => $faker->word,
    //             'telephone_number' => $faker->numberBetween(10000000, 99999999),
    //             'fax_number' => $faker->numberBetween(10000000, 99999999),
    //             'mobile_number' => $faker->numberBetween(10000000, 99999999),
    //             'email_address' => $faker->word,
    //             'contact_person' => $faker->word,
    //             'is_active' => $faker->numberBetween(0, 1),
    //         ));
    //     }
    //     $this->db->insert_batch($this->table, $data);

    // }
}
