<?php


if(!empty($_Commissions)) {
	$commissions = $_Commissions;
	
	$commission_setup_values = $_Commissions['commission_setup_values'];
}

$id = isset($commissions['id']) && $commissions['id'] ? $commissions['id'] : '';
$name = isset($commissions['name']) && $commissions['name'] ? $commissions['name'] : '';
$wht_percentage = isset($commissions['wht_percentage']) && $commissions['wht_percentage'] ? $commissions['wht_percentage'] : '';
$commission_id = isset($commission_setup_values['commission_id']) && $commission_setup_values['commission_id'] ? $commission_setup_values['commission_id'] : '';
$period_id = isset($commission_setup_values['period_id']) && $commission_setup_values['period_id'] ? $commission_setup_values['period_id'] : '';
$percentage_to_finish = isset($commission_setup_values['percentage_to_finish']) && $commission_setup_values['percentage_to_finish'] ? $commission_setup_values['percentage_to_finish'] : '';
$commission_rate_amount = isset($commission_setup_values['commission_rate_amount']) && $commission_setup_values['commission_rate_amount'] ? $commission_setup_values['commission_rate_amount'] : '';
$commission_rate_percentage = isset($commission_setup_values['commission_rate_percentage']) && $commission_setup_values['commission_rate_percentage'] ? $commission_setup_values['commission_rate_percentage'] : '';
$commission_term = isset($commission_setup_values['commission_term']) && $commission_setup_values['commission_term'] ? $commission_setup_values['commission_term'] : '';
$commission_vat = isset($commission_setup_values['commission_vat']) && $commission_setup_values['commission_vat'] ? $commission_setup_values['commission_vat'] : '';
$is_cash_advance = isset($commission_setup_values['is_cash_advance']) && $commission_setup_values['is_cash_advance'] ? $commission_setup_values['is_cash_advance'] : '';
$is_active = isset($commission_setup_values['is_active']) && $commission_setup_values['is_active'] ? $commission_setup_values['is_active'] : '';

?>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<label>Name <span class="kt-font-danger">*</span></label>
			<div class="kt-input-icon kt-input-icon--left">
				<input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>"
					placeholder="Name" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sticky-note"></i></span></span>
			</div>
			<?php echo form_error('name'); ?>
			<span class="form-text text-muted"></span>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<label>WHT <span class="kt-font-danger">*</span></label>
			<div class="kt-input-icon kt-input-icon--left">
				<div class="input-group">
					<input type="text" class="form-control" id="wht_percentage" placeholder="Percentage Rate"
						name="wht_percentage"
						value="<?php echo @$wht_percentage; ?>">
					<div class="input-group-append"><span class="input-group-text">%</span></div>
				</div>
				<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
							class="la la-asterisk"></i></span></span>
			</div>
			<?php echo form_error('name'); ?>
			<span class="form-text text-muted"></span>
		</div>
	</div>

</div>

<div class="row">

	<div class="col-sm-12">


		<div data-repeater-list="" class="col-lg-12">
			<div data-repeater-item="" class="form-group row align-items-center">

				<div class="col-md-1">
					<div class="kt-form__group--inline">
						<div class="kt-form__label">
							<label>&nbsp;</label>
						</div>
					</div>
				</div>

				<div class="col-md-2">
					<div class="kt-form__group--inline">
						<div class="kt-form__label">
							<center><label class="kt-label m-label--single">Percentage to Finish</label></center>
						</div>

					</div>
				</div>

				<div class="col-md-2">
					<div class="kt-form__group--inline">
						<div class="kt-form__label">
							<center><label class="kt-label m-label--single">Amount to Release</label></center>
						</div>

					</div>
				</div>

				<div class="col-md-3">
					<div class="kt-form__group--inline">
						<div class="kt-form__label">
							<center><label class="kt-label m-label--single">Percentage to Release</label></center>
						</div>

					</div>
				</div>

				<div class="col-md-2">
					<div class="kt-form__group--inline">
						<div class="kt-form__label">
							<center><label class="kt-label m-label--single">Terms (Months)</label></center>
						</div>

					</div>
				</div>
			</div>
		</div>


		<?php $this->load->view('commission_values_form'); ?>

	</div>

</div>