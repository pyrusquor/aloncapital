<?php 
if(!empty($_Commissions)) {
    $commission_values = $_Commissions['commission_setup_values'];
}

	if( empty($commission_values) ) {
		for ($i=1; $i < 8; $i++) { 
			$commission_values[]['period_id'] = $i;
		}
	}




?>


<?php if ($commission_values): ?>
    <?php foreach ($commission_values as $key => $commission_value):
        

        $period_id = $commission_value['period_id'];


        if (($period_id >= 2) && ($period_id <= 6)) {
            $period_id = 2;
        }

        if ($period_id == 7) {
            # code...
            $period_id = 3;
        }


        $commission_id = @$commission_value['commission_id'];
        $id     =   isset($commission_value['id']) && $commission_value['id'] ? $commission_value['id'] : '';
        $is_active     =   isset($commission_value['is_active']) && $commission_value['is_active'] ? $commission_value['is_active'] : '';

        ?>
		<div data-repeater-list="" class="col-lg-12">
            <div data-repeater-item="" class="form-group row align-items-center">

                <div class="col-md-1">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__control">
                            <?php echo Dropdown::get_static('periods', $period_id, 'view'); ?>
                            <input type="hidden" id="period_id"  name="commission_values[period_id][<?=$period_id;?>]" value="<?php echo $period_id; ?>">
                            <input type="hidden" id="id"  name="commission_values[commission_id][<?=$period_id;?>]" value="<?php echo $commission_id; ?>"> 
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__control">

                            <div class="kt-input-icon">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="percentage_to_finish" placeholder="Percentage Rate" name="commission_values[percentage_to_finish][<?=$period_id;?>]" value="<?php echo @$commission_value['percentage_to_finish']; ?>">
                                    <div class="input-group-append"><span class="input-group-text">%</span></div>
                                </div>
                                <!-- <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span> -->
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__control">

                            <div class="kt-input-icon kt-input-icon--left">
	                            <div class="input-group">
	                                <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
	                                <input type="text" class="form-control" id="commission_rate_amount" placeholder="Period Amount" name="commission_values[commission_rate_amount][<?=$period_id;?>]" value="<?php echo @$commission_value['commission_rate_amount']; ?>">
	                            </div>
	                        </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__control">

                            <div class="kt-input-icon">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="commission_rate_percentage" placeholder="Percentage Rate" name="commission_values[commission_rate_percentage][<?=$period_id;?>]" value="<?php echo @$commission_value['commission_rate_percentage']; ?>">
                                    <div class="input-group-append"><span class="input-group-text">%</span></div>
                                </div>
                                <!-- <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span> -->
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__control">

                            <div class="kt-input-icon kt-input-icon--left">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text">Months</span></div>
                                    <input type="text" class="form-control" id="commission_term" placeholder="Months" name="commission_values[commission_term][<?=$period_id;?>]" value="<?php echo @$commission_value['commission_term']; ?>">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
	<?php endforeach ?>
<?php endif ?>