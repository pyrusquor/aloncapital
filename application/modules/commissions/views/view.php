<?php

$id = isset($commissions['id']) && $commissions['id'] ? $commissions['id'] : '';
$name = isset($commissions['name']) && $commissions['name'] ? $commissions['name'] : '';

$commission_values = $commissions['commission_setup_values'];

if( empty($commission_values) ) {
    for ($i=1; $i < 4; $i++) { 
        $commission_values[]['period_id'] = $i;
    }
}

?>


<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Commission Setup</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('commissions/update/' . $id); ?>"
                    class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('commissions'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Name:
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $name; ?></span>
                                </div>

                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        WHT:
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $commission_values[0]['commission_vat']; ?></span>
                                </div>

                            </div>
                            <!--end::Form-->
                        </div>
                        <?php if ($commission_values):  ?>
                        <?php foreach ($commission_values as $key => $commission_value):

        $period_id = $commission_value['period_id'];
        $commission_id = $commission_value['commission_id'];
        $id     =   isset($commission_value['id']) && $commission_value['id'] ? $commission_value['id'] : '';
        $is_active     =   isset($commission_value['is_active']) && $commission_value['is_active'] ? $commission_value['is_active'] : '';
        $percentage_to_finish     =   isset($commission_value['percentage_to_finish']) && $commission_value['percentage_to_finish'] ? $commission_value['percentage_to_finish'] : '';
        $commission_rate_amount     =   isset($commission_value['commission_rate_amount']) && $commission_value['commission_rate_amount'] ? $commission_value['commission_rate_amount'] : '';
        $commission_rate_percentage     =   isset($commission_value['commission_rate_percentage']) && $commission_value['commission_rate_percentage'] ? $commission_value['commission_rate_percentage'] : '';
        $commission_term     =   isset($commission_value['commission_term']) && $commission_value['commission_term'] ? $commission_value['commission_term'] : '';
        $commission_vat     =   isset($commission_value['commission_vat']) && $commission_value['commission_vat'] ? $commission_value['commission_vat'] : '';

        ?>
                        <div class="row">
                            <div class="col mb-3 d-flex flex-column align-items-center text-center border-bottom mt-3">
                                <h6 class="kt-widget13__desc pb-2">
                                    Commission
                                </h6>
                                <span
                                    class="kt-widget13__text kt-widget13__text--bold"><?php echo $commission_id; ?></span>
                            </div>
                            <div class="col mb-3 d-flex flex-column align-items-center text-center border-bottom mt-3">
                                <h6 class="kt-widget13__desc pb-2">
                                    Period
                                </h6>
                                <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $period_id; ?></span>
                            </div>
                            <div class="col mb-3 d-flex flex-column align-items-center text-center border-bottom mt-3">
                                <h6 class="kt-widget13__desc pb-2">
                                    Percentage to Finish
                                </h6>
                                <span
                                    class="kt-widget13__text kt-widget13__text--bold"><?php echo $percentage_to_finish; ?></span>
                            </div>
                            <div class="col mb-3 d-flex flex-column align-items-center text-center border-bottom mt-3">
                                <h6 class="kt-widget13__desc pb-2">
                                    Commission Rate Amount
                                </h6>
                                <span
                                    class="kt-widget13__text kt-widget13__text--bold"><?php echo $commission_rate_amount; ?></span>
                            </div>
                            <div class="col mb-3 d-flex flex-column align-items-center text-center border-bottom mt-3">
                                <h6 class="kt-widget13__desc pb-2">
                                    Commission Rate Percentage
                                </h6>
                                <span
                                    class="kt-widget13__text kt-widget13__text--bold"><?php echo $commission_rate_percentage; ?></span>
                            </div>
                            <div class="col mb-3 d-flex flex-column align-items-center text-center border-bottom mt-3">
                                <h6 class="kt-widget13__desc pb-2">
                                    Commission Term
                                </h6>
                                <span
                                    class="kt-widget13__text kt-widget13__text--bold"><?php echo $commission_term; ?></span>
                            </div>
                           
                            <div class="col mb-3 d-flex flex-column align-items-center text-center border-bottom mt-3">
                                <h6 class="kt-widget13__desc pb-2">
                                    Is Active
                                </h6>
                                <span class="kt-widget__data"
                                    style="font-weight: 500"><?php echo Dropdown::get_static('warehouse_status', $is_active, 'view'); ?></span>
                            </div>
                        </div>
                        <?php endforeach ?>
                        <?php endif ?>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>
    </div>
</div>
<!-- begin:: Footer -->