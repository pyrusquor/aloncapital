<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payable_items extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('payable_items/Payable_items_model', 'M_payable_items');
    }

}
