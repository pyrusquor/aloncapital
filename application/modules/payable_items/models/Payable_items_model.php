<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payable_items_model extends MY_Model
{
    public $table = 'payable_items'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'payment_request_id',
        'payment_voucher_id',
        'item_id',
        'payable_type',
        'name',
        'unit_price',
        'quantity',
        'tax_id',
        'rate',
        'tax_amount',
        'total_amount',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update

    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = TRUE;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;


        $this->has_one['payment_request'] = array('foreign_model' => 'payment_request/payment_request_model', 'foreign_table' => 'payment_requests', 'foreign_key' => 'id', 'local_key' => 'payment_request_id');

        $this->has_one['item'] = array('foreign_model'=>'item/item_model','foreign_table'=>'item','foreign_key'=>'id','local_key'=>'item_id');
    }
}