
    <head>
        <title>Welcome to Alon Capital!</title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
		<style>
		.h3-welcome{
		font-family: roboto;
		font-weight: 400;
		color: #0a284d;
		}
		
		.h1-welcome{
		font-family: roboto;
		font-weight: 400;
		color: #52ca11;
		}
		
	    .p-welcome{
		font-family: roboto;
		font-weight: 400;
		color: #0a284d;
		font-size: 11;
		line-height: 2;
		}
		
		.a-welcome{
		color: #52ca11;
		}
		
		.fa:hover {
         opacity: 0.9;
        }
  
        .fa-facebook {
        color: #0a284d;
        }
		
		.fa-instagram {
         color: #0a284d;
        }
		
		.fa-linkedin {
        color: #0a284d;
        }
  
        .fa {
            padding: 20px;
            font-size: 20px;
            width: 30px;
            text-decoration: none;
            margin: 0 auto;
        }
		</style>
    </head>
	
    <body>
	    <p style="text-align:center; margin-left:auto;margin-right:auto;" class="p-welcome"><img src="<?=base_url();?>assets/img/emails/Alon-Welcome.jpg" alt="Alon-Welcome"></p>		
        <h3 style="text-align:center;" class="h3-welcome">Apply for a loan now in 3 simple steps:</h3>

        <table cellspacing="0" style="width: 500px; margin-left:auto;margin-right:auto;">
            <tr>
                <th><h1 style="margin-right:15px;" class="h1-welcome">1</h1></th><td><h3 class="h3-welcome">Log in your account via <a href="https://aloncapital.yggdrasil.ph/login" target=”_blank” class="a-welcome">Alon Capital Loan Management System</h3></td>
            </tr>
            <tr>
                <th><h1 style="margin-right:15px;" class="h1-welcome">2</h1></th><td><h3 class="h3-welcome">Go to <a href="https://aloncapital.yggdrasil.ph/loan" target=”_blank” class="a-welcome">Loans Page</a> and click 'Apply for a Loan'</h3></td>
            </tr>
			            <tr>
                <th><h1 style="margin-right:15px;" class="h1-welcome">3</h1></th><td><h3 class="h3-welcome">Complete the form, upload all requirements, and click 'Submit'</h3></td>
            </tr>
        </table>
		<h3 style="text-align:center;" class="h3-welcome">Our team will contact you shortly once your application has been verified.</h3>
		<br>
		<p style="text-align:center; margin-left:auto;margin-right:auto;" class="p-welcome"><img src="<?=base_url();?>assets/img/emails/Alon-Connect-with-Us.jpg" alt="Alon-Welcome"></p>

    <center>
        <a href="https://www.facebook.com/aloncapitalph/" target=”_blank” class="fa"><img width="30px" src="<?=base_url();?>assets/img/emails/fb.png"></a>
        <a href="https://www.instagram.com/aloncapital/?hl=en" target=”_blank” class="fa"><img width="30px" src="<?=base_url();?>assets/img/emails/ig.png"></a>
		<a href="https://www.linkedin.com/company/alon-capital-philippines/?originalSubdomain=ph" target=”_blank” class="fa"><img width="30px" src="<?=base_url();?>assets/img/emails/linkedin.png"></a>
    </center>
	
	<br><br>
	
	<p style="text-align:center;" class="p-welcome">This is a system generated message. Please do not reply to this email.<br>
                                  For questions and inquiries kindly email us at <a href="mailto:info@aloncapital.ph" target=”_blank” class="a-welcome">info@aloncapital.ph</a><br><br>
                                    
                                  Alon Capital Philippines<br>
                                  <a href="https://www.aloncapital.ph/" target=”_blank” class="a-welcome">www.aloncapital.ph</a><br>
                                  Copyright © <?=date('Y');?>. All rights reserved.
	</p>
	<br>
    </body>
    </html>