<!DOCTYPE html>

<html lang="en">

	<!-- begin::Head -->
	<head>

		<!--begin::Base Path (base relative path for assets of this page) -->
		<base href="../../../">

		<!--end::Base Path -->
		<meta charset="utf-8" />
		<title><?php echo $template['title']; ?></title>
		<meta name="description" content="YGG Solutions - Alon Capital Loan Management System">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" type="image/x-icon" href="<?=base_url();?>assets/media/icons/faviconalon.png">
		
		<?php echo $template['partials']['style']; ?>

	</head>
	<style>
		.select2-container .select2-selection--single .select2-selection__rendered {
		   z-index:1 !important;
		}
	</style>
	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading <?php echo isset($_largescreen) && $_largescreen ? 'kt-aside--minimize' : '';?>">
	

		<!-- begin:: Page -->

		<!-- begin:: Header Mobile -->
		<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed">
			<div class="kt-header-mobile__logo">
				<a href="<?php echo base_url(); ?>">
					LMS
				</a>
			</div>
			<div class="kt-header-mobile__toolbar">
				<button class=" hide kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
			</div>
		</div>
		<!-- end:: Header Mobile -->

		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

				<!-- begin:: Aside -->
				<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
				<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside" style="background-color: transparent!important">

					<!-- begin:: Aside -->
					<?php echo $template['partials']['brand']; ?>
					<!-- end:: Aside -->

					<!-- begin:: Aside Menu -->
					<?php //echo $template['partials']['menu']; ?>
					<!-- end:: Aside Menu -->
				</div>
				<!-- end:: Aside -->

				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

					<!-- begin:: Header -->
					<?php echo $template['partials']['header']; ?>
					<!-- end:: Header -->

					<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

						<!-- begin:: Subheader -->
						<?php //echo $template['partials']['subheader']; ?>
						<!-- end:: Subheader -->

						<!-- begin:: Content -->
						<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

							<!-- begin:: Alert -->
							<?php echo $template['partials']['alert']; ?>
							<!-- end:: Alert -->
							

							<?php echo $template['body']; ?>
						</div>
						<!-- end:: Content -->
					</div>

					<!-- begin:: Footer -->
					<?php echo $template['partials']['footer']; ?>
					<!-- end:: Footer -->
				</div>
			</div>
		</div>
		<!-- end:: Page -->

		<!-- begin::Quick Panel -->
		<div id="kt_quick_panel" class="kt-quick-panel">
			<a href="#" class="kt-quick-panel__close" id="kt_quick_panel_close_btn"><i class="flaticon2-delete"></i></a>
			<div class="kt-quick-panel__nav">
				<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand  kt-notification-item-padding-x" role="tablist">
					<li class="nav-item active">
						<a class="nav-link active" data-toggle="tab" href="#kt_quick_panel_tab_notifications" role="tab">Activities</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_login_history" role="tab">Login History</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_settings" role="tab">Loan Calculator</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_settings_2" role="tab">Past Due Calculator</a>
					</li>
				</ul>
			</div>
			<div class="kt-quick-panel__content">
				<div class="tab-content">
					<div class="tab-pane fade show kt-scroll active" id="kt_quick_panel_tab_notifications" role="tabpanel">
                        <div class="kt-notification-v2" id="side_notification">

                        </div>
					</div>
					<div class="tab-pane fade kt-scroll" id="kt_quick_panel_tab_login_history" role="tabpanel">
						<div class="kt-notification-v2" id="side_login_history">

						</div>
					</div>
					<div class="tab-pane kt-quick-panel__content-padding-x fade kt-scroll" id="kt_quick_panel_tab_settings" role="tabpanel">

						<div id="calc_result">
							
						</div>

						<form class="kt-form" id="loan_calculator">
							<div class="kt-portlet__body">
								<div class="kt-section kt-section--first">

									<div class="form-group">
										<label>Loanable Amount:</label>
										<input id="calc_amount" type="text" class="form-control" placeholder="Enter loanable price">
										<span class="form-text text-muted">Please enter the loanable amount of a property</span>
									</div>

									<div class="form-group">
										<label>Interest Percentage:</label>
										<input id="calc_interest" type="text" class="form-control" placeholder="Enter Interest Rate">
										<span class="form-text text-muted">Please enter the interest rate of loan</span>
									</div>

									<div class="form-group">
										<label>Loan Term:</label>
										<input id="calc_term" type="text" class="form-control" placeholder="Enter loan term">
										<span class="form-text text-muted">Please enter the months to pay the loan</span>
									</div>

								</div>
							</div>

							<div class="kt-portlet__foot">
								<div class="kt-form__actions">
									<button id="btn_loan_calculator" class="btn btn-primary">Calculate</button>
								</div>
							</div>
						</form>
						
						

					</div>
					<div class="tab-pane kt-quick-panel__content-padding-x fade kt-scroll" id="kt_quick_panel_tab_settings_2" role="tabpanel">

						<div id="pd_result">
							
						</div>

						<form class="kt-form" id="pd_calculator">
							<div class="kt-portlet__body">
								<div class="kt-section kt-section--first">

									<div class="form-group">
										<label>Buyer Name:</label>
										<select class="form-control suggests" id="pd_buyer_id" data-module="buyers" data-type="person"></select>
										<!-- <span class="form-text text-muted">Please enter the loanable amount of a property</span> -->
									</div>

									<div class="form-group">
										<label>Project Name:</label>
										<select class="form-control suggests" id="pd_project_id" data-module="projects"></select>
										<!-- <span class="form-text text-muted">Please enter the loanable amount of a property</span> -->
									</div>

									<div class="form-group">
										<label>Property Name:</label>
										<select class="form-control suggests" id="pd_property_id" data-module="properties" data-param="project_id"></select>
										<!-- <span class="form-text text-muted">Please enter the loanable amount of a property</span> -->
									</div>


								</div>
							</div>

							<div class="kt-portlet__foot">
								<div class="kt-form__actions">
									<button id="btn_pd_calculator" class="btn btn-primary">Calculate</button>
								</div>
							</div>
						</form>
						
						

					</div>
				</div>
			</div>
		</div>
		<!-- end::Quick Panel -->

		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>
		<!-- end::Scrolltop -->

		<!-- begin::Sticky Toolbar -->
		<?php //echo $template['partials']['toolbar']; ?>
		<!-- end::Sticky Toolbar -->

		
		<?php echo $template['partials']['script']; ?>
	</body>
	<!-- end::Body -->
</html>