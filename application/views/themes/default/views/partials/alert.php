<div class="row">
    <div class="col">

    <?php if ($this->session->flashdata('success')) {?>

        <!-- Alert Success -->
        <div class="alert alert-success fade show" role="alert">
            <div class="alert-icon"><i class="flaticon-confetti"></i></div>
            <div class="alert-text"><?php echo $this->session->flashdata('success'); ?></div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>

    <?php } else if ($this->session->flashdata('error')) {?>

        <!-- Alert Error -->
        <div class="alert alert-danger fade show" role="alert">
            <div class="alert-icon"><i class="flaticon-danger"></i></div>
            <div class="alert-text"><?php echo $this->session->flashdata('error'); ?></div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>

    <?php } else if ($this->session->flashdata('warning')) {?>

        <!-- Alert warning -->
        <div class="alert alert-warning fade show" role="alert">
            <div class="alert-icon"><i class="flaticon-warning"></i></div>
            <div class="alert-text"><?php echo $this->session->flashdata('warning'); ?></div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>

    <?php } else if ($this->session->flashdata('info')) {?>

        <!-- Alert Info -->
        <div class="alert alert-info fade show" role="alert">
            <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
            <div class="alert-text"><?php echo $this->session->flashdata('info'); ?></div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>

    <?php }?>


    </div>
</div>