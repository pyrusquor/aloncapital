<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
	var KTAppOptions = {
		"colors": {
			"state": {
				"brand": "#5d78ff",
				"dark": "#282a3c",
				"light": "#ffffff",
				"primary": "#5867dd",
				"success": "#34bfa3",
				"info": "#36a3f7",
				"warning": "#ffb822",
				"danger": "#fd3995"
			},
			"base": {
				"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
				"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
			}
		}
	};
</script>
<!-- end::Global Config -->

<!--begin:: Global Mandatory Vendors -->
<script src="<?php echo base_url();?>assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/chart.min.js" type="text/javascript"></script>
<!--end:: Global Mandatory Vendors -->

<!--begin:: Global Optional Vendors -->
<script src="<?php echo base_url();?>assets/vendors/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/custom/js/vendors/bootstrap-datepicker.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/custom/js/vendors/bootstrap-timepicker.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url();?>assets/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url();?>assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script> -->
<script src="<?php echo base_url();?>assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/custom/js/vendors/bootstrap-switch.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url();?>assets/vendors/general/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script> -->
<script src="<?php echo base_url();?>assets/vendors/general/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url();?>assets/vendors/general/handlebars/dist/handlebars.js" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url();?>assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url();?>assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url();?>assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script> -->
<script src="<?php echo base_url();?>assets/vendors/general/nouislider/distribute/nouislider.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url();?>assets/vendors/general/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url();?>assets/vendors/general/autosize/dist/autosize.js" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url();?>assets/vendors/general/clipboard/dist/clipboard.min.js" type="text/javascript"></script> -->
<script src="<?php echo base_url();?>assets/vendors/general/dropzone/dist/dropzone.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url();?>assets/vendors/general/summernote/dist/summernote.js" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url();?>assets/vendors/general/markdown/lib/markdown.js" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url();?>assets/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url();?>assets/vendors/custom/js/vendors/bootstrap-markdown.init.js" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url();?>assets/vendors/general/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url();?>assets/vendors/custom/js/vendors/bootstrap-notify.init.js" type="text/javascript"></script> -->
<script src="<?php echo base_url();?>assets/vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/toastr/build/toastr.min.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url();?>assets/vendors/general/raphael/raphael.js" type="text/javascript"></script> -->
<script src="<?php echo base_url();?>assets/vendors/general/morris.js/morris.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url();?>assets/vendors/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url();?>assets/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script> -->
<script src="<?php echo base_url();?>assets/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url();?>assets/vendors/general/counterup/jquery.counterup.js" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url();?>assets/vendors/general/es6-promise-polyfill/promise.min.js" type="text/javascript"></script> -->
<script src="<?php echo base_url();?>assets/vendors/general/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/custom/js/vendors/sweetalert2.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/jquery.repeater/src/lib.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/general/jquery.repeater/src/repeater.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url();?>assets/vendors/general/dompurify/dist/purify.js" type="text/javascript"></script> -->
<!--end:: Global Optional Vendors -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="<?php echo base_url();?>assets/js/demo1/scripts.bundle.js" type="text/javascript"></script>
<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
<script src="<?php echo base_url();?>assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/vendors/custom/gmaps/gmaps.js" type="text/javascript"></script>
<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<!-- <script src="<?php echo base_url();?>assets/js/demo1/pages/dashboard.js" type="text/javascript"></script> -->
<!--end::Page Scripts -->


<script src="<?php echo base_url();?>assets/js/suggests.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/transaction.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/property.js" type="text/javascript"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.js"></script>

<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script> -->

<script>
	function exportTableToExcel(tableID, filename = ''){
	    var downloadLink;
	    var dataType = 'application/vnd.ms-excel';
	    var tableSelect = document.getElementById(tableID);
	    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
	    
	    // Specify file name
	    filename = filename?filename+'.xls':'excel_data.xls';
	    
	    // Create download link element
	    downloadLink = document.createElement("a");
	    
	    document.body.appendChild(downloadLink);
	    
	    if(navigator.msSaveOrOpenBlob){
	        var blob = new Blob(['\ufeff', tableHTML], {
	            type: dataType
	        });
	        navigator.msSaveOrOpenBlob( blob, filename);
	    }else{
	        // Create a link to the file
	        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
	    
	        // Setting the file name
	        downloadLink.download = filename;
	        
	        //triggering the function
	        downloadLink.click();
	    }
	}
	function formatCurrency(amt) {
        n = amt.toFixed(2);
        if (isNaN(n)) { n = 0 }
        return  n;
    }

    function moneyFormat(amt) {

        if (isNaN(amt)) { amt = 0 }

		amt = (amt).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');

        return  amt;
    }


    var arrows;
	
	arrows = {
		leftArrow: '<i class="la la-angle-left"></i>',
		rightArrow: '<i class="la la-angle-right"></i>'
	}
	
    var datepicker = function () {
    	
    	 // input group and left alignment setup
        // $('#daterangepicker').daterangepicker({
        //     buttonClasses: ' btn',
        //     applyClass: 'btn-primary',
        //     cancelClass: 'btn-secondary'
        //     ranges: {
	       //     'Today': [moment(), moment()],
	       //     'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	       //     'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	       //     'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	       //     'This Month': [moment().startOf('month'), moment().endOf('month')],
	       //     'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	       //  }
        // }, function(start, end, label) {
        //     $('#daterangepicker').val( start.format('2011-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
        // });



        var start = moment().subtract(365, 'days');
	    var end = moment();

	    function cb(start, end) {
	        $('#daterangepicker span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	    }

	    function ecb(start, end) {
	        $('#reservation_daterangepicker span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	    }

	    $('#daterangepicker').daterangepicker({
	        startDate: start,
	        endDate: end,
	        ranges: {
	           'Today': [moment(), moment()],
	           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	           'This Month': [moment().startOf('month'), moment().endOf('month')],
	           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	        }
	    }, cb);

	    cb(start, end);

	    $('#reservation_daterangepicker').daterangepicker({
	        startDate: start,
	        endDate: end,
	        ranges: {
	           'Today': [moment(), moment()],
	           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	           'This Month': [moment().startOf('month'), moment().endOf('month')],
	           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	        }
	    }, cb);

	    ecb(start, end);


		// minimum setup
		$('.datetimePicker').datetimepicker({
            todayHighlight: true,
            autoclose: true,
            format: 'yyyy.mm.dd hh:ii'
        });

		// minimum setup
		$('.datePicker').datepicker({
			// rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			templates: arrows,
			locale: 'no',
			format: 'yyyy-mm-dd',
			autoclose: true
		});

		$('.yearmonthPicker').datepicker({
			// rtl: KTUtil.isRTL(),
			todayHighlight: true,
			format: "yyyy-mm",
			viewMode: "months", 
			minViewMode: "months",
			autoclose: true
		});

		$('.yearPicker').datepicker({
			// rtl: KTUtil.isRTL(),
			todayHighlight: true,
			format: "yyyy",
			viewMode: "years", 
			minViewMode: "years",
			autoclose: true
		});

	}

	var base_url = '';
	base_url = "<?php echo base_url(); ?>";

	$(document).on('click', '.modal_view', function () {
        var btn = $(this);
        swal_view(btn);
	});

	var swal_view = function(btn){
		var id = btn.data('id');
		var type = btn.data('type');
		var url = btn.data('url');

		$.ajax({
			url: base_url + url,
			type: 'GET',
			dataType: "JSON",
			data: { id: id },
			success: function (res) {
				$('#modal_process .modal-content').html(res.html);
				$('#modal_process').modal('show')
			}
		});
	}

	// $(document).on('click','[data-ktmenu-submenu-toggle="click"]', function (){
	// 		$('[data-ktmenu-submenu-toggle="click"]').removeClass('kt-menu__item--open-dropdown kt-menu__item--hover');
	// 		$(this).addClass('kt-menu__item--open-dropdown kt-menu__item--hover');
	// });


</script>


<script type="text/javascript">
<?php if ($this->session->flashdata('success')) {?>
	toastr.success("<?php echo $this->session->flashdata('success'); ?>", "Success");
<?php } else if ($this->session->flashdata('error')) {?>
	toastr.error("<?php echo $this->session->flashdata('error'); ?>", "Error");
<?php } else if ($this->session->flashdata('warning')) {?>
	toastr.warning("<?php echo $this->session->flashdata('warning'); ?>", "Warning");
<?php } else if ($this->session->flashdata('info')) {?>
	toastr.info("<?php echo $this->session->flashdata('info'); ?>", "Info");
<?php }?>
</script>

<?php $this->js_loader->load(); ?>