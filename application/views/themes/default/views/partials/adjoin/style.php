 <?php //if ( ( $_SERVER['REMOTE_ADDR'] != "127.0.0.1" ) && ($_SERVER['REMOTE_ADDR'] != "::1") ): ?>
	<!--begin::Fonts -->
	<script src="<?php echo base_url();?>assets/js/demo1/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
			},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<!--end::Fonts -->
<?php //endif ?>


<!--begin::Page Vendors Styles(used by this page) -->
<link href="<?php echo base_url();?>assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->

<!--begin:: Global Mandatory Vendors -->

<link href="<?php echo base_url();?>assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
<!--end:: Global Mandatory Vendors -->

<!--begin:: Global Optional Vendors -->
<link href="<?php echo base_url();?>assets/vendors/general/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
<!-- <link href="<?php echo base_url();?>assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" /> -->
<link href="<?php echo base_url();?>assets/vendors/general/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
<!-- <link href="<?php echo base_url();?>assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" /> -->
<!-- <link href="<?php echo base_url();?>assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" /> -->
<link href="<?php echo base_url();?>assets/vendors/general/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- <link href="<?php echo base_url();?>assets/vendors/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css" /> -->
<link href="<?php echo base_url();?>assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
<!-- <link href="<?php echo base_url();?>assets/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" /> -->
<link href="<?php echo base_url();?>assets/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/custom/vendors/rems-icon/rems-icon.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/vendors/custom/vendors/rems-icon/rems-icon-new.css" rel="stylesheet" type="text/css" />
<!--end:: Global Optional Vendors -->

<?php $this->css_loader->load(); ?>

<!--begin::Global Theme Styles(used by all pages) -->
<link href="<?php echo base_url();?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/css/demo1/rems-green.css" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles -->

<!--begin::Layout Skins(used by all pages) -->
<link href="<?php echo base_url();?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.css" rel="stylesheet" /> -->
<!--end::Layout Skins -->

<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" rel="stylesheet"> -->


<style>
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>
