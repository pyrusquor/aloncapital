<!--begin::Modal Process-->
<div class="modal fade" id="modal_process" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">

		</div>
	</div>
</div>
<!--end::Modal Process-->

<!--begin::Extra Large Modal-->
<div class="modal fade" id="extra_large_modal_process" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">

		</div>
	</div>
</div>
<!--end::Extra Large Modal-->

<!-- begin:: Footer -->
<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-footer__copyright">
			<?= date('Y'); ?>&nbsp;&copy;&nbsp;<a target="_BLANK" href="https://yggdrasil.ph/" target="_blank" class="kt-link">YGG Solutions</a>&nbsp;ver.&nbsp;<?php echo "0." . get_version(); ?>
		</div>
		<div class="kt-footer__menu hide">
			<a href="#" target="_blank" class="kt-footer__menu-link kt-link">About</a>
			<a href="#" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
			<a href="#" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
		</div>
	</div>
</div>
<!-- end:: Footer -->
