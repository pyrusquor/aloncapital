<?php
	$CI =& get_instance();
	$CI->load->model('menu/Menu_model','menu');
	$CI->load->model('auth/Ion_auth_model','auth');
	$menu = $CI->menu->load_menu();
	// print_r($menu); exit;

	$parent_page = $CI->uri->segment(1);
	$current_page = $CI->uri->segment(1).(($CI->uri->segment(2)) ? '/'.$CI->uri->segment(2) : '');
	$group = json_decode( json_encode($CI->auth->get_users_groups()->result()[0]), true);
	
	// echo $parent_page; exit;
	// echo $CI->menu->has_active_child(1, $current_page); exit;

	// vdebug($menu);
?>

<!-- begin:: Aside Menu -->
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
	<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
		<ul class="kt-menu__nav ">
			<li class="kt-menu__section ">
				<h4 class="kt-menu__section-text kt-font-boldest" data-name="ckayh">MODULES</h4>
				<i class="kt-menu__section-icon flaticon-more-v2"></i>
			</li>
			<?php foreach ($menu as $m): ?>
				<?php if($CI->menu->has_access($group['id'], $m['id']) > 0): ?>
					<?php if($CI->menu->has_permission($m['id']) > 0): ?>
						<?php if ($m['ctr']): ?>
							
						<li class="kt-menu__item  kt-menu__item--submenu <?=(($current_page == $m['link']) ? 'kt-menu__item--open' : '')?> <?=(($CI->menu->has_active_child($m['id'], $parent_page) > 0) ? 'kt-menu__item--open' : '')?>" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"> <!-- kt-menu__item--open kt-menu__item--here -->

							
							<?php if($m['link'] == "") { $m['link'] = "javascript:;"; } else { $m['link'] = base_url($m['link']); } ?>



							<a href="<?php echo $m['link']; ?>" class="kt-menu__link kt-menu__toggle">
								
								<i class="<?php echo $m['icon']; ?>"></i>
								<!-- Original One -->
								<!-- <span class="kt-menu__link-text">
									<?php echo $m['name']; ?>
									<?php if($CI->menu->has_child($m['id']) > 0): ?>
										<i class="kt-menu__ver-arrow la la-angle-right"></i>
									<?php endif; ?>
								</span> -->

								<!-- Editted One -->
								<span class="kt-menu__link-text">
									<?php echo $m['name']; ?>		
								</span>

								<?php //if($CI->menu->has_child($m['id']) > 0): ?>
									<!-- <i class="kt-menu__ver-arrow la la-angle-right"></i> -->
								<?php //endif; ?>
								
							</a>
						</li>

						<?php endif ?>

					<?php endif; ?>
				<?php endif; ?>
			<?php endforeach;?>
		</ul>
	</div>
</div>
<!-- end:: Aside Menu -->