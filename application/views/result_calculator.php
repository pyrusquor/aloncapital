<div class="kt-portlet" style="margin-bottom: 15px;">
	<div class="kt-portlet__body kt-portlet__body--fit">
		<div class="row row-no-padding row-col-separator-xl">
			<div class="col-md-12 col-lg-12 col-xl-12">

				<!--begin:: Widgets/Stats2-1 -->
				<div class="kt-widget1">
					<div class="kt-widget1__item">
						<div class="kt-widget1__info">
							<h3 class="kt-widget1__title">Total Monthly Payment</h3>
							<span class="kt-widget1__desc">MA Result</span>
						</div>
						<span class="kt-widget1__number kt-font-brand"><?=money_php($monthly);?></span>
					</div>
					<div class="kt-widget1__item">
						<div class="kt-widget1__info">
							<h3 class="kt-widget1__title">Total Interest Amount</h3>
							<span class="kt-widget1__desc">Interest Result</span>
						</div>
						<span class="kt-widget1__number kt-font-danger"><?=money_php($interest);?></span>
					</div>
					<div class="kt-widget1__item">
						<div class="kt-widget1__info">
							<h3 class="kt-widget1__title">Total Amount Receivable</h3>
							<span class="kt-widget1__desc">Overall Result</span>
						</div>
						<span class="kt-widget1__number kt-font-success"><?=money_php($total);?></span>
					</div>
				</div>
				<!--end:: Widgets/Stats2-1 -->

			</div>

		</div>
	</div>
</div>