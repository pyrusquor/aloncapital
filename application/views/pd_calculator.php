<div class="kt-portlet" style="margin-bottom: 15px;">
	<div class="kt-portlet__body kt-portlet__body--fit">
		<div class="row row-no-padding row-col-separator-xl">
			<div class="col-md-12 col-lg-12 col-xl-12">

				<?php if ($success): ?>
					<!--begin:: Widgets/Stats2-1 -->
					<div class="kt-widget1">
						<div class="kt-widget1__item">
							<div class="kt-widget1__info">
								<h3 class="kt-widget1__title">Transaction</h3>
								<span class="kt-widget1__desc">Reference #</span>
							</div>

							<span class="kt-widget1__number kt-font-brand">
								<?php echo "<a target='_BLANK' href='".base_url()."transaction/view/".$transaction_id."'>".$reference."</a>";?>
								<br>
								<?=($period);?>
							</span>
						</div>

						<div class="kt-widget1__item">
							<div class="kt-widget1__info">
								<h3 class="kt-widget1__title">Total Past Due Amount</h3>
								<span class="kt-widget1__desc">Past Due</span>
							</div>

							<span class="kt-widget1__number kt-font-danger">
								<?php echo "<a target='_BLANK' href='".base_url()."transaction_payment/form/".$transaction_id."'>".money_php($total_amount_due)."</a>";?>
							</span>
						</div>


						<div class="kt-widget1__item">
							<div class="kt-widget1__info">
								<h3 class="kt-widget1__title">Days Delayed</h3>
								<span class="kt-widget1__desc">Number of days</span>
							</div>

							<span class="kt-widget1__number kt-font-danger">
								<?=($days_delayed);?>
							</span>
						</div>

						<div class="kt-widget1__item">
							<div class="kt-widget1__info">
								<h3 class="kt-widget1__title">Number of Payment</h3>
								<span class="kt-widget1__desc">Delayed payments</span>
							</div>

							<span class="kt-widget1__number kt-font-danger">
								<?=($no_of_mos);?>
							</span>
						</div>

						<div class="kt-widget1__item">
							<div class="kt-widget1__info">
								<h3 class="kt-widget1__title">Next Due Date</h3>
							</div>

							<span class="kt-widget1__number kt-font-danger">
								<?=view_date($last_date);?>
							</span>
						</div>

						
						
						
					</div>
					<!--end:: Widgets/Stats2-1 -->

				<?php else: ?>

					<div class="kt-widget1">
						<div class="kt-widget1__item">
							<div class="kt-widget1__info">
								<h3 class="kt-widget1__title">No Transaction Found</h3>
							</div>
						</div>
					</div>


				<?php endif; ?>
				

			</div>

		</div>
	</div>
</div>