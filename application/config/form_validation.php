<?php defined('BASEPATH') OR exit('No direct script access allowed');

$config['error_prefix'] = '<div class="is-invalid"><div class="error invalid-feedback">';
$config['error_suffix'] = '</div></div>';