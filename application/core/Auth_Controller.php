<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_Controller extends MX_Controller {
    function __construct() {
        parent::__construct();
        
        // set default theme and layout
		$this->template->set_theme('default');
		$this->template->set_layout('default');

		// set partials
		$this->template->set_partial('style','partials/adjoin/style');
		$this->template->set_partial('script','partials/adjoin/script');

    }
}