<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

	protected	$_method		=	'';
	protected	$_class			=	'';
	protected	$_title			=	'';
	protected 	$view_data	=	[];

	protected $user	=	FALSE;

	protected $_table_fillables	=	[];
	protected $_table_columns		=	[];

    protected $timestamps_format = 'Y-m-d H:i:s';

	function __construct () {

		parent::__construct();

		// check if logged in
	  if($this->ion_auth->logged_in()===FALSE)
	  {
	      	redirect('login');
	  } else {

	  	if ( $this->ion_auth->user()->row() ) {

	  		$this->user	=	$this->ion_auth->user()->row();
	  	} else {

	  		redirect('login');
	  	}
	  }


		$this->_method	=	$this->router->method;
		$this->_class		=	$this->router->class;
		$this->_title		= isset($this->_class) && $this->_class ?	ucwords(strtolower(implode(' ', explode( '_', $this->_class)))) : '';
		$current_page = $this->_class."/".$this->_method;

		$this->template->title('Alon Capital',$this->_title);

		// set default theme and layout
		$this->template->set_theme('default');
		$this->template->set_layout('default');

		// set partials
		$this->template->set_partial('style','partials/adjoin/style');
		$this->template->set_partial('brand','partials/aside/brand');
		$this->template->set_partial('menu','partials/aside/menu');
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('alert','partials/alert');
		// $this->template->set_partial('subheader','partials/subheader');
		$this->template->set_partial('footer','partials/footer');
		// $this->template->set_partial('toolbar','partials/toolbar');
		$this->template->set_partial('script','partials/adjoin/script');

		// vdebug($this->session->userdata);

	  	// Check if user group is Administrator
		if ( ($this->session->userdata['group_id'] == "1") || (($this->session->userdata['group_id'] == "2")) ) //administrator and member- groups table
		{	

			$restricted_points = ['create','update','index','view','form','delete'];
			
			$status = false;

			// $current_module = ( $this->_method != "index" ) ? $this->_method : $this->_class;
			$current_module = ( $this->_method == "index" ) ? $this->_class : $current_page;


			if (!in_array($this->_method, $restricted_points)) {
				return; // exit condition no need for validation
			} else {
				$current_module = $this->_class;
			}


			if(is_authorized($current_module, $this->session->userdata, "is_read")) 
			{
				$status = true;

				if($this->_method === "create") 
				{
					if(!is_authorized($current_module, $this->session->userdata, "is_create")) 
					{
						$status = false;
					} 
				}

				if($this->_method === "update") 
				{
					if (!is_authorized($current_module, $this->session->userdata, "is_update")) 
					{
						$status = false;
					}
				}
				
			} 


			if ($this->_method == "view") {
				$data = array();

			  	$trail_data = [
                    'affected_id' => $this->uri->segment('3'),
                    'affected_table' => $this->uri->segment('1'),
                    'action' => 'VIEW',
                    'previous_record' => '',
                    'remarks' => 'record '. $this->uri->segment('1') .' id # '. $this->uri->segment('3') .' has been created ',
                    'extra_tag' => 'success',
                    'icon' => 'flaticon2-plus-1',
                    'created_at' => $this->_the_timestamp(),
                    'created_by' => $this->session->userdata['user_id']
                ];
                $trail_data['new_record'] = json_encode($data);

				if($this->uri->segment('3')){
					$this->db->insert('audit_trail', $trail_data);
				}


			}

			if ( ( !$status ) && ($current_module != "migrate") ){

				// echo $this->_method;
				// echo "<br>";
				// echo $this->_class;
				// echo "<br>";
				// echo $this->_title;
				// echo "<br>";
				// echo $current_module;
				// echo "<br>";
				// vdebug(is_authorized($current_module, $this->session->userdata, "is_read"));
				
				// show_403();

			}

		}
		else 
		{
			if ($this->ion_auth->is_seller()) 
			{
				// seller module only
				$modules = array(
					'dashboard/index',
					'seller_dashboard/index',
					'project/index',
					'project/view',
					'project/showProjects',
					'project/blocks',
					'project/load_blocks',
					'buyer/index',
					'buyer/showItems',
					'buyer/view',
					'prospect/index',
					'prospect/create',
					'prospect/showProspects',
					'seller/index',
					'seller/showItems',
					'seller/view',
					'property/index',
					'property/view',
					'property/showProperties',
					'house/index',
					'house/view',
					'map/index',
					'interior/index',
					'interior/view',
					'transaction/index',
					'transaction/view',
					'transaction_commission/index',
					'transaction_documents/index',
					'transaction/view_ledger',
					'transaction/paginationData',
					'transaction',
					'transaction/index',
					'transaction/view',
					'transaction/view_ledger',
					'transaction/paginationData',
					'transaction_document',
					'transaction_document/index',
					'transaction_document/showItems',
					'transaction_document/showTransactionDocuments',
					'aris/ledger',
					'transaction_commission',
					'transaction_commission/showItems',
					'sellers_reservations/index',
					'sellers_reservations/showSellersReservationss'
				);
				// vdebug($current_page);
	
				if(!in_array($current_page, $modules)){
					show_403();
				}
	
			} else if ($this->ion_auth->is_buyer()) {
				// buyer module only
				$modules = array(
					'dashboard/index',
					'loan/index',
					'loan/showLoans',
					'loan/create',
					'loan/send_loan_info',
					'loan/view',
					'payments/index',
					'applicant/form',
					'applicant/update',
					'applicant/view',
				);
	
				if(!in_array($current_page, $modules)) {
					show_403();
				}
			}
		}
	}

	/**
	 * DATATABLE FILTERS
	 */
	function filterArray( $array, $allowed = [] ) {
		return array_filter(
			$array,
			function ( $val, $key ) use ( $allowed ) { // N.b. $val, $key not $key, $val
				return isset( $allowed[ $key ] ) && ( $allowed[ $key ] === true || $allowed[ $key ] === $val );
			},
			ARRAY_FILTER_USE_BOTH
		);
	}

    function filterKeyword( $data, $search, $field = '' ) {
        $filter = '';

        if ( isset( $search['value'] ) ) {
            $filter = $search['value'];
        }

        if ( ! empty( $filter ) ) {
            if ( ! empty( $field ) ) {
                if ( strpos( strtolower( $field ), 'date' ) !== false ) {
                    // filter by date range
                    $data = $this->filterByDateRange( $data, $filter, $field );
                } else {
                    // filter by column
                    $data = array_filter( $data, function ( $a ) use ( $field, $filter ) {

                        if ($a[ $field ] ==  $filter) {
                            return 1;
                        } else {
                            return 0;
                        }

                        // return (boolean) preg_match( "/$filter/i", $a[ $field ] );

                    } );
                }

            } else {
                // general filter
                $data = array_filter( $data, function ( $a ) use ( $filter ) {
                    return (boolean) preg_grep( "/$filter/i", (array) $a );
                } );
            }
        }

        return $data;
    }

    function filterByDateRange( $data, $filter, $field ) {
        // filter by range
        if ( ! empty( $range = array_filter( explode( '-', $filter ) ) ) ) {
            $filter = $range;
        }

        if ( is_array( $filter ) ) {
            // filter by date range
            $data = array_filter( $data, function ( $a ) use ( $field, $filter ) {
                // hardcoded date format
                $current = date_create_from_format( 'Y-m-d', $a[ $field ] );
                $from_str   = strtotime($filter[0]);
                $from       = date('Y-m-d H:i:s', $from_str);

                $to_str     = strtotime($filter[1] . '23:59:59');
                $to         = date('Y-m-d H:i:s', $to_str);
                $current = $current->format('Y-m-d H:i:s');

                if ( $from <= $current && $to >= $current ) {
                    return true;
                }
                return false;
            } );
        }

        return $data;
    }

//    function filterByDateRange( $data, $filter, $field ) {
//        // filter by range
//        if ( ! empty( $range = array_filter( explode( '|', $filter ) ) ) ) {
//            $filter = $range;
//        }
//
//        if ( is_array( $filter ) ) {
//            foreach ( $filter as &$date ) {
//                // hardcoded date format
//                $date = date_create_from_format( 'm/d/Y', stripcslashes( $date ) );
//            }
//            // filter by date range
//            $data = array_filter( $data, function ( $a ) use ( $field, $filter ) {
//                // hardcoded date format
//
//                $current = date_create_from_format( 'm/d/Y', $a[ $field ] );
//                $from    = $filter[0];
//                $to      = $filter[1];
//                if ( $from <= $current && $to >= $current ) {
//                    return true;
//                }
//
//                return false;
//            } );
//        }
//
//        return $data;
//    }

//    function filterKeyword( $data, $search, $field = '' ) {
//		$filter = '';
//
//		if ( isset( $search['value'] ) ) {
//			$filter = $search['value'];
//		}
//
//		if ( ! empty( $filter ) ) {
//			if ( ! empty( $field ) ) {
//				if ( strpos( strtolower( $field ), 'date' ) !== false ) {
//					// filter by date range
//
//					$data = $this->filterByDateRange( $data, $filter, $field );
//				} else {
//					// filter by column
//					$data = array_filter( $data, function ( $a ) use ( $field, $filter ) {
//
//						if ($a[ $field ] ==  $filter) {
//							return 1;
//						} else {
//							return 0;
//						}
//
//						// return (boolean) preg_match( "/$filter/i", $a[ $field ] );
//
//					} );
//				}
//
//			} else {
//				// general filter
//				$data = array_filter( $data, function ( $a ) use ( $filter ) {
//					return (boolean) preg_grep( "/$filter/i", (array) $a );
//				} );
//			}
//		}
//
//		return $data;
//	}

//	function filterByDateRange( $data, $filter, $field ) {
//		// filter by range
//
//		if ( ! empty( $range = array_filter( explode( '|', $filter ) ) ) ) {
//			$filter = $range;
//		}
//
//		if ( is_array( $filter ) ) {
//			// filter by date range
//			$data = array_filter( $data, function ( $a ) use ( $field, $filter ) {
//				// hardcoded date format
//				$current = date("Y-m-d");
//
//				if (!empty($filter[0])) {
//                    if(sizeof($filter) > 1) {
//                        $from    = $filter[0];
//                        $to      = $filter[1];
//                        if ( $from <= $current && $to >= $current ) {
//                            return true;
//                        }
//                    }
//                };
//
//				return false;
//			} );
//		}
//
//		return $data;
//	}

	function __get_excel_columns ( $_limit = FALSE, $_char = FALSE ) {

		$_letters	=	['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ', 'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ',];

		$_return	=	FALSE;

		if ( $_limit ) {

			foreach ( $_letters as $key => $_letter ) {

				if ( $key < $_limit ) {

					$_return[]	=	$_letter;
				}
			}

			// return $_return;
		}

		// if ( $_char ) {

		// 	foreach ( $_letters as $key => $_letter ) {

		// 		if ( $key !== $_char ) {

		// 			$_return[]	=	$_letter;
		// 		} else {

		// 			$_return[]	=	$_letter;

		// 			break;
		// 		}
		// 	}

		// 	return $_return;
		// }

		return $_return;
	}

	function __get_columns ( $_fills = FALSE ) {

		$_return	=	[];

		if ( $_fills && !empty($_fills) ) {

			foreach ( $_fills as $key => $_dbclm ) {

					$_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ( (strpos( $_name, 'created_') === FALSE) && (strpos( $_name, 'updated_') === FALSE) && (strpos( $_name, 'deleted_') === FALSE) && ($_name !== 'id') && ($_name !== 'user_id')) {

						if ( strpos( $_name, '_id') !== FALSE ) {

							$_column	=	$_name;
							$_return[$key]['value']	=	$_column;
							$_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';
							$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
							$_return[$key]['label']	=	ucwords(strtolower($_title));
						} elseif ( strpos( $_name, 'is_') !== FALSE ) {

							$_column	=	$_name;
							$_return[$key]['value']	=	$_column;
							$_name	=	isset($_name) && $_name ? str_replace('is_', '', $_name) : '';
							$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
							$_return[$key]['label']	=	ucwords(strtolower($_title));
						} else {

							$_return[$key]['value']	=	$_name;
							$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
							$_return[$key]['label']	=	ucwords(strtolower($_title));
						}
					} else {

						continue;
					}
				}

				$_return	=	array_chunk($_return, ceil((count($_return)/2)));
		}

		return $_return;
	}

	function __get_fillables ( $_colms = FALSE, $_fills = FALSE, $_table = FALSE ) {

		$_return	=	[];
		$_no			=	1;

		if ( $_colms && !empty($_colms) && $_fills && !empty($_fills) ) {

			foreach ( $_colms as $key => $_col ) {

					$_required	=	isset($_col->Null) && ( $_col->Null === 'NO') ? 'Yes' : 'No';
					$_field			=	isset($_col->Field) && $_col->Field ? $_col->Field : '';
					$_type			=	isset($_col->Type) && $_col->Type ? $_col->Type : '';

					if ( (strpos( $_field, 'created_') === FALSE) && (strpos( $_field, 'updated_') === FALSE) && (strpos( $_field, 'deleted_') === FALSE) && ($_field !== 'image') && ($_field !== 'user_id') ) {

						if ( in_array($_field, $_fills) ) {

							$_return[$_no]	=	[
												'no'				=>	$_no,
												'name'			=>	$_field,
												'type'			=>	$_type,
												'required'	=>	$_required,
												'dropdown'	=>	$this->__get_dropdown($_field, $_table)
											];

							$_no++;
						} else {

							continue;
						}
					} else {

						continue;
					}
				}
		}

		return $_return;
	}

	function __get_dropdown ( $_field = FALSE, $_table = FALSE ) {
		$_return	=	[];
		$_datas		=	[];

		if ( $_field ) {

			$_select	=	(strpos($_field, '_id') !== FALSE || $_field === 'gender');
			$_bool	=	(strpos($_field, 'is_') !== FALSE);
			$_status	=	$_field === 'status' ? TRUE : FALSE;

			if ( $_select || $_bool || $_status ) {

				if ( $_select || $_status ) {

					if ( $_field === 'classification_id' ) {

						$_datas	=	Dropdown::get_static('classification');
					} elseif ( $_field === 'owner_id' ) {

						$_datas	=	Dropdown::get_static('document_owner');
					} elseif ( $_field === 'security_classification_id' ) {

						$_datas	=	Dropdown::get_static('security');
					} elseif ( $_field === 'issuing_party_id' ) {

						$_datas	=	Dropdown::get_static('parties');
					} elseif ( $_field === 'access_right_id' ) {

						$_datas	=	Dropdown::get_static('access_right');
					} elseif ( $_field === 'land_classification_id' ) {

						$_datas	=	Dropdown::get_static('land_classification');
					} elseif ( $_field === 'ownership_classification_id' ) {

						$_datas	=	Dropdown::get_static('ownership_classification');
					} elseif ( $_field === 'status' ) {

						$_datas	=	Dropdown::get_static('land_inventory_status');

						if( $_table === 'projects'){

							$_datas	=	Dropdown::get_static('project_status');
						}
						if( $_table === 'properties'){

							$_datas	=	Dropdown::get_static('property_status');
						}
					} elseif ( $_field === 'company_id' ) {
						$_datas	=	Dropdown::get_dynamic('companies');
					} elseif (( $_field === 'sales_group_id' )||( $_field === 'sales_arm_id' )) {
						$_datas	=	Dropdown::get_static('group_type');

					} elseif ( $_field === 'seller_position_id' ) {
						$_datas	=	Dropdown::get_dynamic('seller_positions');

					} elseif ( $_field === 'civil_status_id' ) {
						$_datas	=	Dropdown::get_static('civil_status');

					} elseif ( $_field === 'nationality' ) {
						$_datas	=	Dropdown::get_static('nationality');

					} elseif ( $_field === 'housing_membership_id' ) {
						$_datas	=	Dropdown::get_static('housing_membership');

					}  elseif ( $_field === 'gender' ) {
						$_datas	=	Dropdown::get_static('sex');
					} elseif ( $_field === 'hlurb_classification_id' ) {
						$_datas	=	Dropdown::get_static('hlurb_classification');
					} elseif ( $_field === 'project_id' ) {
						$_datas	=	Dropdown::get_dynamic('projects');
					} elseif ( $_field === 'model_id' ) {
						$_datas	=	Dropdown::get_dynamic('house_models');

					} elseif ( $_field === 'interior_id' ) {
						$_datas	=	Dropdown::get_dynamic('house_model_interiors');

					} elseif ( $_field === 'promo_id' ) {
						$_datas	=	Dropdown::get_dynamic('promos');
					} elseif ( $_field === 'bank_id' ) {
						$_datas	=	Dropdown::get_dynamic('banks');
					} elseif ( $_field === 'bank_id' ) {
						$_datas	=	Dropdown::get_dynamic('banks');
					} elseif ( $_field === 'transaction_id' ) {
						$_datas	=	Dropdown::get_dynamic('properties');
					} elseif ( $_field === 'regCode' ) {
						$_datas	=	Dropdown::get_dynamic('address_regions');
					} elseif ( $_field === 'provCode' ) {
						$_datas	=	Dropdown::get_dynamic('address_provinces');
					}


				} else {

					$_datas	=	Dropdown::get_static('bool');
				}

				if ( $_datas ) {

					foreach ( $_datas as $_dkey => $_data ) {

						$_label	=	'';

						if ( $_dkey !== '' ) {

							$_label	=	$_dkey;
							if ( $_data ) {

								$_label	.=	' = '.$_data;
							}

							$_return[]	=	$_label;
						} else {

							continue;
						}
					}
				}
			}
		}

		return $_return;
	}

	function __get_audit($id = 0)
	{
		# code...
		if (isset($this->_table) && !empty($id)) {
			# code...
			$this->trail_library->initiate($id);
			return $this->trail_library->get_record($this->_table,$id);
		}
	}


	 /**
     * private function _the_timestamp()
     *
     * returns a value representing the date/time depending on the timestamp format choosed
     * @return string
     */
    private function _the_timestamp()
    {
        if($this->timestamps_format=='timestamp')
        {
            return time();
        }
        else
        {
            return date($this->timestamps_format);
        }
    }
}