<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('applicant/Applicant_model', 'M_applicant');
        $this->load->model('applicant/Applicant_identification_model', 'M_applicant_identification');
        $this->load->model('applicant/Applicant_employment_model', 'M_applicant_employment');
        $this->admin = "admin";
        $this->password = "password";
    }


    public function extract_all_applicants($admin = "", $password = "", $debug = 0) {

        if ( ($admin == $this->admin) || ($password == $this->password) ) {

            $results = $this->M_applicant
                ->with_employment('fields:employer')
                ->order_by('id', 'DESC')
                ->get_all();

            if ($debug) {
                vdebug($results);
            }


            if ($results) {
                foreach ($results as $key => $result) {
                    $return[$key]['id'] = $result['id'];
                    $return[$key]['company_name'] = $result['id'];
                    $return[$key]['last_name'] = $result['last_name'];
                    $return[$key]['first_name'] = $result['first_name'];
                    $return[$key]['middle_name'] = $result['middle_name'];
                    $return[$key]['spouse_name'] = $result['spouse_name'];
                    $return[$key]['birth_place'] = $result['birth_place'];
                    $return[$key]['birth_date'] = $result['birth_date'];
                    $return[$key]['gender'] = $result['gender'];
                    $return[$key]['nationality'] = $result['nationality'];
                    $return[$key]['civil_status_id'] = $result['civil_status_id'];
                    $return[$key]['present_address'] = $result['present_address'];
                    $return[$key]['mailing_address'] = $result['mailing_address'];
                    $return[$key]['business_address'] = $result['business_address'];
                    $return[$key]['mobile_no'] = $result['mobile_no'];
                    $return[$key]['other_mobile'] = $result['other_mobile'];
                    $return[$key]['email'] = $result['email'];
                    $return[$key]['landline'] = $result['landline'];
                    $return[$key]['social_media'] = $result['social_media'];
                    $return[$key]['regCode'] = $result['regCode'];
                    $return[$key]['provCode'] = $result['provCode'];
                    $return[$key]['citymunCode'] = $result['citymunCode'];
                    $return[$key]['brgyCode'] = $result['brgyCode'];
                    $return[$key]['sitio'] = $result['sitio'];
                }
            }

            if (count($results) == 0) {
                echo json_encode(array( 'success' => 0 ));
            } else {
                echo json_encode(array('data' => $return, 'count' => count($return), 'success' => 1));
            }

        } else {
            
            echo json_encode(array('message' => "wrong api credential", 'success' => 0));

        }
    }

    
}