<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Settings extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function get_tax_amount($amount = 0, $is_lot = 0)
    {   

        $threshold = 0;

        if ($is_lot == 1) {
            $threshold = 1500000;
        } if ($is_lot == 2) {
            $threshold = 0;
        } else {
            $threshold = 2500000;
        }

        if ($threshold < $amount) {
            $amount = $amount * .12;
        } 
       
       $result['amount'] = $amount;

       echo json_encode($result);
    }

 

}
