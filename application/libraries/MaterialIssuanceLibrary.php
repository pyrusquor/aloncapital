<?php


class MaterialIssuanceLibrary
{
    private $data = null;
    private $_model = null;
    private $CI = null;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('material_issuance/Material_issuance_model', 'M_Material_issuance');
        $this->CI->load->library('MaterialIssuanceItemLibrary');
        $this->items_lib = new MaterialIssuanceItemLibrary();
    }

    public function get_obj($id, $with_relations = TRUE)
    {
        if ($with_relations) {
            $this->CI->load->helper('material_request');
            $this->CI->load->library('NoteLibrary');
            $note_lib = new NoteLibrary();
            $obj = $this->CI->M_Material_issuance
                ->with_material_request()
                ->with_warehouse()
                ->with_issued_to_others_staff()
                ->get($id);

            if (!$obj) {
                return false;
            }

            $notes = $note_lib->get_objs_by_parent('material_issuances', $id);
            if ($notes) {
                $obj['notes'] = $notes;
            } else {
                $obj['notes'] = [];
            }

            $items = $this->items_lib->get_objs_by_parent($id);
            $total = 0.0;
            foreach ($items as $idx => $item) {
                $subtotal = $item['material_receiving_item']['unit_cost'] * $item['quantity'];
                $total += $subtotal;
                $items[$idx]['subtotal'] = "$subtotal";
                $items[$idx]['unit_cost'] = $item['material_receiving_item']['unit_cost'];
            }
            $obj['issuance_items'] = $items;
            $obj['total'] = "$total";


            $obj['customer'] = array(
                'id' => null,
                'type' => '',
                'name' => '',
                'url' => ''
            );
            if ($obj['material_request_id']) {
                $customer_ref = mr_customer_type_ref($obj['material_request']['customer_type']);
                $customer = isset($obj['material_request'][$customer_ref['fk']]) ? get_object_from_table($obj['material_request'][$customer_ref['fk']], $customer_ref['table'], false, 'name ASC', null, null, 'id, name') : null;
                if ($customer) {
                    $obj['customer'] = array(
                        'id' => $customer['id'],
                        'type' => mr_customer_type_lookup($obj['material_request']['customer_type']),
                        'name' => $customer['name'],
                        'url' => site_url($customer_ref['controller'] . '/view/' . $customer['id']),
                    );
                }
            }

            $obj['expense_account'] = array(
                'id' => null,
                'name' => '',
                'url' => ''
            );
            if ($obj['material_request_id']) {
                if ($obj['material_request']['accounting_ledger_id']) {
                    $accounting_ledger = get_object_from_table($obj['material_request']['accounting_ledger_id'], 'accounting_ledgers', false, 'name ASC', null, null, 'id, name');
                    if ($accounting_ledger) {
                        $obj['expense_account'] = array(
                            'id' => $accounting_ledger['id'],
                            'name' => $accounting_ledger['name'],
                            'url' => site_url('accounting_ledgers/view/' . $accounting_ledger['id'])
                        );
                    }
                }
            }
        } else {
            $obj = $this->CI->M_Material_issuance->get($id);
        }

        return $obj;
    }

    public function search($params)
    {
        $id = isset($params['id']) ? $params['id'] : null;
        $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';
        $status_list = isset($params['status_list']) && !empty($params['status_list']) ? $params['status_list'] : null;

        if ($status_list) {
            $status_list_arr = explode(",", $status_list);
            $this->CI->M_Material_issuance->where('status', $status_list_arr);
        }

        $this->CI->M_Material_issuance->order_by('id', 'DESC');

        if (!$id) {

            $id = isset($params['id']) && !empty($params['id']) ? $params['id'] : null;
            if ($id) {
                $this->CI->M_Material_issuance->where('id', $id);
            }

            $issuance_type = isset($params['issuance_type']) && !empty($params['issuance_type']) ? $params['issuance_type'] : null;
            if ($issuance_type) {
                $this->CI->M_Material_issuance->where('issuance_type', $issuance_type);
            }

            $reference = isset($params['reference']) && !empty($params['reference']) ? $params['reference'] : null;
            if ($reference) {
                $this->CI->M_Material_issuance->like('reference', $reference, 'both');
            }

            $warehouse_id = isset($params['warehouse_id']) && !empty($params['warehouse_id']) ? $params['warehouse_id'] : null;
            if ($warehouse_id) {
                $this->CI->M_Material_issuance->where('warehouse_id', $warehouse_id);
            }

            $created_by = isset($params['created_by']) && !empty($params['created_by']) ? $params['created_by'] : null;
            if ($created_by) {
                $this->CI->M_Material_issuance->where('created_by', $created_by);
            }

            $created_at = isset($params['created_at']) && !empty($params['created_at']) ? $params['created_at'] : null;
            if ($created_at) {
                $this->CI->M_Material_issuance->where('created_at', $created_at);
            }

            $updated_by = isset($params['updated_by']) && !empty($params['updated_by']) ? $params['updated_by'] : null;
            if ($updated_by) {
                $this->CI->M_Material_issuance->where('updated_by', $updated_by);
            }

            $updated_at = isset($params['updated_at']) && !empty($params['updated_at']) ? $params['updated_at'] : null;
            if ($updated_at) {
                $this->CI->M_Material_issuance->where('updated_at', $updated_at);
            }

            $deleted_by = isset($params['deleted_by']) && !empty($params['deleted_by']) ? $params['deleted_by'] : null;
            if ($deleted_by) {
                $this->CI->M_Material_issuance->where('deleted_by', $deleted_by);
            }

            $deleted_at = isset($params['deleted_at']) && !empty($params['deleted_at']) ? $params['deleted_at'] : null;
            if ($deleted_at) {
                $this->CI->M_Material_issuance->where('deleted_at', $deleted_at);
            }

            $material_request_id = isset($params['material_request_id']) && !empty($params['material_request_id']) ? $params['material_request_id'] : null;
            if ($material_request_id) {
                $this->CI->M_Material_issuance->where('material_request_id', $material_request_id);
            }

            $item_id = isset($params['item_id']) && !empty($params['item_id']) ? $params['item_id'] : null;

            if ($item_id) {
                $this->CI->load->model('material_issuance_item/material_issuance_item_model', 'M_Material_issuance_item');
                $this->CI->load->model('material_receiving_item/material_receiving_item_model', 'M_Material_receiving_item');
                $receiving_items_ids = [];
                $issuance_items_ids = [];
                $receiving_items = $this->CI->M_Material_receiving_item->fields('id, item_id')->where('item_id', $item_id)->get_all();

                if (!$receiving_items) {
                    return [];
                }

                foreach ($receiving_items as $item) {
                    $receiving_items_ids[] = $item['id'];
                }

                if (sizeof($receiving_items_ids) > 0) {
                    $issuance_items = $this->CI->M_Material_issuance_item->fields('id, material_issuance_id')->where('material_receiving_item_id', $receiving_items_ids)->get_all();
                    foreach ($issuance_items as $item) {
                        $issuance_items_ids[] = $item['material_issuance_id'];
                    }
                    if (sizeof($issuance_items_ids) > 0) {
                        $this->CI->M_Material_issuance->where('id', $issuance_items_ids);
                    } else {
                        return [];
                    }
                } else {
                    return [];
                }
            }
        } else {
            $this->CI->M_Material_issuance->where('id', $id);
        }

        $this->CI->M_Material_issuance->order_by('id', 'DESC');

        if ($with_relations === 'yes') {
            $result = [];
            $ids = $this->CI->M_Material_issuance->fields('id')->get_all();
            if ($ids) {
                foreach ($ids as $__id) {
                    $row = $this->get_obj($__id);
                    array_push($result, $row);
                }
            }
        } else {
            $result = $this->CI->M_Material_issuance->get_all();
        }

        return $result;
    }

    public function pre_create($items)
    {

        $status = array(
            'status' => true,
            'errors' => []
        );
        foreach ($items as $item) {

            $issuances = $this->CI->db->where('material_receiving_item_id', $item['material_receiving_item_id'])->get('material_receiving_issuances')->row_array();
            $receiving_item = $this->CI->db->where('id', $item['material_receiving_item_id'])->get('material_receiving_items')->row_array();
            if ($issuances) {
                $stock = (float)$receiving_item['quantity'] - (float)$issuances['quantity'];
            } else {
                $stock = (float)$receiving_item['quantity'];
            }

            if ((float)$item['quantity'] > $stock) {
                $status['status'] = false;
                array_push($status['errors'], 'Insufficient quantity!');
            }
        }

        return $status;
    }

    public function create_master($data, $additional)
    {
        $request = $this->CI->db->insert('material_issuances', $data + $additional);
        if ($request) {
            $ref_id = $this->CI->db->insert_id();
        } else {
            return false;
        }

        return $ref_id;
    }

    public function update_master($id, $data, $additional)
    {
        $old_object = $this->CI->M_Material_issuance->get($id);
        if ($old_object) {
            return $this->CI->M_Material_issuance->update($data + $additional, $id);
        }
        return false;
    }
}
