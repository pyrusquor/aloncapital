<?php defined('BASEPATH') or exit('No direct script access allowed');

class Notify
{
    
    protected $CI;
    
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('session');
    }
    /**
     * success method
     *
     * The success method is used when you an operation or task was completed successfully (i.e) the task was completed with no error nor warning.
     *
     * @param string $msg Custom message you want to send to the notify (Message displayed to the toastr)
     *
     * @return bool
     */
    public function success($msg, $redirect = FALSE)
    {
        if($redirect){
            $this->CI->session->set_flashdata('success', $msg);
            // returns response when session is successfully set and redirect
            redirect($redirect);
        }else{
            $this->CI->session->set_flashdata('success', $msg);
            // returns response when session is successfully set
            return true;
        }
    }
    /**
     * error method
     *
     * The error method is used when you an operation or task was not completed successfully (i.e) the task was completed with an error.
     *
     * @param string $msg Custom message you want to send to the notify (Message displayed to the toastr)
     *
     * @return bool
     */
    public function error($msg, $redirect = FALSE)
    {
        if($redirect){
            $this->CI->session->set_flashdata('error', $msg);
            // returns response when session is successfully set and redirect
            redirect($redirect);
        }else{
            $this->CI->session->set_flashdata('error', $msg);
            // returns response when session is successfully set
            return true;
        }
    }
    /**
     * warning method
     *
     * The warning method is used when you an operation or task was not completed successfully (i.e) the task was completed with a warning.
     *
     * @param string $msg Custom message you want to send to the notify (Message displayed to the toastr)
     *
     * @return bool
     */
    public function warning($msg, $redirect = FALSE)
    {
        if($redirect){
            $this->CI->session->set_flashdata('warning', $msg);
            // returns response when session is successfully set and redirect
            redirect($redirect);
        }else{
            $this->CI->session->set_flashdata('warning', $msg);
            // returns response when session is successfully set
            return true;
        }
    }
    /**
     * info method
     *
     * The info method is used to send an important information to the user
     *
     * @param string $msg Custom message you want to send to the notify (Message displayed to the toastr)
     *
     * @return bool
     */
    public function info($msg, $redirect = FALSE)
    {
        if($redirect){
            $this->CI->session->set_flashdata('warning', $msg);
            // returns response when session is successfully set and redirect
            redirect($redirect);
        }else{
            $this->CI->session->set_flashdata('warning', $msg);
            // returns response when session is successfully set
            return true;
        }
    }
}
