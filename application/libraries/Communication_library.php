<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Communication_library
{
	private $_ci;
	private static $cache = array();

	function __construct($config = array())
	{
		$this->_ci =& get_instance();
		$models = array('app_global/app_global_model' => 'M_app_global');
		$this->_ci->load->model($models);
		
	}

	public static function send($number = '', $text= '')
	{
		$CI = &get_instance();

		$sms = $CI->M_app_global->where('slug', 'sms-api')->get();
		$key = $sms['value'];

		$text = strip_tags($text);
		
		$ch = curl_init();
		$parameters = array(
		    'apikey' => $key, //Your API KEY
		    'number' => $number,
		    'message' => $text,
		    'sendername' => 'PDP'
		);
		curl_setopt( $ch, CURLOPT_URL,'https://semaphore.co/api/v4/messages' );
		curl_setopt( $ch, CURLOPT_POST, 1 );

		//Send the parameters set above with the request
		curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $parameters ) );

		// Receive response from server
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		$output = curl_exec( $ch );
		curl_close ($ch);

		//Show the server response
		return $output;
	}

	public static function sms_account()
	{
		$CI = &get_instance();

		$sms = $CI->M_app_global->where('slug', 'sms-api')->get();
		$key = $sms['value'];

		$ch = curl_init();
		$parameters = array(
		    'apikey' => $key, //Your API KEY
		);

		$link = 'https://api.semaphore.co/api/v4/account';
		$getUrl = $link . '?' . http_build_query($parameters);

		curl_setopt( $ch, CURLOPT_URL, $getUrl );
		curl_setopt( $ch, CURLOPT_POST, 1 );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "GET");
		
		// // Receive response from server
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

		$output = curl_exec( $ch );

		curl_close ($ch);

		//Show the server response
		return $output;
	}



	public function load_fields()
	{

		$data['transactions'] = array(
			'{{ transactions:reference }}' => 'Reference ID',
		);
		
		$data['client'] = array(
			'{{ client:first_name }}' 		=> 'First Name',
			'{{ client:last_name }}' 		=> 'Last Name',
			'{{ client:middle_name }}' 		=> 'Middle Name',
			'{{ client:email }}' 			=> 'Email',
			'{{ client:address }}' 			=> 'Address',
			'{{ client:contact }}' 			=> 'Contact',
		);

		$data['seller'] = array(
			'{{ seller:first_name }}' 		=> 'First Name',
			'{{ seller:last_name }}' 		=> 'Last Name',
			'{{ seller:middle_name }}' 		=> 'Middle Name',
			'{{ seller:email }}' 			=> 'Email',
			'{{ seller:address }}' 			=> 'Address',
			'{{ seller:contact }}' 			=> 'Contact',
		);

		$data['billing'] = array(
			'{{ billing:reservation_rate }}'  				=> 'Reservation Rate',
			'{{ billing:reservation_date }}' 				=> 'Reservation Date',
			'{{ billing:downpayment_rate }}' 				=> 'Downpayment Rate',
			'{{ billing:downpayment_effectivity_date }}' 	=> 'Downpayment Effectivity Date',
			'{{ billing:monthly_dp }}'						=> 'Monthly Downpayment 1 Rate',
			'{{ billing:monthly_dp_2 }}'					=> 'Monthly Downpayment 2 Rate',
			'{{ billing:equity_rate }}' 					=> 'Equity Rate',
			'{{ billing:monthly_equity }}'					=> 'Monthly Equity Rate',
			'{{ billing:equity_effectivity_date }}' 		=> 'Equity Effectivity Date',
			'{{ billing:loan_rate }}' 						=> 'Loan Rate',
			'{{ billing:loan_effectivity_date }}'			=> 'Loan Effectivity Date',
			'{{ billing:monthly_loan }}'					=> 'Monthly Loan Rate',
		);

		$data['payment'] = array(
			'{{ payment:due_date }}'  						=> 'Due Date',
			'{{ payment:planned_total_amount }}' 			=> 'Amount Due',
			'{{ payment:planned_principal_amount }}' 		=> 'Principal Amount Due',
			'{{ payment:planned_interest_amount }}' 		=> 'Interest Amount Due',
		);

		$data['property'] = array(
			'{{ property:property_name }}' => 'Property Name',
			'{{ property:project_name }}' => 'Project Name',
			'{{ property:block }}' => 'Block',
			'{{ property:lot }}' => 'Lot',
			'{{ property:unit_type }}' => 'Unit Type',
			'{{ property:total_selling_price }}' => 'Total Selling Price',
			'{{ property:total_contract_price }}' => 'Total Contract Price',
		);


		$data['contact'] = array(
			'{{ contact:first_name }}' => 'First Name',
			'{{ contact:middle_name }}' => 'Middle Name',
			'{{ contact:last_name }}' => 'Last Name',
			'{{ contact:present_address }}' => 'Present Address',
			'{{ contact:mobile_no }}' => 'Mobile Number',
			'{{ contact:email }}' => 'Email',
		);

		
		return $data;
	}

	public function clean_key($key = ''){

		if ($key == "client") {
			$key = 'buyer';
		}

		if ($key == "address") {
			$key = 'present_address';
		}

		return $key;
	}

	public function process_array($clean_key = false, $results = array(), $clean_val = false){
		if($clean_key) {
			$rows = array_key_exists_r($clean_key, $results, $clean_val);
		} else {
			$rows = array_key_exists_r($clean_val, $results);
		}

		if (isset($rows[$clean_key][$clean_val])) {
			$value = $rows[$clean_key][$clean_val];
		} elseif(isset($rows[$clean_val])) {
			$value = $rows[$clean_val];
		} else {
			$value = @$rows[$clean_key][0][$clean_val];
		}
		
		return $value;
	}

	public function send_group($id = 0) {

   		$CI = &get_instance();
   		$recipients = '';
		$models = array('communication/Communication_group_model' => 'M_comm_group','Communication_model' => 'M_communication','Communication_templates/Communication_template_model' => 'M_comm_template','communication/Communication_setting_model' => 'M_comm_setting', 'transaction/Transaction_model' => 'M_transaction', 'contacts/Contact_model' => 'M_contact');
		$data = $this->load_fields();
		$CI->load->model($models);

		$group = 			$CI->M_comm_group
							->with_setting()->with_template()
							->with_communication()->order_by('id','DESC')
							->get($id);

		if($group){
			$recipients = $group['communication'];
			$template = $group['template'];
		}

		if ($recipients) {
			# code...
			$flag = TRUE;
			foreach ($recipients as $key => $recipient) {
				# code...			
				$number = $recipient['recipient_mobile'];
				$recipient_id = $recipient['recipient_id'];
				$fname = get_value_field($recipient_id,plural($recipient['recipient_table']),'first_name');
				$lname = get_value_field($recipient_id,plural($recipient['recipient_table']),'last_name');
				$fullname = $fname.' '.$lname; 
				$info['name'] = $fullname;
				$info['email'] = $recipient['recipient_address'];
				$info['subject'] = $template['description'];

				if($recipient['recipient_table'] == 'buyer') {
					$transactions = $CI->M_transaction
						->with_buyer()->with_seller()->with_project()->with_property()->with_scheme()
						->with_t_property()->with_billings()->with_buyers()
						->with_sellers()->with_fees()->with_promos()->with_payments()->with_payments()
						->where('buyer_id', $recipient_id)
						->get_all();
						
				} elseif($recipient['recipient_table'] == 'seller') {
					$transactions = $CI->M_transaction
						->with_buyer()->with_seller()->with_project()->with_property()->with_scheme()
						->with_t_property()->with_billings()->with_buyers()
						->with_sellers()->with_fees()->with_promos()->with_payments()->with_payments()
						->where('seller_id', $recipient_id)
						->get_all();
				} else {
					$contact = $CI->M_contact->where('id', $recipient_id)->get();
				}

				if($recipient['recipient_table'] == 'buyer' || $recipient['recipeint_table'] == 'seller') {
					if ($transactions) {
						// Check if buyer/seller has transactions
						// Loop to get each transaction detail for content field
						foreach($transactions as $key => $data) {
	
							$buyer_id = $data['buyer_id'];			
							$seller_id = $data['seller_id'];			
							$project_id = $data['project_id'];			
							$property_id = $data['property_id'];		
			
							if(!empty($template['content'])) {
								$content = $template['content'];
								preg_match_all("/{{{{+(.*?)}}}}/",$content, $fields_array);
								
								$selected_fields = array();
								$last = count($fields_array);
					
								unset($fields_array[$last-1]);
	
								foreach($fields_array[0] as $key => $value) {
					
									if(!empty($value)){
										if (!preg_match('/clients/',$value)){
											
											$value = str_replace('{{{{','',$value);
											$value = str_replace('}}}}','',$value);
											$def_val = $value;
											$new_val = explode(':', $def_val);
											$selected_fields[] = $new_val;
										}
									}
								}
								$results = $data;
								
								foreach($selected_fields as $key => $selected_field){
									
									$default_field = '{{{{'.$selected_field[0].':'.$selected_field[1].'}}}}'; 
					
									$value = "n/a";
																	
									$clean_val = $this->clean_key(trim($selected_field[1]));
									$clean_key = $this->clean_key(trim($selected_field[0]));
									$value = $this->process_array($clean_key, $results, $clean_val);
									
									if($value == "n/a" ){ 
										$clean_key = $this->clean_key(trim($selected_field[0])); 
										$value = $this->process_array($clean_key, $results, $clean_val);
									}
	
									if($clean_key == "additional_information"){
					
										$value = generate($recipient_id,$clean_val);
										
									}
													
									if(!$value){ $value = 'n/a'; }
					
									$content = str_replace($default_field, "<u><b>".$value."</b></u>", $content);
									$email_content = $content;
									$sms_content = strip_tags($content);
								}
							}
	
							if($group['medium_type'] == 'sms') {
								$return = $this->send($number, $sms_content);
							} else if($group['medium_type'] == 'email') {
								$return = $this->send_email($info, $email_content);
							} else {
								$return = $this->send($number, $sms_content);
								$return = $this->send_email($info, $email_content);
							}
							
							$comm['status'] = 1;
							$comm['is_sent'] = 1;
							$comm['date_sent'] = NOW;
							$CI->M_communication->update($comm,$recipient['id']);
						}
					} else {		
						if(!empty($template['content'])) {
							$content = $template['content'];
							preg_match_all("/{{{{+(.*?)}}}}/",$content, $fields_array);
							
							$selected_fields = array();
							$last = count($fields_array);
				
							unset($fields_array[$last-1]);

							foreach($fields_array[0] as $key => $value) {
				
								if(!empty($value)){
									if (!preg_match('/clients/',$value)){
										
										$value = str_replace('{{{{','',$value);
										$value = str_replace('}}}}','',$value);
										$def_val = $value;
										$new_val = explode(':', $def_val);
										$selected_fields[] = $new_val;
									}
								}
							}
							$results = $data;
							
							foreach($selected_fields as $key => $selected_field){
								
								$default_field = '{{{{'.$selected_field[0].':'.$selected_field[1].'}}}}'; 
				
								$value = "n/a";
																
								$clean_val = $this->clean_key(trim($selected_field[1]));
								$clean_key = $this->clean_key(trim($selected_field[0]));
								$value = $this->process_array(false, $results, $clean_val);
								
								if($value == "n/a" ){ 
									$clean_key = $this->clean_key(trim($selected_field[0])); 
									$value = $this->process_array($clean_key, $results, $clean_val);
								}

								if($clean_key == "additional_information"){
				
									$value = generate($recipient_id,$clean_val);
									
								}

								if(!$value){ $value = 'n/a'; }
				
								$content = str_replace($default_field, "<u><b>".$value."</b></u>", $content);
								$email_content = $content;
								$sms_content = strip_tags($content);
							}
						}

						if($group['medium_type'] == 'sms') {
							$return = $this->send($number, $sms_content);
						} else if($group['medium_type'] == 'email') {
							$return = $this->send_email($info, $email_content);
						} else {
							$return = $this->send($number, $sms_content);
							$return = $this->send_email($info, $email_content);
						}
						
						$comm['status'] = 1;
						$comm['is_sent'] = 1;
						$comm['date_sent'] = NOW;
						$CI->M_communication->update($comm,$recipient['id']);
					}
				}

				if($contact) {
					if(!empty($template['content'])) {
						$content = $template['content'];
						preg_match_all("/{{{{+(.*?)}}}}/",$content, $fields_array);
						
						$selected_fields = array();
						$last = count($fields_array);
			
						unset($fields_array[$last-1]);

						foreach($fields_array[0] as $key => $value) {
			
							if(!empty($value)){
								if (!preg_match('/clients/',$value)){
									
									$value = str_replace('{{{{','',$value);
									$value = str_replace('}}}}','',$value);
									$def_val = $value;
									$new_val = explode(':', $def_val);
									$selected_fields[] = $new_val;
								}
							}
						}
						$results = $contact;
						
						foreach($selected_fields as $key => $selected_field){
							
							$default_field = '{{{{'.$selected_field[0].':'.$selected_field[1].'}}}}'; 
			
							$value = "n/a";
															
							$clean_val = $this->clean_key(trim($selected_field[1]));
							$clean_key = $this->clean_key(trim($selected_field[0]));
							$value = $this->process_array(false, $results, $clean_val);
							
							if($value == "n/a" ){ 
								$clean_key = $this->clean_key(trim($selected_field[0])); 
								$value = $this->process_array($clean_key, $results, $clean_val);
							}

							if($clean_key == "additional_information"){
			
								$value = generate($recipient_id,$clean_val);
								
							}

							if(!$value){ $value = 'n/a'; }
			
							$content = str_replace($default_field, "<u><b>".$value."</b></u>", $content);
							$email_content = $content;
							$sms_content = strip_tags($content);
						}
					}

					if($group['medium_type'] == 'sms') {
						$return = $this->send($number, $sms_content);
					} else if($group['medium_type'] == 'email') {
						$return = $this->send_email($info, $email_content);
					} else {
						$return = $this->send($number, $sms_content);
						$return = $this->send_email($info, $email_content);
					}
				}
			}

			if($flag) {
				$CI->M_comm_group->update(['status' => 1], $group['id']);
			}
			return $flag;
		}
	}

	// public static function send_email($recipient = '', $text = '') {
	// 	$CI = &get_instance();
	// 	// require 'vendor/autoload.php'; // If you're using Composer (recommended)
	// 	// Comment out the above line if not using Composer
	// 	require "assets/vendors/general/sendgrid-php/sendgrid-php.php";
	// 	// If not using Composer, uncomment the above line and
	// 	// download sendgrid-php.zip from the latest release here,
	// 	// replacing <PATH TO> with the path to the sendgrid-php.php file,
	// 	// which is included in the download:
	// 	// https://github.com/sendgrid/sendgrid-php/releases

	// 	$_sendgrid = $CI->M_app_global->where('slug', 'sendgrid-api')->get();
	// 	$_name = $CI->M_app_global->where('slug', 'sender-name')->get();
	// 	$_email = $CI->M_app_global->where('slug', 'sender-email')->get();

	// 	$apikey = $_sendgrid['value'];
	// 	$_name = $_name['value'];
	// 	$_email = $_email['value'];

	// 	$email = new \SendGrid\Mail\Mail(); 
	// 	$email->setFrom($_email, $_name); // Edit later on
	// 	$email->setSubject($recipient['subject']);
	// 	$email->addTo($recipient['email'], $recipient['name']);
	// 	$email->addContent("text/html", $text);
		
	// 	$sendgrid = new \SendGrid($apikey);
	// 	$sendgrid->send($email);
	// 	// $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
	// 	// try {
	// 	// 	$response = $sendgrid->send($email);
	// 	// 	print $response->statusCode() . "\n";
	// 	// 	print_r($response->headers());
	// 	// 	print $response->body() . "\n";
	// 	// } catch (Exception $e) {
	// 	// 	echo 'Caught exception: '. $e->getMessage() ."\n";
	// 	// }
		
	// }


	public static function send_email($recipient = '', $text = ''){
		$CI = &get_instance();


   		// $_sendgrid = $CI->M_app_global->where('slug', 'sendgrid-api')->get();
		// $_name = $CI->M_app_global->where('slug', 'sender-name')->get();
		// $_email = $CI->M_app_global->where('slug', 'sender-email')->get();

        // $apikey = $_sendgrid['value'];
        // $_name = $_name['value'];
        // $_email = $_email['value'];

        $contact_email = "info@aloncapital.ph";
        $company_name = "Alon Capital PH";

        $CI->load->library('email');

        // $CI->email->initialize(array(
        //   'protocol' => 'smtp',
        //   'smtp_host' => 'smtp.sendgrid.net',
        //   'smtp_user' => 'apikey',
        //   'smtp_pass' => $apikey,
        //   'smtp_port' => 587,
        //   'crlf' => "\r\n",
        //   'newline' => "\r\n"
        // ));

        $CI->email->set_mailtype('html');
        
        $CI->email->from($contact_email, $company_name);
        $CI->email->to($recipient['email']); 
        $CI->email->bcc('info@aloncapital.ph'); 
        $CI->email->bcc('charleskennethhernandez@gmail.com'); 
        $CI->email->subject($recipient['subject']);
        $CI->email->message($text);
        $result = $CI->email->send();

        return($result);
        
        // echo $this->email->print_debugger();

    }
}