<?php


    class PurchaseOrderItemLibrary
    {
        private $data = null;
        private $CI = null;

        public function __construct()
        {
            $this->CI = &get_instance();
            $this->CI->load->model('purchase_order_item/Purchase_order_item_model', 'M_Purchase_order_item');
        }

        public function get_obj($id, $with_relations = TRUE)
        {
            if ($with_relations) {
                $obj = $this->CI->M_Purchase_order_item
                    ->with_item_group()
                    ->with_item_type()
                    ->with_item_brand()
                    ->with_item_class()
                    ->with_item()
                    ->with_unit_of_measurement()
                    ->with_supplier()
                    ->with_material_request()
                    ->with_purchase_order()
                    ->with_purchase_order_request_item()
                    ->with_purchase_order_request()
                    ->with_warehouse()
                    ->get($id);
            } else {
                $obj = $this->CI->M_Purchase_order_item->get($id);
            }

            return $obj;
        }

        public function get_objs_by_parent($parent_id, $with_relations = TRUE)
        {
            if ($with_relations) {
                $obj = $this->CI->M_Purchase_order_item
                    ->where('purchase_order_id', $parent_id)
                    ->with_item_group()
                    ->with_item_type()
                    ->with_item_brand()
                    ->with_item_class()
                    ->with_item()
                    ->with_unit_of_measurement()
                    ->with_supplier()
                    ->with_material_request()
                    ->with_purchase_order()
                    ->with_purchase_order_request_item()
                    ->with_purchase_order_request()
                    ->with_warehouse()
                    ->get_all();
            } else {
                $obj = $this->CI->M_Purchase_order_item->where('purchase_order_id', $parent_id)->get_all();
            }

            return $obj;
        }

        public function __search($params)
        {
            $id = isset($params['id']) ? $params['id'] : null;
            $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';
            $status_list = isset($params['status_list']) && !empty($params['status_list']) ? $params['status_list'] : null;

            if ($status_list) {
                $status_list_arr = explode(",", $status_list);
                $this->CI->M_Purchase_order_item->where('status', $status_list_arr);
            }

            $this->CI->M_Purchase_order_item->order_by('id', 'DESC');

            if (!$id) {

                $id = isset($params['id']) && !empty($params['id']) ? $params['id'] : null;
                if ($id) {
                    $this->CI->M_Purchase_order_item->where('id', $id);
                }

                $created_by = isset($params['created_by']) && !empty($params['created_by']) ? $params['created_by'] : null;
                if ($created_by) {
                    $this->CI->M_Purchase_order_item->where('created_by', $created_by);
                }

                $created_at = isset($params['created_at']) && !empty($params['created_at']) ? $params['created_at'] : null;
                if ($created_at) {
                    $this->CI->M_Purchase_order_item->where('created_at', $created_at);
                }

                $updated_by = isset($params['updated_by']) && !empty($params['updated_by']) ? $params['updated_by'] : null;
                if ($updated_by) {
                    $this->CI->M_Purchase_order_item->where('updated_by', $updated_by);
                }

                $updated_at = isset($params['updated_at']) && !empty($params['updated_at']) ? $params['updated_at'] : null;
                if ($updated_at) {
                    $this->CI->M_Purchase_order_item->where('updated_at', $updated_at);
                }

                $deleted_by = isset($params['deleted_by']) && !empty($params['deleted_by']) ? $params['deleted_by'] : null;
                if ($deleted_by) {
                    $this->CI->M_Purchase_order_item->where('deleted_by', $deleted_by);
                }

                $deleted_at = isset($params['deleted_at']) && !empty($params['deleted_at']) ? $params['deleted_at'] : null;
                if ($deleted_at) {
                    $this->CI->M_Purchase_order_item->where('deleted_at', $deleted_at);
                }

                $purchase_order_id = isset($params['purchase_order_id']) && !empty($params['purchase_order_id']) ? $params['purchase_order_id'] : null;
                if ($purchase_order_id) {
                    $this->CI->M_Purchase_order_item->where('purchase_order_id', $purchase_order_id);
                }

                $material_receiving_item_id = isset($params['material_receiving_item_id']) && !empty($params['material_receiving_item_id']) ? $params['material_receiving_item_id'] : null;
                if ($material_receiving_item_id) {
                    $this->CI->M_Purchase_order_item->where('material_receiving_item_id', $material_receiving_item_id);
                }

                $quantity = isset($params['quantity']) && !empty($params['quantity']) ? $params['quantity'] : null;
                if ($quantity) {
                    $this->CI->M_Purchase_order_item->where('quantity', $quantity);
                }


            } else {
                $this->CI->M_Purchase_order_item->where('id', $id);
            }

            $this->CI->M_Purchase_order_item->order_by('id', 'DESC');

            if ($with_relations === 'yes') {
                $result = $this->CI->M_Purchase_order_item
                    ->with_purchase_order()
                    ->with_material_receiving_item()
                    ->get_all();
            } else {
                $result = $this->CI->M_Purchase_order_item->get_all();
            }

            return $result;
        }

        public function __create_master($data, $additional)
        {

            if (property_exists($this->CI->M_Purchase_order_item, 'form_fillables')) {
                $fillables = $this->CI->M_Purchase_order_item->form_fillables;
            } else {
                $fillables = $this->CI->M_Purchase_order_item->fillable;
            }

            $item = [];

            foreach ($fillables as $field) {
                if (key_exists($field, $data)) {
                    $item[$field] = $data[$field];
                }
            }

            $request = $this->CI->db->insert('purchase_order_items', $item + $additional);
            if ($request) {
                $ref = $this->CI->db->insert_id();
            } else {
                return false;
            }

            return $ref;
        }

        public function __update_master($id, $data, $additional)
        {
            $old_object = $this->CI->M_Purchase_order_item->get($id);
            return $this->CI->M_Purchase_order_item->update($data + $additional, $id);
        }


    }