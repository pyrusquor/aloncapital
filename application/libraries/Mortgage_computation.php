<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	error_reporting(E_ALL);

class Mortgage_computation {

	public $period_term;
	
	public $effectivity_date;
	
	public $interest_rate;

	public $period_amount;

	public $property_value;

	public $factor_rate;

	public $init_ma;

	public function reconstruct( $billing = array(), $property_value = 0, $monthly_payment = 0 ) {

		$this->period_term = $billing['period_term'];
		
		$this->effectivity_date = $billing['effectivity_date'];
		
		$this->interest_rate = $billing['interest_rate'];

		$this->period_amount = $billing['period_amount'];

		$this->property_value = $property_value;

		$this->factor_rate = 2;

		$this->init_ma = $monthly_payment;

	}

	/* INFORMATION SUMMARY START */
	
	public function monthly_payment() {
		
		if ($this->init_ma) {
			return $this->init_ma;
		}

		if ($this->period_term == 1) {

			return $this->period_amount;	
					
		} else {

			if ( $this->interest_rate != "0.00" ) {

				return  ( $this->compute_percentage( $this->interest_rate ) / 12 * $this->period_amount ) / ( 1 - pow( ( 1 + $this->compute_percentage( $this->interest_rate ) / 12 ) , - ( $this->period_term ) ) );

			} else {

				return  $this->period_amount / $this->period_term;

			}

		}
	
	}
	
	public function total_interest() {
	
		return ( $this->monthly_payment() * $this->period_term - $this->period_amount );
	
	}
	
	public function recommended_monthly_income() {
	
		return round ( $this->monthly_payment() / 0.4,0 );
	
	}
	/* INFORMATION SUMMARY END */
	
	/* NO DOWNPAYMENT START */
	public function nd_monthly_payment() {
	
		return  ( $this->compute_percentage( $this->interest_rate ) / 12 * $this->property_value ) / ( 1 - pow( ( 1 + $this->compute_percentage( $this->interest_rate ) / 12 ) , - ( $this->period_term ) ) );
	
	}
	
	public function nd_total_monthly_payment() {
	
		return $this->nd_monthly_payment() * $this->period_term;
	
	}
	
	public function nd_total_interest() {
	
		return $this->nd_monthly_payment() * $this->period_term - $this->property_value;
	
	}
	/* NO DOWNPAYMENT END */
	
	/* CURRENT DOWNPAYMENT START */
	public function cd_total_monthly_payment() {
	
		return  $this->monthly_payment() * $this->period_term;
	
	}
	
	public function cd_total_interest() {
	
		return $this->monthly_payment() * $this->period_term - $this->period_amount;
	
	}
	/* CURRENT DOWNPAYMENT END */
	
	public function total_interest_savings() {
	
		return $this->nd_total_interest() - $this->cd_total_interest();
	
	}
	
	/* SAVINGS ON DOWNPAYMENT END */
	
	/* GRAPH START */
	public function g_total_monthly_payment() {
	
		return $this->nd_monthly_payment() * $this->period_term;
	
	}
	
	public function g_total_interest() {
	
		return $this->nd_total_interest();
	
	}
	
	public function g_total_principal() {
	
		return $this->g_total_monthly_payment() - $this->g_total_interest();
	
	}
	/* GRAPH END */
	
	/* PAYMENT BREAKDOWN START */
	
	public function breakdown() {
	
		$i = 0;
		
		$x = 1;
		
		$sum_amount = 0;
		
		$breakdown = array();
		
		$date = $this->effectivity_date;

		for( $i = 1; $i <= ( $this->period_term ); $i++) {
		
			/* MONTHLY BREAKDOWN START */
			$breakdown['monthly'][$i]['month'] = $i;
		
			if( $i == 1 ) {

				$breakdown['monthly'][$i]['beginning_balance'] = round( $this->period_amount, $this->factor_rate );
				
			} else {

				$breakdown['monthly'][$i]['beginning_balance'] = round( $breakdown['monthly'][($i - 1)]['ending_balance'], $this->factor_rate );
				
			}

			$interest = round( $breakdown['monthly'][$i]['beginning_balance'] * $this->compute_percentage( $this->interest_rate ) / 12, $this->factor_rate );
			
			$breakdown['monthly'][$i]['interest'] = $interest;

			$sum_amount = $sum_amount + $breakdown['monthly'][$i]['interest'];
			
			$principal = round( $this->monthly_payment() - $breakdown['monthly'][$i]['interest'], $this->factor_rate, PHP_ROUND_HALF_UP);

			$breakdown['monthly'][$i]['principal'] = $principal;
				
			$breakdown['monthly'][$i]['ending_balance'] = round( $breakdown['monthly'][$i]['beginning_balance'] - $principal, $this->factor_rate );

			$due_date = add_months_to_date($date, ($i-1));
			$breakdown['monthly'][$i]['due_date'] = $due_date;

			$next_payment_date = add_months_to_date($due_date, 1);
			$breakdown['monthly'][$i]['next_payment_date'] = $next_payment_date;
			/* MONTHLY BREAKDOWN END */
			
			/* YEARLY BREAKDOWN START */
			if( ( $i % 12 ) == 0 ) {
			
				$breakdown['yearly'][$x]['year'] = $i / 12;
					
				if( ( $i / 12 ) == 1 ) {

					$breakdown['yearly'][$x]['beginning_balance'] = round( $this->period_amount, 8 );
					
				} else {
					
					$breakdown['yearly'][$x]['beginning_balance'] = round( $breakdown['yearly'][$x  - 1]['ending_balance'], $this->factor_rate );

				}
					
				$breakdown['yearly'][$x]['interest'] = $sum_amount;
						
				$breakdown['yearly'][$x]['principal'] = $this->monthly_payment() * 12 - $breakdown['yearly'][$x]['interest'];
						
				$breakdown['yearly'][$x]['ending_balance'] = round( $breakdown['yearly'][$x]['beginning_balance'] - $breakdown['yearly'][$x]['principal'], $this->factor_rate );
					
				$sum_amount = 0;
				
				$x++;
				
			}
			/* YEARLY BREAKDOWN END */
			
			if ($breakdown['monthly'][$i]['ending_balance'] < 0) {
				break;
			}
		
		}
		
		return $breakdown;
	
	}
	
	/* PAYMENT BREAKDOWN END */
	
	
	private function compute_percentage( $value = 0 ) {
	
		if( $value ) {

			return $value / 100;

		}

	}

}

?>