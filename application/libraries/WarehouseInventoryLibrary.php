<?php


    class WarehouseInventoryLibrary
    {
        private $data = null;
        private $_model = null;
        private $CI = null;

        public function __construct()
        {
            $this->CI = &get_instance();
            $this->CI->load->model('warehouse_inventory/warehouse_inventory_model', 'M_Warehouse_inventory');
            $this->CI->load->library('MaterialReceivingLibrary');
            $this->_receiving = new MaterialReceivingLibrary();
        }

        public function get_obj($id, $with_relations = TRUE)
        {
            if ($with_relations) {
                $obj = $this->CI->M_Warehouse_inventory
                    ->with_item()
                    ->with_unit_of_measurement()
                    ->with_warehouse()
                    ->get($id);
            } else {
                $obj = $this->CI->M_Warehouse_inventory->get($id);
            }
            return $obj;
        }

        public function get_item($item_id, $warehouse_id = NULL, $with_relations = TRUE)
        {
            $this->CI->M_Warehouse_inventory->where('item_id', $item_id);
            if ($warehouse_id) {
                $this->CI->M_Warehouse_inventory->where('warehouse_id', $warehouse_id);
            }
            if ($with_relations) {
                $obj = $this->CI->M_Warehouse_inventory
                    ->with_item()
                    ->with_unit_of_measurement()
                    ->with_warehouse()
                    ->get_all();
            } else {
                $obj = $this->CI->M_Warehouse_inventory->get_all();
            }

            return $obj;
        }

        public function search($params)
        {
            $id = isset($params['id']) ? $params['id'] : null;
            $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';
            $status_list = isset($params['status_list']) && !empty($params['status_list']) ? $params['status_list'] : null;

            if ($status_list) {
                $status_list_arr = explode(",", $status_list);
                $this->CI->M_Warehouse_inventory->where('status', $status_list_arr);
            }

            $this->CI->M_Warehouse_inventory->order_by('id', 'DESC');

            if (!$id) {

                $actual_quantity = isset($params['actual_quantity']) && !empty($params['actual_quantity']) ? $params['actual_quantity'] : null;
                if ($actual_quantity) {
                    $this->CI->M_Warehouse_inventory->where('actual_quantity', $actual_quantity);
                }

                $available_quantity = isset($params['available_quantity']) && !empty($params['available_quantity']) ? $params['available_quantity'] : null;
                if ($available_quantity) {
                    $this->CI->M_Warehouse_inventory->where('available_quantity', $available_quantity);
                }

                $created_at = isset($params['created_at']) && !empty($params['created_at']) ? $params['created_at'] : null;
                if ($created_at) {
                    $this->CI->M_Warehouse_inventory->where('created_at', $created_at);
                }

                $created_by = isset($params['created_by']) && !empty($params['created_by']) ? $params['created_by'] : null;
                if ($created_by) {
                    $this->CI->M_Warehouse_inventory->where('created_by', $created_by);
                }

                $deleted_at = isset($params['deleted_at']) && !empty($params['deleted_at']) ? $params['deleted_at'] : null;
                if ($deleted_at) {
                    $this->CI->M_Warehouse_inventory->where('deleted_at', $deleted_at);
                }

                $deleted_by = isset($params['deleted_by']) && !empty($params['deleted_by']) ? $params['deleted_by'] : null;
                if ($deleted_by) {
                    $this->CI->M_Warehouse_inventory->where('deleted_by', $deleted_by);
                }

                $id = isset($params['id']) && !empty($params['id']) ? $params['id'] : null;
                if ($id) {
                    $this->CI->M_Warehouse_inventory->where('id', $id);
                }

                $item_id = isset($params['item_id']) && !empty($params['item_id']) ? $params['item_id'] : null;
                if ($item_id) {
                    $this->CI->M_Warehouse_inventory->where('item_id', $item_id);
                }

                $unit_price = isset($params['unit_price']) && !empty($params['unit_price']) ? $params['unit_price'] : null;
                if ($unit_price) {
                    $this->CI->M_Warehouse_inventory->where('unit_price', $unit_price);
                }

                $unit_of_measurement_id = isset($params['unit_of_measurement_id']) && !empty($params['unit_of_measurement_id']) ? $params['unit_of_measurement_id'] : null;
                if ($unit_of_measurement_id) {
                    $this->CI->M_Warehouse_inventory->where('unit_of_measurement_id', $unit_of_measurement_id);
                }

                $updated_at = isset($params['updated_at']) && !empty($params['updated_at']) ? $params['updated_at'] : null;
                if ($updated_at) {
                    $this->CI->M_Warehouse_inventory->where('updated_at', $updated_at);
                }

                $updated_by = isset($params['updated_by']) && !empty($params['updated_by']) ? $params['updated_by'] : null;
                if ($updated_by) {
                    $this->CI->M_Warehouse_inventory->where('updated_by', $updated_by);
                }

                $warehouse_id = isset($params['warehouse_id']) && !empty($params['warehouse_id']) ? $params['warehouse_id'] : null;
                if ($warehouse_id) {
                    $this->CI->M_Warehouse_inventory->where('warehouse_id', $warehouse_id);
                }


            } else {
                $this->CI->M_Warehouse_inventory->where('id', $id);
            }

            $this->CI->M_Warehouse_inventory->order_by('id', 'DESC');

            if ($with_relations === 'yes') {
                $result = $this->CI->M_Warehouse_inventory
                    ->with_item()
                    ->with_unit_of_measurement()
                    ->with_warehouse()
                    ->get_all();
            } else {
                $result = $this->CI->M_Warehouse_inventory->get_all();
            }

            return $result;
        }

        public function create_master($data, $additional)
        {
            /*
            $request = $this->CI->db->insert('warehouse_inventories', $data + $additional);
            if ($request) {
                return $this->CI->db->insert_id();
            } else {
                return false;
            }
            - create material receiving with 1 item
            */

            $receiving = array(
                'status' => 2,
                'purchase_order_id' => null,
                'warehouse_id' => $data['warehouse_id'],
                'supplier_id' => null,
                'accounting_ledger_id' => null,
                'freight_amount' => 0,
                'landed_cost' => 0,
                'total_cost' => 0,
                'input_tax' => 0,
                'receiving_type' => 1,
            );
            $receiving_id = $this->_receiving->create_master($receiving, $additional);
            $unit_cost = $data['unit_price'] * 1.0;
            $total_cost = ($data['actual_quantity'] * 1.0) * $unit_cost;
            if ($receiving_id) {
                $references = array('material_receiving_id' => $receiving_id);
                $receiving_item = array(
                    'supplier_id' => null,
                    'material_request_id' => null,
                    'purchase_order_id' => null,
                    'purchase_order_item_id' => null,
                    'purchase_order_request_item_id' => null,
                    'purchase_order_request_id' => null,
                    'warehouse_id' => $data['warehouse_id'],
                    'item_group_id' => null,
                    'item_type_id' => null,
                    'item_brand_id' => null,
                    'item_class_id' => null,
                    'item_id' => $data['item_id'],
                    'unit_of_measurement_id' => $data['unit_of_measurement_id'],
                    'item_brand_name' => null,
                    'item_class_name' => null,
                    'item_name' => null,
                    'unit_of_measurement_name' => null,
                    'item_group_name' => null,
                    'item_type_name' => null,
                    'unit_cost' => $unit_cost,
                    'total_cost' => $total_cost,
                    'quantity' => $data['actual_quantity'],
                    'status' => 1,
                );
                $receiving_item_id = $this->_receiving->create_slave($receiving_item, $references, $additional);

                if ($receiving_item_id) {
                    $this->CI->load->library('MaterialMovementLibrary');
                    $movement = new MaterialMovementLibrary();
                    $movement->init('direct_in', $receiving_id, array('time' => $additional['created_at'], 'actor' => $additional['created_by']));
                    $movement->move();
                    return true;
                }
            }

        }

        public function update_master($id, $data, $additional)
        {
            $this->CI->load->library('MaterialReceivingItemLibrary');
            $ri_lib = new MaterialReceivingItemLibrary();

            foreach ($data as $item) {
                $obj = $this->get_obj($id);
                $item['quantity'] = $item['quantity'] * 1.0;
                $mr_item = $ri_lib->get($item['material_receiving_item_id']);
                $mr_item_id = $mr_item['id'];
                $mr_item['stock_data']['remaining'] = $mr_item['stock_data']['remaining'] * 1.0;
                if ($item['quantity'] == $mr_item['stock_data']['remaining']) {
                    break;
                }
                if (array_key_exists('issuance', $mr_item)) {
                    $issuance_qty = $mr_item['issuance']['quantity'] * 1.0;
                } else {
                    $issuance_qty = 0.0;
                }

                unset($mr_item['id']);

                $log = array(
                    'created_at' => $additional['updated_at'],
                    'created_by' => $additional['updated_by'],
                    'material_receiving_id' => null,
                    'warehouse_id' => $mr_item['warehouse_id'],
                    'warehouse_inventory_id' => $id,
                    'sub_warehouse_id' => null,
                    'sub_warehouse_inventory_id' => null,
                    'virtual_warehouse_id' => null,
                    'virtual_warehouse_inventory_id' => null,
                    'item_id' => $mr_item['item_id'],
                    'unit_of_measurement_id' => $mr_item['unit_of_measurement_id'],
                    'unit_price' => $mr_item['unit_cost'],
                    'remarks' => 'Update',
                    'transaction_type' => 5
                );

                if ($item['quantity'] > $mr_item['quantity']) {
                    $diff = $item['quantity'] - $mr_item['quantity'];
                    $available_quantity = $obj['available_quantity'] + $diff;
                    $actual_quantity = $obj['actual_quantity'] + $diff;

                    // log
                    $log['actual_quantity_in'] = $diff;
                    $log['available_quantity_in'] = $diff;
                    $log['actual_quantity_out'] = 0.0;
                    $log['available_quantity_out'] = 0.0;
                } else {
                    $diff = $mr_item['quantity'] - $item['quantity'];
                    $available_quantity = $obj['available_quantity'] - $diff;
                    $actual_quantity = $obj['actual_quantity'] - $diff;

                    // log
                    $log['actual_quantity_in'] = 0.0;
                    $log['available_quantity_in'] = 0.0;
                    $log['actual_quantity_out'] = $diff;
                    $log['available_quantity_out'] = $diff;
                }

                $log['current_actual_quantity'] = $actual_quantity;
                $log['current_available_quantity'] = $available_quantity;

                $mr_item['quantity'] = $item['quantity'];
                $mr_item['total_cost'] = $mr_item['quantity'] * $mr_item['unit_cost'];
                $ri_lib->update_master($mr_item_id, $mr_item, $additional);

                $new_obj = array(
                    'available_quantity' => $available_quantity,
                    'actual_quantity' => $actual_quantity
                );

                $update = $this->CI->M_Warehouse_inventory->update($new_obj + $additional, $id);

                $this->CI->db->insert('warehouse_inventory_logs', $log);

            }

            return true;

        }

    }