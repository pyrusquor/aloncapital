<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trail_library {

	// 
	public function initiate($affected_id) {
   		$CI = &get_instance();
		
		$models = array('audit_trail/Audit_trail_model' => 'M_audit');

		// Load models
		$CI->load->model($models);

		$this->affected_id = $affected_id;
	}


	public function get_record($table = 0, $id = 0){
   		$CI = &get_instance();
   		$params['affected_id'] = $this->affected_id;
   		$params['affected_table'] = $table;
        $data = '';

   		if ($params) {
   		    $data = $CI->M_audit->with_user()->where($params)->get_all();
        }
		return $data;
	}
}

?>