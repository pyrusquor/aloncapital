<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Transaction_library
{

    public $period_term;
    public $collections;

    public function initiate($transaction_id = 0)
    {
        $CI = &get_instance();

        $transaction_models = array('transaction/Transaction_billing_model' => 'M_transaction_billing', 'transaction/Transaction_client_model' => 'M_transaction_client', 'transaction/Transaction_discount_model' => 'M_transaction_discount', 'transaction/Transaction_fee_model' => 'M_transaction_fee', 'transaction/Transaction_payment_model' => 'M_transaction_payment', 'transaction_payment/Transaction_official_payment_model' => 'M_transaction_official_payment', 'transaction/Transaction_property_model' => 'M_transaction_property', 'transaction/Transaction_seller_model' => 'M_transaction_seller', 'ticketing/Ticketing_model' => 'M_Transaction_ticketing', 'transaction/Transaction_model' => 'M_transaction', 'transaction/Transaction_billing_logs_model' => 'M_tbilling_logs', 'transaction_payment/Transaction_penalty_logs_model' => 'M_tpenalty_logs', 'transaction/Transaction_status_logs_model' => 'M_tstatus_logs', 'transaction/Transaction_refund_logs_model' => 'M_transaction_refund_logs', 'transaction_document/transaction_document_model' => 'M_transaction_document', 'checklist/Document_checklist_model' => 'M_document_checklist', 'ar_clearing/Ar_clearing_model' => 'M_Ar_clearing', 'accounting_settings/Accounting_settings_model' => 'M_Accounting_settings', 'accounting_entries/Accounting_entries_model' => 'M_Accounting_entries', 'accounting_entry_items/Accounting_entry_items_model' => 'M_Accounting_entry_items', 'project/Project_model' => 'M_project', 'transaction_commission/Transaction_commission_model' => 'M_Transaction_commission', 'transaction/Transaction_adhoc_fee_model' => 'M_Transaction_adhoc_fee', 'transaction/Transaction_adhoc_fee_schedule_model' => 'M_Transaction_adhoc_fee_schedule', 'accounting_setting_items/Accounting_setting_items_model' => 'M_Accounting_setting_items');

        // Load models
        $CI->load->model($transaction_models);

        $params['transaction_id'] = $transaction_id;

        $this->refund_percentage = .50;
        $this->transaction_id = $transaction_id;
    }

    public function get_amount_paid($type = 0, $period = 0, $cat = 0)
    {
        $CI = &get_instance();

        // 1:total, 2:principal, 3:interest, 4:penalty ; period = 1:rd, 2:dp, 3:loan, 4:eq, 0 : total
        $params['transaction_id'] = $this->transaction_id;

        if ($period) {
            $params['period_id'] = $period;
        }

        $collections = $CI->M_transaction_official_payment->where($params)->get_all();
        $amount = "0.00";

        if ($collections) {
            foreach ($collections as $key => $collection) {
                if ($type == 1) {
                    $amount = $amount + $collection['amount_paid'];
                } elseif ($type == 2) {
                    $amount = $amount + $collection['principal_amount'];
                } elseif ($type == 3) {
                    $amount = $amount + $collection['interest_amount'];
                } elseif ($type == 4) {
                    $amount = $amount + $collection['penalty_amount'];
                } elseif ($type == 5) {
                    $amount = $amount + $collection['grand_total'];
                } elseif ($type == 6) {
                    $amount = $amount + $collection['grand_total'] - $collection['penalty_amount'];
                }
            }
        }

        if ($cat) {
            $totals = $this->get_amount($period);

            if ((empty($amount)) || (empty($totals) || $totals == "0.00")) {
                $percentage = 0;
            } else {
                $percentage = $amount / $totals;
            }

            return convertPercentage($percentage);
        } else {
            return $amount;
        }
    }

    public function get_remaining_balance($type = 0, $period = 0, $cat = 0, $int = 0)
    {
        $CI = &get_instance();

        // 1:total, 2:principal, 3:interest ; period = 1:total, 2:rf, 3:dp, 4:loan
        $params['transaction_id'] = $this->transaction_id;

        if ($period) {
            $params['period_id'] = $period;
        }

        $totals = $this->get_amount($period, $int);

        $amount = $this->get_amount_paid($type, $period);

        $balance = $totals - $amount;

        if ($cat) {

            if ((!$balance) || (!$totals)) {
                $percentage = 0;
            } else {
                $percentage = $balance / $totals;
            }

            return convertPercentage($percentage);
        } else {
            return $balance;
        }
    }

    public function get_remaining_terms($period = 0, $type = 0)
    {
        $CI = &get_instance();

        $terms = 0;
        $params['transaction_id'] = $this->transaction_id;

        if ($period) {
            $params['period_id'] = $period;
        }

        $billing = $CI->M_transaction_billing->with_billings()->get($params);

        $params['is_complete'] = 1;
        $params['transaction_id'] = $this->transaction_id;
        $payments = $CI->M_transaction_payment->where($params)->count_rows();

        if (!$type) {
            $terms = $billing['period_term'] - $payments;
        } else {
            $terms = $billing['period_term'];
        }

        return $terms;
    }

    public function get_amount($period = 0, $type = 0)
    {
        $CI = &get_instance();

        // 0:cp, 1:rf, 2:dp, 3:loan, 4:eq, 6:tcp, 7:tsp, 8:interest

        $params['transaction_id'] = $this->transaction_id;
        $billing_period = array(1, 2, 3, 4);

        if (in_array($period, $billing_period)) {

            $params['period_id'] = $period;

            $billing = $CI->M_transaction_billing->where($params)->get();
            $amount = $billing['period_amount'];

            if ($period == 2) {
                $rf = $this->get_amount(1);
                $rf = 0;
                $amount = $amount + $rf;
            }

            if ($period == 3) {
                $amount = $this->billing_logs(3);
            }

            if ($type) {
                $total = $this->get_amount();

                if (($amount)&&($total)&&($total != 0.00)&&($amount != 0.00)) {
                    $amount = $amount / $total;
                } else {
                    $amount = 0;
                }
            }
        } else if ($type == 8) {

            $params['transaction_id'] = $this->transaction_id;
            $schedule = $CI->M_transaction_payment->where($params)->get_all();

            $amount = "0.00";

            if ($schedule) {
                foreach ($schedule as $key => $sched) {
                    $amount = $amount + $sched['interest_amount'];
                }
            }
        } else {

            $property = $CI->M_transaction_property->where($params)->get();

            if ($period == 6) {
                $amount = $property['total_contract_price'];
            } else if ($period == 7) {
                $amount = $property['total_selling_price'];
            } else {
                $amount = $property['collectible_price'];
            }
        }

        return $amount;
    }

    public function last_payment()
    {
        $CI = &get_instance();

        $params['transaction_id'] = $this->transaction_id;

        $collection = $CI->M_transaction_official_payment->with_payment()->order_by('id', 'DESC')->where($params)->get();

        $row['amount_paid'] = 0.00;
        $row['payment_date'] = '';
        $row['next_payment_date'] = '';

        if ($collection) {
            $row['amount_paid'] = $collection['amount_paid'];
            $row['payment_date'] = $collection['payment_date'];

            $next = $this->next_payment($collection['payment']['id']);

            if ($next['due_date']) {
                $row['next_payment_date'] = $next['due_date'];
            } else {
                $row['next_payment_date'] = $collection['payment']['next_payment_date'];
            }
        }

        return $row;
    }

    public function last_payment_by_period($period_id)
    {
        $CI = &get_instance();

        $params['transaction_id'] = $this->transaction_id;
        $params['period_id'] = $period_id;

        $collection = $CI->M_transaction_official_payment->with_payment()->order_by('id', 'DESC')->where($params)->get();

        $row['amount_paid'] = 0.00;
        $row['payment_date'] = '';
        $row['next_payment_date'] = '';

        if ($collection) {
            $row['amount_paid'] = $collection['amount_paid'];
            $row['payment_date'] = $collection['payment_date'];

            $next = $this->next_payment($collection['payment']['id']);

            if ($next['due_date']) {
                $row['next_payment_date'] = $next['due_date'];
            } else {
                $row['next_payment_date'] = $collection['payment']['next_payment_date'];
            }
        }

        return $row;
    }

    public function next_payment($id = 0)
    {
        $CI = &get_instance();

        $id = $id + 1;
        $params['transaction_id'] = $this->transaction_id;
        $params['id'] = $id;

        $payment = $CI->M_transaction_payment->order_by('id', 'DESC')->where($params)->get();

        $row['amount_paid'] = 0.00;
        $row['due_date'] = '';

        if ($payment) {
            $row['due_date'] = $payment['due_date'];
        }

        return $row;
    }

    public function total_amount_due($type = 0, $date = 0, $table = 0)
    {
        $CI = &get_instance();
        $payments = array();
        $params['transaction_id'] = $this->transaction_id;

        $period_id = get_value_field($this->transaction_id, 'transactions', 'period_id');

        $collections = $CI->M_transaction_official_payment->where($params)->get_all();
        $due = 0;
        $month = 1;

        if (empty($collections) && ($period_id == 1)) {
            $due = $this->get_amount(1);
            if ($type == 4) {
                return $month;
            }
        } else {
            $params['is_complete'] = 0;
            $params['due_date <='] = date('Y-m-d 00:00:00');

            if ($date != 0) {
                $params['due_date <='] = date('Y-m-d 00:00:00', strtotime($date));
            }

            $order['period_id'] = 'ASC';
            $order['id'] = 'ASC';

            $payments = $CI->M_transaction_payment->with_official_payments()->order_by($order)->where($params)->get_all();

            if (!$payments) {
                $transaction_id = $params['transaction_id'];
                $is_complete = $params['is_complete'];
                $ending_date = date('Y-m-31', strtotime($date));
                $starting_date = date('Y-m-01', strtotime($date));

                $customer_where_string = "is_complete = 0 AND transaction_id = " . $transaction_id . " AND due_date BETWEEN '" . $starting_date . "' AND '" . $ending_date . "'";
                $payments = $CI->M_transaction_payment->with_official_payments()->order_by($order)->where($customer_where_string, NULL, NULL, FALSE, FALSE, TRUE)->get_all();
            }

            if ($table) {
                return $payments;
            }

            if ($type == 4) {
                // type = 4 get count only of all unpaid
                return count($payments);
            }

            $total_p = 0;
            $total_i = 0;
            $total_t = 0;
            $penalty = 0;

            if ($payments) {
                foreach ($payments as $key => $payment) {

                    if (isset($payment['official_payments']) && ($payment['official_payments'])) {

                        $officials = $payment['official_payments'];

                        $a = $this->get_remainings_by_transaction_id($payment['id'], $this->transaction_id);

                        $principal = $this->get_paid($officials, 1);
                        $interest = $this->get_paid($officials, 2);
                        $total = $this->get_paid($officials);

                        $total_p = $principal + $total_p;
                        $total_i = $interest + $total_i;
                        $total_t = $total + $total_t;
                    }
                }

                if ($date != 0) {
                    // $penalty = $this->get_penalty();
                }

                $due = array_sum(array_column($payments, 'total_amount')) - $total_t + $penalty + @$a['total_amount_paid'];

                if ($type == 1) {
                    $due = array_sum(array_column($payments, 'principal_amount')) - $total_p + @$a['total_principal'];
                }

                if ($type == 2) {
                    $due = array_sum(array_column($payments, 'interest_amount')) - $total_i + @$a['total_interest'];
                }

                if ($type == 3) {
                    $due = array_sum(array_column($payments, 'total_amount')) - $total_t + @$a['total_amount_paid'];
                }
            }
        }

        if ($table) {
            return $payments;
        } else {
            return $due;
        }
    }

    public function get_total_contract_price()
    {
        $CI = &get_instance();
        $params['transaction_id'] = $this->transaction_id;
        $total_contract_price = $CI->M_transaction_property->where($params)->get();

        if ($total_contract_price) {
            return $total_contract_price['total_contract_price'];
        } else {
            return 0;
        }
    }

    public function get_reservation_fee()
    {
        $CI = &get_instance();
        $params['transaction_id'] = $this->transaction_id;
        $params['period_id'] = 1;
        $reservation_fee = $CI->M_transaction_billing->where($params)->get();

        if ($reservation_fee) {
            return $reservation_fee['period_amount'];
        } else {
            return 0;
        }
    }

    public function get_paid($payments = array(), $type = 0)
    {

        $principal = array_sum(array_column($payments, 'principal_amount'));
        $interest = array_sum(array_column($payments, 'interest_amount'));
        $total = array_sum(array_column($payments, 'total_amount'));

        if ($type == 1) {
            return $principal;
        } else if ($type == 2) {
            return $interest;
        } else {
            return $principal + $interest;
            // return $total;
        }
    }

    public function due_date()
    {
        $CI = &get_instance();

        $params['transaction_id'] = $this->transaction_id;

        $payments = $CI->M_transaction_payment->where($params)->get_all();
        $collections = $CI->M_transaction_official_payment->where($params)->get_all();

        if (empty($collections)) {
            $due_date = $this->effectivity_dates(1);
        } else {
            $params['is_complete'] = 0;
            $params['due_date >='] = date('Y-m-d 00:00:00');
            $payment = $CI->M_transaction_payment->where($params)->get();
            $due_date = $payment['due_date'];
        }

        return $due_date;
    }

    public function cancellation_date()
    {
        $CI = &get_instance();

        $params['transaction_id'] = $this->transaction_id;

        $transaction_refund_logs = $CI->M_transaction_refund_logs->where($params)->get();
        if ($transaction_refund_logs) {
            return $transaction_refund_logs['date_cancellation'];
        }
        return 'n/a';
    }

    public function cancellation_reason()
    {
        $CI = &get_instance();

        $params['transaction_id'] = $this->transaction_id;

        $transaction_refund_logs = $CI->M_transaction_refund_logs->where($params)->get();
        if ($transaction_refund_logs) {
            return $transaction_refund_logs['remarks'];
        }
        return 'n/a';
    }

    public function effectivity_dates($period = 0)
    {
        $CI = &get_instance();

        $params['transaction_id'] = $this->transaction_id;
        $params['period_id'] = $period;

        $billing = $CI->M_transaction_billing->where($params)->get();

        return $billing['effectivity_date'];
    }

    public function interests($period = 0)
    {
        $CI = &get_instance();

        $params['transaction_id'] = $this->transaction_id;
        $params['period_id'] = $period;

        $billing = $CI->M_transaction_billing->where($params)->get();

        return $billing['interest_rate'];
    }

    public function monthly_ammort($period)
    {

        $CI = &get_instance();
        $n = 0.00;
        $params['transaction_id'] = $this->transaction_id;
        $params['period_id'] = $period;

        $billing = $CI->M_transaction_billing->where($params)->get();

        if ($billing) {
            $n = monthly_payment($billing['period_amount'], $billing['interest_rate'], $billing['period_term']);
        }

        return $n;
    }

    public function period_left()
    {
        $CI = &get_instance();

        $params['transaction_id'] = $this->transaction_id;
        $payments_all = $CI->M_transaction_payment->where($params)->count_rows();

        $params['is_complete'] = 1;
        $payment_done = $CI->M_transaction_payment->where($params)->count_rows();

        $left = $payments_all - $payment_done;

        return $left;
    }

    public function total_discounts()
    {
        $CI = &get_instance();

        $d = "0";

        $params['transaction_id'] = $this->transaction_id;
        $discounts = $CI->M_transaction_discount->where($params)->get_all();

        if ($discounts) {
            $d = array_sum(array_column($discounts, 'discount_amount_rate'));
        }

        return $d;
    }

    public function penalty_type()
    {
        $CI = &get_instance();

        $period = "";

        $params['id'] = $this->transaction_id;
        $transaction = $CI->M_transaction->where($params)->get();

        $type = $transaction['penalty_type'];

        return $type;
    }

    public function current_period($type = 0)
    {
        $CI = &get_instance();

        $period = "";

        $params['id'] = $this->transaction_id;
        $transaction = $CI->M_transaction->where($params)->get();

        if ($transaction) {
            if (!$type) {
                $period = Dropdown::get_static('period_names', $transaction['period_id'], 'view');
            } else {
                $period = $transaction['period_id'];
            }
        }

        return $period;
    }

    public function current_status($type = '')
    { //0:general, 1:collection, 2:documentation
        $CI = &get_instance();

        $period = "";

        $params['id'] = $this->transaction_id;
        $transaction = $CI->M_transaction->where($params)->get();

        $field = "general";
        if ($type == 1) {
            $field = "collection";
        } else if ($type == 2) {
            $field = "documentation";
        }

        $status = $transaction[$field . '_status'];

        return $status;
    }

    public function payment_status($type = 0, $date = '')
    {
        // 0 = remarks, 1 = overdue day count
        $CI = &get_instance();

        $last = $this->last_payment();

        $period = $this->current_period();
        // $c_stat = $this->collection_status();
        $c_stat = $this->current_status(1);
        $c_status = Dropdown::get_static('collection_status', $c_stat);

        if ($last['next_payment_date']) {
            $due_date = $last['next_payment_date'];
        } else {
            $due_date = $this->effectivity_dates(1);
        }

        $now = time(); // or your date as well

        if ($date) {
            $now = strtotime($date);
        }

        $your_date = strtotime($due_date);
        $datediff = $now - $your_date;
        $over_due = floor($datediff / (60 * 60 * 24));
        $months = floor($over_due / 30);
        $days = $over_due;

        if (!$type) {
            if ($due_date) {
                if ($c_stat == 5) {
                    return "<div class='btn transaction_custom_btn btn-success'>" . $c_status . "</div><br>";
                } else if (($over_due < 0 && $over_due == -1)) {
                    return "<div class='btn transaction_custom_btn btn-info'>" . $c_status . "<br> + " . abs($over_due) . " day until next collection</div><br>";
                } else if ($over_due == 0) {
                    return "<div class='btn transaction_custom_btn btn-warning'>" . $c_status . "<br>Due Today</div><br>";
                } else if ($over_due < 0) {
                    return "<div class='btn transaction_custom_btn btn-info'>" . $c_status . "<br> + " . abs($days) . " day(s) until next collection</div><br>";
                } else if ($months == 0 && $days > 0) {
                    return "<div class='btn transaction_custom_btn btn-danger'>" . $c_status . "<br>" . abs($days) . ' day(s) overdue</div><br>';
                } else if (($over_due > 0) && ($over_due < 15)) {
                    return "<div class='btn transaction_custom_btn btn-danger'>" . $c_status . "<br>" . abs($days) . ' day(s) overdue</div><br>';
                } else if ($over_due > 14) {
                    return "<div class='btn transaction_custom_btn btn-danger'>" . $c_status . "<br>" . abs($days) . ' day(s) overdue</div><br>';
                }
            }
        } else if ($type == 1) {
            $ret['days'] = $days;
            $ret['due_date'] = $due_date;
            return $ret;
        }
    }

    public function get_penalty($date = 0, $get_all = 0)
    {

        $overdue_det = $this->payment_status(1, $date);
        $penalty_type = $this->penalty_type();

        $overdue = $overdue_det['days'];
        $period = $this->current_period(1);
        $due = 0;

        if (($date != 0) && ($penalty_type == 1)) {
            $due = $this->total_amount_due(3, $date);
        } else {
            $due = $this->monthly_ammort($period);
        }

        $amount = 0;
        $percentage = 3;

        if ($period == 1) {
            $percentage = 0;
        }

        if ($period == 3) {
            $percentage = 5;
        }

        if ($overdue > 0) {
            $amount = ($due * ($percentage / 100)) * ($overdue / 30);
        }
        // vdebug($amount);
        if ($get_all) {
            $ret['amount'] = round($amount, 2);
            $ret['days'] = $overdue;
            $ret['due'] = $due;
            $ret['date_from'] = $overdue_det['due_date'];
            $ret['date_to'] = $date;

            return $ret;
        } else {
            return round($amount, 2);
        }
    }

    public function get_penalty_normal($days = 0, $period = 0, $due = 0)
    {

        $amount = 0;
        $percentage = 0;

        if ($period == 1) {
            $percentage = 0;
        }

        if ($period == 3) {
            $percentage = 5; // 5% penalty for loan
        }

        if ($period == 2) {
            $percentage = 3; // 3% penalty for dp
        }

        if ($days > 0) {
            $amount = ($due * ($percentage / 100)) * ($days / 30);
        }

        return round($amount, 2);
    }

    public function get_overdue_table($date = '', $debug = 0)
    {
        $CI = &get_instance();
        $payments = $this->total_amount_due(0, $date, 1); // get table overdue
        $results = array();

        $transaction = $CI->M_Transaction->get($this->transaction_id);
        $penalty_type = $transaction['penalty_type'];

        $d_billings = $this->get_remaining_dp($this->transaction_id, 1);

        $remaining_balance_billings = $this->get_remaining_dp($this->transaction_id, 0);

        if ($d_billings) {
            $d_last = end($d_billings);
        }; // get transaction downpayment period

        if ($debug == 2) {
            vdebug($payments);
        }

        if (is_array($payments) && ($payments)) {

            $length = count($payments);
            $rem_t = 0;
            $x = 1;
            foreach ($payments as $key => $payment) {
                $t = 0;
                $total_p = 0;
                $total_i = 0;
                $total_t = 0;

                if (isset($payment['official_payments']) && ($payment['official_payments'])) {

                    $officials = $payment['official_payments'];
                    $a = $this->get_remainings($payment['id']);

                    $principal = $this->get_paid($officials, 1);
                    $interest = $this->get_paid($officials, 2);
                    $total = $this->get_paid($officials);
                    $total_p = $principal + $total_p;
                    $total_i = $interest + $total_i;
                    $total_t = $total + $total_t;

                    if ($debug == 3) {
                        echo "<pre>";
                        print_r($officials);
                        echo "<br>";
                        echo "principal " . $principal;
                        echo "<br>";
                        echo "interest " . $interest;
                        echo "<br>";
                        echo "total " . $total;
                        echo "<br>";
                        echo "total_p " . $total_p;
                        echo "<br>";
                        echo "total_i " . $total_i;
                        echo "<br>";
                        echo "total_t " . $total_t;
                        echo "<br>";
                        vdebug($a);
                    }
                }


                $total_amount = @$a['total_principal'] + @$a['total_interest'];

                $monthly_payment = $payment['principal_amount'] + $payment['interest_amount'];

                $monthly_payment = ($payment['total_amount'] > $monthly_payment) ? $payment['total_amount'] : $monthly_payment;

                $total_amount = ($payment['total_amount'] > $total_amount) ? $payment['total_amount'] : $total_amount;

                $period_count = @$payment['official_payments'][0]['period_count'] ? @$payment['official_payments'][0]['period_count'] : 1;

                if (isset($total)) {
                    $t = $total - ($monthly_payment * $period_count);
                }

                // $t = $t >= 0 ? $t : abs($t);

                if ($debug == 5) {
                    echo "total " . " " . $total;
                    echo "<br>";
                    echo "monthly_payment " . " " . $monthly_payment;
                    echo "<br>";
                    echo "period_count " . " " . $period_count;
                    echo "<br>";
                    echo "t " . " " . $t;
                    echo "<br>";
                    die();
                }

                // $t = $t >= 0 ? $t : abs($t);

                if (isset($payment['official_payments']) && ($payment['official_payments'])) {
                    // recompute if it is a negative value
                    if ($t < 0) {
                        $t = $monthly_payment - $total;
                    } else {
                        $t = $monthly_payment - $t;
                    }
                } else {
                    $t = $monthly_payment;
                }

                $t = $t >= 0 ? $t : abs($t);

                $days = date_diffs($date, $payment['due_date']);

                $p = $this->get_penalty_normal($days, $payment['period_id'], $t);

                if ($debug == 4) {
                    echo "<pre>";
                    echo "total: " . $total;
                    echo "<br>";

                    echo "total_amount: " . $total_amount;
                    echo "<br>";

                    echo "total_t: " . $total_t;
                    echo "<br>";

                    echo "monthly_payment: " . $monthly_payment;
                    echo "<br>";

                    echo "t: " . $t;
                    echo "<br>";

                    echo "rem_t: " . $rem_t;
                    echo "<br>";

                    echo "penalty_type: " . $penalty_type;
                    echo "<br>";

                    echo "p: " . print_r($p);
                    echo "<br>";

                    echo "d: " . print_r($d_last);
                    echo "</pre>";
                    die();
                }

                if (empty($t)) {
                    continue;
                }

                # Check penalty type
                # 1 = Standard Payment
                # 2 = On or Before Due Date
                if ($penalty_type == 2) {
                    # Check if downpayment period type
                    if ($payment['period_id'] == "2") {
                        // vdebug($date . " " . $d_last['due_date']);

                        $days = date_diffs($date, $d_last['due_date']);

                        if ($days < 0) {
                            $days = 0;
                        }
                        # Get last payment info and 
                        # calculate the penalty based on by number of day(s) due                      
                        if ($x == $length) {

                            $d_penalty = $this->get_penalty_normal($days, $payment['period_id'], $t + $rem_t);

                            $total_due = $t + $d_penalty;

                            $results[$key]['id'] = $payment['id'];
                            $results[$key]['period'] = $payment['period_id'];
                            $results[$key]['due_date'] = $payment['due_date'];
                            $results[$key]['no_of_months'] = date_diffs($date, $payment['due_date'], 'months');
                            $results[$key]['no_of_days'] = $days;
                            $results[$key]['penalty'] = $d_penalty;
                            $results[$key]['monthly_payment'] = $t;
                            $results[$key]['total_due'] = $total_due;
                        } else {

                            $rem_t = $rem_t + $t;

                            $results[$key]['id'] = $payment['id'];
                            $results[$key]['due_date'] = $payment['due_date'];
                            $results[$key]['no_of_months'] = date_diffs($date, $payment['due_date'], 'months');
                            $results[$key]['no_of_days'] = 0;
                            $results[$key]['monthly_payment'] = $t;
                            $results[$key]['penalty'] = 0;
                            $results[$key]['period'] = $payment['period_id'];
                            $results[$key]['total_due'] = $t;
                            $results[$key]['penalty'] = $p;
                        }
                    } else {
                        $results[$key]['id'] = $payment['id'];
                        $results[$key]['due_date'] = $payment['due_date'];
                        $results[$key]['no_of_months'] = date_diffs($date, $payment['due_date'], 'months');
                        $results[$key]['no_of_days'] = $days;
                        $results[$key]['monthly_payment'] = $t;
                        $results[$key]['penalty'] = $p;
                        $results[$key]['period'] = $payment['period_id'];
                        $results[$key]['total_due'] = $t + $p;
                    }
                } else {

                    $results[$key]['id'] = $payment['id'];
                    $results[$key]['due_date'] = $payment['due_date'];
                    $results[$key]['no_of_months'] = date_diffs($date, $payment['due_date'], 'months');
                    $results[$key]['no_of_days'] = $days;
                    $results[$key]['monthly_payment'] = abs($t);
                    $results[$key]['penalty'] = abs($p);
                    $results[$key]['period'] = $payment['period_id'];
                    $results[$key]['total_due'] = $t + $p;
                }
                $x++;
            }
            # code...
        }

        return $results;
    }

    public function process_mortgage($transaction_id = 0, $type = "", $process = "", $user_id = 0, $tb_id = 0)
    {
        $CI = &get_instance();

        $breakdown = [];

        $monthly_payment = 0;
        $transaction = $CI->M_transaction->with_tbillings()->with_billings()->with_t_property()->get($transaction_id);

        if (($process == "balloon-payment") || ($process == "restructure") || ($process == "continuous-payment")) {
            if ($tb_id) {
                $billing = $CI->M_tbilling_logs->get($tb_id);
                $billings[] = $billing;
            } else {
                $billing = $CI->M_tbilling_logs->where('transaction_id', $transaction_id)->order_by('id', 'desc')->get();
                $billings[] = $billing;
            }
        } else {
            // normal saving
            $transaction = $CI->M_transaction->with_tbillings()->with_billings()->with_t_property()->get($transaction_id);
            $new_billings = $CI->M_transaction_billing->where('transaction_id', $transaction_id)->order_by('order_id', 'asc')->get_all();
            $billings = $transaction['billings'];
            $billings = $new_billings;
        }

        $collectible_price = $transaction['t_property']['collectible_price'];

        if ($billings) {

            if (($type == 'save') && (($process != "balloon-payment"))) {

                $u_additional = [
                    'updated_by' => $user_id,
                    'updated_at' => NOW,
                ];

                $CI->M_transaction_payment->delete(array('transaction_id' => $transaction_id));
                $CI->M_Transaction_official_payment->delete(array('transaction_id' => $transaction_id));
                $CI->M_tbilling_logs->delete(array('transaction_id' => $transaction_id));
                $CI->M_tpenalty_logs->delete(array('transaction_id' => $transaction_id));
            }

            if (($type == 'save') && (($process == "balloon-payment") || ($process == "restructure") || ($process == 'continuous-payment'))) {
                $u_payment['transaction_id'] = $transaction_id;
                $u_payment['is_paid'] = 0;
                $u_payment['period_id'] = $billing['period_id'];
                $a = $CI->M_transaction_payment->update(array('deleted_reason' => $process, 'deleted_at' => NOW), $u_payment);
                // // $a = $CI->M_transaction_payment->delete($u_payment);
                // vdebug($a);
            }

            foreach ($billings as $key => $billing_data) {

                if ($process == "balloon-payment") {
                    // $billing['effectivity_date'] = add_months_to_date($billing['effectivity_date'] + 1);
                    $monthly_payment = $this->monthly_ammort($billing_data['period_id']);
                }

                if ($process == "continuous-payment") {
                    // $billing['effectivity_date'] = add_months_to_date($billing['effectivity_date'] + 1);
                    $monthly_payment = $this->monthly_ammort($billing_data['period_id']);
                }


                $test = $CI->mortgage_computation->reconstruct($billing_data, $collectible_price, $monthly_payment);
                $steps = $CI->mortgage_computation->breakdown();

                if ($type == "save") {

                    $additional = [
                        'created_by' => $user_id,
                        'created_at' => NOW,
                    ];

                    if (isset($steps['monthly']) && ($steps['monthly'])) {

                        foreach ($steps['monthly'] as $key => $step) {
                            $countterm = $step['month'];

                            if ($process == "balloon-payment") {
                                $countterm = returnDecimal($billing_data['remarks']);
                                $countterm = ($countterm + $step['month']);

                                if ($step['ending_balance'] <= 0) {
                                    $step['ending_balance'] = 0;
                                    $step['principal'] = $step['beginning_balance'];
                                }
                            }

                            if ($process == "continuous-payment") {
                                $countterm = returnDecimal($billing_data['remarks']);
                                $countterm = ($countterm + $step['month']);

                                if ($step['ending_balance'] <= 0) {
                                    $step['ending_balance'] = 0;
                                    $step['principal'] = $step['beginning_balance'];
                                }
                            }

                            $payment['transaction_id'] = $transaction_id;
                            $payment['total_amount'] = $step['principal'] + $step['interest'];
                            $payment['principal_amount'] = $step['principal'];
                            $payment['interest_amount'] = $step['interest'];
                            $payment['beginning_balance'] = $step['beginning_balance'];
                            $payment['ending_balance'] = $step['ending_balance'];
                            $payment['period_id'] = $billing_data['period_id'];
                            $payment['particulars'] = ordinal($countterm) . ' ' . Dropdown::get_static('periods', $billing_data['period_id'], 'view') . ' Payment';
                            $payment['is_paid'] = 0;
                            $payment['is_complete'] = 0;
                            $payment['due_date'] = $step['due_date'];
                            $payment['next_payment_date'] = $step['next_payment_date'];

                            if (($step['month'] == 1) && (($process == "balloon-payment") || ($process == "restructure"))) {
                                $payment['process'] = $process;
                                $payment['particulars'] = $payment['particulars'] . " BP";
                            }
                            if ($payment['principal_amount'] > 1) {
                                $a = $CI->M_transaction_payment->insert($payment + $additional);
                            }

                            $steps['monthly'][$key] = $payment['particulars'];
                        }
                    }
                }

                $breakdown[] = $steps;
            }
        }

        return $breakdown;
    }

    // public function recalculate_mortgage_v1($transaction_id = 0, $tb_data = "", $fs_data = false, $user_id = 0, $type = "")
    // {
    //     $CI = &get_instance();
    //     $breakdown = [];
    //     $monthly_payment = 0;
    //     $tcp = 0;

    //     $p = array('transaction_id' => $transaction_id, 'is_paid' => 0, 'is_complete' => 0);

    //     if ($type == "interest_rates") {
    //         $p = array('transaction_id' => $transaction_id, 'is_paid' => 0, 'is_complete' => 0, 'period_id' => 3);
    //     }

    //     $CI->M_transaction_payment->delete($p);

    //     $transaction = $CI->M_transaction->with_tbillings()->with_billings()->with_payments()->with_t_property()->get($transaction_id);
    //     $original_billings = $transaction['billings'];

    //     $additional = [
    //         'created_by' => $user_id,
    //         'created_at' => NOW
    //     ];

    //     $u_additional = [
    //         'updated_by' => $user_id,
    //         'updated_at' => NOW
    //     ];

    //     if (!empty($tb_data)) {
    //         foreach ($tb_data as $key => $value) {
    //             $data = [
    //                 'period_amount' => $value['period_amount'],
    //                 'interest_rate' => $value['interest_rate'],
    //                 'period_term' => $value['period_term'],
    //                 'effectivity_date' => $value['effectivity_date'],
    //             ];

    //             $tb_filter = [
    //                 'transaction_id' => $transaction_id,
    //                 'period_id' => $value['period_id'],
    //             ];
    //             $tcp += $value['period_amount'];

    //             $CI->M_transaction_billing->update($data + $u_additional, $tb_filter);
    //         }

    //         $billings = $CI->M_transaction_billing->where(array('transaction_id' => $transaction_id))->get_all();

    //         if ($billings) {
    //             foreach ($billings as $key => $value) {
    //                 $completed_terms = 0;

    //                 if ($fs_data) {
    //                     $monthly_payment = isset($fs_data[$key]['monthly_payment']) && $fs_data[$key]['monthly_payment'] ? $fs_data[$key]['monthly_payment'] : 0;
    //                 } else {
    //                     $monthly_payment = isset($tb_data[$key]['monthly_payment']) && $tb_data[$key]['monthly_payment'] ? $tb_data[$key]['monthly_payment'] : 0;
    //                 }

    //                 $paid_amount = $CI->M_transaction_official_payment->total_payment_per_period($transaction_id, $value['period_id']);
    //                 $last_payment = $CI->M_transaction_payment->where(array('transaction_id' => $transaction_id, 'period_id' => 2))->order_by('id', 'desc')->get();
    //                 $completed_terms = $CI->M_transaction_payment->mortgage_term_count($transaction_id, $value['period_id']);

    //                 if ($paid_amount != 0 && $value['period_term'] != 0 && $value['period_term'] != 1) {
    //                     $value['period_amount'] -= $paid_amount;                                        // Get new period amount if payments were made
    //                     $value['effectivity_date'] = $last_payment['next_payment_date'];
    //                     $value['period_term'] -= $completed_terms;
    //                     $monthly_payment = $value['period_amount'] / $value['period_term'];
    //                 }

    //                 if ($monthly_payment == 0) {
    //                     if ($value['period_term'] != 0) {
    //                         $monthly_payment = $value['period_amount'] / $value['period_term'];
    //                     } else {
    //                         $monthly_payment = $value['period_amount'];
    //                     }
    //                 }

    //                 // Log transaction billing
    //                 if ($value['period_amount'] != $original_billings[$key]['period_amount']) {
    //                     $beginning_balance = $value['period_amount'];
    //                     $terms = $value['period_term'];
    //                     $effectivity_date = $value['effectivity_date'];
    //                     $interest_rate = $value['interest_rate'];

    //                     $tb_logs['transaction_id'] = $transaction_id;
    //                     $tb_logs['transaction_payment_id'] = 0;
    //                     $tb_logs['period_amount'] = $beginning_balance;
    //                     $tb_logs['period_term'] = $terms;
    //                     $tb_logs['effectivity_date'] = $effectivity_date;
    //                     $tb_logs['starting_balance'] = $beginning_balance;
    //                     $tb_logs['ending_balance'] = "";
    //                     $tb_logs['interest_rate'] = $interest_rate;
    //                     $tb_logs['period_id'] = $value['period_id'];
    //                     $tb_logs['slug'] = 'change-financing-scheme';
    //                     $tb_logs['remarks'] = "Change in Financing Scheme";

    //                     $tb_id = $CI->M_Tbilling_logs->insert($tb_logs + $additional);
    //                 }

    //                 // tcp = property collectable price and financing scheme computation
    //                 $CI->mortgage_computation->reconstruct($value, $tcp, $monthly_payment);

    //                 $steps = $CI->mortgage_computation->breakdown();

    //                 if ($value['period_term'] <= 0 || $value['period_term'] == $completed_terms) {
    //                     continue;
    //                 }

    //                 if (isset($steps['monthly']) && ($steps['monthly'])) {
    //                     foreach ($steps['monthly'] as $key => $step) {
    //                         $countterm = $step['month'] + $completed_terms;

    //                         $payment['transaction_id'] = $transaction_id;
    //                         $payment['total_amount'] = $step['principal'] + $step['interest'];
    //                         $payment['principal_amount'] = $step['principal'];
    //                         $payment['interest_amount'] = $step['interest'];
    //                         $payment['beginning_balance'] = $step['beginning_balance'];
    //                         $payment['ending_balance'] = $step['ending_balance'];
    //                         $payment['period_id'] = $value['period_id'];
    //                         $payment['particulars'] = ordinal($countterm) . ' ' . Dropdown::get_static('periods', $value['period_id'], 'view') . ' Payment';
    //                         $payment['is_paid'] = 0;
    //                         $payment['is_complete'] = 0;
    //                         $payment['due_date'] = $step['due_date'];
    //                         $payment['next_payment_date'] = $step['next_payment_date'];

    //                         $a = $CI->M_transaction_payment->insert($payment + $additional);
    //                         $steps['monthly'][$key] = $payment['particulars'];
    //                     }
    //                 }
    //                 $breakdown[] = $steps;
    //             }
    //         }
    //     }
    // }

    public function recalculate_mortgage($transaction_id = 0, $tb_data = "", $fs_data = false, $user_id = 0, $type = "")
    {
        $CI = &get_instance();
        $breakdown = [];
        $monthly_payment = 0;
        $tcp = 0;

        $p = array('transaction_id' => $transaction_id, 'is_paid' => 0, 'is_complete' => 0);

        if ($type == "interest_rates") {
            $p = array('transaction_id' => $transaction_id, 'is_paid' => 0, 'is_complete' => 0, 'period_id' => 3);
        }

        $CI->M_transaction_payment->delete($p);

        $CI->db->reset_query();
        $transaction = $CI->M_transaction->with_tbillings()->with_billings()->with_payments()->with_t_property()->get($transaction_id);
        $original_billings = $transaction['billings'];
        $tbillings = $transaction['tbillings'];

        $additional = [
            'created_by' => $user_id,
            'created_at' => NOW
        ];

        $u_additional = [
            'updated_by' => $user_id,
            'updated_at' => NOW
        ];


        if (!empty($tb_data)) {

            foreach ($tb_data as $key => $value) {

                $data = [
                    'period_amount' => $value['period_amount'],
                    'interest_rate' => $value['interest_rate'],
                    'period_term' => $value['period_term'],
                    'effectivity_date' => $value['effectivity_date'],
                ];

                $tb_filter = [
                    'transaction_id' => $transaction_id,
                    'period_id' => $value['period_id'],
                ];

                $tcp += $value['period_amount'];

                $CI->M_transaction_billing->update($data + $u_additional, $tb_filter);

                if (empty($tbillings)) {

                    if ($original_billings) {
                        # code...
                        foreach ($original_billings as $key => $original_billing) {

                            if ($original_billing['period_id'] == $value['period_id']) {
                                # code...
                                $tb_logs['slug'] = 'original-financing-scheme';
                                $tb_logs['remarks'] = "Original Financing Scheme";
                                $tb_logs['transaction_id'] = $transaction_id;
                                $tb_logs['transaction_payment_id'] = 0;
                                $tb_logs['period_amount'] = $original_billing['period_amount'];
                                $tb_logs['period_term'] = $original_billing['period_term'];
                                $tb_logs['effectivity_date'] = $original_billing['effectivity_date'];
                                $tb_logs['starting_balance'] = $original_billing['period_amount'];
                                $tb_logs['ending_balance'] = "";
                                $tb_logs['interest_rate'] = $original_billing['interest_rate'];
                                $tb_logs['period_id'] = $original_billing['period_id'];

                                $tb_id = $CI->M_Tbilling_logs->insert($tb_logs + $additional);
                            }
                        }
                    }
                }
            }

            $tb_filter = [
                'transaction_id' => $transaction_id,
            ];

            if ($type == "interest_rates") {
                $tb_filter = [
                    'transaction_id' => $transaction_id,
                    'period_id' => $value['period_id'],
                ];
            }

            $billings = $CI->M_transaction_billing->where($tb_filter)->get_all();

            if ($billings) {
                foreach ($billings as $key => $value) {

                    $tb_logs['slug'] = 'change-financing-scheme';
                    $tb_logs['remarks'] = "Change in Financing Scheme";

                    if ($type == "interest_rates") {
                        $tb_logs['slug'] = 'change-in-interest-rate';
                        $tb_logs['remarks'] = "Change in Interest Rate";
                    }

                    $completed_terms = 0;

                    if ($fs_data) {
                        $monthly_payment = isset($fs_data[$key]['monthly_payment']) && $fs_data[$key]['monthly_payment'] ? $fs_data[$key]['monthly_payment'] : 0;
                    } else {
                        $monthly_payment = isset($tb_data[$key]['monthly_payment']) && $tb_data[$key]['monthly_payment'] ? $tb_data[$key]['monthly_payment'] : 0;
                    }

                    $paid_amount = $CI->M_transaction_official_payment->total_payment_per_period($transaction_id, $value['period_id']);
                    $last_payment = $CI->M_transaction_payment->where(array('transaction_id' => $transaction_id, 'period_id' => 2))->order_by('id', 'desc')->get();
                    $completed_terms = $CI->M_transaction_payment->mortgage_term_count($transaction_id, $value['period_id']);

                    if ($type != "interest_rates") {
                        if ($paid_amount != 0 && $value['period_term'] != 0 && $value['period_term'] != 1) {
                            $value['period_amount'] -= $paid_amount;                                        // Get new period amount if payments were made
                            $value['effectivity_date'] = $last_payment['next_payment_date'];
                            $value['period_term'] -= $completed_terms;
                            $monthly_payment = $value['period_amount'] / $value['period_term'];
                        }
                    }

                    if ($monthly_payment == 0) {
                        
                        if ($value['period_term'] != 0) {
                            $monthly_payment = $value['period_amount'] / $value['period_term'];
                        } else {
                            $monthly_payment = $value['period_amount'];
                        }

                    }

                    if ($type == "interest_rates") {
                        if ($value['interest_rate']) {
                            $monthly_payment = 0;
                        }
                    }
                    
                    // Log transaction billing
                    // if($value['period_amount'] != $original_billings[$key]['period_amount']) {
                    $beginning_balance = $value['period_amount'];
                    $terms = $value['period_term'];
                    $effectivity_date = $value['effectivity_date'];
                    $interest_rate = $value['interest_rate'];

                    $tb_logs['transaction_id'] = $transaction_id;
                    $tb_logs['transaction_payment_id'] = 0;
                    $tb_logs['period_amount'] = $beginning_balance;
                    $tb_logs['period_term'] = $terms;
                    $tb_logs['effectivity_date'] = $effectivity_date;
                    $tb_logs['starting_balance'] = $beginning_balance;
                    $tb_logs['ending_balance'] = "";
                    $tb_logs['interest_rate'] = $interest_rate;
                    $tb_logs['period_id'] = $value['period_id'];

                    $tb_id = $CI->M_Tbilling_logs->insert($tb_logs + $additional);
                    // }


                    // tcp = property collectable price and financing scheme computation
                    $CI->mortgage_computation->reconstruct($value, $tcp, $monthly_payment);

                    $steps = $CI->mortgage_computation->breakdown();

                    if ($value['period_term'] <= 0 || $value['period_term'] == $completed_terms) {
                        continue;
                    }

                    if (isset($steps['monthly']) && ($steps['monthly'])) {
                        foreach ($steps['monthly'] as $key => $step) {
                            $countterm = $step['month'] + $completed_terms;

                            $payment['transaction_id'] = $transaction_id;
                            $payment['total_amount'] = $step['principal'] + $step['interest'];
                            $payment['principal_amount'] = $step['principal'];
                            $payment['interest_amount'] = $step['interest'];
                            $payment['beginning_balance'] = $step['beginning_balance'];
                            $payment['ending_balance'] = $step['ending_balance'];
                            $payment['period_id'] = $value['period_id'];
                            $payment['particulars'] = ordinal($countterm) . ' ' . Dropdown::get_static('periods', $value['period_id'], 'view') . ' Payment';
                            $payment['is_paid'] = 0;
                            $payment['is_complete'] = 0;
                            $payment['due_date'] = $step['due_date'];
                            $payment['next_payment_date'] = $step['next_payment_date'];

                            $a = $CI->M_transaction_payment->insert($payment + $additional);
                            $steps['monthly'][$key] = $payment['particulars'];
                        }
                    }
                    $breakdown[] = $steps;
                }
            }
        }

        // vdebug($tb_data);

        // if(!empty($tb_data)) {

        //     foreach($tb_data as $key => $value) {

        //         $data = [
        //             'period_amount' => $value['period_amount'],
        //             'interest_rate' => $value['interest_rate'],
        //             'period_term' => $value['period_term'],
        //             'effectivity_date' => $value['effectivity_date'],                           
        //         ];

        //         $tb_filter = [
        //             'transaction_id' => $transaction_id,
        //             'period_id' => $value['period_id'],
        //         ]; 

        //         $tcp += $value['period_amount'];

        //         $tb_logs['slug'] = 'change-financing-scheme';
        //         $tb_logs['remarks'] = "Change in Financing Scheme";  

        //         if ($type == "interest_rates") {
        //             $tb_logs['slug'] = 'change-in-interest-rate';
        //             $tb_logs['remarks'] = "Change in Interest Rate";  
        //         }

        //         $completed_terms = 0;

        //         if($fs_data) {
        //             $monthly_payment = isset($fs_data[$key]['monthly_payment']) && $fs_data[$key]['monthly_payment'] ? $fs_data[$key]['monthly_payment'] : 0;
        //         } else {
        //             $monthly_payment = isset($tb_data[$key]['monthly_payment']) && $tb_data[$key]['monthly_payment'] ? $tb_data[$key]['monthly_payment'] : 0;
        //         }

        //         $paid_amount = $CI->M_transaction_official_payment->total_payment_per_period($transaction_id, $value['period_id']);
        //         $last_payment = $CI->M_transaction_payment->where(array('transaction_id'=>$transaction_id, 'period_id'=>2))->order_by('id', 'desc')->get();
        //         $completed_terms = $CI->M_transaction_payment->mortgage_term_count($transaction_id, $value['period_id']);

        //         if ($type != "interest_rates") {
        //             if($paid_amount != 0 && $value['period_term'] != 0 && $value['period_term'] != 1) {
        //                 $value['period_amount'] -= $paid_amount;                                        // Get new period amount if payments were made
        //                 $value['effectivity_date'] = $last_payment['next_payment_date'];    
        //                 $value['period_term'] -= $completed_terms;
        //                 $monthly_payment = $value['period_amount'] / $value['period_term']; 
        //             }
        //         }

        //         if ($type == "due_date") {
        //             if($monthly_payment == 0) {
        //                 if($value['period_term'] != 0) {
        //                     $monthly_payment = $value['period_amount'] / $value['period_term']; 
        //                 } else {
        //                     $monthly_payment = $value['period_amount'];
        //                 }
        //             }
        //         } else {
        //             $monthly_payment = 0;
        //         }

        //         if ($type == "financing_scheme") {
        //              $data = [
        //                 'period_amount' => $value['period_amount'],
        //                 'interest_rate' => $value['interest_rate'],
        //                 'period_term' => $value['period_term'],
        //                 'effectivity_date' => $value['effectivity_date'],                            
        //             ];

        //             $tb_filter = [
        //                 'transaction_id' => $transaction_id,
        //                 'period_id' => $value['period_id'],
        //             ]; 

        //             $CI->M_transaction_billing->update($data + $u_additional, $tb_filter);
        //         }

        //         // Log transaction billing
        //         if($value['period_amount'] != $original_billings[$key]['period_amount']) {
        //             $beginning_balance = $value['period_amount'];
        //             $terms = $value['period_term'];
        //             $effectivity_date = $value['effectivity_date'];
        //             $interest_rate = $value['interest_rate'];

        //             $tb_logs['transaction_id'] = $transaction_id;
        //             $tb_logs['transaction_payment_id'] = 0;
        //             $tb_logs['period_amount'] = $beginning_balance;
        //             $tb_logs['period_term'] = $terms;
        //             $tb_logs['effectivity_date'] = $effectivity_date;
        //             $tb_logs['starting_balance'] = $beginning_balance;
        //             $tb_logs['ending_balance'] = "";
        //             $tb_logs['interest_rate'] = $interest_rate;
        //             $tb_logs['period_id'] = $value['period_id'];

        //             $tb_id = $CI->M_Tbilling_logs->insert($tb_logs + $additional);
        //         }

        //         // tcp = property collectable price and financing scheme computation
        //         $CI->mortgage_computation->reconstruct($value, $tcp, $monthly_payment);

        //         $steps = $CI->mortgage_computation->breakdown();

        //         // echo $tcp;
        //         // echo "<br>";
        //         // echo $monthly_payment;
        //         // echo "<br>";
        //         // print_r($value);
        //         // echo "<br>";
        //         // vdebug($steps);

        //         if($value['period_term'] <= 0 || $value['period_term'] == $completed_terms) { continue; }

        //         if(isset($steps['monthly']) && ($steps['monthly'])) {
        //             foreach($steps['monthly'] as $key => $step) {
        //                 $countterm = $step['month'] + $completed_terms;

        //                 $payment['transaction_id'] = $transaction_id;
        //                 $payment['total_amount'] = $step['principal'] + $step['interest'];
        //                 $payment['principal_amount'] = $step['principal'];
        //                 $payment['interest_amount'] = $step['interest'];
        //                 $payment['beginning_balance'] = $step['beginning_balance'];
        //                 $payment['ending_balance'] = $step['ending_balance'];
        //                 $payment['period_id'] = $value['period_id'];
        //                 $payment['particulars'] = ordinal($countterm) . ' ' . Dropdown::get_static('periods', $value['period_id'], 'view') . ' Payment';
        //                 $payment['is_paid'] = 0;
        //                 $payment['is_complete'] = 0;
        //                 $payment['due_date'] = $step['due_date'];
        //                 $payment['next_payment_date'] = $step['next_payment_date'];

        //                 $a = $CI->M_transaction_payment->insert($payment + $additional);
        //                 $steps['monthly'][$key] = $payment['particulars'];

        //             }
        //         }
        //         $breakdown[] = $steps;


        //     }

        // }
    }

    public function process_adhoc_fee($fees)
    {
        $CI = &get_instance();

        if (isset($fees['transaction_id'])) {
            $CI->M_Transaction_adhoc_fee_schedule->delete(array('transaction_id' => $fees['transaction_id'], 'is_paid' => 0, 'is_complete' => 0));
        }

        $CI->adhoc_computation->reconstruct($fees);
        $result = $CI->adhoc_computation->breakdown();

        return $result;
    }

    public function general_status()
    {
        $CI = &get_instance();

        // a. Reserved - orange
        // b. On - Going - blue
        // c. Completed - green
        // d. Cancelled - red
        // e. Rebooked - red

        $status = 1;
        $balance = $this->get_remaining_balance(2, 0);
        $period_id = $this->current_period(1);

        if ($period_id == 1) {
            $status = 1;
        }

        if ($period_id == 2) {
            $status = 2;
        }

        if ((!$balance) || ($balance == "0.00")) {
            $status = 3;
        } else if ($period_id == 3) {
            $status = 2;
        }

        return $status;
    }

    public function collection_status()
    {
        $CI = &get_instance();

        // a. Pre - Reserved - yellow
        // b. Reserved - orange
        // c. On - Time - blue
        // d. Past Due - red
        // e. Fully Paid - green

        $status = 1;

        $rf_paid = $this->get_amount_paid(1);
        $rf_amount = $this->get_amount(1);
        $balance = $this->get_remaining_balance(2, 0);

        if ($rf_paid == $rf_amount) {
            $status = 2;
        }

        $penalty = $this->get_penalty(date('Y-m-d'));

        if (($status != 2) && (!$penalty)) {
            $status = 3;
        } else if (($status != 2) && ($penalty)) {
            $status = 4;
        }

        if ((!$balance) || ($balance == "0.00")) {
            $status = 5;
        }

        return $status;
    }

    // public function update_period_status(){
    //           $CI = &get_instance();

    //           $period_id = $this->current_period(1);
    //           $new_period_id = $period_id;
    //           $period_amount = $this->get_amount($period_id);
    //           $total_principal_paid = $this->get_amount_paid( 2, $period_id );
    //           $diff = $total_principal_paid - $period_amount;

    //           if ($diff < 1) {
    //               $new_period_id = $period_id + 1;
    //           }

    //           $general_status  = $this->general_status();
    //           $collection_status  = $this->collection_status();

    //           $d['period_id'] = $new_period_id;
    //           $d['general_status'] = $general_status;
    //           $d['collection_status'] = $collection_status;
    //           $d['documentation_status'] = 1;

    //           $transaction_id = $this->transaction_id;
    //        $CI->M_transaction->update($d,$transaction_id);

    //        $r['period_amount'] = $period_amount;
    //        $r['total_principal_paid'] = $total_principal_paid;
    //        $r['d'] = $d;
    //        return $r;
    // }

    public function update_period_status()
    {
        $CI = &get_instance();

        $period_id = $this->current_period(1);
        $new_period_id = $period_id;
        $period_amount = $this->get_amount($period_id);
        $total_principal_paid = $this->get_amount_paid(2, $period_id);

        if ($total_principal_paid >= $period_amount) {
            $new_period_id = $period_id + 1;
        }

        $general_status = $this->general_status();
        $collection_status = $this->collection_status();

        $d['period_id'] = $new_period_id;
        $d['general_status'] = $general_status;
        $d['collection_status'] = $collection_status;
        $d['documentation_status'] = 1;

        $transaction_id = $this->transaction_id;
        $CI->M_transaction->update($d, $transaction_id);

        $r['period_amount'] = $period_amount;
        $r['total_principal_paid'] = $total_principal_paid;
        $r['d'] = $d;
        return $r;
    }

    public function get_refund_details()
    { // type = 0:all, 1:amount, 2:status
        // Refund = ((Total Payment - Reservation)* Refund Percentage)-(Misc Fee + Processing Fee+Penalty)
        // Note: Variable values are Refund Percentage and Processing Fee
        // Standard Refund Percentage = 50%
        $CI = &get_instance();

        $rf = floatval($this->get_amount(1));
        $total_principal_paid = $this->get_amount_paid(2);
        $total_interest_paid = $this->get_amount_paid(3);
        $total_amount_paid = floatval($total_principal_paid) + floatval($total_interest_paid);

        $refund = $this->refund_percentage;

        $refund_amount = (($total_amount_paid - $rf) * $refund);

        return $refund_amount;
    }

    public function set_refund_percentage($percentage)
    {
        $this->refund_percentage = floatval($percentage);
    }

    public function update_transaction_status($user_id = 0, $type = '', $new_status = 0, $remarks = '')
    {
        $CI = &get_instance();

        $types = ['0' => 'general', '1' => 'collection', '3' => 'documentation'];

        if ($new_status) {
            $additional = [
                'created_by' => $user_id,
                'created_at' => NOW,
            ];

            if (!$remarks) {
                $remarks = 'System automated movement.';
            }

            $status = $this->current_status($type);
            $data['transaction_id'] = $this->transaction_id;
            $data['previous_status'] = (isset($status) && !empty($status) ? $status : 0);
            $data['current_status'] = $new_status;
            $data['type'] = $type;
            $data['remarks'] = $remarks;
            $CI->M_tstatus_logs->insert($data + $additional);

            $u_additional = [
                'updated_by' => $user_id,
                'updated_at' => NOW,
            ];

            $u_data[$types[$type] . "_status"] = $new_status;

            $CI->M_transaction->update($u_data + $u_additional, $this->transaction_id);
        }

        return true;
    }

    public function get_remainings($transaction_payment_id = 0)
    {
        $CI = &get_instance();

        $params['is_paid'] = 1;
        $params['is_complete'] = 1;
        $params['id <'] = $transaction_payment_id;

        $payments = $CI->M_transaction_payment->with_official_payments()->where($params)->order_by('id', 'DESC')->get_all();

        // vdebug($payments);

        $total_amount_paid = 0;
        $total_principal = 0;
        $total_interest = 0;

        if ($payments) {
            foreach ($payments as $key => $payment) {

                if (isset($payment['official_payments'])) {
                    break;
                } else {
                    $total_amount_paid += $payment['total_amount'];
                    $total_principal += $payment['principal_amount'];
                    $total_interest += $payment['interest_amount'];
                }
            }
        }

        $results['total_amount_paid'] = $total_amount_paid;
        $results['total_principal'] = $total_principal;
        $results['total_interest'] = $total_interest;

        return $results;
    }

    public function get_remainings_by_transaction_id($transaction_id = 0)
    {
        $CI = &get_instance();

        $params['is_paid'] = 0;
        $params['is_complete'] = 0;
        $params['transaction_id'] = $transaction_id;

        $payments = $CI->M_transaction_payment->with_official_payments()->where($params)->order_by('id', 'DESC')->get_all();

        $total_amount_paid = 0;
        $total_principal = 0;
        $total_interest = 0;

        if ($payments) {
            foreach ($payments as $key => $payment) {

                // if (isset($payment['official_payments'])) {
                //     break;
                // } else {
                $total_amount_paid += $payment['total_amount'];
                $total_principal += $payment['principal_amount'];
                $total_interest += $payment['interest_amount'];
                // }
            }
        }

        $results['total_amount_paid'] = $total_amount_paid;
        $results['total_principal'] = $total_principal;
        $results['total_interest'] = $total_interest;

        return $results;
    }


    public function filter_entries($off_payment_data = array(), $is_waived = false, $transaction_id = 0, $for_recognition = 0, $entry_type_id = 0)
    {
        $CI = &get_instance();

        $transaction = $CI->M_transaction->with_payments()->with_t_property()->get($transaction_id);
        $project_id = $transaction['project_id'];
        $company_id = get_value_field($transaction['project_id'], 'projects', 'company_id');
        $is_recognized = $transaction['is_recognized'];

        $filter['company_id'] = $company_id;
        $filter['project_id'] = $project_id;

        // Get all accounting settings base on period_id
        $period_id = $off_payment_data['period_id'];
        $penalty_amount = $off_payment_data['penalty_amount'];
        $interest_amount = $off_payment_data['interest_amount'];

        if ($for_recognition) {

            $filter['period_id'] = 5;
        } else {

            $filter['period_id'] = $period_id;
        }

        if ($entry_type_id) {
            # code...
            $filter['entry_types_id'] = $entry_type_id;
        }

        if ($is_recognized) {
            $filter['for_recognize'] = 1;
        }

        // Return accounting settings base on period id
        $accounting_setting = $CI->M_Accounting_settings->with_accounting_setting_item()->get($filter);

        if (!$accounting_setting) { // search without project id
            unset($filter['project_id']);
            $accounting_setting = $CI->M_Accounting_settings->with_accounting_setting_item()->get($filter);
        }

        return $accounting_setting;
    }

    public function generate_entries($off_payment_id = 0, $clear_transaction = false, $for_recognition = false, $collection_data = false, $debug = 0)
    {

        $CI = &get_instance();

        if ($off_payment_id) {
            // Get transaction official payments base on payment_id

            $off_payment_data = $CI->M_transaction_official_payment->get($off_payment_id);

            $transaction_id = $off_payment_data['transaction_id'];
            $payment_type_id = $off_payment_data['payment_type_id'];

            $t_penalty_data = $CI->M_tpenalty_logs->get(["transaction_official_payment_id" => $off_payment_id]);

            $transaction = $CI->M_transaction->with_payments()->with_t_property()->get($transaction_id);

            $is_waived = 1;

            // Check payment if waived
            if ($t_penalty_data) {
                $is_waived = $t_penalty_data['is_waived'];
            }

            // Check payment type if its a cheque

            $is_cheque = $this->is_cheque($payment_type_id);


            if ($clear_transaction) {    // If payment is for clearing, clear transaction first

                $accounting_setting =  $this->filter_entries($off_payment_data, $is_waived, $transaction_id, 0, 1);

                $res = $this->clear_transaction($accounting_setting, $transaction, $off_payment_data);
            } elseif ($for_recognition) { // If payment is for recognition, create accounting entries for recognize sale

                $accounting_setting =  $this->filter_entries($off_payment_data, $is_waived, $transaction_id, true, 1, 1);

                $res = $this->sales_recognize($accounting_setting, $transaction, $off_payment_data);
            } else {

                if (!$is_cheque) { // If payment type is not a cheque generate accounting entries

                    $accounting_setting =  $this->filter_entries($off_payment_data, $is_waived, $transaction_id, 0, 1);

                    // Save entries to accounting entries
                    $res = $this->insert_entries($accounting_setting, $transaction, $off_payment_data);
                } else { // If payment type is cheque save to ar clearing

                    // Save to ar clearing

                    $res = $this->save_ar_clearing($payment_type_id, $off_payment_data, $transaction, $collection_data);
                }

                $journal_setting =  $this->filter_entries($off_payment_data, $is_waived, $transaction_id, 0, 2);

                // vdebug($journal_setting);

                if ($journal_setting) {

                    $this->insert_entries($journal_setting, $transaction, $off_payment_data);
                }
            }

            if ($debug) {
                vdebug($accounting_setting);
            }

            return $res;
        }
    }

    public function clear_transaction($accounting_setting = array(), $transaction = array(), $info = array())
    {
        $CI = &get_instance();
        $check_ar_table = ["transaction_id" => $transaction['id']];

        $ar = $CI->M_Ar_clearing->get($check_ar_table);

        $cleared = ["is_active" => 3];
        $CI->M_Ar_clearing->update($cleared, $ar['id']);

        $accounting_setting_item = $accounting_setting['accounting_setting_item'];

        $project_id = $transaction['project_id'];

        $project = $CI->M_project->get($project_id);

        // Entries
        $entries['or_number'] = $info['or_number'];
        $entries['company_id'] = $project['company_id'];
        $entries['invoice_number'] = $info['or_number'];
        $entries['journal_type'] = 3;
        $entries['payment_date'] = $info['or_date'];
        $entries['payee_type'] = "buyers";
        $entries['payee_type_id'] = $transaction['buyer_id'];
        $entries['remarks'] = $info['remarks'];
        $entries['cr_total'] = $info['amount_paid'];
        $entries['dr_total'] = $info['amount_paid'];
        $entries['is_approve'] = 1;


        $remarks = $info['remarks'];

        $accounting_setting_id = $accounting_setting['id'];

        $accounting_setting_items = $CI->M_Accounting_setting_items->get_all(["accounting_settings_id" => $accounting_setting_id]);

        $entry_id = $CI->M_Accounting_entries->insert($entries);

        foreach ($accounting_setting_items as $key => $entry) {
            $entry_item['amount'] = $amount = $this->get_entry_item_amount($entry['origin_id'], $info);

            if ($amount <= 0) {
                continue;
            }

            $entry_item['accounting_entry_id'] = $entry_id;
            $entry_item['ledger_id'] = $entry['accounting_entry'];
            $entry_item['dc'] = $entry['accounting_entry_type'];
            $entry_item['payee_type'] = "buyers";
            $entry_item['payee_type_id'] = $transaction['buyer_id'];
            $entry_item['is_reconciled'] = 0;
            $entry_item['description'] = $remarks;

            $CI->M_Accounting_entry_items->insert($entry_item);
        }

        $CI->db->trans_complete(); # Completing transaction

        /*Optional*/
        if ($CI->db->trans_status() === false) {
            # Something went wrong.
            $CI->db->trans_rollback();
            $response['status'] = 0;
            $response['message'] = 'Error!';
        } else {
            # Everything is Perfect.
            # Committing data to the database.
            $CI->db->trans_commit();
            $response['status'] = 1;
            $response['entry_id'] = $entry_id;
            $response['message'] = 'Transaction successfully cleared.';
        }
        return $response;
    }

    public function is_cheque($payment_type_id = 0)
    {
        if ($payment_type_id) {
            if ($payment_type_id == 2 || $payment_type_id == 3 || $payment_type_id == 4 || $payment_type_id == 6) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function insert_entries($accounting_setting = array(), $transaction = array(), $info = array())
    {
        $CI = &get_instance();
        $accounting_setting_item = $accounting_setting['accounting_setting_item'];

        $project_id = $transaction['project_id'];
        $property_id = $transaction['property_id'];

        $project = $CI->M_project->get($project_id);

        // Entries
        $entries['or_number'] = $info['or_number'];
        $entries['company_id'] = $project['company_id'];
        $entries['project_id'] = $project_id;
        $entries['property_id'] = $property_id;
        $entries['invoice_number'] = $info['or_number'];
        $entries['journal_type'] = ($accounting_setting['entry_types_id'] == 2) ? 1 : 3;
        $entries['payment_date'] = $info['or_date'];
        $entries['payee_type'] = "buyers";
        $entries['payee_type_id'] = $transaction['buyer_id'];
        $entries['remarks'] = $info['remarks'];
        $entries['cr_total'] = $info['amount_paid'];
        $entries['dr_total'] = $info['amount_paid'];
        $entries['is_approve'] = 1;

        $remarks = $info['remarks'];

        $accounting_setting_id = $accounting_setting['id'];

        // Insert entries based on returned accounting setting

        # code...

        $accounting_setting_items = $CI->M_Accounting_setting_items->get_all(["accounting_settings_id" => $accounting_setting_id]);

        $entry_id = $CI->M_Accounting_entries->insert($entries);


        foreach ($accounting_setting_items as $key => $entry) {
            $entry_item['amount'] = $amount = $this->get_entry_item_amount($entry['origin_id'], $info);

            if ($amount <= 0) {
                continue;
            }

            $entry_item['accounting_entry_id'] = $entry_id;
            $entry_item['ledger_id'] = $entry['accounting_entry'];
            $entry_item['dc'] = $entry['accounting_entry_type'];
            $entry_item['payee_type'] = "buyers";
            $entry_item['payee_type_id'] = $transaction['buyer_id'];
            $entry_item['is_reconciled'] = 0;
            $entry_item['description'] = $remarks;



            $CI->M_Accounting_entry_items->insert($entry_item);
        }

        $CI->db->trans_complete(); # Completing transaction

        /*Optional*/
        if ($CI->db->trans_status() === false) {
            # Something went wrong.
            $CI->db->trans_rollback();
            $response['status'] = 0;
            $response['message'] = 'Error!';
        } else {
            # Everything is Perfect.
            # Committing data to the database.
            $CI->db->trans_commit();
            $response['status'] = 1;
            $response['entry_id'] = $entry_id;
            $response['message'] = 'Entries successfully generated!';
        }
        return $response;
    }

    public function sales_recognize($accounting_setting = array(), $transaction = array(), $info = array())
    {
        $CI = &get_instance();

        $project_id = $transaction['project_id'];

        $project = $CI->M_project->get($project_id);

        $all_payment = $CI->M_Transaction_official_payment->get_all(["transaction_id" => $transaction['id']]);

        // Entries
        $entries['or_number'] = $info['or_number'];
        $entries['company_id'] = $project['company_id'];
        $entries['invoice_number'] = $info['or_number'];
        $entries['journal_type'] = 3;
        $entries['payment_date'] = $info['or_date'];
        $entries['payee_type'] = "buyers";
        $entries['payee_type_id'] = $transaction['buyer_id'];
        $entries['remarks'] = $info['remarks'];
        $entries['cr_total'] = $info['amount_paid'];
        $entries['dr_total'] = $info['amount_paid'];
        $entries['is_approve'] = 1;


        $remarks = $info['remarks'];

        $accounting_setting_id = $accounting_setting['id'];

        $accounting_setting_items = $CI->M_Accounting_setting_items->get_all(["accounting_settings_id" => $accounting_setting_id]);

        // Insert Entries

        $entry_id = $CI->M_Accounting_entries->insert($entries);

        $_off_data['entry_id'] =  $entry_id;

        if ($entry_id) {

            $_off_res = $CI->M_Transaction_official_payment->update($_off_data, $info['id']);
        }

        // Insert entry items base on returned accounting setting items

        foreach ($accounting_setting_items as $key => $entry) {
            $entry_item['amount'] = $amount = $this->get_sales_after_recog_amount($key, $all_payment, $transaction);

            if ($amount <= 0) {
                continue;
            }

            $entry_item['accounting_entry_id'] = $entry_id;
            $entry_item['ledger_id'] = $entry['accounting_entry'];
            $entry_item['dc'] = $entry['accounting_entry_type'];
            $entry_item['payee_type'] = "buyers";
            $entry_item['payee_type_id'] = $transaction['buyer_id'];
            $entry_item['is_reconciled'] = 0;
            $entry_item['description'] = $remarks;

            $CI->M_Accounting_entry_items->insert($entry_item);
        }

        $CI->db->trans_complete(); # Completing transaction

        /*Optional*/
        if ($CI->db->trans_status() === false) {
            # Something went wrong.
            $CI->db->trans_rollback();
            $response['status'] = 0;
            $response['message'] = 'Error!';
        } else {
            # Everything is Perfect.
            # Committing data to the database.
            $CI->db->trans_commit();
            $response['status'] = 1;
            $response['entry_id'] = $entry_id;
            $response['message'] = 'Entries successfully generated!';
        }
        return $response;
    }

    public function get_entry_item_amount($origin_id = 0, $info = array())
    {
        // 4 = Interest
        // 5 = Penalty

        if ($origin_id) {

            $interest_amount = $info['interest_amount'];
            $penalty_amount = $info['penalty_amount'];
            $principal_amount = $info['principal_amount'];
            $amount_paid = $info['amount_paid'];
            $rebate_amount = $info['rebate_amount'];

            if ($origin_id == 2) {
                // principal
                return $principal_amount;
            } elseif ($origin_id == 4) {
                // Interest
                return $interest_amount;
            } elseif ($origin_id == 5) {
                // Penalty
                return $penalty_amount;
            } elseif ($origin_id == 9) {
                // Penalty
                // $r = $this->get_remainings_by_transaction_id($info['transaction_id']);

                // return $r['total_interest'];
                return $rebate_amount;
            } else {

                return $amount_paid;
            }
        }
    }

    public function get_sales_after_recog_amount($key = 0, $payments = array(), $transaction = array())
    {

        $customers_deposit = 0;
        $accounts_receivable = $transaction['t_property']['collectible_price'];
        $tcp = $transaction['t_property']['total_contract_price'];
        $interest_income = 0; // Collected
        $interest_receivable = 0; // Not yet collected
        $cash_in_bank = 0;
        $penalty_income = 0;

        $transaction_payments = $transaction['payments'];

        // Computations

        foreach ($payments as  $payment) {
            $customers_deposit += $payment['principal_amount'];
            $interest_income += $payment['interest_amount'];
            $cash_in_bank += $payment['amount_paid'];
            $penalty_income += $payment['penalty_amount'];
        }

        foreach ($transaction_payments as  $t_payment) {
            $interest_receivable += $t_payment['interest_amount'];
        }

        $unearned_interest_income = $interest_receivable - $interest_income;

        $tax_percent = 12;

        $tax = ($tax_percent / 100) * $tcp;

        $output_vat = ($interest_income / 1.12) * 0.12;

        switch ($key) {
            case 0: //Cash in bank
                return $cash_in_bank;
                break;
            case 1: // Penalty Income
                return $penalty_income;
                break;
            case 2: // Output VAT
                return $output_vat;
                break;
            case 14: // Interest Receivable
                return $interest_receivable;
                break;
            case 13: // Unearned Interest Income
                return $unearned_interest_income;
                break;
            case 14: // Accounts Receivable
                return $accounts_receivable;
                break;
            default:
                return 0;
                break;
        }
    }

    public function save_ar_clearing($payment_type_id = 0, $info = array(), $transaction = array(), $_collect_data = array())
    {

        $CI = &get_instance();
        $collection_data = $_collect_data['info'];

        // $ar_clearing["payment_type"] = $payment_type_id;
        // $ar_clearing["amount"] = $info['amount_paid'];
        $ar_clearing["buyer_id"] = $transaction['buyer_id'];
        $ar_clearing["project_id"] = $transaction['project_id'];
        $ar_clearing["property_id"] = $transaction['property_id'];
        $ar_clearing["transaction_id"] = $transaction['id'];
        $ar_clearing["is_active"] = 1;

        $ar_clearing["period_count"] = $collection_data['period_count'];
        $ar_clearing["period_id"] = $collection_data['period_id'];
        $ar_clearing["payment_date"] = $collection_data['payment_date'];
        $ar_clearing["or_date"] = $collection_data['or_date'];
        $ar_clearing["is_waived"] = $collection_data['is_waived'];
        $ar_clearing["penalty_amount"] = $collection_data['penalty_amount'];
        $ar_clearing["amount_paid"] = $collection_data['amount_paid'];
        $ar_clearing["payment_type_id"] = $collection_data['payment_type_id'];
        $ar_clearing["post_dated_check_id"] = $collection_data['post_dated_check_id'];
        $ar_clearing["cash_amount"] = $collection_data['cash_amount'];
        $ar_clearing["check_deposit_amount"] = $collection_data['check_deposit_amount'];
        $ar_clearing["receipt_type"] = $collection_data['receipt_type'];
        $ar_clearing["OR_number"] = $collection_data['OR_number'];
        $ar_clearing["adhoc_payment"] = $collection_data['adhoc_payment'];
        $ar_clearing["grand_total"] = $collection_data['grand_total'];
        $ar_clearing["remarks"] = @$collection_data['remarks'];
        $ar_clearing["transaction_payment_id"] = $_collect_data['transaction_payment_id'];
        $ar_clearing["payment_application"] = $_collect_data['payment_application'];

        // Insert ar clearing data

        $result = $CI->M_Ar_clearing->insert($ar_clearing);

        $CI->db->trans_complete(); # Completing transaction

        if ($CI->db->trans_status() === false) {

            $CI->db->trans_rollback();
            $response['status'] = 0;
            $response['message'] = 'Error!';
        } else {

            $CI->db->trans_commit();
            $response['status'] = 1;
            $response['message'] = 'Successfully saved to AR Clearing!';
        }

        echo json_encode($response);
        exit();
    }

    public function check_commission_status($transaction_id = 0, $user_id = 0)
    {
        $CI = &get_instance();

        $_document_checklists    =    $CI->M_document_checklist->fields('document_id')->where('checklist_id', 4)->get_all();

        if ($_document_checklists) {

            $_ids = [];

            foreach ($_document_checklists as $key => $value) {
                $_ids[$key] = $value['document_id'];
            }

            $_file_count = $CI->M_transaction_document->where('transaction_id', $transaction_id)->where('document_id', $_ids)->as_array()->count_rows();
            $_has_file = $CI->M_transaction_document->where(['transaction_id' => $transaction_id, 'has_file' => 1])->where('document_id', $_ids)->as_array()->count_rows();

            $this->initiate($transaction_id);
            $amount = $this->get_amount_paid(1, 0, 1);

            $transaction_commissions = $CI->M_Transaction_commission->where('transaction_id', $transaction_id)->get_all();

            foreach ($transaction_commissions as $key => $value) {
                if ($value['period_type_id'] == 1) {
                    if ($_file_count == $_has_file) {
                        if ($value['status'] != 2) {
                            $data = [
                                'status' => 2,
                                'updated_by' => $user_id,
                                'updated_at' => NOW,
                            ];
                            $result = $CI->M_Transaction_commission->update($data, $value['id']);
                        }
                    }
                } else {
                    if ($amount >= $value['percentage_rate_to_collect']) {
                        if ($value['status'] != 2) {
                            $data = [
                                'status' => 2,
                                'updated_by' => $user_id,
                                'updated_at' => NOW,
                            ];
                            $result = $CI->M_Transaction_commission->update($data, $value['id']);
                        }
                    }
                }
            }
        }
    }

    public function get_remaining_dp($transaction_id, $days = 0)
    {
        $CI = &get_instance();
        $period_id = 2; // downpayment
        $result = [];

        if ($transaction_id) {

            if ($days) {
                $result = $CI->M_transaction_payment->where(['transaction_id' => $transaction_id, 'period_id' => $period_id, 'is_paid' => 0, 'is_complete' => 0])->get_all();
            } else {
                $result = $CI->M_transaction_payment->where(['transaction_id' => $transaction_id, 'period_id' => $period_id, 'is_paid' => 1, 'is_complete' => 0])->get_all();

                if (empty($result)) {
                    $result = $CI->M_transaction_payment->where(['transaction_id' => $transaction_id, 'period_id' => $period_id, 'is_paid' => 0, 'is_complete' => 0])->get_all();
                }
            }

            return $result;
        } else {
            return false;
        }
    }

    public function movein_mortgage($transaction_id, $fs_data, $user_id)
    {
        $CI = &get_instance();
        $breakdown = [];
        $monthly_payment = 0;
        $tcp = 0;

        $remaining_balance = $this->get_remaining_balance(1, 3);

        // delete unpaid transaction
        $CI->M_transaction_payment->delete(array('transaction_id' => $transaction_id, 'is_paid' => 0, 'is_complete' => 0));

        $transaction = $CI->M_transaction->with_tbillings()->with_billings()->with_payments()->with_t_property()->get($transaction_id);

        $return['status'] = 0;
        $return['message'] = 'Oops! Please refresh the page and try again.';

        $billings = $transaction['billings'];
        $additional = [
            'created_by' => $user_id,
            'created_at' => NOW
        ];

        $u_additional = [
            'updated_by' => $user_id,
            'updated_at' => NOW
        ];

        if ($billings) {
            foreach ($billings as $key => $value) {
                if ($value['period_id'] === '3') {
                    $completed_terms = 0;
                    $monthly_payment = 0;
                    $terms = 0;

                    // get number of completed terms count
                    $completed_terms = $CI->M_transaction_payment->completed_terms_count($transaction_id, $value['period_id']);
                    $completed_terms = $completed_terms[0]['count'];

                    // check for balloon payments
                    $params['transaction_id'] = $transaction_id;
                    $params['period_id'] = $value['period_id'];
                    $params['payment_application'] = 2;

                    $balloon_payments = $CI->M_transaction_official_payment->where($params)->get_all();

                    // count periods from balloon payments
                    // subtract from remaining period to get the total number of period terms left
                    if ($balloon_payments) {

                        foreach ($balloon_payments as $k => $v) {
                            $terms += intval($v['period_count']);
                        }
                    }

                    // remaining terms = period_term - (completed terms - number of terms from balloon payment)
                    $period_term = intval($value['period_term']) - (intval($completed_terms) + intval($terms));
                    $value['period_term'] = $period_term;


                    // monthly payment
                    $monthly_payment = floatval($remaining_balance) / $period_term;

                    // tcp = property collectable price and financing scheme computation
                    $CI->mortgage_computation->reconstruct($value, $tcp, $monthly_payment);

                    $steps = $CI->mortgage_computation->breakdown();

                    if ($value['period_term'] <= 0 || $value['period_term'] == $completed_terms) {
                        continue;
                    }

                    if (isset($steps['monthly']) && ($steps['monthly'])) {
                        foreach ($steps['monthly'] as $key => $step) {
                            $countterm = $step['month'] + $completed_terms;

                            $payment['transaction_id'] = $transaction_id;
                            $payment['total_amount'] = $step['principal'] + $step['interest'];
                            $payment['principal_amount'] = $step['principal'] +  $step['interest'];
                            $payment['interest_amount'] = 0;
                            $payment['beginning_balance'] = $step['beginning_balance'];
                            $payment['ending_balance'] = $step['ending_balance'];
                            $payment['period_id'] = $value['period_id'];
                            $payment['particulars'] = ordinal($countterm) . ' ' . Dropdown::get_static('periods', $value['period_id'], 'view') . ' Payment';
                            $payment['is_paid'] = 0;
                            $payment['is_complete'] = 0;
                            $payment['due_date'] = $step['due_date'];
                            $payment['next_payment_date'] = $step['next_payment_date'];

                            $a = $CI->M_transaction_payment->insert($payment + $additional);
                            $steps['monthly'][$key] = $payment['particulars'];
                        }
                    }
                    $breakdown[] = $steps;
                }
                continue;
            }

            $return['message'] = 'Move in process success!';
            $return['status'] = 1;
        }

        return $return;
    }

    public function get_date_of_reaching_twenty_five_percent()
    {
        $totals = $this->get_amount(0);

        $CI = &get_instance();

        $params['transaction_id'] = $this->transaction_id;

        $collections = $CI->M_transaction_official_payment->where($params)->get_all();

        $amount = "0.00";

        if ($collections) {

            foreach ($collections as $key => $collection) {

                $amount = $amount + $collection['principal_amount'];

                if ((empty($amount)) || (empty($totals) || $totals == "0.00")) {
                    $percentage = 0;
                } else {
                    $percentage = $amount / $totals;
                }

                if (convertPercentage($percentage) >= 25) {

                    return view_date($collection['payment_date']);
                }
            }
        }

        return 'N/A';
    }


    public function billing_logs($period_id = 0)
    {
     
        $CI = &get_instance();

        $amount = 0;
        $params['transaction_id'] = $this->transaction_id;
        $params['period_id'] = $period_id;

        $payment = $CI->M_transaction_payment->order_by('id', 'ASC')->where($params)->get();

        if ($payment) {

            $amount = $payment['beginning_balance'];

        }

        return $amount;
    }
}
