<?php defined('BASEPATH') or exit('No direct script access allowed');

class Dropdown
{
    private $_ci;
    private static $cache = array();

    public function __construct($config = array())
    {
        $this->_ci = &get_instance();
    }

    public static function get_dynamic($name, $value = false, $text = 'name', $key = 'id', $action = 'form', $order_by = false, $where = array())
    {
        $CI = &get_instance();

        // basic dropdown only
        if ($order_by) {
            $CI->db->order_by($order_by, 'asc');
        } else {
            $CI->db->order_by($text, 'asc');
        }

        if ($where) {
            foreach ($where as $keyx => $w) {
                $CI->db->where($keyx, $w);
            }
        }

        $CI->db->where("deleted_at IS NULL");

        $result = $CI->db->get($name)->result_array();


        $rows = refactor_rows($result, $key, $text);

        if (($action != "form")) {
            return @$rows[$value];
        }

        return $rows;
    }

    public static function get($name = [], $model, $val = 'id', $text = 'name', $placeholder = '')
    {

        $CI = &get_instance();

        $CI->load->model($model);

        $result = $CI->$model->fields($name)->order_by($text)->get_all();

        $option = array('' => ucwords($placeholder));
        $rows = refactor_rows($result, $val, $text);
        return $option + $rows;
    }

    public static function get_static($name, $value = false, $action = "form")
    {
        if (isset(self::$cache['static' . $name])) {
            $array = self::$cache['static' . $name];

            if (($value) || ($action != "form")) {
                return @$array[$value];
            }

            return $array;
        }

        switch ($name) {

            case "bool":
                $array = array('0' => 'No', '1' => 'Yes');
                break;
                // $array = array('' => 'Yes / No', '1' => 'Yes', '0' => 'No');
            case "days":
                // $array = array('0'=>'No', '1'=>'Yes');
                $array = array('' => 'Select Day', '1' => 'Monday', '2' => 'Tuesday', '3' => 'Wednesday', '4' => 'Thursday', '5' => 'Friday', '6' => 'Saturday', '7' => 'Sunday');
                break;

            case "civil_status":
                $array = array('' => 'Select Civil Status', '1' => 'Single', '2' => 'Married', '3' => 'Widow/Widower');
                break;

            case "security":
                $array = array('' => 'Select Security Type', '1' => 'Confidential', '2' => 'Highly Confidential', '3' => 'Restricted');
                break;

            case "classification":
                $array = array('' => 'Select Document Classification', '1' => 'Client Document', '2' => 'Company Document', '3' => 'Government Document');
                break;

            case "parties":
                $array = array('' => 'Select Issuing Party', '1' => 'Company', '2' => 'Client', '3' => 'City Hall', '4' => 'Registrar', '5' => "Engineer's Office", '6' => "Planning Office", '7' => "City Assessors", '8' => "Capelco", '9' => "MRWD", '10' => "Bureau of Lands", '11' => "Registry of Deeds", '12' => "Pag-ibig", '13' => "Bank");
                break;

            case "appointee":
                $array = array('' => 'Select Client Appointee', '4' => 'Principal', '1' => 'Spouse', '2' => 'Attorney in fact', '3' => 'Co-borrower');
                break;

            case "hlurb_classification":
                $array = array('' => 'Select HLURB Classification', '1' => 'PD957', '2' => 'BP220', '3' => 'Socialize');
                break;

            case "land_classification":
                $array = array('' => 'Select Land Classification', '1' => 'Agricultural', '2' => 'Agricultural, Orchard', '3' => 'Agricultural, Brgy. Road', '4' => 'Agricultural, Commercial, Brgy. Road', '5' => 'Brgy. Road', '6' => 'Cogonal', '7' => 'Cogonal, Agricultural', '8' => 'Cogonal, Residential', '9' => 'Cogonld., Agricultural, Residential', '10' => 'Commercial', '11' => 'Commercial, Residential', '12' => 'Drainage', '13' => 'Easement', '14' => 'Educational', '15' => 'Existing Road', '16' => 'Fishpond', '17' => 'Fishpond (28,926);ROW(1,562)', '18' => 'Fishpond, Agricultural', '19' => 'Fishpond, Swampy', '20' => 'Land', '21' => 'Machinery', '22' => 'Nipa Ld.; Residential, Road Right of Way', '23' => 'Partly Agricultural., Partly Res., Partly Roadlot', '24' => 'Pasture, Agricultural', '25' => 'Private Road', '26' => 'Public Easement', '27' => 'Residential', '28' => 'Residential (Public Easement)', '29' => 'Residential, Parking Space', '30' => 'Residential, Road', '31' => 'Residential/Agricultural', '32' => 'Residential/Commercial', '33' => 'Road', '34' => 'Un. Riceland (Agricultural)', '35' => 'Un.Irr.Riceld. (Agricultural)', '36' => 'Sugar Ld. (Agricultural)', '37' => 'Pasture, Residential');
                break;

            case "ownership_classification":
                $array = array('' => 'Select Ownership Classification', '1' => 'Purchase', '2' => 'Joint-Venture');
                break;

            case "land_inventory_status":
                // $array = array('0'=>'Select Status', '1'=>'For Research', '2'=>'For Negotiation', '3'=>'For Purchase', '4'=>'Purchased - For Transfer of Title', '5'=>'Purchased - Under BOD and Other Name', '6'=>'Purchased - Title Transferred', '7'=>'Sold', '8'=>'Transferred', '9'=>'Subdivided', '10'=>'Consolidated', '11'=>'Joint Venture','12'=>'New','13'=>'Converted to Project','14'=>'Cancelled' , '15'=>'Consolidated and Subdivided');
                $array = array('' => 'Select Status', '1' => 'For Research', '2' => 'For Negotiation', '3' => 'For Purchase', '4' => 'Purchased - For Transfer of Title', '5' => 'Purchased - Under BOD and Other Name', '6' => 'Purchased - Title Transferred', '7' => 'Sold', '8' => 'Transferred', '9' => 'Subdivided', '10' => 'Consolidated', '11' => 'Joint Venture', '12' => 'New', '13' => 'Converted to Project', '14' => 'Cancelled', '15' => 'Consolidated and Subdivided');
                break;

            case "sex":
                $array = array('' => 'Select gender', '1' => 'Male', '2' => 'Female');
                break;

            case "user_type":
                $array = array('' => 'Select User Type', '1' => 'Client', '2' => 'Payor', '3' => 'Personnel', '4' => 'Agent', '5' => 'Broker');
                break;

            case "house_ownership_type":
                $array = array('' => 'Select House Ownership Type', '1' => 'Owned', '2' => 'Rent');
                break;

            case "employment_type":
                $array = array('' => 'Select Employment Type', '1' => 'Contractual', '2' => 'Probationary', '3' => 'Regular', '4' => 'Consultancy', '5' => 'Self-Employed');
                break;

            case "client_type":
                $array = array('' => 'Select Client Type', '1' => 'Customer', '2' => 'Payor');
                break;

            case "client_transaction_type":
                $array = array('' => 'Select Client Transaction Type', '1' => 'Principal', '2' => 'Spouse', '3' => 'AIF', '4' => "Co-Buyer", '5' => "Company");
                break;

            case "client_status_type":
                $array = array('' => 'Select Client Status Type', '1' => 'Prospect', '2' => 'Sure Buyer');
                break;

            case "field_types":
                $array = array('text' => 'Text', 'sqm' => 'SQM', 'php' => 'Currency', 'datetime' => 'DateTime', 'choices' => 'Choices');
                break;

            case "project_type":
                $array = array('' => 'Select Project Type', '1' => 'Subdivision', '2' => 'Condominium', '3' => 'Commercial Lot');
                break;
            case "sub_project_type":
                $array = array('' => 'Select Project Type', '1' => 'House & Lots', '2' => 'Lots Only', '3' => 'Condominium', '4' => 'Commercial');
                break;

            case "project_status":
                $array = array('' => 'Select Project Status', '1' => 'Ongoing', '2' => 'Completed');
                break;

            case "unit_type":
                $array = array('' => 'Select Unit Type', '1' => 'Freya (SA35)', '2' => 'Andrei (SA36)', '3' => 'Andrea (SA50)', '4' => 'Bettina (SD50)', '5' => 'Elysa (2FSA86)', '6' => 'Cassandra (2FSA112)', '7' => 'Lots Only', '8' => 'Happy Duplex (1FDP34)', '9' => 'Commercial', '10' => 'Parking', '11' => 'DAFFODIL (2FRH45)', '12' => 'Dianna', '13' => 'Dianne', '14' => 'Deluxe Lots (2BR Home)', '15' => ' Premiere Lots (4BR Home)', '16' => ' Exclusive Lots (3BR Home)');
                break;
            case "option_unit_type":
                $array = array('' => 'Select Option Unit Type', '1' => 'Bare', '2' => 'Fully Finished', '3' => 'Finished');
                break;

            case "construction_method":
                $array = array('' => 'Select Construction Method', '1' => 'Cast-In Place', '2' => 'Pre-Cast', '3' => 'Hybrid');
                break;

            case "property_option":
                $array = array('' => 'Select Property Option', '1' => 'Main Road', '2' => 'Corner/Park', '3' => 'Corner Main Road/Corner Park', '4' => 'Creekside', '5' => 'Riverside');
                break;

            case "property_status":
                $array = array('' => 'Select Property Status', '1' => 'Available', '2' => 'Reserved', '3' => 'Sold', '4' => 'Hold', '5' => 'Not For Sale');

                break;

            case "person_type":
                $array = array('' => 'Select Person Type', '1' => 'Principal', '2' => 'Co-borrower');
                break;

            case "contact_type":
                $array = array('' => 'Select Contact Type', '1' => 'Mobile', '2' => 'Landline');
                break;

            case "contact_sub_type":
                $array = array('' => 'Select Sub Contact Type', '1' => 'Smart', '2' => 'Sun', '3' => 'Globe', '4' => 'Abroad', '5' => 'Other');
                break;

            case "group":
                $array = array('' => 'Select Process', '1' => 'Research Phase', '2' => 'Preparation Phase', '3' => 'Registration/ Transfer', '4' => 'Prepare Company Documents For Conversion', '5' => 'Prepare Tax Related Documents', '6' => 'Process Government Forms and Certification for Conversion', '7' => 'Prepare Engineering Plans for Conversion', '8' => 'Preparation of Documents for subdivision purpose', '9' => 'Preparation of Documents Needed if named under corp', '10' => 'Issuance of New Individual Titles Based on Subdivision Plan', '11' => 'Process Endorsement and Certification', '12' => 'Process Endorsement and Certification', '13' => 'FADP Application', '14' => 'Project Feasibility Study Documents', '15' => 'FADP Attached Documents- Engineering Plans', '16' => 'Approved FADP', '17' => 'PALC Application', '18' => 'Secure Certified True Copy of Proof of Acquisition Documents', '19' => 'PALC Attached Documents-Government Issued Documents', '20' => 'Approved PALC', '21' => 'CR-LTS Application', '22' => 'Secure Certified True Copy of Proof of Acquisition Documents', '23' => 'CR-LTS Attached Project Feasibility Study Documents', '24' => 'CR-LTS Attached - Company Prepared Documents', '25' => 'Secure Certified copy of Certification', '26' => 'CR-LTS Government Issued Documents', '27' => 'CR-LTS  Attached Documents- Engineering Plans', '28' => 'Approved CR-LTS', '29' => 'Individual Titling Documents', '30' => 'Issuance of New Individual Titles', '31' => 'Reservation');
                break;

            case "dependency":
                $array = array('' => 'Select Process', '1' => 'Research Phase', '2' => 'Preparation Phase', '3' => 'Registration/ Transfer', '4' => 'Prepare Company Documents For Conversion', '5' => 'Prepare Tax Related Documents', '6' => 'Process Government Forms and Certification for Conversion', '7' => 'Prepare Engineering Plans for Conversion', '8' => 'Preparation of Documents for subdivision purpose', '9' => 'Preparation of Documents Needed if named under corp', '10' => 'Issuance of New Individual Titles Based on Subdivision Plan', '11' => 'Process Endorsement and Certification', '12' => 'Process Endorsement and Certification', '13' => 'FADP Application', '14' => 'Project Feasibility Study Documents', '15' => 'FADP Attached Documents- Engineering Plans', '16' => 'Approved FADP', '17' => 'PALC Application', '18' => 'Secure Certified True Copy of Proof of Acquisition Documents', '19' => 'PALC Attached Documents-Government Issued Documents', '20' => 'Approved PALC', '21' => 'CR-LTS Application', '22' => 'Secure Certified True Copy of Proof of Acquisition Documents', '23' => 'CR-LTS Attached Project Feasibility Study Documents', '24' => 'CR-LTS Attached - Company Prepared Documents', '25' => 'Secure Certified copy of Certification', '26' => 'CR-LTS Government Issued Documents', '27' => 'CR-LTS  Attached Documents- Engineering Plans', '28' => 'Approved CR-LTS', '29' => 'Individual Titling Documents', '30' => 'Issuance of New Individual Titles', '31' => 'Reservation');
                break;

            case "document_owner":
                // $array = array( '0'=>'Select Owner', '1'=>'Legal Staff', '2'=>'Legal Head', '3'=>'Agent', '4'=>'Sales Admin', '5'=>'AR Staff', '6'=>'AR Head', '7'=>'Titling Officer', '8'=>'Tax', '9'=>'Engineering', '10'=>'Cashier', '11'=>'Sales Coordinator', '12'=>'Client', '13'=>'Credit and Collection');
                $array = array('' => 'Select Owner', '1' => 'Legal Staff', '2' => 'Legal Head', '3' => 'Agent', '4' => 'Sales Admin', '5' => 'AR Staff', '6' => 'AR Head', '7' => 'Titling Officer', '8' => 'Tax', '9' => 'Engineering', '10' => 'Cashier', '11' => 'Sales Coordinator', '12' => 'Client', '13' => 'Credit and Collection');
                break;

            case "access_right":
                // $array = array( ''=>'Select Access Right', '1'=>'Legal Staff', '2'=>'Legal Head', '3'=>'Agent', '4'=>'Sales Admin', '5'=>'AR Staff', '6'=>'AR Head', '7'=>'Titling Officer', '8'=>'Tax', '9'=>'Engineering', '10'=>'Cashier', '11'=>'Sales Coordinator', '12'=>'Client', '13'=>'Credit and Collection');
                $array = array('1' => 'Legal Staff', '2' => 'Legal Head', '3' => 'Agent', '4' => 'Sales Admin', '5' => 'AR Staff', '6' => 'AR Head', '7' => 'Titling Officer', '8' => 'Tax', '9' => 'Engineering', '10' => 'Cashier', '11' => 'Sales Coordinator', '12' => 'Client', '13' => 'Credit and Collection');
                break;

            case "companies":
                $array = array('' => 'Select Company', '1' => 'PDPI', '2' => 'SHJDC');
                break;

            case "document_checklist_category":
                // $array = array( '0'=>'General', '1'=>'Land Inventory', '2'=>'Licensing', '3'=>'Sales', '4'=>'Construction', '5'=>'Financing');
                $array = array('' => 'Select Category Name', '1' => 'General', '2' => 'Land Inventory', '3' => 'Licensing', '4' => 'Sales', '5' => 'Construction', '6' => 'Financing');
                break;

            case "nationality":
                $array = array('Select Nationality', 'Afghan', 'Albanian', 'Algerian', 'American', 'Andorran', 'Angolan', 'Antiguans', 'Argentinean', 'Armenian', 'Australian', 'Austrian', 'Azerbaijani', 'Bahamian', 'Bahraini', 'Bangladeshi', 'Barbadian', 'Barbudans', 'Batswana', 'Belarusian', 'Belgian', 'Belizean', 'Beninese', 'Bhutanese', 'Bolivian', 'Bosnian', 'Brazilian', 'British', 'Bruneian', 'Bulgarian', 'Burkinabe', 'Burmese', 'Burundian', 'Cambodian', 'Cameroonian', 'Canadian', 'CapeVerdean', 'CentralAfrican', 'Chadian', 'Chilean', 'Chinese', 'Colombian', 'Comoran', 'Congolese', 'CostaRican', 'Croatian', 'Cuban', 'Cypriot', 'Czech', 'Danish', 'Djibouti', 'Dominican', 'Dutch', 'EastTimorese', 'Ecuadorean', 'Egyptian', 'Emirian', 'EquatorialGuinean', 'Eritrean', 'Estonian', 'Ethiopian', 'Fijian', 'Filipino', 'Finnish', 'French', 'Gabonese', 'Gambian', 'Georgian', 'German', 'Ghanaian', 'Greek', 'Grenadian', 'Guatemalan', 'Guinea-Bissauan', 'Guinean', 'Guyanese', 'Haitian', 'Herzegovinian', 'Honduran', 'Hungarian', 'I-Kiribati', 'Icelander', 'Indian', 'Indonesian', 'Iranian', 'Iraqi', 'Irish', 'Israeli', 'Italian', 'Ivorian', 'Jamaican', 'Japanese', 'Jordanian', 'Kazakhstani', 'Kenyan', 'KittianandNevisian', 'Kuwaiti', 'Kyrgyz', 'Laotian', 'Latvian', 'Lebanese', 'Liberian', 'Libyan', 'Liechtensteiner', 'Lithuanian', 'Luxembourger', 'Macedonian', 'Malagasy', 'Malawian', 'Malaysian', 'Maldivan', 'Malian', 'Maltese', 'Marshallese', 'Mauritanian', 'Mauritian', 'Mexican', 'Micronesian', 'Moldovan', 'Monacan', 'Mongolian', 'Moroccan', 'Mosotho', 'Motswana', 'Mozambican', 'Namibian', 'Nauruan', 'Nepalese', 'NewZealander', 'Nicaraguan', 'Nigerian', 'Nigerien', 'NorthKorean', 'NorthernIrish', 'Norwegian', 'Omani', 'Pakistani', 'Palauan', 'Panamanian', 'PapuaNewGuinean', 'Paraguayan', 'Peruvian', 'Polish', 'Portuguese', 'Qatari', 'Romanian', 'Russian', 'Rwandan', 'SaintLucian', 'Salvadoran', 'Samoan', 'SanMarinese', 'SaoTomean', 'Saudi', 'Scottish', 'Senegalese', 'Serbian', 'Seychellois', 'SierraLeonean', 'Singaporean', 'Slovakian', 'Slovenian', 'SolomonIslander', 'Somali', 'SouthAfrican', 'SouthKorean', 'Spanish', 'SriLankan', 'Sudanese', 'Surinamer', 'Swazi', 'Swedish', 'Swiss', 'Syrian', 'Taiwanese', 'Tajik', 'Tanzanian', 'Thai', 'Togolese', 'Tongan', 'Trinidadian/Tobagonian', 'Tunisian', 'Turkish', 'Tuvaluan', 'Ugandan', 'Ukrainian', 'Uruguayan', 'Uzbekistani', 'Venezuelan', 'Vietnamese', 'Welsh', 'Yemenite', 'Zambian', 'Zimbabwean');
                break;

            case "development_status":
                $array = array('' => 'Select Development Status', '1' => '10%', '2' => '20%', '3' => '30%', '4' => '40%', '5' => '50%', '6' => '60%', '7' => '70%', '8' => '80%', '9' => '90%', '100%' => 'Japan');
                break;

            case "license_status":
                $array = array('' => 'Select License Status', '1' => 'Ongoing FADP', '2' => 'Approved  FADP', '3' => '30%', '4' => '40%', '5' => '50%', '6' => '60%', '7' => '70%', '8' => '80%', '9' => '90%', '9' => '100%');
                break;

            case "financing_scheme":
                $array = array('' => 'Select Financing Scheme', '1' => 'In-house', '2' => 'Bank', '3' => 'HDMF-Window 2', '4' => 'HDMF - Retail Window');
                break;

            case "group_type":
                $array = ['' => 'Select Group Type', '1' => 'Twin Hearts Realty Group', '2' => 'SHJDC', '3' => 'PDP Global Sales'];
                break;

            case "applicant_type":
                $array = ['' => 'Select Applicant Type', '1' => 'SME Loan Account', '2' => 'Personal Loan Account'];
                break;

            case "position_type":
                $array = array('' => 'Select Position Type', '1' => 'Agent', '2' => 'Broker');
                break;

            case "housing_membership":
                $array = array('' => 'Select Housing Membership', '1' => 'SSS', '2' => 'GSIS', '3' => 'Pag-ibig', '4' => 'Others');
                break;

            case "occupation_type":
            
                $array = array('' => 'Select Employment Type', '1' => 'Employed - Government', '2' => 'Employed - Private', '3' => 'Self-employed', '4' => 'OFW', '5' => 'Immigrant', '6' => 'Others', '7' => 'Unemployed');
                break;

            case "local_employment_type":
                $array = array('' => 'Select Occupation Type', '1' => 'Locally-Based', '2' => 'Foreign Worker', '4' => 'Others');
                break;

            case "foreign_employment_type":
                $array = array('' => 'Select Foreign Employment Type', '1' => 'OFW', '2' => 'Immigrant', '4' => 'Others');
                break;

            case "occupation":
                $array = array('' => 'Select Occupation', '1' => 'Caregiver & Caretaker', '2' => 'Janitorial & related Works', '3' => 'Cook, Waiter, Bartender & Related Work', '4' => 'Household Service Worker', '5' => 'Laborer & General Worker', '6' => 'Nurses & Other Professionals', '7' => 'Plumber & Pipe Fitter', '8' => 'Seafarers', '9' => 'Wielder & Frame Cutter', '10' => 'Wiremen & Electrical Worker', '11' => 'Others');
                break;

            case "industry":
                $array = array('Select Industry', "Accounting", "Airlines/Aviation", "Alternative Dispute Resolution", "Alternative Medicine", "Animation", "Apparel & Fashion", "Architecture & Planning", "Arts & Crafts", "Automotive", "Aviation & Aerospace", "Banking", "Biotechnology", "Broadcast Media", "Building Materials", "Business Supplies & Equipment", "Capital Markets", "Chemicals", "Civic & Social Organization", "Civil Engineering", "Commercial Real Estate", "Computer & Network Security", "Computer Games", "Computer Hardware", "Computer Networking", "Computer Software", "Construction", "Consumer Electronics", "Consumer Goods", "Consumer Services", "Cosmetics", "Dairy", "Defense & Space", "Design", "Education Management", "E-learning", "Electrical & Electronic Manufacturing", "Entertainment", "Environmental Services", "Events Services", "Executive Office", "Facilities Services", "Farming", "Financial Services", "Fine Art", "Fishery", "Food & Beverages", "Food Production", "Fundraising", "Furniture", "Gambling & Casinos", "Glass, Ceramics & Concrete", "Government Administration", "Government Relations", "Graphic Design", "Health, Wellness & Fitness", "Higher Education", "Hospital & Health Care", "Hospitality", "Human Resources", "Import & Export", "Individual & Family Services", "Industrial Automation", "Information Services", "Information Technology & Services", "Insurance", "International Affairs", "International Trade & Development", "Internet", "Investment Banking/Venture", "Investment Management", "Judiciary", "Law Enforcement", "Law Practice", "Legal Services", "Legislative Office", "Leisure & Travel", "Libraries", "Logistics & Supply Chain", "Luxury Goods & Jewelry", "Machinery", "Management Consulting", "Maritime", "Marketing & Advertising", "Market Research", "Mechanical or Industrial Engineering", "Media Production", "Medical Device", "Medical Practice", "Mental Health Care", "Military", "Mining & Metals", "Motion Pictures & Film", "Museums & Institutions", "Music", "Nanotechnology", "Newspapers", "Nonprofit Organization Management", "Oil & Energy", "Online Publishing", "Outsourcing/Offshoring", "Package/Freight Delivery", "Packaging & Containers", "Paper & Forest Products", "Performing Arts", "Pharmaceuticals", "Philanthropy", "Photography", "Plastics", "Political Organization", "Primary/Secondary", "Printing", "Professional Training", "Program Development", "Public Policy", "Public Relations", "Public Safety", "Publishing", "Railroad Manufacture", "Ranching", "Real Estate", "Recreational", "Facilities & Services", "Religious Institutions", "Renewables & Environment", "Research", "Restaurants", "Retail", "Security & Investigations", "Semiconductors", "Shipbuilding", "Sporting Goods", "Sports", "Staffing & Recruiting", "Supermarkets", "Telecommunications", "Textiles", "Think Tanks", "Tobacco", "Translation & Localization", "Transportation/Trucking/Railroad", "Utilities", "Venture Capital", "Veterinary", "Warehousing", "Wholesale", "Wine & Spirits", "Wireless", "Writing & Editing");
                break;

            case "occupation_location":
                $array = array('' => 'Select Location of Occupation', '1' => 'Australia', '2' => 'Bahrain', '3' => 'Canada', '4' => 'China', '5' => 'France', '6' => 'Greece', '7' => 'Germany', '8' => 'Hongkong', '9' => 'Italy', '10' => 'Japan', '11' => 'Kuwait', '12' => 'Malaysia', '13' => 'Qatar', '14' => 'Saudi Arabia', '15' => 'Singapore', '16' => 'Spain', '17' => 'South Korea', '18' => 'Taiwan', '19' => 'UAE', '20' => 'United Kingdom', '21' => 'USA', '21' => 'Philippines', '23' => 'Others');
                break;

            case "ids":
                $array = array('' => 'Select Type of ID', 'Philippine Passport', 'Driver’s license', 'SSS UMID Card', 'PhilHealth ID', 'TIN Card', 'Postal ID', 'HMDF ID', 'Voter’s ID', 'PRC ID', 'Senior Citizen ID', 'OFW ID');
                break;

            case "value_type":
                $array = array('' => 'Select Value Type', '1' => 'Percentage', '2' => 'Currency');
                break;

            case "add_to":
                $array = array('' => 'Select Options', '2' => 'Total Contract Price');
                break;

            case "deduct_to":
                $array = array('' => 'Select Options', '1' => 'Total Selling Price', '2' => 'Total Contract Price', '3' => 'Collectible Price', '4' => 'Total Lot Price', '5' => 'Total House Price', '6' => 'Loan Amount');
                break;

            case "fees_type":
                $array = array('' => 'Select Options', '1' => 'Individual', '2' => 'Compounded');
                break;

            case "periods":
                $array = array('' => 'Select Options', '1' => 'RA', '2' => 'DP', '3' => 'LOAN', '4' => 'EQ', '5' => 'OTHERS');
                break;

            case "period_names":
                $array = array('' => 'Select Options', '1' => 'Reservation', '2' => 'Downpayment', '3' => 'Loan', '4' => 'Equity', '5' => 'Others');
                break;

            case "commissionable_types":
                $array = array('' => 'Select Options', '1' => 'Total Selling Price', '2' => 'Total Property Price', '3' => 'Total Contract Price', '4' => 'Computation #1', '5' => 'Computation #2', '6' => 'Computation #3', '7' => 'Computation #4', '8' => 'Computation #5', '9' => 'Computation #6', '10' => 'Computation #7', '11' => 'Computation #8', '12' => 'House and Lot (Andrea, Cassandra. Elysa, Freya, Gabrielle)', '13' => 'Sitio Uno', '14' => 'Twin Hearts Residences', '15' => 'Lots Only (Except Socialized Lots)', '16' => 'St. Joseph (Lots Only) **', '17' => 'Computation #9');
                break;

            case "commission_status":
                $array = array('' => 'Select Options', '1' => 'Pending', '2' => 'For RFP', '3' => 'Processing', '4' => 'Released');
                break;

            case "inventory_status":
                $array = array('' => 'Select Options', '0' => 'Inactive', '1' => 'Active');
                break;

            case "payment_types":
                $array = array('' => 'Select Options', '1' => 'Cash', '2' => 'Regular Cheque', '3' => 'Post Dated Cheque', '4' => 'Online Deposits', '5' => 'Wire Transfer', '6' => 'Cash & Cheque', '7' => 'Cash & Bank Remittance', '8' => 'Journal Voucher', '9' => 'Others', '10' => 'Direct Deposit');
                break;

            case "package_types":
                $array = array('' => 'Select Options', '1' => 'Basic', '2' => 'As Improved', '3' => 'Fully Furnished');
                break;

            case "pdc_status":
                $array = array('' => 'Select Options', '1' => 'Pending', '2' => 'Processing', '3' => 'Cleared', '4' => 'Bounced');
                break;

            case "loan_status":
                $array = array('' => 'Select Options', '1' => 'Pending', '2' => 'Reviewed', '3' => 'Disapproved', '4' => 'Approved');
                break;

            case "loan_scheme":
                $array = array('' => 'Select Options', '1' => 'Lumpsum', '2' => 'Monthly Installment', '3' => 'Salary Loan');
                break;

            case "loan_terms":
                $array = array('' => 'Select Options', '1' => 'Lumpsum', '2' => 'Monthly Installment');
                break;

            case "disbursement_type":
                $array = array('' => 'Select Options', '1' => 'Cash', '2' => 'Wire Transfer', '3' => 'Gcash', '4' => 'Cheques');
                break;

            case "general_stages":
                $array = array('' => 'Select Options', '1' => 'Pre-Reservation', '2' => 'Reserved', '3' => 'On-going', '4' => 'Complete');
                break;

            case "document_stages":
                $array = array('' => 'Select Options', '1' => '1. Official Sale', '2' => ' 2. Pre Filing', '3' => '3. First Filing', '4' => ' 4. Appraisal', '5' => '5. NOA Confirmation', '6' => '6. Title Transfer - BIR', '7' => '7. Title Transfer - Ecar', '8' => '8. Title Transfer - RD', '9' => '9. Update Tax Declaration', '10' => '10. Second Filing', '11' => '11. Upon Loan Take Out', '12' => '12. Complete');
                break;

            case "warehouse_status":
                $array = array('' => 'Select Options', '0' => 'Inactive', '1' => 'Active');
                break;

            case "company_status":
                $array = array('' => 'Select Options', '0' => 'Inactive', '1' => 'Active');
                break;

            case "user_group":
                $array = array('' => 'Select Options', '1' => 'Admin', '2' => 'General User', '3' => 'Seller', '4' => 'Buyer');
                break;

            case "general_status":
                $array = array('' => 'Select Options', '1' => 'Reserved', '2' => ' On - Going', '3' => 'Completed', '4' => 'Cancelled', '5' => 'Rebooked');
                break;

            case "collection_status":
                $array = array('' => 'Select Options', '1' => 'Pre - Reserved', '2' => 'Reserved', '3' => 'On - Time', '4' => 'Past Due', '5' => 'Fully Paid');
                break;

            case "past_dues":
                $array = array('0' => 'Select Option', '1' => 'First Notice', '2' => 'Second Notice', '3' => 'Last Notice');
                break;
            case "letter_requests":
                $array = array('0' => 'Select Option', '1' => 'Discount', '2' => 'Payment Extension', '3' => 'Change of Due Date', '4' => 'Change of Name', '5' => 'Change of DP %', '6' => 'Change of Financing Scheme', '7' => 'Change of Term', '9' => 'Others');
                break;
            case "ticket_categories":
                $array = array('general' => 'General', 'past_due_notices' => 'Past Due Notices', 'ar_notes' => 'AR Notes', 'construction_issue' => 'Construction Issue', 'payment_issue' => 'Payment Issue', 'letter_of_request' => 'Request of Letter');
                break;
            case "letter_request_status":
                $array = array('0' => 'Select Option', '1' => 'Pending', '2' => 'Approved', '3' => 'Disapproved', '4' => 'Cancelled');
                break;
            case "past_due_status":
                $array = array('0' => 'Select Option', '1' => 'Sent to Client', '2' => 'Received by Client', '3' => 'Done');
                break;
            case "accounting_entries_journal_type":
                $array = array('0' => 'Select Option', '1' => 'General Journal', '2' => 'Sales Journal', '3' => 'Cash Receipt Journal', '4' => 'Purchase Journal', '5' => 'Cash Payment Journal');
                break;
            case "accounting_entries_payee_type":
                $array = array('0' => 'Select Option', '1' => 'Suppliers', '2' => 'Sellers', '3' => 'Staff', '4' => 'Contractors', '5' => 'Buyers');
                break;
            case "payee_type":
                $array = array('' => 'Select Option', 'suppliers' => 'Suppliers', 'sellers' => 'Sellers', 'staff' => 'Staff', 'contractors' => 'Contractors', 'buyers' => 'Buyers');
                break;
            case "reservation_status":
                $array = array('0' => 'Select Option', '1' => 'Reserved', '2' => 'Completed', '3' => 'Cancelled', '4' => 'Expired');
                break;
            case "supplier_types":
                $array = array('' => 'Select Option', '1' => 'Local Supplier', '2' => 'Manila Supplier', '3' => 'International Supplier');
                break;
            case "contractor_types":
                $array = array('' => 'Select Option', '1' => 'Local Contractor', '2' => 'Manila Contractor', '3' => 'International Contractor');
                break;
            case "ap_payment_types":
                $array = array('' => 'Select Option', '1' => 'Cash', '2' => 'Advances', '3' => 'Credit', '4' => 'PDC');
                break;
            case "delivery_terms":
                $array = array('' => 'Select Option', '1' => 'Deliver to Warehouse', '2' => 'Pick up on Pier (Roxas)', '3' => 'Pick up on Airport');
                break;
            case "cheque_status":
                $array = array('' => 'Select Options', '0' => 'Inactive', '1' => 'Active');
                break;
            case "vat_types":
                $array = array('' => 'Select Options', '0' => 'Non-VAT', '1' => 'VAT');
                break;
            case "tax_types":
                $array = array('' => 'Select Options', '0' => 'Goods', '1' => 'Service');
                break;
            case "accounting_setting_types":
                $array = array('' => 'Select Options', '1' => 'Collections', '2' => 'Reservations', '3' => 'Payment Request', '4' => 'Payment Voucher',);
                break;
            case "accounting_setting_items_origin":
                $array = array('' => 'Select Options', '1' => 'Principal', '2' => 'Interest', '3' => 'Penalty');
                break;
            case "ar_clearing_status":
                $array = array('' => 'Select Options', '1' => 'For Clearing', '2' => 'Bounced', '3' => 'Cleared');
                break;
            case "tax_rates":
                $array = array('' => 'Select Tax Rate', '0' => '0', '1.50' => '1.50', '3' => '3', '5' => '5', '7.50' => '7.50');
                break;
            case "customer_care_status":
                $array = array('0' => 'Select Option', '1' => 'New Request', '2' => 'Forwarded', '3' => 'Work in progress', '4' => 'On Hold', '5' => 'Completed', '6' => 'Closed', '7' => 'Cancelled');
                break;
            case "sales_recognize_status":
                $array = array('0' => 'Not Recognized', '1' => 'Recognized');
                break;
            case "scheme_types":
                $array = array('1' => 'Standard', '2' => 'Zero Downpayment', '3' => 'Fixed Loan Amount', '4' => 'Fixed Downpayment', '5' => 'Cash Payment',);
                break;
            case "app_value_types":
                $array = array('1' => 'String', '2' => 'Integer', '3' => 'Float', '4' => 'Decimal', '5' => 'Boolean');
                break;
            case "accounting_setting_entry_types":
                $array = array('1' => 'Receipt', '2' => 'Journal', '3' => 'Payment', '4' => 'Contra');
                break;
            case "accounting_setting_module_types":
                $array = array('1' => 'Commissions', '2' => 'Collections', '3' => 'Purchase Request', '4' => 'Purchase Invoice', '5' => 'Waive', '6' => 'Seller Reservations');
                break;
            case "accounting_setting_origin":
                $array = array('1' => 'Total', '2' => 'Principal', '3' => 'Discount', '4' => 'Interest', '5' => 'Penalty', '6' => 'TSP', '7' => 'TCP', '8' => 'Collectible', '9' => 'Rebates');
                break;
            case "personnel_status":
                $array = array('' => 'Select Options', '0' => 'Inactive', '1' => 'Active');
                break;
            case "refund_categories":
                $array = array('' => 'Select Options', '.50' => '50% for Restructure or Refund', '.75' => '75% for upgrade (transfer with higher TCP)');
                break;
            case "penalty_types":
                $array = array('' => 'Select Options', '1' => 'Standard Payment', '2' => 'On or Before Due Date');
                break;
            case "app_global_value":
                $array = array('' => 'Select Type', '1' => 'Communication Settings');
                break;
            case "property_types":
                $array = array('0' => 'Select Property Type', '1' => 'Residential Lot', '2' => 'Commercial Lot', '3' => 'House and Lot', '4' => 'Condominium',);
                break;
            case "payment_status":
                $array = array('' => 'Approved', '1' => 'Cancelled', '2' => 'Deleted');
                break;
            case "job_request_status":
                $array = array('' => 'Select Options', '1' => 'Request Filed', '2' => 'Approved', '3' => 'Disapproved', '4' => 'Cancelled');
                break;
            case "payment_request_status":
                $array = array('' => 'Select Options', '0' => 'Incomplete', '1' => 'Complete');
                break;
            case "job_order_priority_level":
                $array = array('' => 'Select Options', '1' => 'Urgent (2-3 Working Days)');
                break;
            case "customer_type":
                $array = array('' => 'Select Options', 'buyers' => 'Buyer', 'sellers' => 'Seller', 'contractors' => 'Contractor', 'suppliers' => 'Supplier', 'companies' => 'Company', 'departments' => 'Department', 'projects' => 'Project', 'properties' => 'Property', 'staff' => 'Staff');
                break;
            case "mr_customer_type":
                $array = array(
                    0 => 'Select Option',
                    'project' => 'Project',
                    'sub_project' => 'Sub-Project',
                    'amenity' => 'Amenity',
                    'sub_amenity' => 'Sub-Amenity',
                    'block_and_lot' => 'Block & Lot',
                    'department' => 'Department',
                    'vehicle_equipment' => 'Vehicle / Equipment',
                );
                break;
            case "mr_status":
                $array = array(
                    0 => 'Select Option',
                    1 => 'New Request',
                    2 => 'For RPO',
                    3 => 'For Issuance',
                    4 => 'Issued',
                    5 => 'Cancelled',
                );
                break;
            case "mr_type":
                $array = array(
                    0 => 'Select Option',
                    1 => "For Purchase",
                    2 => "For Issuance",
                    3 => "For Warehouse Transfer"
                );
                break;
            case "rpo_status":
                $array = array(
                    0 => 'Select Option',
                    1 => 'New Request',
                    2 => 'Approved',
                    3 => 'Denied',
                    4 => 'Cancelled'
                );
                break;
            case "cnv_status":
                $array = array(
                    0 => 'Select Option',
                    1 => 'New Canvass',
                    2 => 'PO Created',
                    3 => 'Rejected'
                );
                break;
            case "po_status":
                $array = array(
                    0 => 'Select Option',
                    1 => 'New Request',
                    2 => 'Approved',
                    3 => 'Denied',
                    4 => 'Cancelled',
                    5 => 'Amended',

                );
                break;
            case "po_item_status":
                $array = array(
                    0 => 'Select Option',
                    1 => 'Pending',
                    2 => 'Received',
                    3 => 'Rejected',
                    4 => 'Amended'
                );
                break;
            case "mrr_status":
                $array = array(
                    0 => 'Select Option',
                    1 => 'PO Amended',
                    2 => 'Item Received',
                    3 => 'Wrong Delivery',
                    4 => 'Returned to Supplier',
                    5 => 'Partial Delivery'
                );
                break;
            case "mrr_type":
                $array = array(
                    0 => 'Select Option',
                    1 => 'Direct In',
                    2 => 'Standard Receiving Process',
                    3 => 'Transfer',
                );
                break;
            case "issuance_type":
                $array = array(
                    0 => 'Select Option',
                    1 => 'Standard Issuance',
                    2 => 'Direct Out',
                    3 => 'Transfer'
                );
                break;
            case "mrr_item_status":
                $array = array(
                    0 => 'Select Option',
                    1 => 'Item Received',
                    2 => 'Amended',
                    3 => 'Item Rejected',
                );
                break;
            case "inventory_log_transaction_type":
                $array = array(
                    0 => 'Select Option',
                    1 => 'Normal Transaction',
                    2 => 'Transfer: Add',
                    3 => 'Transfer: Subtract',
                    4 => 'Insert',
                    5 => 'Update',
                    6 => 'Material Receiving',
                    7 => 'Material Releasing',
                );
                break;
            case "warehouse_type":
                $array = array(
                    0 => 'Select Option',
                    1 => 'Warehouse',
                    2 => 'Sub Warehouse',
                    3 => 'Virtual Warehouse'
                );
                break;
            case "vehicle_equipment_group":
                $array = array(
                    '' => 'Select Options',
                    '1' => 'Delivery Equipment',
                    '2' => 'Transportation Equipment',
                    '3' => 'Heavy Equipment',
                    '4' => 'Construction Equipment',
                    '5' => 'Small Tools Machinery	',
                    '6' => 'Machinery',
                    '7' => 'Others',
                );
                break;
            case "fuel_type":
                $array = array(
                    '' => 'Select Options',
                    '1' => 'Gas',
                    '2' => 'Diesel',
                );
                break;
            case "body_type":
                $array = array(
                    '' => 'Select Options',
                    '1' => 'Motorcycle',
                    '2' => 'Sedan',
                    '3' => 'Sport-Utility Vehicle (SUV)',
                    '4' => 'Van/Minivan     ',
                    '5' => 'Pickup Truck',
                    '6' => 'Jeep',
                    '7' => 'Flat Bed Body',
                    '8' => 'Van Body (Truck)',
                    '9' => 'Contractor Body',
                    '10' => 'Dump Bodies',
                    '11' => 'Temperature-Controlled Body',
                    '12' => 'Tankers',
                );
                break;
            case "vehicle_activity_type":
                $array = array(
                    '' => 'Select Options',
                    '1' => 'Delivery',
                    '2' => 'Pickup',
                );
                break;
            case "production_status":
                $array = array(
                    '' => 'Select Options',
                    '0' => 'Inactive',
                    '1' => 'Active',
                );
                break;
            case "bank_status":
                $array = array(
                    '' => 'Select Options',
                    '1' => 'Inactive',
                    '2' => 'Active',
                );
                break;
            case "construction_order_customer_type":
                $array = array(
                    '' => 'Select Option',
                    'project' => 'Project',
                    'sub_project' => 'Sub-Project',
                    'amenity' => 'Amenity',
                    'sub_amenity' => 'Sub-Amenity',
                    'block_and_lot' => 'Block & Lot',
                    'department' => 'Department',
                    'vehicle_equipment' => 'Vehicle / Equipment',
                );
                break;
            case "construction_order_status":
                $array = array(
                    '' => 'Select Option',
                    1 => 'New Request',
                    2 => 'For RPO',
                    3 => 'For Issuance',
                    4 => 'Issued',
                    5 => 'Cancelled',
                );
                break;
            case "contact_group_status":
                $array = array(
                    '' => 'Select Options',
                    '1' => 'Inactive',
                    '2' => 'Active',
                );
                break;
            case "payment_type":
                $array = array(
                    '0' => 'Cash',
                    '1' => 'Cheque',
                );
                break;
            case "receipt_type":
                $array = array(
                    '0' => 'Select Type',
                    '1' => 'Collection Receipt',
                    '2' => 'Acknowledgement Receipt',
                    '3' => 'Provisionary Receipt',
                    '4' => 'Official Receipt',
                    '5' => 'Manual Receipt',
                );
                break;
            case "past_due_notice_type":
                $array = array(
                    '0' => 'Select Type',
                    '1' => '1st Notice to Buyer',
                    '2' => '2nd Notice to Buyer',
                    '3' => '3rd Notice to Buyer',
                    '4' => 'Notice for Cancellation',
                );
                break;
            case "source_referral_type":
                $array = array(
                    '0' => 'Select Type',
                    '1' => 'Facebook Ads',
                    '2' => 'Flyers',
                    '3' => 'Referral',
                    '4' => 'Seek Cap',
                    '5' => 'vOffice',
                );
                break;
            case "optional_bool":
                $array = array(
                    '0' => 'Select Type',
                    '1' => 'No',
                    '2' => 'Yes',
                );
                break;
            case "is_recognized":
                $array = array(
                    '0' => 'Before Recognition',
                    '1' => 'After Recognition',
                );
                break;
            case "request_status":
                $array = array(
                    '' => 'Select Status',
                    0 => 'Encoded',
                    1 => 'Approved',
                    2 => 'Disapproved',
                    3 => 'Posted',
                );
                break;
            case "leasing_property_unit_type":
                $array = array(
                    0 => 'Select Unit Type',
                    1 => 'Commercial',
                    2 => 'Dormitory',
                    3 => 'Generator',
                    4 => 'Lot',
                    5 => 'Motor',
                    6 => 'Office',
                    7 => 'Parking',
                    8 => 'Residential',
                    9 => 'Riser',
                    10 => 'Share',
                    11 => 'Signage',
                    12 => 'Staffhouse',
                    13 => 'Storage',
                    14 => 'Warehouse',
                );
                break;

            case "leasing_property_general_status":
                $array = array(
                    0 => 'Select Status',
                    1 => 'Occupied',
                    2 => 'Vacant',
                    3 => 'Under Construction',
                );
                break;

            case "leasing_tenant_type":
                $array = ['' => 'Select Buyer Type', '1' => 'Individual', '2' => 'Company'];
                break;

            case "construction_template_stages":
                $array = [
                    1 => 'Foundation',
                    2 => 'Construction'
                ];
                break;

            case "payment_request_payment_status":
                $array = [
                    '' => 'Select Payment Status',
                    0 => 'Unpaid',
                    1 => 'Paid'
                ];
                break;

            case "laborer_type":
                $array = [
                    '' => 'Select Laborer Type',
                    1 => 'Non Skilled',
                    2 => 'Skilled'
                ];
                break;

            case "supplier_vat_type":
                $array = [
                    '' => 'Select Vat Type',
                    1 => 'VAT (Goods)',
                    2 => 'VAT (Services)',
                    3 => 'VAT (Goods and Services)',
                    4 => 'NON-VAT'
                ];
                break;

            case "customer_service_category":
                $array = [
                    '' => 'Select Category',
                    1 => 'Sales',
                    2 => 'Marketing',
                    3 => 'C&C',
                    4 => 'AR',
                    5 => 'Engineering',
                    6 => 'Project Admin',
                    7 => 'Leasing',
                    8 => 'Property Management',
                    9 => 'Legal',
                    10 => 'HR',
                    11 => 'Other',
                ];
                break;

            case "customer_service_status":
                $array = [
                    '' => 'Select Status',
                    1 => 'Active',
                    2 => 'Closed',
                ];
                break;

            case "customer_service_sub_status":
                $array = [
                    '' => 'Select Status',
                    1 => 'Ongoing',
                    2 => 'Forwarded',
                    3 => 'Completed',
                ];
                break;

            case "customer_service_forward_status":
                $array = [
                    '' => 'Select Status',
                    1 => 'Ongoing',
                    2 => 'Completed',
                ];
                break;

            case "budgeting_setup_status":
                $array = [
                    '' => 'Select Status',
                    1 => 'Pending',
                    2 => 'Approved',
                    3 => 'Disapproved',
                ];
                break;

            case "sellers_reservation_payment_status":
                $array = [
                    '' => 'Select Status',
                    1 => 'Unpaid',
                    2 => 'Paid',
                ];
                break;

                // case "SHJDC" :
                //     $array['items'] = [
                //         'gsm' => [
                //             'id' => "gsm",
                //             'text' => "General Sales Manager (GSM)"
                //         ],
                //         'ps' => [
                //             'id' => "ps",
                //             'text' => "Property Specialist (PS)"
                //         ],
                //         'rp' => [
                //             'id' => "rp",
                //             'text' => "Referral Partner (RP)"
                //         ],
                //     ];

                // break;

            default:
                $array = array();
                break;
        }

        // set variable cache
        self::$cache['static' . $name] = $array;

        if (($value) || ($action != "form")) {
            return @$array[$value];
        }

        // else if($value == "0")
        // {
        //     return "N/A";
        // }

        return $array;
    }
}
