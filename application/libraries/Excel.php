<?php if( ! defined('BASEPATH'))exit('No direct script allowed');

require_once APPPATH.'third_party/PHPExcel/Classes/PHPExcel.php';
require_once APPPATH.'third_party/PHPExcel/Classes/PHPExcel/IOFactory.php';

Class Excel extends PHPExcel{
    function __construct()
    {
        parent::__construct();
    }
}