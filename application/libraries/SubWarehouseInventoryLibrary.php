<?php


    class SubWarehouseInventoryLibrary
    {
        private $data = null;
        private $_model = null;
        private $CI = null;

        public function __construct()
        {
            $this->CI = &get_instance();
            $this->CI->load->model('sub_warehouse_inventory/sub_warehouse_inventory_model', '_model');
        }

        public function get_obj($id, $with_relations = TRUE)
        {
            if ($with_relations) {
                $obj = $this->CI->_model
                    ->with_warehouse()
                    ->with_sub_warehouse()
                    ->with_item()
                    ->with_unit_of_measurement()
                    ->get($id);
            } else {
                $obj = $this->CI->_model->get($id);
            }

            return $obj;
        }

        public function get_item($item_id, $warehouse_id = NULL, $with_relations = TRUE)
        {
            $this->CI->_model->where('item_id', $item_id);
            if ($warehouse_id) {
                $this->CI->_model->where('sub_warehouse_id', $warehouse_id);
            }
            if ($with_relations) {
                $obj = $this->CI->_model
                    ->with_item()
                    ->with_unit_of_measurement()
                    ->with_warehouse()
                    ->get_all();
            } else {
                $obj = $this->CI->_model->get_all();
            }

            return $obj;
        }

        public function search($params)
        {
            $id = isset($params['id']) ? $params['id'] : null;
            $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';
            $status_list = isset($params['status_list']) && !empty($params['status_list']) ? $params['status_list'] : null;

            if ($status_list) {
                $status_list_arr = explode(",", $status_list);
                $this->CI->_model->where('status', $status_list_arr);
            }

            $this->CI->_model->order_by('id', 'DESC');

            if (!$id) {

                $id = isset($params['id']) && !empty($params['id']) ? $params['id'] : null;
                if ($id) {
                    $this->CI->_model->where('id', $id);
                }

                $created_by = isset($params['created_by']) && !empty($params['created_by']) ? $params['created_by'] : null;
                if ($created_by) {
                    $this->CI->_model->where('created_by', $created_by);
                }

                $created_at = isset($params['created_at']) && !empty($params['created_at']) ? $params['created_at'] : null;
                if ($created_at) {
                    $this->CI->_model->where('created_at', $created_at);
                }

                $updated_by = isset($params['updated_by']) && !empty($params['updated_by']) ? $params['updated_by'] : null;
                if ($updated_by) {
                    $this->CI->_model->where('updated_by', $updated_by);
                }

                $updated_at = isset($params['updated_at']) && !empty($params['updated_at']) ? $params['updated_at'] : null;
                if ($updated_at) {
                    $this->CI->_model->where('updated_at', $updated_at);
                }

                $deleted_by = isset($params['deleted_by']) && !empty($params['deleted_by']) ? $params['deleted_by'] : null;
                if ($deleted_by) {
                    $this->CI->_model->where('deleted_by', $deleted_by);
                }

                $deleted_at = isset($params['deleted_at']) && !empty($params['deleted_at']) ? $params['deleted_at'] : null;
                if ($deleted_at) {
                    $this->CI->_model->where('deleted_at', $deleted_at);
                }

                $warehouse_id = isset($params['warehouse_id']) && !empty($params['warehouse_id']) ? $params['warehouse_id'] : null;
                if ($warehouse_id) {
                    $this->CI->_model->where('warehouse_id', $warehouse_id);
                }

                $sub_warehouse_id = isset($params['sub_warehouse_id']) && !empty($params['sub_warehouse_id']) ? $params['sub_warehouse_id'] : null;
                if ($sub_warehouse_id) {
                    $this->CI->_model->where('sub_warehouse_id', $sub_warehouse_id);
                }

                $item_id = isset($params['item_id']) && !empty($params['item_id']) ? $params['item_id'] : null;
                if ($item_id) {
                    $this->CI->_model->where('item_id', $item_id);
                }

                $unit_of_measurement_id = isset($params['unit_of_measurement_id']) && !empty($params['unit_of_measurement_id']) ? $params['unit_of_measurement_id'] : null;
                if ($unit_of_measurement_id) {
                    $this->CI->_model->where('unit_of_measurement_id', $unit_of_measurement_id);
                }

                $actual_quantity = isset($params['actual_quantity']) && !empty($params['actual_quantity']) ? $params['actual_quantity'] : null;
                if ($actual_quantity) {
                    $this->CI->_model->where('actual_quantity', $actual_quantity);
                }

                $available_quantity = isset($params['available_quantity']) && !empty($params['available_quantity']) ? $params['available_quantity'] : null;
                if ($available_quantity) {
                    $this->CI->_model->where('available_quantity', $available_quantity);
                }

                $unit_price = isset($params['unit_price']) && !empty($params['unit_price']) ? $params['unit_price'] : null;
                if ($unit_price) {
                    $this->CI->_model->where('unit_price', $unit_price);
                }


            } else {
                $this->CI->_model->where('id', $id);
            }

            $this->CI->_model->order_by('id', 'DESC');

            if ($with_relations === 'yes') {
                $result = $this->CI->_model
                    ->with_warehouse()
                    ->with_sub_warehouse()
                    ->with_item()
                    ->with_unit_of_measurement()
                    ->get_all();
            } else {
                $result = $this->CI->_model->get_all();
            }

            return $result;
        }

        public function create_master($data, $additional)
        {
            $request = $this->CI->db->insert('sub_warehouse_inventories', $data + $additional);
            if ($request) {
                return $this->CI->db->insert_id();
            } else {
                return false;
            }
        }

        public function update_master($id, $data, $additional)
        {
            $old_object = $this->CI->_model->get($id);
            return $this->CI->_model->update($data + $additional, $id);
        }

    }