<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commission_library {

	public function __construct(){

		$this->u_additional = [
			'updated_by' => 1,
			'updated_at' => NOW
		];

		$this->additional = [
			'created_by' => 1,
			'created_at' => NOW
		];
	}

	public function initiate( $transaction_id = 0 ) {
   		$CI = &get_instance();
		
		$transaction_models = array('transaction/Transaction_billing_model' => 'M_transaction_billing','transaction/Transaction_client_model' => 'M_transaction_client','transaction/Transaction_discount_model' => 'M_transaction_discount','transaction/Transaction_fee_model' => 'M_transaction_fee','transaction/Transaction_payment_model' => 'M_transaction_payment','transaction_payment/Transaction_official_payment_model' => 'M_transaction_official_payment','transaction/Transaction_property_model' => 'M_transaction_property','transaction/Transaction_seller_model' => 'M_transaction_seller','transaction_commission/Transaction_commission_model' => 'M_Transaction_commission', 'ticketing/Ticketing_model' => 'M_Transaction_ticketing', 'transaction/Transaction_model' => 'M_transaction', 'transaction/Transaction_billing_logs_model' => 'M_tbilling_logs', 'transaction_payment/Transaction_penalty_logs_model' => 'M_tpenalty_logs','transaction/Transaction_status_logs_model' => 'M_tstatus_logs','payment_request/Payment_request_model' => 'M_payment_request','payment_voucher/Payment_voucher_model' => 'M_payment_voucher');

		// Load models
		$CI->load->model($transaction_models);
	}

	public function update_status($transaction_commission_id = 0,$status = 0){
   		$CI = &get_instance();

   		$CI->load->model( array('transaction_commission/Transaction_commission_model' => 'M_Transaction_commission','payment_request/Payment_request_model' => 'M_payment_request') );

        $oof = $CI->M_Transaction_commission->get($transaction_commission_id);

        $pr['reference'] = "PR-".uniqidReal(); // change to pr id helper
        $pr['payable_type_id'] = 1;
        $pr['origin_id'] = $oof['id'];
		$pr['gross_amount'] = $oof['commission_amount_rate'];
		$pr['wht_amount'] = $oof['wht_rate'];
		$pr['wht_percentage'] = $oof['wht_rate'];
		$pr['net_amount'] = get_net($oof['commission_amount_rate'],$oof['wht_rate']);
		$pr['payee_type'] = "sellers";
		$pr['payee_type_id'] = $oof['seller_id'];
		$pr['due_date'] = NOW;
		$pr['remarks'] = "Approved Commission from sales";


		$payment_request_id = $CI->M_payment_request->insert($pr + $this->additional);

		return true;
	} 


	
}

?>