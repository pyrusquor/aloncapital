<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adhoc_computation {
    public $terms;
    public $effectivity_date;
    public $amount;
    public $tcp;
    public $init_ma;

    public function reconstruct($fee = array(), $monthly_payment = 0) {
        $this->terms = $fee['terms'];
        $this->effectivity_date = $fee['effectivity_date'];
        $this->amount = $fee['amount'];
        $this->tcp = $fee['tcp'];
        $this->init_ma = $monthly_payment;
    }

    public function monthly_payment() {
        if ($this->init_ma) {
            return $this->init_ma;
        }

        if ($this->terms == 1) {
            return $this->amount;
        } else {
            return $this->amount / $this->terms;
        }
    }

    public function breakdown() {
        $i = 0;
        $x = 1;
        $total_amount = 0;
        $breakdown = array();
        $date = $this->effectivity_date;
        for($i = 1; $i <= ($this->terms); $i++) {

            /* MONTHLY BREAKDOWN START */
            $due_date = add_months_to_date($date, ($i-1));
            $next_payment_date = add_months_to_date($date, $i);

            $breakdown['monthly'][$i]['month'] = $i;

            if($i == 1) {
                $breakdown['monthly'][$i]['beginning_balance'] = $this->amount;
            } else {
                $breakdown['monthly'][$i]['beginning_balance'] = $breakdown['monthly'][($i - 1)]['ending_balance'];
            }

            $breakdown['monthly'][$i]['amount'] = $this->monthly_payment();
            $breakdown['monthly'][$i]['ending_balance'] = $breakdown['monthly'][$i]['beginning_balance'] - $breakdown['monthly'][$i]['amount'];
            $breakdown['monthly'][$i]['due_date'] = $due_date;
            $breakdown['monthly'][$i]['next_payment_date'] = $next_payment_date;
            /* MONTHLY BREAKDOWN END */

            /* YEARLY BREAKDOWN START */
			if( ( $i % 12 ) == 0 ) {
			
				$breakdown['yearly'][$x]['year'] = $i / 12;
					
				if( ( $i / 12 ) == 1 ) {
					$breakdown['yearly'][$x]['beginning_balance'] = $this->amount;
					
				} else {
					$breakdown['yearly'][$x]['beginning_balance'] = $breakdown['yearly'][$x - 1]['ending_balance'];
				}
						
				$breakdown['yearly'][$x]['amount'] = $this->monthly_payment() * 12;
				$breakdown['yearly'][$x]['ending_balance'] = $breakdown['yearly'][$x]['beginning_balance'];
					
				$x++;
				
			}
			/* YEARLY BREAKDOWN END */
			
			if ($breakdown['monthly'][$i]['ending_balance'] < 0) {
				break;
			}
        }

        return $breakdown;
    }
}

?>