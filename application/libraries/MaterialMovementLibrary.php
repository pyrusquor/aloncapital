<?php


    class MaterialMovementLibrary
    {
        public $movement = null;
        public $movement_type = null;
        public $meta = [];

        // receiving
        public $destination = null;
        public $destination_items = [];

        // releasing
        public $source = null;
        public $source_items = [];

        // logging
        public $log_type = null;

        private $CI = null;

        public function __construct()
        {
            $this->CI = $this->CI = &get_instance();
            $this->CI->load->helper('inventory_log');
        }

        /**
         * @param $movement_type : 'receive', 'release', 'direct_in', 'direct_out', 'warehouse_transfer'
         * @param $id : int
         * @param $meta : ['actor', 'time']
         */
        public function init($movement_type, $id, $meta)
        {
            $this->movement_type = $movement_type;
            $this->meta = $meta;

            switch ($movement_type) {
                case 'direct_in':
                case 'receive':
                    $this->CI->load->model('warehouse_inventory/warehouse_inventory_model', '_destination_item');
                    $this->CI->load->model('warehouse/warehouse_model', '_destination_warehouse');
                    $this->CI->load->model('material_receiving/material_receiving_model', '_receiving_model');
                    $this->CI->load->model('material_receiving_item/material_receiving_item_model', '_receiving_item');
                    $this->movement = $this->CI->_receiving_model->get($id);
                    $this->destination = $this->CI->_destination_warehouse->get($this->movement['warehouse_id']);
                    break;
                case 'direct_out':
                case 'release':
                    $this->CI->load->model('warehouse_inventory/warehouse_inventory_model', '_source_item');
                    $this->CI->load->model('warehouse/warehouse_model', '_source_warehouse');
                    $this->CI->load->model('material_receiving/material_issuance_model', '_releasing_model');
                    $this->CI->load->model('material_receiving_item/material_issuance_item_model', '_releasing_item');
                    $this->movement = $this->CI->_releasing_model->get($id);
                    $this->source = $this->CI->_source_warehouse->get($this->movement['warehouse_id']);
                    break;
            }

        }

        # region generic
        private function get_item($item_id, $unit_of_measurement_id)
        {
            if ($this->movement_type == 'receive' || $this->movement_type == 'direct_in') {
                return $this->CI->_destination_item
                    ->where('warehouse_id', $this->destination['id'])
                    ->where('unit_of_measurement_id', $unit_of_measurement_id)
                    ->where('item_id', $item_id)
                    ->get();
            } else {
                return $this->CI->_source_item
                    ->where('warehouse_id', $this->source['id'])
                    ->where('unit_of_measurement_id', $unit_of_measurement_id)
                    ->where('item_id', $item_id)
                    ->get();
            }
        }

        # endregion

        # region receiving
        public function move()
        {
            switch ($this->movement_type) {
                case 'direct_in':
                case 'receive':
                    $this->receive();
                    break;
                case 'direct_out':
                case 'release':
                    $this->release();
                    break;
            }
        }

        private function prep_destination_item($item)
        {
            $_destination_item = $this->get_item($item['item_id'], $item['unit_of_measurement_id']);
            if (!$_destination_item) {
                $destination_item = $this->generate_destination_item($item);
            } else {
                $destination_item = $_destination_item;
                $destination_item['updated_by'] = $this->meta['actor'];
                $destination_item['updated_at'] = $this->meta['time'];
                $destination_item['actual_quantity'] += $item['quantity'];
                $destination_item['available_quantity'] += $item['quantity'];
                $destination_item['unit_price'] = $item['unit_cost'];
            }

            return $destination_item;
        }

        private function prep_source_item($item)
        {
            $_receiving_item = $this->CI->db->where('id', $item['material_receiving_item_id'])->get('material_receiving_items')->row_array();
            $_source_item = $this->get_item($_receiving_item['item_id'], $_receiving_item['unit_of_measurement_id']);
            if (!$_source_item) {
                return false;
            }
            $source_item = $_source_item;
            $source_item['updated_by'] = $this->meta['actor'];
            $source_item['updated_at'] = $this->meta['time'];

            if(array_key_exists('virtual_destination', $this->meta)){
                if (!$this->meta['virtual_destination']) {
                    $source_item['actual_quantity'] -= $item['quantity'];
                }
            }else {
                $source_item['actual_quantity'] -= $item['quantity'];
            }
            $source_item['available_quantity'] -= $item['quantity'];

            return $source_item;
        }

        private function generate_destination_item($item)
        {
            $destination_item = array(
                "actual_quantity" => $item['quantity'],
                "available_quantity" => $item['quantity'],
                "created_at" => $this->meta['time'],
                "created_by" => $this->meta['actor'],
                "item_id" => $item['item_id'],
                "unit_price" => $item['unit_cost'],
                "unit_of_measurement_id" => $item['unit_of_measurement_id'],
                "warehouse_id" => $this->movement['warehouse_id'],
            );
            return $destination_item;
        }

        private function receive()
        {
            $items = $this->CI->_receiving_item->where('material_receiving_id', $this->movement['id'])->get_all();
            foreach ($items as $item) {
                $this->warehouse_receive($this->prep_destination_item($item), $item);
            }
        }

        private function release()
        {
            $items = $this->CI->_releasing_item->where('material_issuance_id', $this->movement['id'])->get_all();
            foreach ($items as $item) {
                $source_item = $this->prep_source_item($item);
                if ($source_item) {
                    $this->warehouse_release($source_item, $item);
                }
            }
        }

        private function warehouse_release($source_item, $movement_item)
        {
            $ref = $source_item['id'];

            $available = $movement_item['quantity'];
            if(array_key_exists('virtual_destination', $this->meta)){
                if ($this->meta['virtual_destination']) {
                    $actual = 0.0;
                }else{
                    $actual = $movement_item['quantity'];
                }
            }else {
                $actual = $movement_item['quantity'];
            }
            $current_available = $source_item['available_quantity'];
            $current_actual = $source_item['actual_quantity'];

            unset($source_item['id']);
            $this->CI->_source_item->update($source_item, $ref);


            if(array_key_exists('virtual_source', $this->meta) || array_key_exists('virtual_destination', $this->meta)){
                if ($this->meta['virtual_source'] || $this->meta['virtual_destination']) {
                    $log_type = inventory_log_transaction_keyval('release_virtual');
                }else{
                    if(array_key_exists('transaction', $this->meta)){
                        $log_type = inventory_log_transaction_keyval($this->meta['transaction']);
                    }else{
                        $log_type = inventory_log_transaction_keyval($this->movement_type);
                    }
                }
            }else {
                $log_type = inventory_log_transaction_keyval($this->movement_type);
            }

            $log_data = array(
                'created_at' => NOW,
                'created_by' => $this->movement['created_by'],
                'material_receiving_id' => null,
                'material_releasing_id' => $this->movement['id'],
                'actual_quantity_in' => 0.0,
                'actual_quantity_out' => $actual,
                'available_quantity_in' => 0.0,
                'available_quantity_out' => $available,
                'warehouse_id' => $source_item['warehouse_id'],
                'warehouse_inventory_id' => $ref,
                'sub_warehouse_id' => null,
                'sub_warehouse_inventory_id' => null,
                'virtual_warehouse_id' => null,
                'virtual_warehouse_inventory_id' => null,
                'item_id' => $source_item['item_id'],
                'unit_of_measurement_id' => $source_item['unit_of_measurement_id'],
                'unit_price' => $source_item['unit_price'],
                'remarks' => $log_type['label'],
                'transaction_type' => $log_type['key'],
                'current_actual_quantity' => $current_actual,
                'current_available_quantity' => $current_available,
            );
            $this->create_log($log_data);

        }

        private function warehouse_receive($destination_item, $movement_item)
        {
            if (key_exists('id', $destination_item)) {
                $ref = $destination_item['id'];
                unset($destination_item['id']);
                $this->CI->_destination_item->update($destination_item, $ref);
            } else {
                $this->CI->db->insert($this->CI->_destination_item->table, $destination_item);
                $ref = $this->CI->db->insert_id();
            }

            if (array_key_exists('virtual_source', $this->meta) || array_key_exists('virtual_destination', $this->meta)) {
                if ($this->meta['virtual_source'] || $this->meta['virtual_destination']) {
                    $log_type = inventory_log_transaction_keyval('receive_virtual');
                } else {
                    if(array_key_exists('transaction', $this->meta)){
                        $log_type = inventory_log_transaction_keyval($this->meta['transaction']);
                    }else{
                        $log_type = inventory_log_transaction_keyval($this->movement_type);
                    }
                }
            }else{
                $log_type = inventory_log_transaction_keyval($this->movement_type);
            }

            $log_data = array(
                'created_at' => NOW,
                'created_by' => $this->movement['created_by'],
                'material_receiving_id' => $this->movement['id'],
                'actual_quantity_in' => $movement_item['quantity'],
                'actual_quantity_out' => 0.0,
                'available_quantity_in' => $movement_item['quantity'],
                'available_quantity_out' => 0.0,
                'warehouse_id' => $destination_item['warehouse_id'],
                'warehouse_inventory_id' => $ref,
                'sub_warehouse_id' => null,
                'sub_warehouse_inventory_id' => null,
                'virtual_warehouse_id' => null,
                'virtual_warehouse_inventory_id' => null,
                'item_id' => $destination_item['item_id'],
                'unit_of_measurement_id' => $destination_item['unit_of_measurement_id'],
                'unit_price' => $destination_item['unit_price'],
                'remarks' => $log_type['label'],
                'transaction_type' => $log_type['key'],
                'current_actual_quantity' => $destination_item['actual_quantity'],
                'current_available_quantity' => $destination_item['available_quantity'],
            );
            $this->create_log($log_data);

        }

        # endregion

        # region logs
        public function create_log($data)
        {
            if(array_key_exists('source_id', $this->meta)){
                $data['source_warehouse_id'] = $this->meta['source_id'];
            }
            if(array_key_exists('destination_id', $this->meta)){
                $data['destination_warehouse_id'] = $this->meta['destination_id'];
            }

            $this->CI->db->insert('warehouse_inventory_logs', $data);
        }

        # endregion
    }