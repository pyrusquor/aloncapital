<?php


    class MaterialIssuanceItemLibrary
    {
        private $data = null;
        private $CI = null;

        public function __construct()
        {
            $this->CI = &get_instance();
            $this->CI->load->model('material_issuance_item/Material_issuance_item_model', 'M_Material_issuance_item');
        }

        public function get_obj($id, $with_relations = TRUE)
        {
            if ($with_relations) {
                $obj = $this->CI->M_Material_issuance_item
                    ->with_material_issuance()
                    ->with_material_receiving_item()
                    ->get($id);
            } else {
                $obj = $this->CI->M_Material_issuance_item->get($id);
            }

            return $obj;
        }

        public function get_objs_by_parent($parent_id, $with_relations = TRUE)
        {
            if ($with_relations) {
                $obj = $this->CI->M_Material_issuance_item
                    ->where('material_issuance_id', $parent_id)
                    ->with_material_issuance()
                    ->with_material_receiving_item()
                    ->get_all();
            } else {
                $obj = $this->CI->M_Material_issuance_item->where('material_issuance_id', $parent_id)->get_all();
            }

            return $obj;
        }

        public function search($params)
        {
            $id = isset($params['id']) ? $params['id'] : null;
            $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';
            $status_list = isset($params['status_list']) && !empty($params['status_list']) ? $params['status_list'] : null;

            if ($status_list) {
                $status_list_arr = explode(",", $status_list);
                $this->CI->M_Material_issuance_item->where('status', $status_list_arr);
            }

            $this->CI->M_Material_issuance_item->order_by('id', 'DESC');

            if (!$id) {

                $id = isset($params['id']) && !empty($params['id']) ? $params['id'] : null;
                if ($id) {
                    $this->CI->M_Material_issuance_item->where('id', $id);
                }

                $created_by = isset($params['created_by']) && !empty($params['created_by']) ? $params['created_by'] : null;
                if ($created_by) {
                    $this->CI->M_Material_issuance_item->where('created_by', $created_by);
                }

                $created_at = isset($params['created_at']) && !empty($params['created_at']) ? $params['created_at'] : null;
                if ($created_at) {
                    $this->CI->M_Material_issuance_item->where('created_at', $created_at);
                }

                $updated_by = isset($params['updated_by']) && !empty($params['updated_by']) ? $params['updated_by'] : null;
                if ($updated_by) {
                    $this->CI->M_Material_issuance_item->where('updated_by', $updated_by);
                }

                $updated_at = isset($params['updated_at']) && !empty($params['updated_at']) ? $params['updated_at'] : null;
                if ($updated_at) {
                    $this->CI->M_Material_issuance_item->where('updated_at', $updated_at);
                }

                $deleted_by = isset($params['deleted_by']) && !empty($params['deleted_by']) ? $params['deleted_by'] : null;
                if ($deleted_by) {
                    $this->CI->M_Material_issuance_item->where('deleted_by', $deleted_by);
                }

                $deleted_at = isset($params['deleted_at']) && !empty($params['deleted_at']) ? $params['deleted_at'] : null;
                if ($deleted_at) {
                    $this->CI->M_Material_issuance_item->where('deleted_at', $deleted_at);
                }

                $material_issuance_id = isset($params['material_issuance_id']) && !empty($params['material_issuance_id']) ? $params['material_issuance_id'] : null;
                if ($material_issuance_id) {
                    $this->CI->M_Material_issuance_item->where('material_issuance_id', $material_issuance_id);
                }

                $material_receiving_item_id = isset($params['material_receiving_item_id']) && !empty($params['material_receiving_item_id']) ? $params['material_receiving_item_id'] : null;
                if ($material_receiving_item_id) {
                    $this->CI->M_Material_issuance_item->where('material_receiving_item_id', $material_receiving_item_id);
                }

                $quantity = isset($params['quantity']) && !empty($params['quantity']) ? $params['quantity'] : null;
                if ($quantity) {
                    $this->CI->M_Material_issuance_item->where('quantity', $quantity);
                }


            } else {
                $this->CI->M_Material_issuance_item->where('id', $id);
            }

            $this->CI->M_Material_issuance_item->order_by('id', 'DESC');

            if ($with_relations === 'yes') {
                $result = $this->CI->M_Material_issuance_item
                    ->with_material_issuance()
                    ->with_material_receiving_item()
                    ->get_all();
            } else {
                $result = $this->CI->M_Material_issuance_item->get_all();
            }

            return $result;
        }

        public function create_master($data, $additional)
        {

            if (property_exists($this->CI->M_Material_issuance_item, 'form_fillables')) {
                $fillables = $this->CI->M_Material_issuance_item->form_fillables;
            } else {
                $fillables = $this->CI->M_Material_issuance_item->fillable;
            }

            $item = [];

            foreach ($fillables as $field) {
                if (key_exists($field, $data)) {
                    $item[$field] = $data[$field];
                }
            }

            $request = $this->CI->db->insert('material_issuance_items', $item + $additional);
            if ($request) {
                $ref = $this->CI->db->insert_id();
            } else {
                return false;
            }

            return $ref;
        }

        public function update_issuances($id)
        {
            $this->CI->load->library('MaterialReceivingIssuanceLibrary');
            $ri_lib = new MaterialReceivingIssuanceLibrary();

            $issuance_item = $this->get_obj($id);

            if(! $issuance_item) { return false; }

            $receiving_issuance = $ri_lib->get_by_material_receiving_item($issuance_item['material_receiving_item_id']);
            if($receiving_issuance){
                $data = array(
                    'quantity' => (float)$issuance_item['quantity'] + (float)$receiving_issuance['quantity']
                );
                $additional = array(
                    'updated_at' => NOW,
                    'updated_by' => $issuance_item['created_by']
                );
                $ri_lib->update_master($receiving_issuance['id'], $data, $additional);
            }else{
                $data = array(
                    'material_receiving_item_id' => $issuance_item['material_receiving_item_id'],
                    'quantity' => $issuance_item['quantity'],
                );
                $additional = array(
                    'created_at' => NOW,
                    'created_by' => $issuance_item['created_by']
                );
                $ri_lib->create_master($data, $additional);
            }
        }

        public function update_master($id, $data, $additional)
        {
            $old_object = $this->CI->M_Material_issuance_item->get($id);
            return $this->CI->M_Material_issuance_item->update($data + $additional, $id);
        }


    }