<?php


    class ItemTransferLibrary
    {
        public $item = null;
        public $source = null;
        public $source_type = '';
        public $destination = null;
        public $destination_type = '';
        public $quantity = 0.0;
        public $meta = [];

        public $source_inventory_item = [];
        public $destination_inventory_item = [];
        public $source_key = '';
        public $destination_key = '';
        public $source_data = [];
        public $destination_data = [];

        private $CI = null;

        public function __construct()
        {
            $this->CI = &get_instance();
        }

        public function init($source_inventory_item_id, $source_type, $source_id, $destination_type, $destination_id, $quantity, $meta){

            switch ($source_type) {
                case 'sub_warehouse':
                    $this->CI->load->model('sub_warehouse_inventory/sub_warehouse_inventory_model', '_source');
                    $this->CI->load->model('sub_warehouse/sub_warehouse_model', '_source_warehouse');
                    break;
                case 'virtual_warehouse':
                    $this->CI->load->model('virtual_warehouse_inventory/virtual_warehouse_inventory_model', '_source');
                    $this->CI->load->model('virtual_warehouse/virtual_warehouse_model', '_source_warehouse');
                    break;
                case 'warehouse': // warehouse
                    $this->CI->load->model('warehouse_inventory/warehouse_inventory_model', '_source');
                    $this->CI->load->model('warehouse/warehouse_model', '_source_warehouse');
                    break;
            }

            switch ($destination_type) {
                case 'sub_warehouse':
                    $this->CI->load->model('sub_warehouse_inventory/sub_warehouse_inventory_model', '_destination');
                    $this->CI->load->model('sub_warehouse/sub_warehouse_model', '_destination_warehouse');
                    break;
                case 'virtual_warehouse':
                    $this->CI->load->model('virtual_warehouse_inventory/virtual_warehouse_inventory_model', '_destination');
                    $this->CI->load->model('virtual_warehouse/virtual_warehouse_model', '_destination_warehouse');
                    break;
                case 'warehouse': // warehouse
                    $this->CI->load->model('warehouse_inventory/warehouse_inventory_model', '_destination');
                    $this->CI->load->model('warehouse/warehouse_model', '_destination_warehouse');
                    break;
            }

            $this->source_inventory_item = $this->CI->_source->get($source_inventory_item_id);

            $this->item = get_object_from_table($this->source_inventory_item['item_id'], 'item', false);

            $this->source_type = $source_type;
            $this->destination_type = $destination_type;

            $this->source_key = $source_type . "_id";
            $this->destination_key = $destination_type . "_id";

            $this->quantity = $quantity;
            
            $this->meta = $meta;

            $this->source = $this->CI->_source_warehouse->get($source_id);
            $this->destination = $this->CI->_destination_warehouse->get($destination_id);
            
            $this->get_destination_item();
        }
        
        # region logs

        public function get_last_log_for_item_in_warehouse($warehouse_key, $warehouse_id)
        {
            $this->CI->db->order_by('created_at', 'DESC');
            $log = $this->CI->db->get_where('warehouse_inventory_logs', array(
                $warehouse_key => $warehouse_id,
                'item_id' => $this->item['id']
            ))->row();

            return $log;
        }

        public function update_log_type($log_id, $type)
        {
            $this->CI->db->set('transaction_type', $type);
            $this->CI->db->where('id', $log_id);
            $this->CI->db->update('warehouse_inventory_logs');
        }
        
        # endregion

        private function get_destination_item()
        {
            $this->destination_inventory_item = $this->CI->_destination->where($this->destination_key, $this->destination['id'])->where('item_id', $this->item['id'])->get();
        }

        private function generate_destination_item()
        {
            $fields = array(
                "actual_quantity" => $this->destination_data['actual_quantity'],
                "available_quantity" => $this->destination_data['available_quantity'],
                "created_at" => $this->meta['time'],
                "created_by" => $this->meta['actor'],
                "item_id" => $this->item['id'],
                "unit_price" => $this->source_inventory_item['unit_price'],
                "unit_of_measurement_id" => $this->source_inventory_item['unit_of_measurement_id'],
                $this->destination_key => $this->destination['id'],
            );

            if($this->destination_type == "sub_warehouse"){
                $fields['warehouse_id'] = $this->destination['warehouse_id'];
            }
            $this->destination_inventory_item = $fields;
        }

        private function set_source_data()
        {
            $this->source_data['available_quantity'] = $this->source_inventory_item['available_quantity'] - $this->quantity;
            $this->source_data['actual_quantity'] = $this->source_inventory_item['actual_quantity'] - $this->quantity;
        }
        
        private function set_destination_data()
        {
            if( $this->destination_inventory_item ){
                $this->destination_data['available_quantity'] = $this->destination_inventory_item['available_quantity'] + $this->quantity;
                $this->destination_data['actual_quantity'] = $this->destination_inventory_item['actual_quantity'] + $this->quantity;
            }else{
                $this->destination_data['available_quantity'] = $this->quantity;
                $this->destination_data['actual_quantity'] = $this->quantity;
            }
        }

        private function update_source()
        {
            $additional = [
                'updated_by' => $this->meta['actor'],
                'updated_at' => $this->meta['time']
            ];
            return $this->CI->_source->update($this->source_data + $additional, $this->source_inventory_item['id']);
        }
        
        private function update_destination()
        {
            $additional = [
                'updated_by' => $this->meta['actor'],
                'updated_at' => $this->meta['time']
            ];
            return $this->CI->_destination->update($this->destination_data + $additional, $this->destination_inventory_item['id']);
        }

        public function transfer()
        {
            $this->set_source_data();
            $this->update_source();
            $source_log = $this->get_last_log_for_item_in_warehouse($this->source_key, $this->source['id']);
            $this->update_log_type($source_log->id, 3);

            $this->set_destination_data();
            if( $this->destination_inventory_item ){
                $this->update_destination();
                $target_log = $this->get_last_log_for_item_in_warehouse($this->destination_key, $this->destination['id']);
                $this->update_log_type($target_log->id, 2);
            }else{
                $this->generate_destination_item();
                $ref = $this->CI->db->insert($this->CI->_destination->table, $this->destination_inventory_item);
                if($ref){
                    $target_log = $this->get_last_log_for_item_in_warehouse($this->item['id'], $this->destination['id']);
                    $this->update_log_type($target_log->id, 2);
                }
            }

        }
    }