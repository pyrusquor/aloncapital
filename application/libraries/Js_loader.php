<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Js_loader extends MX_Controller
{
    public $scripts = array();
    public $scripts_code;
    public function __construct()
    {
        parent::__construct();
    }

    public function queue($source)
    {
        if(gettype($source)=== 'string')
        {
            array_push($this->scripts, $source);
        }
        elseif (gettype($source)=== 'array')
        {
           $this->scripts = array_merge($this->scripts, $source);
        }
        else
        {
            return;
        }
    }

    public function queue_code($tring)
    {
        $this->scripts_code = $tring;
    }

    public function load()
    {
        $path = FCPATH .'assets/modules/'.$this->router->fetch_class();
        $controller = $this->router->fetch_class();
        $method = $this->router->fetch_method();

        if(!empty($this->scripts))
        {
            foreach($this->scripts as $js)
            {
                $fileloc = FCPATH .'assets/'.$js;
                if(file_exists($fileloc))
                {
                   // echo "<script language='javascript' type='text/javascript' src='".site_url("application/assets/$js")."'></script>\n";
                    echo "<script language='javascript' type='text/javascript' src='".site_url("assets/$js")."'></script>\n";
                }else{
                    echo "<script language='javascript' type='text/javascript' src='".$js."'></script>\n";
                }
            }
        }

        if($path !== false AND is_dir($path))
        {
            if(file_exists($path.'/'.$method.'.js'))
            {
                echo "<script language='javascript' type='text/javascript' src='".site_url("assets/modules/$controller/$method").".js'></script>\n";
            }
        }

        if(!empty($this->scripts_code))
        {
            echo "<script language='javascript' type='text/javascript'>".$this->scripts_code."</script>";
        }
    }
}
