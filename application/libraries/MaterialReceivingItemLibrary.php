<?php


    class MaterialReceivingItemLibrary
    {
        public $data = null;
        private $CI = null;

        public function __construct()
        {
            $this->CI = &get_instance();
            $this->CI->load->model('material_receiving_item/material_receiving_item_model', 'M_Material_receiving_item');
            $this->CI->load->model('material_receiving/material_receiving_issuance_model', 'M_Material_receiving_issuance');
            $this->CI->load->model('material_issuance_item/material_issuance_item_model', 'M_Material_issuance_item');
        }

        public function get_by_mr($mr)
        {
            $__items = $this->CI->db->select('id')->where(array(
                'material_receiving_id' => $mr,
                'deleted_at' => null
            ))->get('material_receiving_items')->result_array();
            $items = [];

            if($__items){
                foreach($__items as $__item){
                    $item = $this->get($__item['id']);
                    if($item){
                        array_push($items, $item);
                    }
                }
            }
            return $items;
        }

        public function get_by_item($item_id, $warehouse_id = null, $no_empty = false)
        {
            $rows = $this->CI->db->select('id')->where(
                array(
                    'item_id' => $item_id,
                    'deleted_at' => null
                )
            )->get('material_receiving_items')->result_array();

            $items = [];

            foreach($rows as $row){
                $item = $this->get($row['id'], $warehouse_id, $no_empty);
                if($item){
                    array_push($items, $item);
                }
            }
            return $items;
        }

        public function get($id, $warehouse_id = null, $no_empty = false)
        {
            $item = $this->CI->M_Material_receiving_item->where('id', $id)->
            with_item_group()->
            with_item_type()->
            with_item_brand()->
            with_item_class()->
            with_item()->
            with_unit_of_measurement()->
            with_supplier()->
            with_material_request()->
            with_material_receiving()->
            with_purchase_order()->
            with_purchase_order_request_item()->
            with_purchase_order_request()->
            with_warehouse()->
            get();

            if ($item) {
                $issuance = $this->CI->M_Material_receiving_issuance->where('material_receiving_item_id', $id)->get();
                if ($issuance) {
                    $item['issuance'] = $issuance;
                    $item['issuances'] = $this->CI->M_Material_issuance_item->where('material_receiving_item_id', $id)->with_material_issuance()->get_all();
                } else {
                    $item['issuance'] = array(
                        'material_receiving_id' => $item['material_receiving_id'],
                        'material_receiving_issuance_id' => null,
                        'material_receiving_item_id' => $item['id'],
                        'quantity' => 0
                    );
                    $item['issuances'] = [];
                }
                $item['stock_data'] = array(
                    'remaining' => $item['quantity'] - $item['issuance']['quantity']
                );
            }
            if ($no_empty) {
                if ($item['stock_data']['remaining'] <= 0) {
                    return false;
                }
            }
            return $item;
        }

        public function update_master($id, $data, $additional)
        {
            $request = [];
            if (property_exists($this->CI->M_Material_receiving_item, 'form_fillables')) {
                $fillables = $this->CI->M_Material_receiving_item->form_fillables;
            } else {
                $fillables = $this->CI->M_Material_receiving_item->fillable;
            }

            foreach ($fillables as $field) {
                if (array_key_exists($field, $data)) {
                    $request[$field] = $data[$field];
                }
            }
            return $this->CI->M_Material_receiving_item->update($request + $additional, $id);
        }

    }