<?php


    class NoteLibrary
    {
        private $data = null;
        private $_model = null;
        private $CI = null;

        public function __construct()
        {
            $this->CI = &get_instance();
            $this->CI->load->model('note/Note_model', 'M_Note');
        }

        public function get_obj($id, $with_relations = TRUE)
        {
            if ($with_relations) {
                $obj = $this->CI->M_Note
                    ->with_creator()
                    ->with_updator()
                    ->get($id);
            } else {
                $obj = $this->CI->M_Note->get($id);
            }

            return $obj;
        }

        public function get_objs_by_parent($object_type, $object_type_id, $with_relations = TRUE)
        {
            $this->CI->M_Note->where('object_type', $object_type);
            $this->CI->M_Note->where('object_type_id', $object_type_id);
            if ($with_relations) {
                $obj = $this->CI->M_Note
                    ->with_creator()
                    ->with_updator()
                    ->get_all();
            } else {
                $obj = $this->CI->M_Note->get_all();
            }

            return $obj;
        }

        public function search($params)
        {
            $id = isset($params['id']) ? $params['id'] : null;
            $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';
            $status_list = isset($params['status_list']) && !empty($params['status_list']) ? $params['status_list'] : null;

            if ($status_list) {
                $status_list_arr = explode(",", $status_list);
                $this->CI->M_Note->where('status', $status_list_arr);
            }

            $this->CI->M_Note->order_by('id', 'DESC');

            if (!$id) {

                $id = isset($params['id']) && !empty($params['id']) ? $params['id'] : null;
                if ($id) {
                    $this->CI->M_Note->where('id', $id);
                }

                $created_by = isset($params['created_by']) && !empty($params['created_by']) ? $params['created_by'] : null;
                if ($created_by) {
                    $this->CI->M_Note->where('created_by', $created_by);
                }

                $created_at = isset($params['created_at']) && !empty($params['created_at']) ? $params['created_at'] : null;
                if ($created_at) {
                    $this->CI->M_Note->where('created_at', $created_at);
                }

                $updated_by = isset($params['updated_by']) && !empty($params['updated_by']) ? $params['updated_by'] : null;
                if ($updated_by) {
                    $this->CI->M_Note->where('updated_by', $updated_by);
                }

                $updated_at = isset($params['updated_at']) && !empty($params['updated_at']) ? $params['updated_at'] : null;
                if ($updated_at) {
                    $this->CI->M_Note->where('updated_at', $updated_at);
                }

                $deleted_by = isset($params['deleted_by']) && !empty($params['deleted_by']) ? $params['deleted_by'] : null;
                if ($deleted_by) {
                    $this->CI->M_Note->where('deleted_by', $deleted_by);
                }

                $deleted_at = isset($params['deleted_at']) && !empty($params['deleted_at']) ? $params['deleted_at'] : null;
                if ($deleted_at) {
                    $this->CI->M_Note->where('deleted_at', $deleted_at);
                }

                $content = isset($params['content']) && !empty($params['content']) ? $params['content'] : null;
                if ($content) {
                    $this->CI->M_Note->where('content', $content);
                }

                $object_type = isset($params['object_type']) && !empty($params['object_type']) ? $params['object_type'] : null;
                if ($object_type) {
                    $this->CI->M_Note->where('object_type', $object_type);
                }

                $object_type_id = isset($params['object_type_id']) && !empty($params['object_type_id']) ? $params['object_type_id'] : null;
                if ($object_type_id) {
                    $this->CI->M_Note->where('object_type_id', $object_type_id);
                }


            } else {
                $this->CI->M_Note->where('id', $id);
            }

            $this->CI->M_Note->order_by('id', 'ASC');

            if ($with_relations === 'yes') {
                $result = [];
                $ids = $this->CI->M_Note->fields('id')->get_all();
                if($ids){
                    foreach($ids as $__id){
                        $row = $this->get_obj($__id);
                        array_push($result, $row);
                    }
                }
            } else {
                $result = $this->CI->M_Material_issuance->get_all();
            }

            return $result;
        }

        public function create_master($data, $additional)
        {
            $request = $this->CI->db->insert('notes', $data + $additional);
            if ($request) {
                return $this->CI->db->insert_id();
            } else {
                return false;
            }
        }

        public function update_master($id, $data, $additional)
        {
            $old_object = $this->CI->M_Note->get($id);
            return $this->CI->M_Note->update($data + $additional, $id);
        }


    }