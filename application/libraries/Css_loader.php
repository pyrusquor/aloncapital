<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Css_loader extends MX_Controller
{
    public $styles = array();
    public $styles_code;
    public function __construct()
    {
        parent::__construct();
    }

    public function queue($source)
    {
        if(gettype($source)=== 'string')
        {
            array_push($this->styles, $source);
        }
        elseif (gettype($source)=== 'array')
        {
           $this->styles = array_merge($this->styles, $source);
        }
        else
        {
            return;
        }
    }

    public function queue_code($tring)
    {
        $this->styles_code = $tring;
    }
    
    public function load()
    {
        $path = FCPATH .'assets/modules/'.$this->router->fetch_class();
        $controller = $this->router->fetch_class();
        $method = $this->router->fetch_method();
        // $controller = $this->router->fetch_class();
        if(!empty($this->styles))
        {
            foreach($this->styles as $css)
            {
                $fileloc = FCPATH.'assets/'.$css;
                if(file_exists($fileloc))
                {
                   echo "<link rel='stylesheet' type='text/css' href='".site_url("assets/$css")."' />\n";
                }else{
                    echo "<link rel='stylesheet' type='text/css' href='".$css."' />\n";
                }
            }
        }

        if ( ($path !== FALSE) && is_dir($path) ) {

            if ( file_exists($path.'/'.$method.'.css') ) {

                echo "<link rel='stylesheet' type='text/css' href='".site_url("assets/modules/$controller/$method").".css' />\n";
            }
        }

        if(!empty($this->styles_code))
        {
            echo "<style rel='stylesheet' type='text/css'>".$this->styles_code."</style>";
        }
    }
}
