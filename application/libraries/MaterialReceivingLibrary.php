<?php


class MaterialReceivingLibrary
{
    private $data = null;
    private $M_Material_receiving = null;
    private $CI = null;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('material_receiving/material_receiving_model', 'M_Material_receiving');
        $this->CI->load->model('material_receiving_item/material_receiving_item_model', 'M_Material_receiving_item');
    }

    public function get_obj($id, $with_relations = TRUE)
    {
        $mrr = $this->CI->M_Material_receiving->with_company()->with_accounting_ledger()->with_purchase_order()->with_supplier()->with_approving_staff()->with_requesting_staff()->get($id);

        return $mrr;
    }

    public function get_items($id, $warehouse_id = null)
    {
        $this->CI->load->library('MaterialReceivingItemLibrary');
        $item_lib = new MaterialReceivingItemLibrary();
        $this->CI->M_Material_receiving_item->where('item_id', $item_id);
        if ($warehouse_id) {
            $this->CI->M_Material_receiving_item->where('warehouse_id', $warehouse_id);
        }

        $__items = $this->CI->M_Material_receiving_item->get_all();
        $items = [];

        if ($__items) {
            foreach ($__items as $item) {
                $material_receiving_item = $item_lib->get($item['id']);
                if ($material_receiving_item) {
                    array_push($items, $material_receiving_item);
                }
            }
        }

        return $items;
    }

    public function get_items_by_item_id($item_id, $warehouse_id = null, $empty = true, $limit = 5)
    {
        $this->CI->load->helper('material_receiving');
        $this->CI->load->library('MaterialReceivingItemLibrary');
        $item_lib = new MaterialReceivingItemLibrary();

        $this->CI->db->select('id, item_id');
        $this->CI->db->where('item_id', $item_id);
        if ($warehouse_id) {
            $this->CI->db->where('warehouse_id', $warehouse_id);
        } else {
            $warehouse_id = null;
        }
        $__items = $this->CI->db->get('material_receiving_items')->result_array();

        //            $this->CI->M_Material_receiving_item->fields(array('id', 'item_id'));
        //            $this->CI->M_Material_receiving_item->where('item_id', $item_id);
        //            if ($warehouse_id) {
        //                $this->CI->M_Material_receiving_item->where('warehouse_id', $warehouse_id);
        //            }
        //
        //            $__items = $this->CI->M_Material_receiving_item->get();
        $items = [];

        if ($__items) {
            foreach ($__items as $item) {
                $material_receiving_item = $item_lib->get($item['id'], $warehouse_id, $empty);
                if ($material_receiving_item) {
                    $material_receiving_item['receiving_type_label'] = mrr_type_lookup($material_receiving_item['material_receiving']['receiving_type']);
                    array_push($items, $material_receiving_item);
                }
            }
        }

        return $items;
    }

    public function get_items_by_material_request($id, $warehouse_id = null)
    {
        $items = [];

        $this->CI->load->library('MaterialReceivingItemLibrary');
        $item_lib = new MaterialReceivingItemLibrary();

        $material_request_items = $this->CI->db->where('material_request_id', $id)->order_by('created_at', 'ASC')->get('material_request_items')->result();

        if ($material_request_items) {

            $item_ids = [];
            foreach ($material_request_items as $mri) {
                array_push($item_ids, $mri->item_id);
            }

            $this->CI->M_Material_receiving_item->where('item_id', $item_ids);
            if ($warehouse_id) {
                $this->CI->M_Material_receiving_item->where('warehouse_id', $warehouse_id);
            }

            $__items = $this->CI->M_Material_receiving_item->get_all();

            if ($__items) {
                foreach ($__items as $item) {
                    $material_receiving_item = $item_lib->get($item['id']);
                    if ($material_receiving_item) {
                        array_push($items, $material_receiving_item);
                    }
                }
            }
        }

        return $items;
    }

    public function search($params)
    {
        $id = isset($params['id']) ? $params['id'] : null;
        $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';
        $status_list = isset($params['status_list']) && !empty($params['status_list']) ? $params['status_list'] : null;

        if ($status_list) {
            $status_list_arr = explode(",", $status_list);
            $this->CI->M_Material_receiving->where('status', $status_list_arr);
        }

        if (!$id) {

            $company_id = isset($params['company_id']) && !empty($params['company_id']) ? $params['company_id'] : null;
            $accounting_ledger_id = isset($params['accounting_ledger_id']) && !empty($params['accounting_ledger_id']) ? $params['accounting_ledger_id'] : null;
            $warehouse_id = isset($params['warehouse_id']) && !empty($params['warehouse_id']) ? $params['warehouse_id'] : null;
            $purchase_order_id = isset($params['purchase_order_id']) && !empty($params['purchase_order_id']) ? $params['purchase_order_id'] : null;
            $supplier_id = isset($params['supplier_id']) && !empty($params['supplier_id']) ? $params['supplier_id'] : null;
            $status = isset($params['status']) && !empty($params['status']) ? $params['status'] : null;
            $reference = isset($params['reference']) && !empty($params['reference']) ? $params['reference'] : null;
            $receiving_type = isset($params['receiving_type']) && !empty($params['receiving_type']) ? $params['receiving_type'] : null;

            if ($company_id) {
                $this->CI->M_Material_receiving->where('company_id', $company_id);
            }
            if ($accounting_ledger_id) {
                $this->CI->M_Material_receiving->where('accounting_ledger_id', $accounting_ledger_id);
            }
            if ($warehouse_id) {
                $this->CI->M_Material_receiving->where('warehouse_id', $warehouse_id);
            }
            if ($purchase_order_id) {
                $this->CI->M_Material_receiving->where('purchase_order_id', $purchase_order_id);
            }
            if ($supplier_id) {
                $this->CI->M_Material_receiving->where('supplier_id', $supplier_id);
            }

            if ($status) {
                $this->CI->M_Material_receiving->where('status', $status);
            }
            if ($reference) {
                $this->CI->M_Material_receiving->like('reference', $reference, 'both');
            }
            if ($receiving_type) {
                $this->CI->M_Material_receiving->where('receiving_type', $receiving_type);
            }
        } else {
            $this->CI->M_Material_receiving->where('id', $id);
        }

        $this->CI->M_Material_receiving->order_by('id', 'DESC');

        if ($with_relations === 'yes') {
            $result = $this->CI->M_Material_receiving->with_company()->with_accounting_ledger()->with_purchase_order()->with_supplier()->with_warehouse()->with_approving_staff()->with_requesting_staff()->get_all();
        } else {
            $result = $this->CI->M_Material_receiving->get_all();
        }

        return $result;
    }

    public function create_master($data, $additional)
    {
        $data['reference'] = uniqidMRR();
        // $request = $this->M_Material_receiving_request->insert($data);
        $request = $this->CI->db->insert('material_receivings', $data + $additional);
        if ($request) {
            return $this->CI->db->insert_id();
        } else {
            return false;
        }
    }

    public function create_slave($data, $references, $additional)
    {
        $this->CI->load->model('material_receiving_item/Material_receiving_item_model', '_item');

        $item = array(
            'material_receiving_id' => $references['material_receiving_id'],
            'supplier_id' => $data['supplier_id'],
            'material_request_id' => $data['material_request_id'],
            'purchase_order_id' => $data['purchase_order_id'],
            'purchase_order_item_id' => $data['purchase_order_item_id'],
            'purchase_order_request_item_id' => $data['purchase_order_request_item_id'],
            'purchase_order_request_id' => $data['purchase_order_request_id'],
            'warehouse_id' => $data['warehouse_id'],
            'item_group_id' => $data['item_group_id'],
            'item_type_id' => $data['item_type_id'],
            'item_brand_id' => $data['item_brand_id'],
            'item_class_id' => $data['item_class_id'],
            'item_id' => $data['item_id'],
            'unit_of_measurement_id' => $data['unit_of_measurement_id'],
            'item_brand_name' => $data['item_brand_name'],
            'item_class_name' => $data['item_class_name'],
            'item_name' => $data['item_name'],
            'unit_of_measurement_name' => $data['unit_of_measurement_name'],
            'item_group_name' => $data['item_group_name'],
            'item_type_name' => $data['item_type_name'],
            'unit_cost' => $data['unit_cost'],
            'total_cost' => $data['total_cost'],
            'freight_cost' => $data['freight_cost'],
            'quantity' => $data['quantity'],
            'status' => $data['status'],
            'expiration_date' => $data['expiration_date'],
        );
        $mr_item_status = $item['status'] * 1;
        switch ($mr_item_status) {
            case 1:
                $receiving_status = 2;
                break;
            case 2:
                $receiving_status = 4;
                break;
            case 3:
                $receiving_status = 3;
                break;
            default:
                $receiving_status = 1;
                break;
        }
        $log_ref = null;
        if ($item['purchase_order_id']) {
            set_field('purchase_order_items', 'receiving_status', $receiving_status, $data['purchase_order_item_id'], $additional['created_by']);
            $this->CI->load->model('purchase_order/purchase_order_model', 'M_Purchase_order');

            $po = $this->CI->M_Purchase_order->get($item['purchase_order_id']);

            $this->CI->load->helper('purchase_order_helper');

            $log = $po;
            unset($log['id']);
            $log['purchase_order_id'] = $po['id'];
            $log['log_time'] = NOW;
            $log['description'] = 'Update from Material Receiving ID: ' . $item['material_receiving_id'];
            $this->CI->db->insert('purchase_order_logs', $log);
            $log_ref = $this->CI->db->insert_id();
        }
        $ref = $this->CI->M_Material_receiving_item->insert($item + $additional);
        if ($item['purchase_order_item_id'] && $log_ref) {
            $this->CI->load->model('purchase_order_item/purchase_order_item_model', 'M_Purchase_order_item');
            $item_log = $this->CI->M_Purchase_order_item->get($item['purchase_order_item_id']);
            unset($item_log['id']);
            $item_log['purchase_order_log_id'] = $log_ref;
            $item_log['purchase_order_item_id'] = $item['purchase_order_item_id'];
            $item_log['description'] = 'Update from Material Receiving Item ID: ' . $ref;
            $item_log['material_receiving_id'] = $item['material_receiving_id'];
            $item_log['material_receiving_item_id'] = $ref;
            $this->CI->db->insert('purchase_order_item_logs', $item_log);
        }
        return $ref;
    }

    public function update_master($id, $data, $additional)
    {

        $old_object = $this->CI->M_Material_receiving->get($id);
        if ($old_object['status'] == 2) {
            $data['status'] = 5;
        }
        return $this->CI->M_Material_receiving->update($data + $additional, $id);
    }
}
