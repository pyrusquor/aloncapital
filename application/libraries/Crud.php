<?php

class Crud extends MX_Controller
{
    public $table_name;
    public $data; // data array
    public $id; //primary key 
    public $select = '*'; //selected columns
    public $primary_col = 'sys_id'; //primary key or the unique key of table

    //to use where ===> array(array('columnName','value'));
    public $where = NULL;

    public $order_by = NULL; //column name for sorting
    public $order_type = 'ASC'; //sorting type ASC : DESC
    public $limit = 500; // result limit
    public $last_inserted_id;


    public function create()
    {
        $query = $this->db->insert($this->table_name,$this->data);
        if($query)
        {
            $this->last_inserted_id = $this->db->insert_id();
            return TRUE;
            
        }
    }

    public function batch_create()
    {
        $query = $this->db->insert_batch($this->table_name,$this->data);
        if($query)
        {
            return TRUE;
        }
    }

    public function update()
    {
        $this->db->where($this->primary_col,$this->id);
        $query = $this->db->update($this->table_name,$this->data);
        if($query)
        {
            return TRUE;
        }
    }

    public function batch_update()
    {
        $query = $this->db->update_batch($this->table_name,$thid->data,$this->primary_col);
        if($query)
        {
            return TRUE;
        }
    }

    public function temp_delete()
    {
        $this->db->where($this->primary_col,$this->id);
        $query = $this->db->update($this->table_name,$this->data);
        if($query)
        {
            return TRUE;
        }
    }

    public function perma_delete()
    {
        $this->db->where($this->primary_col,$this->id);
        $query = $this->db->delete($this->table_name);
        if($query)
        {
            return TRUE;
        }
    }

    private function conditions()
    {
        if($this->where != NULL)
        {
            $where_array = $this->where;
            for($i = 0; $i < COUNT($where_array); $i++)
            {
                $this->db->where($where_array[$i][0],$where_array[$i][1]);
            }
        }

        if($this->order_by != NULL)
        {
            $this->db->order_by($this->order_by,$this->order_type);
        }

        if($this->limit > 0)
        {
            $this->db->limit($this->limit);
        }
    }

    public function fetch_all()
    {
        $this->conditions();

        $query = $this->db->select($this->select)
        ->from($this->table_name)
        ->get();
        if($query)
        {
            return $query->result();
        }
    }

    public function fetch_by_id()
    {
        $query = $this->db->select($this->select)
        ->from($this->table_name)
        ->where($this->primary_col,$this->id)
        ->get();
        if($query)
        {
            if($query->num_rows() > 0)
            {
                return $query->row();
            }
        }
    }

    public function countRow()
    {
        $this->conditions();
        
        $this->db->select($this->select);

        $query = $this->db->from($this->table_name)
        ->get();
        if($query)
        {
            return $query->num_rows();
        }
    }

    
}